#include "ClientMessage.h"

namespace Client
{
    const std::vector<std::function<std::shared_ptr<ClientMessage>(const Shared::JSon::JSon&)>> ClientMessage::enumToMessageMap
    {
        ClientMoveMessage::ParseJSon,
        UpdatePlayerStateMessage::ParseJSon
    };
}
