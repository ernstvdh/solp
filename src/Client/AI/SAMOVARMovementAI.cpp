#include "../Client.h"
#include "../ClientAI.h"

#include "Shared/Util.h"
#include "Shared/Config.h"

#include "glm/geometric.hpp"
#include "glm/gtx/rotate_vector.hpp"

#define COM ,

namespace Client
{
    CLIENT_AI(SAMOVARMovementAI,
    {
    public:
        struct ClientState
        {
            uint64_t tick = 0;
            double x = 0.0;
            double y = 0.0;
            float speed = 0.0;
        };

        int client;
        glm::vec2 goalPos;
        int remainingCasts;
        int activeActionTicks;
        uint64_t step;
        uint64_t tick;
        std::vector<ClientState> path;

        SAMOVARMovementAI(int clientId) : client(clientId)
        {
            // we start at step 1 because 0 is initialization
            step = 1;
            tick = 0;
            activeActionTicks = 0;
            remainingCasts = 0;
            client = clientId;

            std::ifstream ifs;
            ifs.open(std::to_string(client) + ".path.samovar.txt", std::ifstream::in);

            if (!ifs.is_open())
            {
                SOLP_ERROR("Could not open previous samovar path that should have filename (%s)\nTerminating....",
                    std::string(std::to_string(client) + ".path.samovar.txt").c_str());
                std::terminate();
            }

            while (!ifs.eof())
            {
                ClientState cs;

                ifs >> cs.tick;
                ifs >> cs.x;
                ifs >> cs.y;
                ifs >> cs.speed;

                // we increase the tick as to compensate for the higher update rate in our simulator
                // SAMOVAR works with 1 sec ticks, and we can have more than 1
                cs.tick *= Shared::Config::clientAIUpdatesPerSec;

                if (!ifs.good())
                    return;

                if (!path.empty() && path.back().tick >= cs.tick)
                {
                    SOLP_ERROR("Provided SAMOVAR path for client %d is either incorrectly ordered or has double commands on 1 tick (tick: %d)... terminating...", client, cs.tick);
                    std::terminate();
                }

                path.push_back(std::move(cs));
            }
        }

        std::pair<bool COM glm::vec2> GetStartPosition()
        {
            if (path.size() <= 0)
            {
                SOLP_ERROR("Loaded path has length 0, and hence does not contain a starting position... terminating....");
                std::terminate();
            }

            return std::make_pair(true, glm::vec2(path[0].x, path[0].y));
        }

        void StopMoving()
        {
            // we are finished with our current goal
            // we send a not moving packet and will
            // start moving again in the next round
            owner->info.moving = false;

            owner->SendStateUpdate();

            // if not end of path we need to calculate how many actions we can have before the next movement
            // otherwise we just set the amount of actions to the target amount
            if (step < path.size())
            {
                // decrease by an additional 2 because we do not start the action this tick
                int availableTicks = path[step].tick - tick - 2;
                // * 2 because an action takes 2 seconds, + 1 because we have a 1 tick pause after each action
                remainingCasts = std::min((uint64_t)(availableTicks / ((Shared::Config::clientAIUpdatesPerSec * 2) + 1)), Shared::Config::targetInteractionsPerPause);
            }
            else
                remainingCasts = Shared::Config::targetInteractionsPerPause;
        }

        void UpdateAI(std::chrono::milliseconds& diff)
        {
            tick++;

            if (step >= path.size())
                return;

            float secDiff = diff.count() / 1000.0f;
            if (owner->info.moving)
            {
                // we have moved with the configured movement speed with time diff
                // in the direction we are facing
                owner->info.pos = owner->info.pos + owner->info.ori * secDiff * path[step - 1].speed;
                if (owner->info.pos.x >= Shared::Config::mapX)
                    owner->info.pos.x = (float)Shared::Config::mapX - Shared::Util::MapCoordEpsilon();
                if (owner->info.pos.x < 0.0f)
                    owner->info.pos.x = 0.0f;
                if (owner->info.pos.y >= Shared::Config::mapY)
                    owner->info.pos.y = (float)Shared::Config::mapY - Shared::Util::MapCoordEpsilon();
                if (owner->info.pos.y < 0.0f)
                    owner->info.pos.y = 0.0f;
            }

            if (path[step].tick == tick)
            {
                step++;
                goalPos = glm::vec2(path[step - 1].x, path[step - 1].y);

                owner->info.moving = true;
            }

            if (owner->info.moving)
            {
                bool stopMoving = false;

                //TODO: no magic numbers....
                if (glm::length(glm::distance(owner->info.pos, goalPos)) > 1.0)
                {
                    owner->info.ori = glm::normalize(goalPos - owner->info.pos);

                    glm::vec2 afterMovementPos = owner->info.pos + owner->info.ori * secDiff * path[step - 1].speed;
                    // if the distance in the current position to the goal is smaller than the distance from the new position we can stop moving
                    if (glm::length(glm::distance(owner->info.pos, goalPos)) < glm::length(glm::distance(afterMovementPos, goalPos)))
                    {
                        StopMoving();
                        return;
                    }
                }
                else
                {
                    StopMoving();
                    return;
                }

                owner->SendStateUpdate();
                return;
            }
            else if (activeActionTicks != 0)
            {
                activeActionTicks--;
                if (activeActionTicks == 0)
                {
                    remainingCasts--;

                    owner->currentAction.duration = 0;

                    owner->SendFinishAction();
                    owner->SendStateUpdate();
                    return;
                }
                else
                {
                    owner->currentAction.duration -= diff.count();
                    return;
                }
            }
            else if (remainingCasts > 0)
            {
                if (auto entity = owner->RandomEntityInRange(Shared::Config::actionRadius))
                {
                    activeActionTicks = Shared::Config::clientAIUpdatesPerSec * 2;

                    owner->currentAction.type = Processing::Zone::ActionType::SPELL;
                    owner->currentAction.duration = 2000;
                    owner->currentAction.range = Shared::Config::actionRadius;
                    owner->currentAction.source = client;
                    owner->currentAction.target = entity->first;

                    owner->SendStartAction();
                }
                else
                    remainingCasts--;
            }
        }
    });
}
