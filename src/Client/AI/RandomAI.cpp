#define _USE_MATH_DEFINES
#include <cmath>

#include "../Client.h"
#include "../ClientAI.h"

#include "Shared/Util.h"
#include "Shared/Config.h"

#include "glm/geometric.hpp"
#include "glm/gtx/rotate_vector.hpp"

#define COM ,

namespace Client
{
    CLIENT_AI(RandomAI,
    {
    public:
        int client;
        glm::vec2 goalPos;
        int remainingCasts;

        RandomAI(int clientId) : client(clientId) {}

        std::pair<bool COM glm::vec2> GetStartPosition()
        {
            return std::make_pair(false, glm::vec2());
        }

        void UpdateAI(std::chrono::milliseconds& diff)
        {
            if (owner->info.moving)
            {
                // we have moved with the configured movement speed with time diff
                // in the direction we are facing
                float secDiff = diff.count() / 1000.0f;
                owner->info.pos = owner->info.pos + owner->info.ori * secDiff * Shared::Config::walkingDistancePerSec;
                if (owner->info.pos.x >= Shared::Config::mapX)
                    owner->info.pos.x = (float)Shared::Config::mapX - Shared::Util::MapCoordEpsilon();
                if (owner->info.pos.x < 0.0f)
                    owner->info.pos.x = 0.0f;
                if (owner->info.pos.y >= Shared::Config::mapY)
                    owner->info.pos.y = (float)Shared::Config::mapY - Shared::Util::MapCoordEpsilon();
                if (owner->info.pos.y < 0.0f)
                    owner->info.pos.y = 0.0f;

                if (glm::length(glm::distance(owner->info.pos, goalPos)) < Shared::Config::walkingDistancePerSec)
                {
                    // we are finished with our current goal
                    // we send a not moving packet and will
                    // start moving again in the next round
                    owner->info.moving = false;

                    owner->SendStateUpdate();

                    // if the simulator is configured to cast spells do so
                    if (Shared::Config::targetInteractionsPerPause > 0)
                    {
                        remainingCasts = Shared::Config::targetInteractionsPerPause;
                        // we also start an action, for now just start actions at the end of movement
                        // only start an action if we can find a target in range
                        if (auto entity = owner->RandomEntityInRange(Shared::Config::actionRadius))
                        {
                            owner->currentAction.type = Processing::Zone::ActionType::SPELL;
                            owner->currentAction.duration = 2000;
                            owner->currentAction.range = Shared::Config::actionRadius;
                            owner->currentAction.source = client;
                            owner->currentAction.target = entity->first;

                            owner->SendStartAction();
                        }
                        else
                            remainingCasts--;
                    }

                    return;
                }
            }
            else if (owner->currentAction.duration != 0)
            {
                if (diff.count() >= owner->currentAction.duration)
                {
                    remainingCasts--;

                    owner->currentAction.duration = 0;

                    owner->SendFinishAction();
                    owner->SendStateUpdate();
                    return;
                }
                else
                {
                    owner->currentAction.duration -= diff.count();
                    return;
                }
            }
            else if (remainingCasts > 0)
            {
                if (auto entity = owner->RandomEntityInRange(Shared::Config::actionRadius))
                {
                    owner->currentAction.type = Processing::Zone::ActionType::SPELL;
                    owner->currentAction.duration = 2000;
                    owner->currentAction.range = Shared::Config::actionRadius;
                    owner->currentAction.source = client;
                    owner->currentAction.target = entity->first;

                    owner->SendStartAction();
                }
                else
                    remainingCasts--;
            }
            else
            {
                do
                {
                    goalPos = Shared::Util::GetRandomPositionOnMap(owner->generator);
                } while (glm::distance(goalPos, owner->info.pos) < Shared::Config::walkingDistancePerSec);

                owner->info.moving = true;
            }

            owner->info.ori = glm::normalize(goalPos - owner->info.pos);
            // we add some random degrees to make movement a bit more realistic
            std::uniform_real_distribution<float> distribution(-0.2f * M_PI, 0.2f * M_PI);
            owner->info.ori = glm::rotate(owner->info.ori, distribution(owner->generator));

            owner->SendStateUpdate();
        }
    });
}
