#include <fstream>
#include <utility>

#include "../Client.h"
#include "../ClientAI.h"

#include "Shared/Config.h"

#define COM ,

namespace Client
{
    CLIENT_AI(ReplayAI,
    {
    public:
        uint64_t step;
        int client;

        std::vector<std::pair<Processing::Zone::EntityInfo COM Processing::Zone::Action>> path;

        ReplayAI(int clientId)
        {
            step = 0;
            client = clientId;

            std::ifstream ifs;
            ifs.open(std::string(owner->GetID()->Serialize() + ".path.client.solp.txt"), std::ifstream::in);

            if (!ifs.is_open())
            {
                SOLP_ERROR("Could not open previous client path that should have filename (%s)\nTerminating....",
                    std::string(owner->GetID()->Serialize() + ".path.client.solp.txt").c_str());
                std::terminate();
            }

            // skip header
            ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            while (!ifs.eof())
            {
                uint64_t tick;

                ifs >> tick;

                Processing::Zone::EntityInfo ei;

                ifs >> ei.pos.x;
                ifs >> ei.pos.y;
                ifs >> ei.ori.x;
                ifs >> ei.ori.y;
                ifs >> ei.moving;

                Processing::Zone::Action a;

                int type;
                ifs >> type;
                a.type = (Processing::Zone::ActionType)type;
                ifs >> a.source;
                ifs >> a.target;
                ifs >> a.range;
                ifs >> a.duration;

                if (!ifs.good())
                    return;

                path.emplace_back(std::move(ei), std::move(a));
            }
        }

        std::pair<bool COM glm::vec2> GetStartPosition()
        {
            if (path.size() <= 0)
            {
                SOLP_ERROR("Loaded path has length 0, and hence does not contain a starting position... terminating....");
                std::terminate();
            }

            return std::make_pair(true, path[0].first.pos);
        }

        void UpdateAI(std::chrono::milliseconds& diff)
        {
            step++;
            
            // we finished our path.. stop here.. shutdown should come soon anyway
            if (step >= path.size())
                return;

            owner->info = path[step].first;

            // if was casting and is still casting we do nothing
            if (owner->currentAction.duration != 0 && path[step].second.duration != 0)
            {
                owner->currentAction = path[step].second;
                return;
            }

            // we know here that the new current step is not going to have an action active
            // so we finish the action
            if (owner->currentAction.duration != 0)
                owner->SendFinishAction();

            owner->SendStateUpdate();

            owner->currentAction = path[step].second;
            // if we are now casting something we send a start action message
            if (owner->currentAction.duration != 0)
                owner->SendStartAction();
        }
    });
}
