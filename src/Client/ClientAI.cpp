#include <mutex>
#include <unordered_map>

#include "Shared/Debug.h"
#include "Shared/Config.h"
#include "ClientAI.h"

namespace Client
{
    std::unordered_map<std::string, std::function<std::shared_ptr<ClientAI>(int)>>& GetAIs()
    {
        static std::unordered_map<std::string, std::function<std::shared_ptr<ClientAI>(int)>> ais;
        return ais;
    }

    std::mutex& GetMutex()
    {
        static std::mutex mutex;
        return mutex;
    }

    void ClientAI::AddAI(const std::string& aiName, const std::function<std::shared_ptr<ClientAI>(int)>& ai)
    {
        std::lock_guard<std::mutex> lck(GetMutex());

        GetAIs()[aiName] = ai;
    }

    std::shared_ptr<ClientAI> ClientAI::GetActiveAI(Client* creator, int clientId)
    {
        auto& ais = GetAIs();
        auto ai = ais.find(Shared::Config::clientAIName);

        if (ai == ais.end())
        {
            SOLP_ERROR("Active client AI does not exist... terminating...");
            std::terminate();
        }
        else
        {
            auto activeAI = ai->second(clientId);
            activeAI->SetOwner(creator);
            return activeAI;
        }
    }
}
