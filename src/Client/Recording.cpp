#include <fstream>

#include "Client.h"
#include "Recording.h"
#include "Shared/Singleton.h"

namespace Client
{
    Recording::~Recording()
    {
        // we here store the recorded path in a file
        std::fstream fs;
        fs.open(owner->GetID()->Serialize() + ".path.client.solp.txt", std::fstream::out | std::fstream::trunc);

        if (!fs.is_open())
            return;

        fs << "tick\tposx\tposy\torix\toriy\tmoving\tinteracting\tactiontype\tsource\ttarget\trange\tduration\n";
        for (int i = 0; i < path.size(); i++)
        {
            fs << i << "\t" << path[i].first.pos.x << "\t" << path[i].first.pos.y << "\t" << path[i].first.ori.x
                << "\t" << path[i].first.ori.y << "\t" << path[i].first.moving << "\t"
                << "\t" << (int)(path[i].second.type) << "\t" << path[i].second.source << "\t" << path[i].second.target
                << "\t" << path[i].second.range << "\t" << path[i].second.duration << "\n";
        }
    }

    void Recording::Record()
    {
        path.emplace_back(owner->info, owner->currentAction);
    }
}
