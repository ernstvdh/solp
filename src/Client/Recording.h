#ifndef SOLP_CLIENT_RECORDING_H
#define SOLP_CLIENT_RECORDING_H

namespace Processing
{
    namespace Zone
    {
        struct EntityInfo;
        struct Action;
    }
}

namespace Client
{
    class Client;
    class Recording
    {
    public:
        Recording(Client* client) : owner(client) {}
        ~Recording();

        void Record();

    private:
        Client* owner;

        // variable in which we store the path the client is walking
        std::vector<std::pair<Processing::Zone::EntityInfo, Processing::Zone::Action>> path;
    };
}

#endif
