#ifndef SOLP_CLIENT_CLIENTMESSAGE_H
#define SOLP_CLIENT_CLIENTMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

#include "Processing/Zone/EntityInfo.h"

namespace Client
{
    class ClientMessage : public Shared::Message::Message
    {
    public:
        enum ClientMessageType
        {
            MOVE,
            UPDATE_PLAYER,

            MAX_VALUE
        };

        ClientMessage()
        {
            processType = Shared::Id::ProcessType::CLIENT;
        }

        ClientMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
        {
            MESSAGE_GET_FROM_JSON_STRICT(clientMessageType);
        }

        ClientMessageType clientMessageType;

        static std::shared_ptr<ClientMessage> ParseJSon(const Shared::JSon::JSon& json)
        {
            ClientMessageType cmt;
            json.QueryStrict("clientMessageType", cmt);

            if (cmt >= ClientMessageType::MAX_VALUE)
            {
                SOLP_ERROR("received out of bounds tracker message type, aborting...");
                std::terminate();
            }

            return enumToMessageMap[cmt](json);
        }

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const
        {
            Shared::Message::Message::SerializeToJSon(json);
            MESSAGE_ADD_TO_JSON(clientMessageType);
        }

    private:
        static const std::vector<std::function<std::shared_ptr<ClientMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
    };

    class ClientMoveMessage : public ClientMessage
    {
    public:
        ClientMoveMessage()
        {
            clientMessageType = ClientMessageType::MOVE;
        }

        ClientMoveMessage(const Shared::JSon::JSon& json) : ClientMessage(json)
        {
            MESSAGE_GET_FROM_JSON_STRICT(moveX);
            MESSAGE_GET_FROM_JSON_STRICT(moveY);
            MESSAGE_GET_FROM_JSON_STRICT(moveZ);
        }

        double moveX, moveY, moveZ;

        static std::shared_ptr<ClientMoveMessage> ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<ClientMoveMessage>(json);
        }

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ClientMessage::SerializeToJSon(json);
            MESSAGE_ADD_TO_JSON(moveX);
            MESSAGE_ADD_TO_JSON(moveY);
            MESSAGE_ADD_TO_JSON(moveZ);
        }
    };

    class UpdatePlayerStateMessage : public ClientMessage
    {
    public:
        UpdatePlayerStateMessage()
        {
            clientMessageType = ClientMessageType::UPDATE_PLAYER;
        }

        UpdatePlayerStateMessage(int id, const Processing::Zone::EntityInfo& entityInfo, const uint64_t counter, const int lPhase)
        {
            clientMessageType = ClientMessageType::UPDATE_PLAYER;
            playerId = id;
            pInfo = entityInfo;
            aCounter = counter;
            phase = lPhase;
        }

        int playerId;
        Processing::Zone::EntityInfo pInfo;
        uint64_t aCounter;
        int phase;

        UpdatePlayerStateMessage(const Shared::JSon::JSon& json) : ClientMessage(json)
        {
            MESSAGE_GET_FROM_JSON_STRICT(playerId);
            MESSAGE_GET_FROM_JSON_STRICT(pInfo);
            MESSAGE_GET_FROM_JSON_STRICT(aCounter);
            MESSAGE_GET_FROM_JSON_STRICT(phase);
        }

        static std::shared_ptr<UpdatePlayerStateMessage> ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<UpdatePlayerStateMessage>(json);
        }

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ClientMessage::SerializeToJSon(json);
            MESSAGE_ADD_TO_JSON(playerId);
            MESSAGE_ADD_TO_JSON(pInfo);
            MESSAGE_ADD_TO_JSON(aCounter);
            MESSAGE_ADD_TO_JSON(phase);
        }
    };
}

#endif
