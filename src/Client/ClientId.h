#ifndef SOLP_CLIENT_CLIENTID_H
#define SOLP_CLIENT_CLIENTID_H

#include "Shared/Id.h"

namespace Client
{
    class ClientId : public Shared::Id
    {
    public:
        ClientId(int pId) : Shared::Id(Shared::Id::ProcessType::CLIENT), id(pId) {}
        ClientId(const Shared::JSon::JSon& json) : Shared::Id(Shared::Id::ProcessType::CLIENT)
        {
            GET_FROM_JSON_STRICT(id);
        }
        ~ClientId() {}

        int id;

        std::string Serialize() const
        {
            return std::to_string(type) + "_" + std::to_string(id);
        }

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(type);
            ADD_TO_JSON(id);
        }

        bool equals(const std::shared_ptr<const Id>& that) const
        {
            if (type != that->type)
                return false;

            std::shared_ptr<const ClientId> otherId(std::static_pointer_cast<const ClientId>(that));

            if (id != otherId->id)
                return false;

            return true;
        }

        static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<ClientId>(json);
        }
    };
}

#endif
