#ifndef SOLP_CLIENT_CLIENTAI_H
#define SOLP_CLIENT_CLIENTAI_H

#include <vector>
#include <memory>
#include <functional>

#include "glm/vec2.hpp"

#define CLIENT_AI(aiName, classContent)                                                         \
    class aiName : public ClientAI classContent;                                                \
    ClientAIBinding binding##aiName (std::string(#aiName), [](int clientId){ return std::make_shared<aiName>(clientId); })

namespace Client
{
    class Client;
    class ClientAI
    {
    public:
        virtual ~ClientAI() {}

        static void AddAI(const std::string& aiName, const std::function<std::shared_ptr<ClientAI>(int)>& ai);
        static std::shared_ptr<ClientAI> GetActiveAI(Client* creator, int clientId);

        virtual std::pair<bool, glm::vec2> GetStartPosition() = 0;
        virtual void UpdateAI(std::chrono::milliseconds& diff) = 0;

    protected:
        Client* owner;

        void SetOwner(Client* creator) { owner = creator; }
    };

    class ClientAIBinding
    {
    public:
        ClientAIBinding(const std::string& aiName, const std::function<std::shared_ptr<ClientAI>(int)>& ai)
        {
            ClientAI::AddAI(aiName, ai);
        }
    };
}

#endif
