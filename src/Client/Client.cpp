#include "Client.h"
#include "ClientId.h"
#include "ClientAI.h"
#include "Recording.h"

#include "Balancing/Entry/EntryId.h"
#include "Shared/Config.h"
#include "Shared/Util.h"
#include "Shared/Singleton.h"

namespace Client
{
    Client::Client(int pId) : generator(time(NULL))
    {
        id = std::make_shared<ClientId>(pId);
        connected = false;
        init = false;
        ownPhase = 0;
        counter = 0;

        monitorHook = std::make_shared<ClientServerHookImpl>(pId);
        Shared::Singleton::monitorHooks->Add(monitorHook);

        if (Shared::Config::recordClients)
            record = std::unique_ptr<Recording>(new Recording(this));

        ai = ClientAI::GetActiveAI(this, pId);
    }

    Client::~Client()
    {
        for (auto& entityInfo : entityInfoMap)
            delete entityInfo.second;
    }

    int Client::Run()
    {
        RegisterWithMonitor();

        std::chrono::steady_clock::time_point lastAIUpdate = std::chrono::steady_clock::now();
        std::chrono::milliseconds diff;

        RequestRouterConnection();

        while (true)
        {
            if (Shared::Singleton::shutdown.load())
                break;

            while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
            {
                // Monitoring
                monitorHook->receivedMessages++;

                //SOLP_DEBUG(DEBUG_CLIENT, "Received message: %s", mess->Serialise().c_str());
                if (auto result = std::dynamic_pointer_cast<BL::Entry::EntryResultMessage>(mess))
                    HandleEntryResult(result);
                else if (auto playerupdate = std::dynamic_pointer_cast<UpdatePlayerStateMessage>(mess))
                    HandlePutInWorldMessage(playerupdate);
                else if (auto playeraction = std::dynamic_pointer_cast<Processing::Zone::ActionStartMessage>(mess))
                    HandleActionStartMessage(playeraction);
                else if (auto connect = std::dynamic_pointer_cast<Processing::Router::ConnectToRouterMessage>(mess))
                    HandleConnectToRouterMessage(connect);
                else if (auto erase = std::dynamic_pointer_cast<Processing::Zone::ErasePlayerMessage>(mess))
                    HandleErasePlayerMessage(erase);
            }

            if (init)
            {
                diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - lastAIUpdate);
                lastAIUpdate = std::chrono::steady_clock::now();
                UpdateAI(diff);
            }
        }

        return 0;
    }

    void Client::RequestRouterConnection()
    {
        std::shared_ptr<Shared::Message::Message> request(
            Shared::Message::Message::MakeMessage<BL::Entry::RequestEntryMessage>(
            id, std::make_shared<const BL::Entry::EntryId>(Shared::Config::gLBIp)));
        SendMessageTemporary(request);
        SOLP_DEBUG(DEBUG_CLIENT, "sending message: %s", request->Serialise().c_str());
    }

    void Client::HandleConnectToRouterMessage(std::shared_ptr<Processing::Router::ConnectToRouterMessage>& connect)
    {
        routerPos = connect->position;
    }

    void Client::HandleEntryResult(std::shared_ptr<BL::Entry::EntryResultMessage> result)
    {
        SOLP_DEBUG(DEBUG_CLIENT, "Received router id to connect to.");
        routerId = result->routerId;
        connected = true;
        LinkToRemove(result->senderID);

        auto hasStartPosition = ai->GetStartPosition();
        auto enter = Shared::Message::Message::MakeMessage<Processing::Router::ConnectToRouterMessage>(id, routerId);
        if (hasStartPosition.first)
        {
            enter->requestPosition = true;
            enter->position = hasStartPosition.second;
        }
        SendMessageTemporary(enter);

        counter++;
    }

    void Client::HandlePutInWorldMessage(std::shared_ptr<UpdatePlayerStateMessage> putInWorld)
    {
        if (putInWorld->playerId == std::static_pointer_cast<const ClientId>(id)->id)
        {
            ownPhase = putInWorld->phase;

            // last client location we have received from the router
            routerPos = putInWorld->pInfo.pos;

            if (!init)
            {
                if (Shared::Config::recordClients)
                    record->Record();
                // only update our local player info,
                // from this moment on the client will decide its own info
                // the router/zones will catch up eventually
                info.pos = putInWorld->pInfo.pos;
                init = true;
                // we update the AI every ... seconds so add a timed event
                SetTimedLinkEvent(1000.0 / Shared::Config::clientAIUpdatesPerSec);
            }

            while (!outgoingTimes.empty())
            {
                // if this is handled we can add the response time
                if (outgoingTimes.front().second <= putInWorld->aCounter)
                {
                    // the actual response time calculation happens here
                    uint64_t rtime = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::steady_clock::now() - outgoingTimes.front().first).count();
                    outgoingTimes.pop();
                    // Monitoring
                    monitorHook->responseTimes.push(rtime);
                }
                else
                    break;
            }

            SOLP_DEBUG(DEBUG_CLIENT, "client (self) moved to position (%f,%f)", putInWorld->pInfo.pos.x, putInWorld->pInfo.pos.y);

            for (auto itx = visibleEntities.begin(); itx != visibleEntities.end();)
            {
                for (auto ity = itx->second.begin(); ity != itx->second.end();)
                {
                    int corners = Shared::Util::CellCornersInRange(
                        putInWorld->pInfo.pos, Shared::Config::visionRadius, itx->first, ity->first);

                    //TODO: figure out why this doesn't work (inconsistent with the cellData at the router)
                    /*if (corners == 0)
                    {
                        for (const auto& entity : ity->second)
                            HandleEntityOutOfRange(entity.first);

                        ity = itx->second.erase(ity);
                    }
                    else */if (corners < 4)
                    {
                        for (auto entity = ity->second.begin(); entity != ity->second.end();)
                        {
                            if (!Shared::Util::IsInRange(entity->second->second.pos, putInWorld->pInfo.pos, Shared::Config::visionRadius))
                            {
                                HandleEntityOutOfRange(entity->first);
                                entity = ity->second.erase(entity);
                            }
                            else
                                entity++;
                        }

                        if (ity->second.empty())
                            ity = itx->second.erase(ity);
                        else
                            ity++;
                    }
                    else
                        ity++;
                }


                if (itx->second.empty())
                    itx = visibleEntities.erase(itx);
                else
                    itx++;
            }
        }
        else
            SOLP_DEBUG(DEBUG_CLIENT, "client (not self) moved to position (%f,%f)", putInWorld->pInfo.pos.x, putInWorld->pInfo.pos.y);

        bool inRange = Shared::Util::IsInRange(putInWorld->pInfo.pos, routerPos, Shared::Config::visionRadius);
        auto alreadyExists = entityInfoMap.find(putInWorld->playerId);
        if (alreadyExists != entityInfoMap.end())
        {
            auto oldCellCoords = Shared::Util::PositionToCellCoords(alreadyExists->second->second.pos);
            auto newCellCoords = Shared::Util::PositionToCellCoords(putInWorld->pInfo.pos);
            alreadyExists->second->first = putInWorld->phase;
            alreadyExists->second->second = std::move(putInWorld->pInfo);
            
            if (!inRange)
            {
                // if the entity is not any more in range we can safely remove its state now
                HandleEntityOutOfRange(putInWorld->playerId);

                const auto x = visibleEntities.find(oldCellCoords.first);
                const auto y = x->second.find(oldCellCoords.second);

                y->second.erase(putInWorld->playerId);
                if (y->second.empty())
                {
                    x->second.erase(y);

                    if (x->second.empty())
                        visibleEntities.erase(x);
                }
            }
            else
            {
                // The entity moved from one cell to another change data structure accordingly
                visibleEntities[newCellCoords.first][newCellCoords.second].emplace(putInWorld->playerId, alreadyExists->second);

                if (oldCellCoords.first != newCellCoords.first || oldCellCoords.second != newCellCoords.second)
                {
                    const auto x = visibleEntities.find(oldCellCoords.first);
                    const auto y = x->second.find(oldCellCoords.second);

                    y->second.erase(putInWorld->playerId);
                    if (y->second.empty())
                    {
                        x->second.erase(y);

                        if (x->second.empty())
                            visibleEntities.erase(x);
                    }
                }
            }
        }
        else if (inRange)
        {
            auto cellIdCoords = Shared::Util::PositionToCellCoords(putInWorld->pInfo.pos);
            // this object is deleted if necessary in HandleEntityOutOfRange
            // or when the clients shutdown to avoid it being considered a memory leak
            std::pair<int, Processing::Zone::EntityInfo>* eInfo = new std::pair<int, Processing::Zone::EntityInfo>(putInWorld->phase, std::move(putInWorld->pInfo));

            // does not already exists so add to cell map
            visibleEntities[cellIdCoords.first][cellIdCoords.second].emplace(putInWorld->playerId, eInfo);
            entityInfoMap.emplace(putInWorld->playerId, eInfo);

            // Monitoring
            monitorHook->visibleClients++;
        }
    }

    //TODO: do something meaningful here?
    void Client::HandleActionStartMessage(std::shared_ptr<Processing::Zone::ActionStartMessage> actionstart)
    {

    }
    //TODO: handle fire message
    //TODO: handle hit message

    void Client::HandleErasePlayerMessage(std::shared_ptr<Processing::Zone::ErasePlayerMessage> erase)
    {
        auto entity = entityInfoMap.find(erase->playerId);
        if (entity == entityInfoMap.end())
            return;

        std::pair<int, int> cellCoords = Shared::Util::PositionToCellCoords(entity->second->second.pos);

        HandleEntityOutOfRange(erase->playerId);
        visibleEntities[cellCoords.first][cellCoords.second].erase(erase->playerId);

        // cleanup
        if (visibleEntities[cellCoords.first][cellCoords.second].empty())
            visibleEntities[cellCoords.first].erase(cellCoords.second);
        if (visibleEntities[cellCoords.first].empty())
            visibleEntities.erase(cellCoords.first);
    }

    void Client::SendStateUpdate()
    {
        std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage> toSend(
            Shared::Message::Message::MakeMessage<Processing::Zone::UpdatePlayerStateMessage>(id, routerId));
        toSend->pInfo = info;
        toSend->playerId = std::static_pointer_cast<const ClientId>(id)->id;
        toSend->aCounter = counter;

        // Monitoring
        // for monitoring we store here the time in ms of sending
        outgoingTimes.push(std::make_pair(std::chrono::steady_clock::now(), counter));

        SendMessagePersistent(toSend);

        counter++;
    }

    void Client::SendStartAction()
    {
        std::shared_ptr<Processing::Zone::ActionStartMessage> toSend(
            Shared::Message::Message::MakeMessage<Processing::Zone::ActionStartMessage>(id, routerId));

        toSend->playerId = std::static_pointer_cast<const ClientId>(id)->id;
        toSend->action = currentAction;
        toSend->aCounter = counter;

        SOLP_DEBUG(DEBUG_INTERACTION, "Send interaction.");

        SendMessagePersistent(toSend);

        counter++;
    }

    void Client::SendFinishAction()
    {
        std::shared_ptr<Processing::Zone::ActionFireMessage> toSend(
            Shared::Message::Message::MakeMessage<Processing::Zone::ActionFireMessage>(id, routerId));

        toSend->playerId = std::static_pointer_cast<const ClientId>(id)->id;
        toSend->fire = currentAction;
        toSend->aCounter = counter;

        SendMessagePersistent(toSend);

        counter++;
    }

    void Client::HandleEntityOutOfRange(int eId)
    {
        auto entity = entityInfoMap.find(eId);
        delete entity->second;
        entityInfoMap.erase(entity);

        // Monitoring
        monitorHook->visibleClients--;
    }

    void Client::UpdateAI(std::chrono::milliseconds& diff)
    {
        ai->UpdateAI(diff);

        if (Shared::Config::recordClients)
            record->Record();
    }

    const std::pair<const int, std::pair<int, Processing::Zone::EntityInfo>*>* Client::RandomEntityInRange(const double range)
    {
        int self = std::static_pointer_cast<const ClientId>(id)->id;

        std::vector<const std::pair<const int, std::pair<int, Processing::Zone::EntityInfo>*>*> possibilities;
        auto cells = Shared::Util::CellCoordsInRange(info.pos, range);
        for (auto cellx : cells)
        {
            for (auto celly : cellx.second)
            {
                if (auto entities = EntitiesInCell(cellx.first, celly.first))
                {
                    for (const auto& entity : *entities)
                    {
                        if (!Shared::Util::IsInRange(info.pos, entity.second->second.pos, range))
                            continue;

                        if (entity.first == self)
                            continue;
                        
                        int times = 1;
                        if (Shared::Config::targetSelectionPreferFriends)
                        {
                            if (Shared::Singleton::playerRelations[self].count(entity.first) < 1)
                            {
                                if (Shared::Config::targetSelectionIgnoreNonFriends)
                                    continue;

                                if (Shared::Config::friendSelectionModifier < -1)
                                    times = Shared::Config::friendSelectionModifier * -1;
                            }
                            else
                            {
                                if (Shared::Config::friendSelectionModifier > 1)
                                    times = Shared::Config::friendSelectionModifier;
                            }
                        }

                        if (Shared::Config::targetSelectionOnlyOwnPhase)
                        {
                            if (ownPhase != entity.second->first)
                            {
                                auto ownZone = Shared::Util::PositionToZoneCoords(info.pos);
                                auto otherZone = Shared::Util::PositionToZoneCoords(entity.second->second.pos);

                                // if not in the same phase but in the same zone we do not allow interaction
                                if (ownZone.first == otherZone.first && ownZone.second == otherZone.second)
                                    continue;
                            }
                        }

                        for (int i = 0; i < times; i++)
                            possibilities.push_back(&entity);
                        
                    }
                }
            }
        }

        // nobody in range so just return NULL
        if (possibilities.empty())
            return NULL;

        std::uniform_int_distribution<size_t> distribution(0, possibilities.size() - 1);
        return possibilities[distribution(generator)];
    }

    const std::unordered_map<int, std::pair<int, Processing::Zone::EntityInfo>*>* Client::EntitiesInCell(const int cellx, const int celly) const
    {
        auto findx = visibleEntities.find(cellx);
        if (findx == visibleEntities.end())
            return NULL;

        auto findy = findx->second.find(celly);
        if (findy == findx->second.end())
            return NULL;

        return &(findy->second);
    }

    void Client::ClientServerHookImpl::MonitorTick(int t)
    {
        std::vector<uint64_t> resTimes;
        uint64_t resTime;
        while (responseTimes.pop(resTime))
            resTimes.push_back(resTime);

        tickInfo.push_back(std::tuple<int, uint64_t, uint64_t, std::vector<uint64_t>>(
            t, visibleClients.load(), receivedMessages.exchange(0), resTimes));
    }

    void Client::ClientServerHookImpl::StoreData()
    {
        {
            std::fstream fs;
            fs.open(std::to_string(Shared::Id::ProcessType::CLIENT) + "_" + std::to_string(id) +
                ".client.solp.txt", std::fstream::out | std::fstream::trunc);

            fs << "tick\tvisible\tmessages\n";
            for (const auto& info : tickInfo)
                fs << std::get<0>(info) << "\t" << std::get<1>(info) << "\t" << std::get<2>(info) << "\n";
        }

        {
            std::fstream fs;
            fs.open(std::to_string(Shared::Id::ProcessType::CLIENT) + "_" + std::to_string(id) +
                ".responsetimes.client.solp.txt", std::fstream::out | std::fstream::trunc);

            fs << "tick\trtime\n";
            for (const auto& info : tickInfo)
                for (const uint64_t& rtime : std::get<3>(info))
                    fs << std::get<0>(info) << "\t" << rtime << "\n";
        }
    }
}
