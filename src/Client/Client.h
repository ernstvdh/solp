#ifndef SOLP_CLIENT_CLIENT_H
#define SOLP_CLIENT_CLIENT_H

#include <random>
#include <queue>

#include "glm/vec2.hpp"

#include "Shared/Link/Linkable.h"
#include "Shared/JSon/JSon.h"
#include "Monitoring/Server/ServerHook.h"
#include "Balancing/Entry/EntryMessage.h"
#include "Processing/Zone/ZoneMessage.h"
#include "Processing/Router/RouterMessage.h"
#include "Processing/Zone/EntityInfo.h"
#include "ClientMessage.h"

namespace Client
{
    class ClientAI;
    class Recording;
    class Client : public Shared::Link::Linkable
    {
    public:
        Client(int pId);
        ~Client();

        int Run();

        std::default_random_engine generator;

        Processing::Zone::EntityInfo info;
        bool connected;
        std::shared_ptr<const Processing::Router::RouterId> routerId;
        Processing::Zone::Action currentAction;
        // every new action/movement gets a new number, this way we can
        // properly check the response time and this makes our live easier
        // @ the server because we can use this number to verify if certain
        // actions have been received or not
        uint64_t counter;

        void UpdateAI(std::chrono::milliseconds& diff);

        void SendStateUpdate();
        void SendStartAction();
        void SendFinishAction();

        const std::pair<const int, std::pair<int, Processing::Zone::EntityInfo>*>* RandomEntityInRange(const double range);

    private:
        bool init;
        glm::vec2 routerPos;
        int ownPhase;

        std::unordered_map<int, std::unordered_map<int, std::unordered_map<int, std::pair<int, Processing::Zone::EntityInfo>*>>> visibleEntities;
        std::unordered_map<int, std::pair<int, Processing::Zone::EntityInfo>*> entityInfoMap;

        std::queue<std::pair<std::chrono::steady_clock::time_point, uint64_t>> outgoingTimes;

        void RequestRouterConnection();
        void HandleEntryResult(std::shared_ptr<BL::Entry::EntryResultMessage> result);
        void HandlePutInWorldMessage(std::shared_ptr<UpdatePlayerStateMessage> putInWorld);
        void HandleActionStartMessage(std::shared_ptr<Processing::Zone::ActionStartMessage> actionstart);
        void HandleErasePlayerMessage(std::shared_ptr<Processing::Zone::ErasePlayerMessage> erase);
        void HandleConnectToRouterMessage(std::shared_ptr<Processing::Router::ConnectToRouterMessage>& connect);

        void HandleEntityOutOfRange(int eId);
        
        const std::unordered_map<int, std::pair<int, Processing::Zone::EntityInfo>*>* EntitiesInCell(const int cellx, const int celly) const;

        // Monitoring
        class ClientServerHookImpl : public Monitor::Server::ServerHookInterface
        {
        public:
            ClientServerHookImpl(const int lId) :
                visibleClients(0), receivedMessages(0), id(lId), responseTimes(0) {}
            ~ClientServerHookImpl() { StoreData(); }

            void MonitorTick(int t);

            std::atomic<uint64_t> visibleClients;
            std::atomic<uint64_t> receivedMessages;

            boost::lockfree::queue<uint64_t, boost::lockfree::fixed_sized<false>> responseTimes;
        private:
            void StoreData();

            const int id;
            std::list<std::tuple<int, uint64_t, uint64_t, std::vector<uint64_t>>> tickInfo;
        };

        std::shared_ptr<ClientServerHookImpl> monitorHook;

        std::shared_ptr<ClientAI> ai;
        std::unique_ptr<Recording> record;
    };
}

#endif
