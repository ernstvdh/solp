#include <string>
#include <fstream>

#include "Zone.h"

#include "Shared/Id.h"
#include "Shared/Config.h"
#include "Shared/System/System.h"

namespace Monitor
{
    namespace Zone
    {
        Zone::Zone(const int x, const int y, const int phase) : handledEntities(0), ownX(x), ownY(y), ownPhase(phase)
        {
            uint64_t utime, stime;
            if (!Shared::System::GetThreadTimesForId(Shared::System::GetThreadId(), utime, stime, startTime))
            {
                SOLP_ERROR("Getting thread start time failed even when calling from own thread... terminating...");
                std::terminate();
            }
        }

        void Zone::MonitorTick(int t)
        {
            tickInfo.push_back(std::tuple<int, uint64_t>(t, handledEntities.load()));
        }

        void Zone::StoreData()
        {
            std::fstream fs;
            fs.open(std::to_string(Shared::Id::ProcessType::CELL) + "_" + std::to_string(ownX) +
                "_" + std::to_string(ownY) + "_" + std::to_string(ownPhase) + "-" + std::to_string(startTime) +
                "-" + Shared::Config::localIP + ".processing.zone.solp.txt", std::fstream::out | std::fstream::trunc);

            fs << "tick\tentities\n";
            for (const auto& info : tickInfo)
                fs << std::get<0>(info) << "\t" << std::get<1>(info) << "\n";

            fs.close();
        }
    }
}
