#ifndef SOLP_MONITORING_ZONE_ZONE_H
#define SOLP_MONITORING_ZONE_ZONE_H

#include <list>
#include <atomic>

#include "Monitoring/Server/ServerHook.h"

namespace Monitor
{
    namespace Zone
    {
        class Zone : public Monitor::Server::ServerHookInterface
        {
        public:
            Zone(const int x, const int y, const int phase);
            ~Zone() { StoreData(); }

            void MonitorTick(int t);

            std::atomic<uint64_t> handledEntities;
        private:
            void StoreData();

            const int ownX;
            const int ownY;
            const int ownPhase;
            uint64_t startTime;
            std::list<std::tuple<int, uint64_t>> tickInfo;
        };
    }
}

#endif
