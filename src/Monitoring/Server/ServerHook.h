#ifndef SOLP_MONITORING_SERVER_SERVERHOOK_H
#define SOLP_MONITORING_SERVER_SERVERHOOK_H

#include <vector>
#include <mutex>
#include <memory>
#include <algorithm>

namespace Monitor
{
    namespace Server
    {
        class ServerHookInterface
        {
        public:
            ServerHookInterface() {}
            virtual ~ServerHookInterface() {}

            virtual void MonitorTick(int t) = 0;

        protected:
            // this should be called only ones in the destructor
            // of the derived class
            virtual void StoreData() = 0;
        };

        class ServerHook
        {
            std::mutex mutex;
            std::vector<std::weak_ptr<ServerHookInterface>> hooked;
            std::vector<std::weak_ptr<ServerHookInterface>> toInsert;

        public:
            void Tick(int t)
            {
                {
                    std::lock_guard<std::mutex> lck(mutex);
                    hooked.insert(hooked.end(), toInsert.begin(), toInsert.end());
                    toInsert.clear();
                }

                hooked.erase(std::remove_if(hooked.begin(), hooked.end(),
                    [this, t](const std::weak_ptr<ServerHookInterface>& hook)->bool
                    {
                        if (auto locked = hook.lock())
                        {
                            locked->MonitorTick(t);
                            return false;
                        }
                        else
                            return true;
                    }
                ), hooked.end());
            }

            void Add(std::weak_ptr<ServerHookInterface> hook)
            {
                std::lock_guard<std::mutex> lck(mutex);
                toInsert.push_back(hook);
            }
        };
    }
}

#endif
