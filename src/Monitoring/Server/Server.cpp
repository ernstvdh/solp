#include <thread>
#include <algorithm>

#include "Server.h"
#include "ServerId.h"

#include "Shared/Config.h"
#include "Shared/Singleton.h"
#include "Shared/Net/ConnectionId.h"
#include "Shared/Net/ConnectionMgr.h"

namespace Monitor
{
    namespace Server
    {
        Server::Server()
        {
            id = std::make_shared<const ServerId>(Shared::Config::localIP);
            serverMonitorHook = std::make_shared<MonitorServerHookImpl>(this);
            Shared::Singleton::monitorHooks->Add(serverMonitorHook);
        }

        int Server::Run()
        {
            ListenOn(Shared::Id::ProcessType::SERVER_MONITOR);

            std::chrono::steady_clock::time_point measureTime = std::chrono::steady_clock::now() +
                std::chrono::seconds(Shared::Config::serverMonitorIntervalInSec);
            uint64_t utime;
            uint64_t stime;
            uint64_t start;
            uint64_t tick = 1;

            // Add our own thread id to the map
            if (Shared::System::GetThreadTimesForId(Shared::System::GetThreadId(), utime, stime, start))
                serverMonitorHook->AddThreadInfo(Shared::System::GetThreadId(), start, id);

            // Add connectionMgr thread to the map
            for (int i = 0; i < Shared::Net::ConnectionMgr::singleton->startTimes.size(); i++)
            {
                // wait for the connection thread to be initialized
                while (Shared::Net::ConnectionMgr::singleton->startTimes[i] == 0)
                    std::this_thread::sleep_for(std::chrono::microseconds(100));

                serverMonitorHook->AddThreadInfo(Shared::Net::ConnectionMgr::singleton->threadIds[i],
                    Shared::Net::ConnectionMgr::singleton->startTimes[i],
                    std::make_shared<const Shared::Net::ConnectionId>(std::to_string(i) + "_" + Shared::Config::localIP));
            }

            // we wake up the thread every 50ms to check if we need to monitor
            SetTimedLinkEvent(50);

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    if (auto request = std::dynamic_pointer_cast<AnnounceThreadIdMessage>(mess))
                        HandleThreadIdMessage(request);
                    else if (auto hooktotal = std::dynamic_pointer_cast<HookTotalLoadMessage>(mess))
                        HandleHookTotalLoadMessage(hooktotal);
                    else if (auto hookprocesstype = std::dynamic_pointer_cast<HookProcessTypeLoadMessage>(mess))
                        HandleHookProcessTypeLoadMessage(hookprocesstype);
                    else if (auto hookprocessid = std::dynamic_pointer_cast<HookIDLoadMessage>(mess))
                        HandleHookIDLoadMessage(hookprocessid);
                }

                if (std::chrono::steady_clock::now() > measureTime)
                {
                    measureTime = measureTime + std::chrono::seconds(Shared::Config::serverMonitorIntervalInSec);

                    Shared::Singleton::monitorHooks->Tick(tick);
                    tick++;
                }
            }

            return 0;
        }

        void Server::HandleThreadIdMessage(const std::shared_ptr<AnnounceThreadIdMessage>& announce)
        {
            serverMonitorHook->AddThreadInfo(announce->threadId, announce->startTime, announce->senderID);
        }

        void Server::HandleHookTotalLoadMessage(const std::shared_ptr<HookTotalLoadMessage>& hooktotal)
        {
            serverMonitorHook->HookTotalLoad(hooktotal->maxTotalLoad, hooktotal->senderID, hooktotal->add);
        }

        void Server::HandleHookProcessTypeLoadMessage(const std::shared_ptr<HookProcessTypeLoadMessage>& hookprocesstype)
        {
            serverMonitorHook->HookProcessTypeLoad(hookprocesstype->processType, hookprocesstype->maxTotalLoad,
                hookprocesstype->senderID, hookprocesstype->add);
        }

        void Server::HandleHookIDLoadMessage(const std::shared_ptr<HookIDLoadMessage>& hookid)
        {
            serverMonitorHook->HookIDLoad(hookid->id, hookid->loadThresholds, hookid->senderID, hookid->add);
        }

        void Server::MonitorServerHookImpl::AddThreadInfo(uint64_t threadId, uint64_t startTime, const std::shared_ptr<const Shared::Id>& id)
        {
            auto found = idHooks.find(id->Serialize());

            threadInfo[threadId][startTime].id = id;
            if (found != idHooks.end())
                threadInfo[threadId][startTime].loadThresholds = found->second;
        }

        void Server::MonitorServerHookImpl::HookIDLoad(const std::shared_ptr<const Shared::Id>& toMonitor,
            const std::vector<double>& thresholds, const std::shared_ptr<const Shared::Id>& toInform, const bool add)
        {
            auto found = idHooks.find(toMonitor->Serialize());
            if (found == idHooks.end())
            {
                if (!add)
                {
                    SOLP_WARNING("Removing hooked id for monitoring but requesting id does not exist");
                    return;
                }

                auto hook = std::make_shared<std::unordered_map<std::string, std::tuple<std::shared_ptr<const Shared::Id>, std::vector<double>, size_t>>>();

                // if the hook is new we have to add it to the appropriate threadinfo container
                // if this container does not yet exist we do not have to do anything
                // the threadinfo container will add the shared_ptr to our hooks when it is created
                //TODO: optimize this?
                for (auto& threadsWithThreadId : threadInfo)
                    for (auto& threadsWithStartTime : threadsWithThreadId.second)
                        if (threadsWithStartTime.second.id)
                            if (threadsWithStartTime.second.id->equals(toMonitor))
                                threadsWithStartTime.second.loadThresholds = hook;

                idHooks[toMonitor->Serialize()] = hook;
            }

            if (add)
            {
                std::vector<double> sortedThresholds { 0.0 } ;
                sortedThresholds.insert(sortedThresholds.end(), thresholds.begin(), thresholds.end());
                std::sort(sortedThresholds.begin(), sortedThresholds.end());
                (*idHooks[toMonitor->Serialize()])[toInform->Serialize()] =
                    std::tuple<std::shared_ptr<const Shared::Id>, std::vector<double>, size_t>(toInform, std::move(sortedThresholds), 0);
            }
            else
            {
                found->second->erase(toInform->Serialize());
                if (found->second->empty())
                    idHooks.erase(found);
            }
        }

        void Server::MonitorServerHookImpl::HookTotalLoad(const double threshold,
            const std::shared_ptr<const Shared::Id>& toInform, const bool add)
        {
            if (add)
            {
                totalLoadHooks.second[toInform->Serialize()] =
                    std::tuple<std::shared_ptr<const Shared::Id>, double, bool>(toInform, threshold, false);
            }
            else
                totalLoadHooks.second.erase(toInform->Serialize());
        }

        void Server::MonitorServerHookImpl::HookProcessTypeLoad(const Shared::Id::ProcessType toMonitor,
            const double threshold, const std::shared_ptr<const Shared::Id>& toInform, const bool add)
        {
            if (add)
            {
                processTypeHooks[toMonitor].second[toInform->Serialize()] =
                    std::tuple<std::shared_ptr<const Shared::Id>, double, bool>(toInform, threshold, false);
            }
            else
                processTypeHooks[toMonitor].second.erase(toInform->Serialize());
        }

        void Server::MonitorServerHookImpl::HandleIDHooks(ThreadInfo& tInfo, const double diff)
        {
            if (tInfo.loadThresholds)
            {
                for (auto& thresholds : *tInfo.loadThresholds)
                {
                    size_t curThres = std::get<2>(thresholds.second);
                    size_t newThres = curThres;
                    const std::vector<double>& availThresholds = std::get<1>(thresholds.second);

                    if (diff < availThresholds[curThres])
                    {
                        if (curThres == 0)
                        {
                            continue;
                        }
                        if (diff < availThresholds[curThres - 1])
                        {
                            for (size_t i = 0; i < curThres; i++)
                            {
                                if (diff < availThresholds[i])
                                {
                                    newThres = i;
                                    break;
                                }
                            }
                        }
                        else if (diff < availThresholds[curThres] - Shared::Config::loadMonitoringDeviation)
                        {
                            newThres = curThres - 1;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (curThres + 1 >= availThresholds.size())
                        {
                            continue;
                        }
                        if (diff > availThresholds[curThres + 1])
                        {
                            for (size_t i = availThresholds.size() - 1; i > curThres; i--)
                            {
                                if (diff > availThresholds[i])
                                {
                                    newThres = i;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }

                    std::get<2>(thresholds.second) = newThres;

                    auto message(std::make_shared<HookIDLoadResMessage>(diff, tInfo.id));
                    message->senderID = owner->GetID();
                    message->receiverID = std::get<0>(thresholds.second);
                    owner->SendMessagePersistent(message);
                }
            }
        }

        void Server::MonitorServerHookImpl::HandleTotalLoadHooks()
        {
            for (auto& thresholds : totalLoadHooks.second)
            {
                // if true it has already been informed
                // in this case we check if the load is now lower
                // in which case we reply with the lower load
                if (std::get<2>(thresholds.second))
                {
                    if (std::get<1>(thresholds.second) > totalLoadHooks.first)
                    {
                        std::get<2>(thresholds.second) = false;
                        auto message(std::make_shared<HookTotalLoadMessage>(totalLoadHooks.first, false));
                        message->senderID = owner->GetID();
                        message->receiverID = std::get<0>(thresholds.second);
                        owner->SendMessagePersistent(message);
                    }
                }
                else if (std::get<1>(thresholds.second) <= totalLoadHooks.first)
                {
                    std::get<2>(thresholds.second) = true;
                    auto message(std::make_shared<HookTotalLoadMessage>(totalLoadHooks.first, true));
                    message->senderID = owner->GetID();
                    message->receiverID = std::get<0>(thresholds.second);
                    owner->SendMessagePersistent(message);
                }
            }
        }

        void Server::MonitorServerHookImpl::HandleProcessTypeLoad()
        {
            for (auto& hookedProcessType : processTypeHooks)
            {
                for (auto& thresholds : hookedProcessType.second.second)
                {
                    // if true it has already been informed
                    // in this case we check if the load is now lower
                    // in which case we reply with the lower load
                    if (std::get<2>(thresholds.second))
                    {
                        if (std::get<1>(thresholds.second) > hookedProcessType.second.first)
                        {
                            std::get<2>(thresholds.second) = false;
                            auto message(std::make_shared<HookProcessTypeLoadMessage>(hookedProcessType.second.first,
                                (Shared::Id::ProcessType)hookedProcessType.first, false));
                            message->senderID = owner->GetID();
                            message->receiverID = std::get<0>(thresholds.second);
                            owner->SendMessagePersistent(message);
                        }
                    }
                    else if (std::get<1>(thresholds.second) <= hookedProcessType.second.first)
                    {
                        std::get<2>(thresholds.second) = true;
                        auto message(std::make_shared<HookProcessTypeLoadMessage>(hookedProcessType.second.first,
                            (Shared::Id::ProcessType)hookedProcessType.first, true));
                        message->senderID = owner->GetID();
                        message->receiverID = std::get<0>(thresholds.second);
                        owner->SendMessagePersistent(message);
                    }
                }
            }
        }

        void Server::MonitorServerHookImpl::MonitorTick(int t)
        {
            // reset load aggregators for this tick
            totalLoadHooks.first = 0.0;
            for (auto& ptHooks : processTypeHooks)
                ptHooks.second.first = 0.0;

            uint64_t totalUTime = 0;
            uint64_t totalSTime = 0;

            lastTick = t;
            auto threads = Shared::System::GetThreadIdsOfPId(processId);
            for (const auto thread : threads)
            {
                if (Shared::System::GetThreadTimesForId(thread, utime, stime, start))
                {
                    ThreadInfo& tInfo = threadInfo[thread][start];
                    uint64_t prevstime = 0;
                    uint64_t prevutime = 0;
                    if (!tInfo.times.empty())
                    {
                        ThreadTimes& times = tInfo.times.back();
                        prevstime = times.sysTime;
                        prevutime = times.userTime;
                    }

                    uint64_t newsutime = stime - prevstime + utime - prevutime;

                    double diff = (double)newsutime / 1000000.0;

                    tInfo.times.emplace_back(stime, utime, t);

                    HandleIDHooks(tInfo, diff);
                    totalLoadHooks.first += diff;
                    // we need to check if we know the id of the thread
                    if (tInfo.id)
                    {
                        // if so we check if we are hooked on this type
                        auto found = processTypeHooks.find(tInfo.id->type);
                        if (found != processTypeHooks.end())
                        {
                            found->second.first += diff;
                        }
                    }

                    totalUTime += utime;
                    totalSTime += stime;
                }
            }

            totalLoadPerTick.emplace_back(totalSTime, totalUTime, t);

            HandleTotalLoadHooks();
            HandleProcessTypeLoad();
        }

        void Server::MonitorServerHookImpl::StoreData()
        {
            // aggregate all data without an id
            std::vector<ThreadTimes> misc;
            for (int i = 1; i <= lastTick; i++)
                misc.emplace_back(0, 0, i);

            // before shutdown we write the data to a csv file
            for (auto& threadid : threadInfo)
            {
                for (auto& start : threadid.second)
                {
                    if (!start.second.id)
                    {
                        for (const auto& info : start.second.times)
                        {
                            misc[info.tickNumber - 1].sysTime += info.sysTime;
                            misc[info.tickNumber - 1].userTime += info.userTime;
                        }
                        continue;
                    }

                    std::fstream fs;
                    std::string filename = Shared::Config::localIP + "-" + std::to_string(threadid.first) +
                        "-" + std::to_string(start.first) + std::string(".server.monitor.solp.txt");
                    if (start.second.id)
                        filename = start.second.id->Serialize() + std::string("-") + filename;
                    fs.open(filename, std::fstream::out | std::fstream::trunc);

                    fs << "tick\tstime\tutime\n";

                    utime = 0;
                    stime = 0;
                    if (!start.second.times.empty())
                        start.second.times.pop_back();
                    for (const auto& info : start.second.times)
                    {
                        fs << info.tickNumber << "\t" << (double)(info.sysTime - stime) / 1000000.0 <<
                            "\t" << (double)(info.userTime - utime) / 1000000.0 << "\n";
                        utime = info.userTime;
                        stime = info.sysTime;
                    }

                    fs.close();
                }
            }

            {
                utime = 0;
                stime = 0;
                std::fstream fs;
                fs.open("misc." + Shared::Config::localIP + ".server.monitor.solp.txt", std::fstream::out | std::fstream::trunc);
                fs << "tick\tstime\tutime\n";

                // we remove the last element because shutdown might screw up the results
                if (!misc.empty())
                    misc.pop_back();
                for (const auto& info : misc)
                {
                    fs << info.tickNumber << "\t" << (double)(info.sysTime - stime) / 1000000.0 <<
                        "\t" << (double)(info.userTime - utime) / 1000000.0 << "\n";
                    utime = info.userTime;
                    stime = info.sysTime;
                }
            }

            {
                utime = 0;
                stime = 0;
                std::fstream fs;
                fs.open("total." + Shared::Config::localIP + ".server.monitor.solp.txt", std::fstream::out | std::fstream::trunc);
                fs << "tick\tstime\tutime\n";
                
                // we remove the last element because shutdown might screw up the results
                if (!totalLoadPerTick.empty())
                    totalLoadPerTick.pop_back();
                for (const auto& info : totalLoadPerTick)
                {
                    fs << info.tickNumber << "\t" << (double)(info.sysTime - stime) / 1000000.0 <<
                        "\t" << (double)(info.userTime - utime) / 1000000.0 << "\n";
                    utime = info.userTime;
                    stime = info.sysTime;
                }
            }
        }
    }
}
