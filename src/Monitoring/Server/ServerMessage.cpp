#include "ServerMessage.h"

namespace Monitor
{
    namespace Server
    {
        const std::vector<std::function<std::shared_ptr<ServerMessage>(const Shared::JSon::JSon&)>> ServerMessage::enumToMessageMap
        {
            AnnounceThreadIdMessage::ParseJSon,
            HookTotalLoadMessage::ParseJSon,
            HookProcessTypeLoadMessage::ParseJSon,
            HookIDLoadMessage::ParseJSon,
            HookIDLoadResMessage::ParseJSon
        };
    }
}
