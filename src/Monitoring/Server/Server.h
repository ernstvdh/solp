#ifndef SOLP_MONITORING_SERVER_SERVER_H
#define SOLP_MONITORING_SERVER_SERVER_H

#include <list>
#include <utility>
#include <unordered_map>

#include "ServerHook.h"
#include "ServerMessage.h"

#include "Shared/Link/Linkable.h"
#include "Shared/System/System.h"

namespace Monitor
{
    namespace Server
    {
        class Server : public Shared::Link::Linkable
        {
        public:
            Server();
            ~Server() {}

            int Run();

        private:
            struct ThreadTimes
            {
                ThreadTimes(uint64_t sys, uint64_t user, uint64_t tick) :
                    sysTime(sys), userTime(user), tickNumber(tick) {}

                uint64_t sysTime;
                uint64_t userTime;
                uint64_t tickNumber;
            };

            struct ThreadInfo
            {
                std::shared_ptr<const Shared::Id> id;
                std::list<ThreadTimes> times;
                std::shared_ptr<std::unordered_map<std::string, std::tuple<std::shared_ptr<const Shared::Id>, std::vector<double>, size_t>>> loadThresholds;
            };

            class MonitorServerHookImpl : public ServerHookInterface
            {
            public:
                MonitorServerHookImpl(Server* creator) : processId(Shared::System::GetProcessId()),
                    utime(0), stime(0), start(0), lastTick(1), owner(creator) {}
                ~MonitorServerHookImpl() { StoreData(); }

                void MonitorTick(int t);
                void AddThreadInfo(uint64_t threadId, uint64_t startTime, const std::shared_ptr<const Shared::Id>& id);
                void HookIDLoad(const std::shared_ptr<const Shared::Id>& toMonitor,
                    const std::vector<double>& thresholds, const std::shared_ptr<const Shared::Id>& toInform, const bool add);
                void HookTotalLoad(const double threshold, const std::shared_ptr<const Shared::Id>& toInform, const bool add);
                void HookProcessTypeLoad(const Shared::Id::ProcessType toMonitor,
                    const double threshold, const std::shared_ptr<const Shared::Id>& toInform, const bool add);

            private:
                void StoreData();
                void HandleIDHooks(ThreadInfo& tInfo, const double diff);
                void HandleTotalLoadHooks();
                void HandleProcessTypeLoad();

                Server* owner;

                const uint64_t processId;

                uint64_t utime;
                uint64_t stime;
                uint64_t start;
                uint64_t lastTick;

                std::list<ThreadTimes> totalLoadPerTick;
                std::unordered_map<uint64_t, std::unordered_map<uint64_t, ThreadInfo>> threadInfo;

                // for the process type load and the total load we store the load in the unordered_map
                // because doing a lookup for each process is cheap
                // for the id we only store who wants to get threshold updates in the map but we add
                // this information to the threadinfo for cheap access
                std::unordered_map<int, std::pair<double,
                    std::unordered_map<std::string, std::tuple<std::shared_ptr<const Shared::Id>, double, bool>>>> processTypeHooks;
                std::pair<double, std::unordered_map<std::string, std::tuple<std::shared_ptr<const Shared::Id>, double, bool>>> totalLoadHooks;
                std::unordered_map<std::string, std::shared_ptr<std::unordered_map<
                    std::string, std::tuple<std::shared_ptr<const Shared::Id>, std::vector<double>, size_t>>>> idHooks;
            };

            void HandleThreadIdMessage(const std::shared_ptr<AnnounceThreadIdMessage>& announce);
            void HandleHookTotalLoadMessage(const std::shared_ptr<HookTotalLoadMessage>& hooktotal);
            void HandleHookProcessTypeLoadMessage(const std::shared_ptr<HookProcessTypeLoadMessage>& hookprocesstype);
            void HandleHookIDLoadMessage(const std::shared_ptr<HookIDLoadMessage>& hookid);

            std::shared_ptr<MonitorServerHookImpl> serverMonitorHook;

        };
    }
}

#endif
