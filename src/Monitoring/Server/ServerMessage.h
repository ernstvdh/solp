#ifndef SOLP_MONITORING_SERVER_SERVERMESSAGE_H
#define SOLP_MONITORING_SERVER_SERVERMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace Monitor
{
    namespace Server
    {
        class ServerMessage : public Shared::Message::Message
        {
        public:
            enum ServerMessageType
            {
                ANNOUNCE_THREAD_ID,
                HOOK_TOTAL_LOAD,
                HOOK_PROCESS_TYPE_LOAD,
                HOOK_ID_LOAD,
                HOOK_ID_LOAD_RES,

                MAX_VALUE
            };

            ServerMessage()
            {
                processType = Shared::Id::ProcessType::SERVER_MONITOR;
            }

            ServerMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(serverMessageType);
            }

            ServerMessageType serverMessageType;

            static std::shared_ptr<ServerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                ServerMessageType smt;
                json.QueryStrict("serverMessageType", smt);

                if (smt >= ServerMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds tracker message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[smt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(serverMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<ServerMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class AnnounceThreadIdMessage : public ServerMessage
        {
        public:
            AnnounceThreadIdMessage()
            {
                serverMessageType = ServerMessageType::ANNOUNCE_THREAD_ID;
            }

            AnnounceThreadIdMessage(const uint64_t thread, const uint64_t start)
            {
                serverMessageType = ServerMessageType::ANNOUNCE_THREAD_ID;
                threadId = thread;
                startTime = start;
            }

            AnnounceThreadIdMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(threadId);
                MESSAGE_GET_FROM_JSON_STRICT(startTime);
            }

            uint64_t threadId;
            uint64_t startTime;

            static std::shared_ptr<AnnounceThreadIdMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<AnnounceThreadIdMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(threadId);
                MESSAGE_ADD_TO_JSON(startTime);
            }
        };

        class HookTotalLoadMessage : public ServerMessage
        {
        public:
            HookTotalLoadMessage()
            {
                serverMessageType = ServerMessageType::HOOK_TOTAL_LOAD;
            }

            HookTotalLoadMessage(const double load, const bool addHook)
            {
                serverMessageType = ServerMessageType::HOOK_TOTAL_LOAD;
                maxTotalLoad = load;
                add = addHook;
            }

            HookTotalLoadMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(maxTotalLoad);
                MESSAGE_GET_FROM_JSON_STRICT(add);
            }

            double maxTotalLoad;
            bool add;

            static std::shared_ptr<HookTotalLoadMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<HookTotalLoadMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(maxTotalLoad);
                MESSAGE_ADD_TO_JSON(add);
            }
        };

        class HookProcessTypeLoadMessage : public ServerMessage
        {
        public:
            HookProcessTypeLoadMessage()
            {
                serverMessageType = ServerMessageType::HOOK_PROCESS_TYPE_LOAD;
            }

            HookProcessTypeLoadMessage(const double load, const Shared::Id::ProcessType pt, const bool addHook)
            {
                serverMessageType = ServerMessageType::HOOK_PROCESS_TYPE_LOAD;
                maxTotalLoad = load;
                processType = pt;
                add = addHook;
            }

            HookProcessTypeLoadMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(maxTotalLoad);
                MESSAGE_GET_FROM_JSON_STRICT(processType);
                MESSAGE_GET_FROM_JSON_STRICT(add);
            }

            double maxTotalLoad;
            Shared::Id::ProcessType processType;
            bool add;

            static std::shared_ptr<HookProcessTypeLoadMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<HookProcessTypeLoadMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(maxTotalLoad);
                MESSAGE_ADD_TO_JSON(processType);
                MESSAGE_ADD_TO_JSON(add);
            }
        };

        class HookIDLoadMessage : public ServerMessage
        {
        public:
            HookIDLoadMessage()
            {
                serverMessageType = ServerMessageType::HOOK_ID_LOAD;
            }

            HookIDLoadMessage(std::vector<double>&& load, const std::shared_ptr<const Shared::Id>& lid, bool addHook)
            {
                serverMessageType = ServerMessageType::HOOK_ID_LOAD;
                loadThresholds = load;
                id = lid;
                add = addHook;
            }

            HookIDLoadMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(loadThresholds);
                MESSAGE_GET_FROM_JSON_STRICT(id);
                MESSAGE_GET_FROM_JSON_STRICT(add);
            }

            std::vector<double> loadThresholds;
            std::shared_ptr<const Shared::Id> id;
            bool add;

            static std::shared_ptr<HookIDLoadMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<HookIDLoadMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(loadThresholds);
                MESSAGE_ADD_TO_JSON(id);
                MESSAGE_ADD_TO_JSON(add);
            }
        };

        class HookIDLoadResMessage : public ServerMessage
        {
        public:
            HookIDLoadResMessage()
            {
                serverMessageType = ServerMessageType::HOOK_ID_LOAD_RES;
            }

            HookIDLoadResMessage(const double load, const std::shared_ptr<const Shared::Id>& lid)
            {
                serverMessageType = ServerMessageType::HOOK_ID_LOAD_RES;
                currentLoad = load;
                id = lid;
            }

            HookIDLoadResMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(currentLoad);
                MESSAGE_GET_FROM_JSON_STRICT(id);
            }

            double currentLoad;
            std::shared_ptr<const Shared::Id> id;

            static std::shared_ptr<HookIDLoadResMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<HookIDLoadResMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(currentLoad);
                MESSAGE_ADD_TO_JSON(id);
            }
        };
    }
}

#endif
