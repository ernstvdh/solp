#ifndef SOLP_MONITORING_SERVER_SERVERID_H
#define SOLP_MONITORING_SERVER_SERVERID_H

#include "Shared/Id.h"

namespace Monitor
{
    namespace Server
    {
        class ServerId : public Shared::IpId
        {
        public:
            ServerId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::SERVER_MONITOR) {}
            ServerId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::SERVER_MONITOR) {}
            ~ServerId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ServerId>(json);
            }
        };
    }
}

#endif
