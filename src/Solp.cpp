#include <thread>
#include <iostream>
#include <memory>
#include "Shared/Debug.h"
#include "Shared/Config.h"
#include "Shared/Singleton.h"
#include "Shared/PingPong/PingPong.h"
#include "Shared/Net/ConnectionMgr.h"
#include "Shared/Link/LinkMgr.h"
#include "Client/Client.h"
#include "Balancing/Server/Server.h"
#include "Balancing/Global/Global.h"
#include "Balancing/Tracker/Tracker.h"
#include "Balancing/Entry/Entry.h"
#include "Processing/Router/Router.h"
#include "Monitoring/Server/Server.h"

#include "Shared/System/System.h"

uint64_t Shared::Singleton::processStartTime{ 0 };
std::atomic_bool Shared::Singleton::shutdown{ false };
std::unique_ptr<Monitor::Server::ServerHook> Shared::Singleton::monitorHooks =
                std::unique_ptr<Monitor::Server::ServerHook>(new Monitor::Server::ServerHook());
std::unique_ptr<Shared::ShutdownHook> Shared::Singleton::shutdownHooks =
                std::unique_ptr<Shared::ShutdownHook>(new Shared::ShutdownHook());
std::unique_ptr<Shared::Link::LinkEventTimed> Shared::Singleton::timedEvents =
    std::unique_ptr<Shared::Link::LinkEventTimed>(new Shared::Link::LinkEventTimed());
std::vector<std::unordered_set<int>> Shared::Singleton::playerRelations = std::vector<std::unordered_set<int>>();

Shared::Net::ConnectionMgr* Shared::Net::ConnectionMgr::singleton =
                new Shared::Net::ConnectionMgr();

Shared::Link::LinkMgr* Shared::Link::LinkMgr::singleton =
                new Shared::Link::LinkMgr();

std::shared_ptr<Shared::PingPong::PingPong> Shared::PingPong::PingPong::singleton =
                std::make_shared<Shared::PingPong::PingPong>();

int StartConnectionMgr()
{
    // the singleton connectionMgr object should be made before
    // this function is called
    int signal = Shared::Net::ConnectionMgr::singleton->Run();

    return signal;
}

int StartPingPong()
{
    srand(time(NULL));
    // the singleton pingPong object should be made before
    // this function is called
    int signal = Shared::PingPong::PingPong::singleton->Run();

    // as soon as ping pong finishes (due to an error or shutdown)
    // we reset it so that the linkable gets deleted
    Shared::PingPong::PingPong::singleton.reset();

    return signal;
}

int StartTracker()
{
    BL::Tracker::Tracker tr;
    return tr.Run();
}

int StartClient(int pId)
{
    Client::Client cl(pId);
    return cl.Run();
}

int StartEntryBalancing()
{
    BL::Entry::Entry eb;
    return eb.Run();
}

int StartRouter(int rId)
{
    Processing::Router::Router rt(rId);
    return rt.Run();
}

int StartGlobalBalancing()
{
    BL::Global::Global gb;
    return gb.Run();
}

int StartServerBalancing()
{
    BL::Server::Server sb;
    return sb.Run();
}

int StartServerMonitor()
{
    Monitor::Server::Server sm;
    return sm.Run();
}

bool InitLocalIP()
{
    Shared::Config::localIP = Shared::PingPong::PingPong::singleton->GetIP();

    if (Shared::Config::localIP == "failure")
        return false;

    return true;
}

int main(int argc, char* argv[])
{
    // flush immediately otherwise we don't get output when using prun
    setvbuf(stdout, NULL, _IONBF, 0);

    if (argc >= 6)
    {
        for (int i = 0; i < argc; i++)
            SOLP_DEBUG(DEBUG_SOLP_COMMAND_LINE_ARG, "Arg #%d is: %s", i, argv[i]);

        Shared::Config::gLBIp = argv[1];
        int numTrackers = std::stoi(argv[2]);

        if (3 + numTrackers + 2 > argc)
        {
            SOLP_ERROR("Wrong number of arguments given to Solp.");
            return 1;
        }

        int numClientMachines = std::stoi(argv[3 + numTrackers]);

        if (3 + numTrackers + 1 + numClientMachines + 1 > argc)
        {
            SOLP_ERROR("Wrong number of arguments given to Solp.");
            return 1;
        }

        int numRouterMachines = std::stoi(argv[3 + numTrackers + 1 + numClientMachines]);

        if (3 + numTrackers + 1 + numClientMachines + 1 + numRouterMachines > argc)
        {
            SOLP_ERROR("Wrong number of arguments given to Solp.");
            return 1;
        }

        // we output the commandline parameters so that we can store it with our other logs
        std::fstream fs;
        fs.open("commandline.arguments.solp.txt", std::fstream::out | std::fstream::trunc);

        fs << "globalIP: " << Shared::Config::gLBIp << "\n";
        fs << "numTrackers: " << numTrackers << "\n";

        fs << "trackerIPs:";
        for (int i = 0; i < numTrackers; i++)
        {
            Shared::Config::trackerIPs.push_back(argv[3 + i]);
            fs << " " << argv[2 + 1 + i];
        }

        fs << "\nclientIPs:";
        for (int i = 0; i < numClientMachines; i++)
        {
            Shared::Config::clientIPs.push_back(argv[3 + numTrackers + 1 + i]);
            fs << " " << argv[3 + numTrackers + 1 + i];
        }

        fs << "\nrouterIPs:";
        for (int i = 0; i < numRouterMachines; i++)
        {
            Shared::Config::routerIPs.push_back(argv[3 + numTrackers + 1 + numClientMachines + 1 + i]);
            fs << " " << argv[3 + numTrackers + 1 + numClientMachines + 1 + i];
        }

        fs.close();
    }
    else
        SOLP_WARNING("didnt receive program parameters, reading ips from config");

    int configLoadResult = Shared::Config::LoadConfig(argc < 6);
    // if error occurred
    if (configLoadResult > 0)
    {
        SOLP_ERROR("Failed to read config... terminating....");
        return 1;
    }

    // store the program start time so that it can be used for
    // identification of logs belonging to a specific run
    uint64_t utime, stime, start;
    if (!Shared::System::GetProcessTimesForCurrent(utime, stime, start))
        return 1;
    Shared::Singleton::processStartTime = start;

    unsigned int n = std::thread::hardware_concurrency();
    std::cout << n << " concurrent threads are supported.\n";

    // we have to init the connection mgr before we use it
    // we have to wait till after the config is loaded so we know
    // how many network threads we have to start
    Shared::Net::ConnectionMgr::singleton->Init();
    std::thread cmgr(StartConnectionMgr);
    std::thread pp(StartPingPong);
    
    // we first utilize the connection mgr to discover our local ip
    if (!InitLocalIP())
        return 1;

    SOLP_DEBUG(DEBUG_SOLP, "Successfully initialized local ip: %s.", Shared::Config::localIP.c_str());

    // if needed load player relation data
    if (Shared::Config::loadPlayerRelations)
    {
        std::ifstream ifs;
        ifs.open(std::to_string(Shared::Config::numClients) + "player.relations.solp.txt", std::ifstream::in);

        if (!ifs.is_open())
        {
            SOLP_ERROR("Could not open player relations file (%d.player.relations.solp.txt).. terminating....", Shared::Config::numClients);
            std::terminate();
        }

        // init the relations vector for the amount of clients
        Shared::Singleton::playerRelations.resize(Shared::Config::numClients);

        while (!ifs.eof())
        {
            int from;
            int to;
            ifs >> from;
            ifs >> to;

            if (!ifs.good())
                break;

            Shared::Singleton::playerRelations[from].insert(to);
        }
    }

    std::thread tracker(StartTracker);

    // we have to wait now till the tracker is ready, because of custom tracker handling...
    //TODO: try to get rid of this special behaviour
    // WARNING! The tracker communicator (LinkMgr) does not use PopMessage and
    // will therefore not be able to deal with a non existing tracker....
    while (!Shared::Link::LinkMgr::singleton->IsTrackerReady())
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // before we start everything else we start the monitor
    std::thread sm(StartServerMonitor);

    if (Shared::Config::clientsOnServer)
        Shared::Config::clientIPs = Shared::Config::trackerIPs;

    int isClientHost = 1;
    for (auto& ip : Shared::Config::clientIPs)
    {
        if (!Shared::Config::clientsOnServer && ip == Shared::Config::gLBIp)
        {
            SOLP_ERROR("Clients are not allowed on the server, but the global balancer has the same ip as the clients... terminating...");
            std::terminate();
        }

        if (ip == Shared::Config::localIP)
            break;
        else
            isClientHost++;
    }
    
    if (isClientHost > Shared::Config::clientIPs.size())
        isClientHost = 0;

    std::shared_ptr<std::thread> entryBL;
    std::shared_ptr<std::thread> glLB;
    std::vector<std::shared_ptr<std::thread>> vRt;
    std::shared_ptr<std::thread> sb = NULL;

    // only if we are not a client host or if we allow clients on server
    // we initialize server specific services
    if (isClientHost == 0 || Shared::Config::clientsOnServer)
    {
        if (Shared::Config::localIP == Shared::Config::gLBIp)
        {
            entryBL = std::make_shared<std::thread>(StartEntryBalancing);
            glLB = std::make_shared<std::thread>(StartGlobalBalancing);
        }

        if (Shared::Config::zonesWithRouter)
        {
            for (int i = 0; i < Shared::Config::routersPerMachine; i++)
                vRt.emplace_back(std::make_shared<std::thread>(std::bind(StartRouter, i)));
            sb = std::make_shared<std::thread>(StartServerBalancing);
        }
        else if (std::any_of(Shared::Config::routerIPs.begin(), Shared::Config::routerIPs.end(),
            [](std::string ip){ return ip == Shared::Config::localIP; }))
        {
            for (int i = 0; i < Shared::Config::routersPerMachine; i++)
                vRt.emplace_back(std::make_shared<std::thread>(std::bind(StartRouter, i)));
        }
        else
        {
            sb = std::make_shared<std::thread>(StartServerBalancing);
        }
    }

    // we start the clients after everything else to give them
    // a chance to be initialized before we potentially start
    // hundreds of clients
    std::vector<std::thread> clients;
    if (isClientHost)
    {
        int start = 0;
        int end = Shared::Config::numClients;

        if (isClientHost != 1)
            start = std::round((static_cast<float>(isClientHost - 1) / static_cast<float>(Shared::Config::clientIPs.size()))
                * Shared::Config::numClients);

        if (isClientHost != Shared::Config::clientIPs.size())
            end = std::round((static_cast<float>(isClientHost) / static_cast<float>(Shared::Config::clientIPs.size()))
                * Shared::Config::numClients);

        SOLP_DEBUG(DEBUG_SOLP, "Starting clients with %d <= id < %d.", start, end);
        for (int i = start; i < end; i++)
            clients.push_back(std::thread(std::bind(StartClient, i)));
    }

    // if the server shutdown time is not zero we will wait
    // till the timer expires after which we will set the shutdown bool
    if (Shared::Config::serverShutdownTimeInSec)
    {
        SOLP_DEBUG(DEBUG_SOLP, "Shutdown time is: %d.", Shared::Config::serverShutdownTimeInSec);
        std::this_thread::sleep_for(std::chrono::seconds(Shared::Config::serverShutdownTimeInSec));
        SOLP_DEBUG(DEBUG_SOLP, "Shutdown timer expired.. shutting down...", Shared::Config::serverShutdownTimeInSec);
        Shared::Singleton::shutdown.store(true);
        Shared::Singleton::shutdownHooks->Shutdown();
    }

    for (auto& client : clients)
        client.join();

    // only if we are not a client host or if we allow clients on server
    // we initialize server specific services
    if (isClientHost == 0 || Shared::Config::clientsOnServer)
    {
        for (auto& rt : vRt)
            rt->join();
        if (sb)
            sb->join();

        if (Shared::Config::localIP == Shared::Config::gLBIp)
        {
            glLB->join();
            entryBL->join();
        }
    }

    sm.join();

    tracker.join();
    pp.join();
    Shared::Net::ConnectionMgr::singleton->Stop();
    cmgr.join();

    Shared::PingPong::PingPong::singleton.reset();
    delete Shared::Link::LinkMgr::singleton;
    delete Shared::Net::ConnectionMgr::singleton;

    return 0;
}
