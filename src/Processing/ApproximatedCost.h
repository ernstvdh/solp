#ifndef SOLP_PROCESSING_APPROXIMATEDCOST_H
#define SOLP_PROCESSING_APPROXIMATEDCOST_H

namespace Processing
{
    namespace ApproximatedCost
    {
        void ZoneMovementHandler();
        void SpellHandler();
        void SpellVisionCheck();
    }
}

#endif
