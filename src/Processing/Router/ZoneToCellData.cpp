#include "ZoneToCellData.h"
#include "CellData.h"
#include "Router.h"

#include "Shared/Util.h"


namespace Processing
{
    namespace Router
    {
        ZoneToCellData::ZoneToCellData(Router* creator, CellData* cd) : CellDataHook(cd), owner(creator), inAfterCleanup(false) {}

        void ZoneToCellData::AddOwnClient(const int playerId, const int zoneX, const int zoneY, const int zonePhase)
        {
            ownedClients[playerId] = std::unique_ptr<std::tuple<int, int, int>>(new std::tuple<int, int, int>(zoneX, zoneY, zonePhase));
        }

        std::unique_ptr<std::tuple<int, int, int>>* ZoneToCellData::GetOwnedData(const int playerId)
        {
            auto owned = ownedClients.find(playerId);
            if (owned != ownedClients.end())
                return &owned->second;
            else
                return NULL;
        }

        void ZoneToCellData::EraseEntity(const int zoneX, const int zoneY, const int zonePhase, const int playerId, bool zoneTransfer)
        {
            SOLP_DEBUG(DEBUG_ZONE_TO_CELL_DATA, "Erase entity (%d) from zone (%d, %d).", playerId, zoneX, zoneY);

            // if owned we only try a zone change is zoneTransfer is true
            // otherwise we just erase from the cell and be done
            std::unique_ptr<std::tuple<int, int, int>>* owned = GetOwnedData(playerId);
            if (owned && zoneTransfer)
                owned->release();

            // at this point we might already have deleted this entity
            // it could also be that a state change from this zone
            // to another is queued
            // we also know that within the zone we have to erase
            // the entity from this is really the last state we should know
            // of, if the entity moved back into range we will
            // get a new state update anyway

            // either a state for this entity for this zone is queued in which
            // case we delete this unused state
            // or the cell MIGHT have a state for this entity in the sending zone
            // in which case we just forward to erase to the celldata
            auto entityQueue = entityQueuedForZone.find(playerId);
            if (entityQueue != entityQueuedForZone.end())
            {
                auto size = entityQueue->second.size();
                entityQueue->second.erase(std::remove_if(entityQueue->second.begin(), entityQueue->second.end(),
                    [zoneX, zoneY, zonePhase](std::tuple<int, int, int, Processing::Zone::EntityInfo, int, int, bool>& entity)
                {
                    if (zoneX == std::get<0>(entity) && zoneY == std::get<1>(entity) && zonePhase == std::get<2>(entity))
                        return true;

                    return false;
                }), entityQueue->second.end());

                if (entityQueue->second.size() < size)
                {
                    // if a state was deleted we do cleanup
                    // if size 0 we can delete this vector from the unordered_map
                    if (size == 1)
                        entityQueuedForZone.erase(playerId);

                    return;
                }
                else if (!entityQueue->second.empty())
                {
                    if (!owned || (owned && zoneTransfer))
                    {
                        // we try to move the entity instead of deleting it if we have a queued state
                        auto newState = entityQueue->second[0];
                        // delete this state, we only use it ones
                        entityQueue->second.erase(entityQueue->second.begin());
                        // cleanup
                        if (entityQueue->second.empty())
                            entityQueuedForZone.erase(playerId);

                        // if owned we have to set the state to the new zone
                        if (owned)
                            owned->reset(new std::tuple<int, int, int>(std::get<0>(newState), std::get<1>(newState), std::get<2>(newState)));

                        if (std::get<6>(newState))
                        {
                            // if beacon we add the client beacon to the cell state
                            // we still erase the entity because it really is not anymore
                            // in one of our cells
                            cellData->ClientBeacon(std::get<4>(newState), std::get<5>(newState), playerId);
                        }
                        else
                        {
                            // move entity instead of erase, we return after this call to avoid the erase
                            cellData->UpdateCellData(std::get<4>(newState), std::get<5>(newState),
                                playerId, std::move(std::get<3>(newState)), std::get<2>(newState));

                            return;
                        }
                    }
                }

                // otherwise we try to erase the entity in the cell data
            }

            cellData->EraseEntity(playerId);
        }

        void ZoneToCellData::UpdateCellData(const int zoneX, const int zoneY, const int zonePhase,
            const int playerId, Processing::Zone::EntityInfo&& newData, bool beacon)
        {
            SOLP_DEBUG(DEBUG_ZONE_TO_CELL_DATA, "Update entity (%d) data from zone (%d, %d, %d).", playerId, zoneX, zoneY, zonePhase);
            const std::pair<int, int> cellCoords(Shared::Util::PositionToCellCoords(newData.pos));

            // if we are interested we have to check if this is not a data update
            // sent while we deleted our state, after which we again became interested
            // in this cell again, after which we receive this message that
            // we should therefor drop (because we wait for the initial state update)
            if (IsUninitializedCellForZone(cellCoords.first, cellCoords.second, zoneX, zoneY, zonePhase))
                return;

            // if owned by this cell we have special behaviour
            std::unique_ptr<std::tuple<int, int, int>>* oldZone = GetOwnedData(playerId);
            // beacons are only relevant if the player is owned by this router
            if (!oldZone && beacon)
                return;
            // if already exists we need to check what zone the entity is currently in
            // if the entity is in the same zone as the new state we can just continue
            // otherwise we need to store the state till the entity is not anymore
            // in the previous zone
            auto entityInfo = cellData->GetEntityInfo(playerId);
            if ((oldZone && oldZone->get()) || entityInfo)
            {
                // If we receive a message from the zone we are currently in then we
                // know that this message is sent before we are possibly moved to a
                // different zone. In this case we can just push the message
                // to the cellData.
                // If the message is sent by a different zone we need to queue it.
                // the coordinates are not the same so special behaviour
                // otherwise just continue with the cell update

                std::tuple<int, int, int> oldZoneCoords;

                if (!oldZone || !oldZone->get())
                {
                    std::pair<int, int> zoneXY = Shared::Util::PositionToZoneCoords(entityInfo->ef.pos);
                    std::get<0>(oldZoneCoords) = zoneXY.first;
                    std::get<1>(oldZoneCoords) = zoneXY.second;
                    std::get<2>(oldZoneCoords) = entityInfo->zonePhase;
                }
                else
                    oldZoneCoords = *oldZone->get();
                
                // the coordinates are not the same so special behaviour
                // otherwise just continue with the cell update
                if (std::get<0>(oldZoneCoords) != zoneX || std::get<1>(oldZoneCoords) != zoneY || std::get<2>(oldZoneCoords) != zonePhase)
                {
                    for (auto& queued : entityQueuedForZone[playerId])
                    {
                        if (std::get<0>(queued) == zoneX && std::get<1>(queued) == zoneY && std::get<2>(queued) == zonePhase)
                        {
                            std::get<3>(queued) = std::move(newData);
                            std::get<4>(queued) = cellCoords.first;
                            std::get<5>(queued) = cellCoords.second;
                            std::get<6>(queued) = beacon;
                            return;
                        }
                    }

                    entityQueuedForZone[playerId].emplace_back(
                        zoneX, zoneY, zonePhase, std::move(newData), cellCoords.first, cellCoords.second, beacon);
                    return;
                }
            }

            // if we actually place this owned entity in a new zone we change its state
            if (oldZone && (!oldZone->get() || (std::get<0>(*oldZone->get()) != zoneX || std::get<1>(*oldZone->get()) != zoneY || std::get<0>(*oldZone->get()) != zonePhase)))
                oldZone->reset(new std::tuple<int, int, int>(zoneX, zoneY, zonePhase));

            if (beacon)
                cellData->ClientBeacon(cellCoords.first, cellCoords.second, playerId);
            else
                cellData->UpdateCellData(cellCoords.first, cellCoords.second, playerId, std::move(newData), zonePhase);
        }

        void ZoneToCellData::AddCellData(const int cellX, const int cellY, const int zoneX, const int zoneY,
            const int zonePhase, const int numZones, const uint64_t zoneInterestUId, std::unordered_map<int, Processing::Zone::EntityInfo>&& toAdd)
        {
            // does nothing if we lost interest in this cell in the meantime
            InitializeCellForZone(cellX, cellY, zoneX, zoneY, zonePhase, numZones, zoneInterestUId);

            // if this is an old intialization message that has arrived after reinitialization of this cell
            // then we know another initialization request for this cell/zone is still open, in this case return
            if (IsUninitializedCellForZone(cellX, cellY, zoneX, zoneY, zonePhase))
                return;

            for (auto entity : toAdd)
                UpdateCellData(zoneX, zoneY, zonePhase, entity.first, std::move(entity.second), false);
        }

        bool ZoneToCellData::IsUninitializedCellForZone(const int cellX, const int cellY, const int zoneX, const int zoneY, const int zonePhase)
        {
            const auto& xCell = cellZonesState.find(cellX);
            // not found means this zone is not yet initialized
            if (xCell == cellZonesState.end())
                return true;

            const auto& yCell = xCell->second.find(cellY);
            // if not found we are finished
            if (yCell == xCell->second.end())
                return true;

            // now we can check if this particular zone in this cell is initialized
            for (const auto& zone : yCell->second)
            {
                if (std::get<0>(zone) == zoneX && std::get<1>(zone) == zoneY)
                {
                    // if we don't have state for this particular phase (if no phase server
                    // has responded yet with the amount of phases) then we can return true
                    if (std::get<2>(zone).size() == 0)
                        return true;

                    // else we return the actual state of this phase
                    // (return the opposite of the actual state because we want to know if the cell is UNinitialized)
                    return !std::get<2>(zone)[zonePhase];
                }
            }

            // if we are here we know that this particular zone has not been found
            // in the current state we have for this cell, meaning that this zone
            // is not initialized
            return true;
        }

        void ZoneToCellData::InitializeCellForZone(const int cellX, const int cellY, const int zoneX, const int zoneY,
            const int zonePhase, const int numZones, const uint64_t zoneInterestUId)
        {
            const auto& xCell = cellZonesState.find(cellX);
            if (xCell != cellZonesState.end())
            {
                const auto& yCell = xCell->second.find(cellY);
                if (yCell != xCell->second.end())
                {
                    for (auto zone = yCell->second.begin(); zone != yCell->second.end(); zone++)
                    {
                        if (std::get<0>(*zone) == zoneX && std::get<1>(*zone) == zoneY && std::get<3>(*zone) == zoneInterestUId)
                        {
                            // if needs be resize the number of phases for this particular zone
                            if (std::get<2>(*zone).size() < numZones)
                            {
                                // we also need to connect to all the newly known phases

                                for (int i = std::get<2>(*zone).size(); i < numZones; i++)
                                {
                                    std::shared_ptr<const Processing::Zone::ZoneId> zoneId(std::make_shared<const Processing::Zone::ZoneId>(zoneX, zoneY, i));

                                    // we can have multiple links open to this particular zones
                                    // link is removed in LostInterestInCellBeforeCleanup
                                    owner->LinkTo(zoneId);
                                    auto connect = Shared::Message::Message::MakeMessage<Processing::Zone::RouterConnectMessage>(owner->GetID(), zoneId);
                                    connect->cellX = cellX;
                                    connect->cellY = cellY;
                                    connect->interestId = zoneInterestUId;
                                    owner->SendMessagePersistent(connect);
                                }

                                std::get<2>(*zone).resize(numZones, false);
                            }

                            std::get<2>(*zone)[zonePhase] = true;
                        }
                    }
                }
            }
        }

        void ZoneToCellData::InterestedInCell(int x, int y)
        {
            const std::unordered_map<int, std::unordered_map<int, bool>> zones(Shared::Util::ZonesCoveredByCell(x, y));

            for (auto& zonex : zones)
            {
                for (auto& zoney : zonex.second)
                {
                    zoneInterestUniqueId++;

                    // we require an initialization from all zones handling this cell
                    // status updates sent by zones that have not yet been initialized
                    // will be dropped, because they might still be arriving from a
                    // previous moment we were connected
                    cellZonesState[x][y].emplace_back(zonex.first, zoney.first, std::vector<bool>(1, false), zoneInterestUniqueId);

                    std::shared_ptr<const Processing::Zone::ZoneId> zoneId(std::make_shared<const Processing::Zone::ZoneId>(zonex.first, zoney.first, 0));

                    // we can have multiple links open to this particular zones
                    // link is removed in LostInterestInCellBeforeCleanup
                    owner->LinkTo(zoneId);
                    auto connect = Shared::Message::Message::MakeMessage<Processing::Zone::RouterConnectMessage>(owner->GetID(), zoneId);
                    connect->cellX = x;
                    connect->cellY = y;
                    connect->interestId = zoneInterestUniqueId;
                    owner->SendMessagePersistent(connect);
                }
            }
        }

        void ZoneToCellData::LostInterestInCellBeforeCleanup(int x, int y)
        {
            // disconnect from zones handling this particular cell
            for (auto& zone : cellZonesState[x][y])
            {
                for (int i = 0; i < std::get<2>(zone).size(); i++)
                {
                    std::shared_ptr<const Processing::Zone::ZoneId> zoneId(std::make_shared<const Processing::Zone::ZoneId>(std::get<0>(zone), std::get<1>(zone), i));

                    auto disconnect = Shared::Message::Message::MakeMessage<Processing::Zone::RouterDisconnectMessage>(owner->GetID(), zoneId);
                    disconnect->cellX = x;
                    disconnect->cellY = y;
                    owner->SendMessagePersistent(disconnect);

                    // remove the link we added in InterestedInCell
                    owner->LinkToRemove(zoneId);
                }
            }

            // remove cell zone state, we do not need to wait for this information to be "complete"
            // because we use a uniqueID so we can differentiate new data based on that ID
            cellZonesState[x].erase(y);
            if (cellZonesState[x].empty())
                cellZonesState.erase(x);

            // cleanup the queued entity states
            // all new states in the cell we are deleting now
            // are invalidated
            // beacons can also safely be deleted because
            // we were appareantly interested in this cell
            // so we should not have any beacons for it....
            //TODO: make sure this assumption about beacons is correct
            std::for_each(entityQueuedForZone.begin(), entityQueuedForZone.end(),
                [x, y](std::pair<const int, std::vector<std::tuple<int, int, int, Processing::Zone::EntityInfo, int, int, bool>>>& vector)
            {
                vector.second.erase(std::remove_if(vector.second.begin(), vector.second.end(),
                    [x, y](const std::tuple<int, int, int, Processing::Zone::EntityInfo, int, int, bool>& tuple)
                {
                    if (x == std::get<4>(tuple) && y == std::get<5>(tuple))
                        return true;

                    return false;
                }), vector.second.end());
            });
            Shared::Util::map_erase_if(entityQueuedForZone,
                [x, y](const std::pair<int, std::vector<std::tuple<int, int, int, Processing::Zone::EntityInfo, int, int, bool>>>& vector)
            {
                return vector.second.empty();
            });

            // store what entities just got removed from celldata
            auto& cell = *cellData->GetCellData(x, y);
            for (const auto& entity : cell)
                moveAfterCleanup.push(entity.first);
        }

        void ZoneToCellData::LostInterestInCellAfterCleanup(int x, int y)
        {
            // avoid getting a very big callstack because lost interest
            // can happen from this method as well because we are moving
            // entities (possibly owned by us)
            if (inAfterCleanup)
                return;

            inAfterCleanup = true;

            while (!moveAfterCleanup.empty())
            {
                int playerId = moveAfterCleanup.front();
                moveAfterCleanup.pop();

                auto findEntity = entityQueuedForZone.find(playerId);
                if (findEntity == entityQueuedForZone.end())
                    continue;

                // if not found it cannot be an empty vector
                // we switch state to the first state in the queue
                auto newState = findEntity->second[0];
                // delete this state, we only use it ones
                findEntity->second.erase(findEntity->second.begin());
                // cleanup
                if (findEntity->second.empty())
                    entityQueuedForZone.erase(playerId);

                if (std::get<5>(newState))
                {
                    // if beacon we add the client beacon to the cell state
                    cellData->ClientBeacon(std::get<4>(newState), std::get<5>(newState), playerId);
                }
                else
                {
                    // move entity, set onDelete flag so that the old cell does not get modified
                    cellData->UpdateCellData(std::get<4>(newState), std::get<5>(newState),
                        playerId, std::move(std::get<3>(newState)), std::get<2>(newState));
                }
            }

            inAfterCleanup = false;
        }
    }
}
