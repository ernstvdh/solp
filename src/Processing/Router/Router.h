#ifndef SOLP_PROCESSING_ROUTER_ROUTER_H
#define SOLP_PROCESSING_ROUTER_ROUTER_H

#include "CellData.h"
#include "ZoneToCellData.h"
#include "Shared/Link/Linkable.h"
#include "Monitoring/Server/ServerHook.h"
#include "RouterMessage.h"
#include "Client/ClientId.h"
#include "Processing/Zone/ZoneMessage.h"
#include "Processing/Zone/ZoneId.h"
#include <map>
#include <list>
#include <queue>
#include <random>

namespace Processing
{
    namespace Router
    {
        struct PlayerData
        {
            PlayerData();

            std::shared_ptr<const Client::ClientId> id;
            Processing::Zone::EntityInfo pInfo;
            // handling of these messages is not yet confirmed by the zones the router
            // talks to, 
            std::deque<std::shared_ptr<Shared::Message::Message>> messagesWithoutConfirm;
            uint64_t lastConfirmedMessage = 0;
            std::tuple<int, int, int> currentZone;

            const std::unique_ptr<IMPlayerData> imdata;
        };

        class Router : public Shared::Link::Linkable
        {
        public:
            Router(int id);
            ~Router();

            int Run();

        private:
            void AnnounceSelfToSelector();
            void HandleConnectToRouterMessage(std::shared_ptr<ConnectToRouterMessage>& connect);
            void HandleAddPlayerResponseMessage(std::shared_ptr<Processing::Zone::AddPlayerResponseMessage>& zoneaddresponse);
            void HandleGetPlayerZoneMessage(std::shared_ptr<GetPlayerZoneMessage>& getplayerzone);
            void HandleZoneCellStateInitMessage(std::shared_ptr<Processing::Zone::ZoneCellStateInitMessage>& initplayer);
            void HandlePlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate);
            void HandleActionStartMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart);
            void HandleActionStartClientMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart);
            void HandleActionStartZoneMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart);
            void HandleActionFireMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire);
            void HandleActionFireClientMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire);
            void HandleActionFireZoneMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire);
            void HandleActionHitMessage(std::shared_ptr<Processing::Zone::ActionHitMessage>& actionhit);
            void HandleErasePlayerMessage(std::shared_ptr<Processing::Zone::ErasePlayerMessage>& eraseplayer);
            void HandleEntityBeaconMessage(std::shared_ptr<Processing::Zone::EntityBeaconMessage>& beacon);

            void HandleClientPlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate);
            void HandleZonePlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate);
            void HandleZoneEntityTransferMessage(std::shared_ptr<Processing::Zone::EntityZoneTransferPublicMessage>& entitytransfer);

            void SendPlayerUpdateMessage(int playerId, const Processing::Zone::EntityInfo& pInfo, const uint64_t counter);
            void SendPlayerActionMessage(int playerId, const Processing::Zone::Action& action, const uint64_t counter);
            void SendPlayerActionFireMessage(int playerId, const Processing::Zone::Action& action, const uint64_t counter);
            void SendPlayerClaimMessage(int playerId, const glm::vec2& pos, const int zonePhase);

            void LastMessageConfirmedForPlayer(int playerId, const uint64_t lastMessageHandled, PlayerData* pd = NULL);

            std::unordered_map<int, PlayerData*> connectedClients;
            std::unique_ptr<CellData> cellData;
            std::unique_ptr<ZoneToCellData> zoneToCellData;

            std::default_random_engine engine;

            // Monitoring
            class RouterServerHookImpl : public Monitor::Server::ServerHookInterface
            {
            public:
                RouterServerHookImpl(const int lId) :
                    connectedClients(0), receivedMessages(0), id(lId) {}
                ~RouterServerHookImpl() { StoreData(); }

                void MonitorTick(int t);

                std::atomic<uint64_t> connectedClients;
                std::atomic<uint64_t> receivedMessages;
            private:
                void StoreData();

                const int id;
                std::list<std::tuple<int, uint64_t, uint64_t>> tickInfo;
            };

            std::shared_ptr<RouterServerHookImpl> monitorHook;
        };
    }
}

#endif
