#ifndef SOLP_PROCESSING_ROUTER_ROUTERMESSAGE_H
#define SOLP_PROCESSING_ROUTER_ROUTERMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

#include "Processing/Zone/ZoneId.h"

namespace Processing
{
    namespace Router
    {
        class RouterMessage : public Shared::Message::Message
        {
        public:
            enum RouterMessageType
            {
                CONNECT,
                GET_PLAYER_ZONE,
                REPLY_PLAYER_ZONE,

                MAX_VALUE
            };

            RouterMessage()
            {
                processType = Shared::Id::ProcessType::ROUTER;
            }

            RouterMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(routerMessageType);
            }

            RouterMessageType routerMessageType;

            static std::shared_ptr<RouterMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                RouterMessageType rmt;
                json.QueryStrict("routerMessageType", rmt);

                if (rmt >= RouterMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds router message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[rmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(routerMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<RouterMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };


        class ConnectToRouterMessage : public RouterMessage
        {
        public:
            ConnectToRouterMessage()
            {
                routerMessageType = RouterMessageType::CONNECT;
                requestPosition = false;
            }

            ConnectToRouterMessage(const glm::vec2& pos)
            {
                routerMessageType = RouterMessageType::CONNECT;
                requestPosition = true;
                position = pos;
            }

            ConnectToRouterMessage(const Shared::JSon::JSon& json) : RouterMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(requestPosition);
                MESSAGE_GET_FROM_JSON_STRICT(position);
            }

            bool requestPosition;
            glm::vec2 position;

            static std::shared_ptr<ConnectToRouterMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ConnectToRouterMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                RouterMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(requestPosition);
                MESSAGE_ADD_TO_JSON(position);
            }
        };

        class GetPlayerZoneMessage : public RouterMessage
        {
        public:
            GetPlayerZoneMessage()
            {
                routerMessageType = RouterMessageType::GET_PLAYER_ZONE;
            }

            GetPlayerZoneMessage(const int playerId)
            {
                routerMessageType = RouterMessageType::GET_PLAYER_ZONE;
                player = playerId;
            }

            GetPlayerZoneMessage(const Shared::JSon::JSon& json) : RouterMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(player);
            }

            int player;

            static std::shared_ptr<GetPlayerZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<GetPlayerZoneMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                RouterMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(player);
            }
        };

        class ReplyPlayerZoneMessage : public RouterMessage
        {
        public:
            ReplyPlayerZoneMessage()
            {
                routerMessageType = RouterMessageType::REPLY_PLAYER_ZONE;
                found = false;
            }

            ReplyPlayerZoneMessage(const int playerId, const bool foundPlayer, const int x, const int y, const int phase)
            {
                routerMessageType = RouterMessageType::REPLY_PLAYER_ZONE;
                player = playerId;
                found = foundPlayer;
                zoneX = x;
                zoneY = y;
                zonePhase = phase;
            }

            ReplyPlayerZoneMessage(const Shared::JSon::JSon& json) : RouterMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(player);
                MESSAGE_GET_FROM_JSON_STRICT(found);
                MESSAGE_GET_FROM_JSON_STRICT(zoneX);
                MESSAGE_GET_FROM_JSON_STRICT(zoneY);
                MESSAGE_GET_FROM_JSON_STRICT(zonePhase);
            }

            int player;
            bool found;
            int zoneX;
            int zoneY;
            int zonePhase;

            static std::shared_ptr<ReplyPlayerZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ReplyPlayerZoneMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                RouterMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(player);
                MESSAGE_ADD_TO_JSON(found);
                MESSAGE_ADD_TO_JSON(zoneX);
                MESSAGE_ADD_TO_JSON(zoneY);
                MESSAGE_ADD_TO_JSON(zonePhase);
            }
        };
    }
}

#endif
