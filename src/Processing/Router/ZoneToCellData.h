#ifndef SOLP_PROCESSING_ROUTER_ZONETOCELLDATA_H
#define SOLP_PROCESSING_ROUTER_ZONETOCELLDATA_H

#include <memory>
#include <queue>
#include <unordered_map>

#include "CellData.h"

namespace Client
{
    class ClientId;
}

namespace Processing
{
    namespace Router
    {
        class ZoneToCellData : public CellDataHook
        {
        public:
            ZoneToCellData(Router* creator, CellData* cd);

            void AddOwnClient(const int playerId, const int zoneX, const int zoneY, const int zonePhase);

            void EraseEntity(const int zoneX, const int zoneY, const int zonePhase, const int playerId, bool zoneTransfer);
            void UpdateCellData(const int zoneX, const int zoneY, const int zonePhase,
                const int playerId, Processing::Zone::EntityInfo&& newData, bool beacon);

            void AddCellData(const int cellX, const int cellY, const int zoneX, const int zoneY,
                const int zonePhase, const int numZones, const uint64_t zoneInterestUId,
                std::unordered_map<int, Processing::Zone::EntityInfo>&& toAdd);

            void InterestedInCell(int x, int y) override;
            void LostInterestInCellBeforeCleanup(int x, int y) override;
            void LostInterestInCellAfterCleanup(int x, int y) override;

        private:
            bool IsUninitializedCellForZone(const int cellX, const int cellY, const int zoneX, const int zoneY, const int zonePhase);
            void InitializeCellForZone(const int cellX, const int cellY, const int zoneX, const int zoneY,
                const int zonePhase, const int numZones, const uint64_t zoneInterestUId);

            std::unique_ptr<std::tuple<int, int, int>>* GetOwnedData(const int playerId);

            Router* const owner;

            std::unordered_map<int, std::unordered_map<int, std::vector<std::tuple<int, int, std::vector<bool>, uint64_t>>>> cellZonesState;
            uint64_t zoneInterestUniqueId = 0;

            // queued states updates data
            std::unordered_map<int, std::unique_ptr<std::tuple<int, int, int>>> ownedClients;
            std::unordered_map<int, std::vector<std::tuple<int, int, int, Processing::Zone::EntityInfo, int, int, bool>>> entityQueuedForZone;
            std::queue<int> moveAfterCleanup;
            bool inAfterCleanup;
        };
    }
}

#endif
