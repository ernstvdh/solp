#ifndef SOLP_PROCESSING_ROUTER_ROUTERID_H
#define SOLP_PROCESSING_ROUTER_ROUTERID_H

#include "Shared/Id.h"

namespace Processing
{
    namespace Router
    {
        class RouterId : public Shared::IpId
        {
        public:
            RouterId(const int lId, const std::string& lIp) :
                Shared::IpId(lIp, Shared::Id::ProcessType::ROUTER), id(lId) {}
            RouterId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::ROUTER)
            {
                GET_FROM_JSON_STRICT(id);
            }
            ~RouterId() {}

            int id;

            std::string Serialize() const
            {
                //return Shared::IpId::Serialize() ;
                return std::to_string(type) + "_" + std::to_string(id) + "_" + ip;
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::IpId::SerializeToJSon(json);
                ADD_TO_JSON(id);
            }

            bool equals(const std::shared_ptr<const Id>& that) const
            {
                if (type != that->type)
                    return false;

                std::shared_ptr<const RouterId> otherId(std::static_pointer_cast<const RouterId>(that));

                if (id != otherId->id)
                    return false;

                return true;
            }

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RouterId>(json);
            }
        };
    }
}

#endif
