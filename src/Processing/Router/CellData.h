#ifndef SOLP_PROCESSING_ROUTER_CELLDATA_H
#define SOLP_PROCESSING_ROUTER_CELLDATA_H

#include <functional>
#include <list>
#include <vector>
#include <unordered_map>

#include "Processing/Zone/EntityInfo.h"

namespace Client
{
    class ClientId;
}

namespace Shared
{
    namespace Message
    {
        class Message;
    }
}

namespace Processing
{
    namespace Router
    {
        class Router;
        struct PlayerData;
        class CellDataHook;

        struct IMPlayerData
        {
        public:
            IMPlayerData(PlayerData* const playerData) : pd(playerData), beacon(false), init(false), zonePhase(0) {}

            glm::vec2& GetPosition() const;
            Processing::Zone::EntityInfo& GetEntityInfo() const;
            std::shared_ptr<const Client::ClientId>& GetClientId() const;
            uint64_t GetLastMessageHandled() const;

            bool beacon;
            int zonePhase;
            std::pair<int, int> beaconCellCoords;
            std::unordered_map<int, std::unordered_map<int, bool>> cells;

            bool init;

        private:
            PlayerData* pd;
        };

        struct IMEntityInfo
        {
        public:
            Processing::Zone::EntityInfo ef;
            int zonePhase;
        };

        class CellData
        {
        public:
            CellData(Router* creator) : owner(creator), hook(NULL) {}

            void AddOwnClient(IMPlayerData* pd);
            void ClientBeacon(const int cellX, const int cellY, const int playerId);

            void EraseEntity(const int playerId);
            void UpdateCellData(const int cellX, const int cellY, const int playerId, Processing::Zone::EntityInfo&& newData, const int zonePhase);

            // this method alters the sender and receiver (but nothing else!)
            // always provide this method with a copy of the message that needs to be forwarded
            void ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker, const glm::vec2& pos);
            void ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker,
                const glm::vec2& pos1, const glm::vec2& pos2);

            void Hook(CellDataHook* hk) { hook = hk; }

            std::unordered_map<int, IMEntityInfo>* GetCellData(int x, int y);
            std::list<IMPlayerData*>* ClientsInCell(int x, int y);
            IMEntityInfo* GetEntityInfo(const int playerId);

        private:
            void MoveClient(IMPlayerData* pd, const glm::vec2& oldPos, const glm::vec2& newPos, const int oldZonePhase, const int newZonePhase);
            void UpdateClientVisibleAfterMovement(IMPlayerData* pd, const glm::vec2& oldPos, const glm::vec2& newPos);

            void IncInterestInCell(int x, int y, IMPlayerData* pd);
            void DecrInterestInCell(int x, int y, IMPlayerData* pd);

            void EraseCellData(const int cellX, const int cellY);

            Router* const owner;
            CellDataHook* hook;

            std::unordered_map<int, IMPlayerData*> ownedClients;
            std::unordered_map<int, std::unordered_map<int, std::list<IMPlayerData*>>> clientLocation;
            std::unordered_map<int, std::unordered_map<int, uint64_t>> clientBeaconsPerCell;

            std::unordered_map<int, std::pair<int, int>> entityToCell;
            std::unordered_map<int, std::unordered_map<int, std::unordered_map<int, IMEntityInfo>>> cellInfo;
        };

        class CellDataHook
        {
        public:
            virtual void InterestedInCell(int x, int y) {}
            virtual void LostInterestInCellBeforeCleanup(int x, int y) {}
            virtual void LostInterestInCellAfterCleanup(int x, int y) {}

        protected:
            CellDataHook(CellData* cd) : cellData(cd) { cellData->Hook(this); }

            CellData* cellData;
        };
    }
}

#endif
