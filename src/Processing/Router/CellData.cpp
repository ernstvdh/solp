#include <set>

#include "CellData.h"
#include "Router.h"

#include "Shared/Util.h"
#include "Client/ClientMessage.h"

namespace Processing
{
    namespace Router
    {
        glm::vec2& IMPlayerData::GetPosition() const { return pd->pInfo.pos; }
        Processing::Zone::EntityInfo& IMPlayerData::GetEntityInfo() const { return pd->pInfo; }
        std::shared_ptr<const Client::ClientId>& IMPlayerData::GetClientId() const { return pd->id; }
        uint64_t IMPlayerData::GetLastMessageHandled() const { return pd->lastConfirmedMessage; }

        void CellData::AddOwnClient(IMPlayerData* pd)
        {
            // send the client its position before we send it the state of other entities
            // if we do not do this the client will drop state for all entities till it receives
            // its first position from the zone -> router -> client
            //TODO: make this cleaner.... the interest management should handle this issues
            // (that the client does not know its position at the start but receiving updates for other entities)
            // Send message back to client to inform the client of its starting position
            std::shared_ptr<ConnectToRouterMessage> conReply(
                Shared::Message::Message::MakeMessage<Processing::Router::ConnectToRouterMessage>(owner->GetID(), pd->GetClientId()));
            conReply->position = pd->GetPosition();
            owner->SendMessagePersistent(conReply);

            ownedClients[pd->GetClientId()->id] = pd;
            pd->cells = Shared::Util::CellCoordsInRange(pd->GetPosition(), Shared::Config::visionRadius + Shared::Config::cellPreloadDistance);

            for (auto& cellx : pd->cells)
            {
                for (auto& celly : cellx.second)
                {
                    IncInterestInCell(cellx.first, celly.first, pd);

                    // send the newly joined player all the states contained in this cell
                    for (const auto& entity : cellInfo[cellx.first][celly.first])
                    {
                        // only send state if in vision
                        if (Shared::Util::CalculateDistance(pd->GetPosition(), entity.second.ef.pos) > Shared::Config::visionRadius)
                            continue;

                        std::shared_ptr<Client::UpdatePlayerStateMessage> mess(
                            std::make_shared<Client::UpdatePlayerStateMessage>(entity.first, entity.second.ef, pd->GetLastMessageHandled(), entity.second.zonePhase));
                        mess->senderID = owner->GetID();
                        mess->receiverID = pd->GetClientId();
                        owner->SendMessagePersistent(mess);
                    }
                }
            }
        }

        void CellData::ClientBeacon(const int cellX, const int cellY, const int playerId)
        {
            auto ownClient = ownedClients.find(playerId);
            if (ownClient == ownedClients.end())
                return;

            // if already beacon in other cell we remove the old beacon
            if (ownClient->second->beacon)
                DecrInterestInCell(ownClient->second->beaconCellCoords.first, ownClient->second->beaconCellCoords.second, NULL);

            ownClient->second->beacon = true;
            ownClient->second->beaconCellCoords = std::pair<int, int>(cellX, cellY);
            IncInterestInCell(cellX, cellY, NULL);
        }

        void CellData::MoveClient(IMPlayerData* pd, const glm::vec2& oldPos, const glm::vec2& newPos, const int oldZonePhase, const int newZonePhase)
        {
            std::unordered_map<int, std::unordered_map<int, bool>> newCells(
                Shared::Util::CellCoordsInRange(newPos, Shared::Config::visionRadius + Shared::Config::cellPreloadDistance));

            pd->zonePhase = newZonePhase;

            Shared::Util::Diff2DMap(pd->cells, newCells);
            for (auto& cellx : pd->cells)
            {
                for (auto& celly : cellx.second)
                {
                    // if the bool is false then this cell was not present in the oldMap
                    // in this case we have to add it to our cells of interest set
                    if (!celly.second)
                    {
                        IncInterestInCell(cellx.first, celly.first, pd);
                    }
                    else
                    {
                        // otherwise the cell used to be in our interest cell but it is not any more
                        // if it is in our extended interest range (to prevent excessive reconnects)
                        // we add it to our new interest set, otherwise we drop it (decr the interest in cell)
                        if (Shared::Util::CellInRange(newPos,
                            Shared::Config::visionRadius + Shared::Config::cellUnloadDistance,
                            cellx.first, celly.first))
                            newCells[cellx.first][celly.first] = true;
                        else
                            DecrInterestInCell(cellx.first, celly.first, pd);
                    }
                }
            }

            // we swap the old cells with the new ones
            std::swap(pd->cells, newCells);

            UpdateClientVisibleAfterMovement(pd, oldPos, newPos);

            // if the client is moved it invalidates the beacon
            // beacons are as everything else zone specific so they will come
            // in order, if it is followed by a movement update it means we have found
            // the client, if in the meantime it gets moved out of range
            // we will anyway get a new beacon
            // NOTE that we end this method with this, that way we don't lose interest
            // in the cell containing the client when dropping the beacon interest
            if (pd->beacon)
            {
                pd->beacon = false;
                DecrInterestInCell(pd->beaconCellCoords.first, pd->beaconCellCoords.second, NULL);
            }
        }

        //TODO: if the client actually might get out of range he will receive a erase for itself
        // we currently do not handle this case, a solution would be to keep the state at the client
        // till the new position is announced
        void CellData::EraseEntity(const int playerId)
        {
            //TODO: implement that we also remove the owned client temporarily when it is being erased
            // for now we just send updates even if they might get declined

            auto previousCellCoords = entityToCell.find(playerId);
            // we don't have to broadcast the change to the old location
            if (previousCellCoords != entityToCell.end())
            {
                // if existed before we send an update based on the old and the new location
                auto oldCell = GetCellData(previousCellCoords->second.first, previousCellCoords->second.second);
                if (oldCell)
                {
                    auto message = std::make_shared<Processing::Zone::ErasePlayerMessage>(playerId);

                    ForwardMessageToInRange(std::bind(
                        &Shared::Message::Message::MakeCopy<Processing::Zone::ErasePlayerMessage>, message),
                        (*oldCell)[playerId].ef.pos);
                }
                else
                {
                    // it should never the case that a entity is in a cell that we are not interested in...
                    SOLP_ERROR("Cell data expected but does not exist... terminating...");
                    std::terminate();
                }

                entityToCell.erase(playerId);
                oldCell->erase(playerId);
            }
        }

        void CellData::UpdateCellData(const int cellX, const int cellY, const int playerId, Processing::Zone::EntityInfo&& newData, const int zonePhase)
        {
            auto ownsClient = ownedClients.find(playerId);
            auto newCell = GetCellData(cellX, cellY);

            // prepare new message
            auto message = std::make_shared<Client::UpdatePlayerStateMessage>(playerId, newData,
                ownsClient == ownedClients.end() ? 0 : ownsClient->second->GetLastMessageHandled(), zonePhase);

            auto previousCellCoords = entityToCell.find(playerId);
            // if onDelete is true the old cell is being deleted...
            // we don't have to broadcast the change to the old location
            if (previousCellCoords != entityToCell.end())
            {
                // if existed before we send an update based on the old and the new location
                auto oldCell = GetCellData(previousCellCoords->second.first, previousCellCoords->second.second);
                if (oldCell)
                {
                    ForwardMessageToInRange(std::bind(
                        &Shared::Message::Message::MakeCopy<Client::UpdatePlayerStateMessage>, message),
                        newData.pos, (*oldCell)[playerId].ef.pos);
                }
                else
                {
                    // it should never the case that a entity is in a cell that we are not interested in...
                    SOLP_ERROR("Cell data expected but does not exist... terminating...");
                    std::terminate();
                }

                if (previousCellCoords->second.first != cellX ||
                    previousCellCoords->second.second != cellY)
                {
                    // move state from old cell to new cell
                    // not really necessary now because we send the whole
                    // new state, but could be useful later on
                    // for now we just delete the state in the old cell
                    //auto oldPlayerData = oldCell->find(playerId);
                    oldCell->erase(playerId);

                    // the new and the old cell are not the same
                    // it could be the case that we are moving
                    // to a non existing cell, in this case we have
                    // to erase the current entity to cell entry
                    if (!newCell)
                        entityToCell.erase(playerId);
                }
            }
            else
            {
                // new entity so we only send an update based on the initial location
                ForwardMessageToInRange(std::bind(
                    &Shared::Message::Message::MakeCopy<Client::UpdatePlayerStateMessage>, message), newData.pos);
            }

            if (ownsClient != ownedClients.end())
            {
                glm::vec2 oldPos = ownsClient->second->GetPosition();
                int oldZonePhase = ownsClient->second->zonePhase;
                ownsClient->second->GetEntityInfo() = newData;

                // if the client has not yet received this update we will now send it
                // the call to moveclient will never send state updates for the client itself
                //TODO: use IsInRange in the ForwardMessageToInRange functions as well
                // otherwise there is a very very very small chance that the message is send twice
                // or not at all
                if (!Shared::Util::IsInRange(oldPos, newData.pos,Shared::Config::visionRadius))
                {
                    message->senderID = owner->GetID();
                    message->receiverID = ownsClient->second->GetClientId();
                    owner->SendMessagePersistent(message);
                }

                MoveClient(ownsClient->second, oldPos, ownsClient->second->GetPosition(), oldZonePhase, zonePhase);
            }

            // not interest in the new cell, so we don't have data for it
            if (!newCell)
                return;

            // add new data to data structures
            entityToCell[playerId] = std::pair<int, int>(cellX, cellY);
            (*newCell)[playerId].ef = std::move(newData);
            (*newCell)[playerId].zonePhase = zonePhase;
        }

        void CellData::IncInterestInCell(int x, int y, IMPlayerData* pd)
        {
            std::list<IMPlayerData*>& vPd = clientLocation[x][y];
            uint64_t& beacons = clientBeaconsPerCell[x][y];

            // initialize this particular cell if it hasn't been initialized yet
            // and provides us with a handle to the cell content
            const auto& cell = cellInfo[x][y];

            // if it is empty and without active beacons
            // we have to link to the cell
            if (vPd.empty() && beacons == 0)
                hook->InterestedInCell(x, y);

            if (pd)
            {
                // finally add the client to this list for this cell
                vPd.push_back(pd);
            }
            else
            {
                // if NULL we add a beacon
                beacons++;
            }
        }

        void CellData::DecrInterestInCell(int x, int y, IMPlayerData* pd)
        {
            std::list<IMPlayerData*>& vPd = clientLocation[x][y];
            uint64_t& beacons = clientBeaconsPerCell[x][y];

            if (pd)
            {
                // erase only one element.. should not contain more than 1 of each anyway
                vPd.erase(std::find(vPd.begin(), vPd.end(), pd));
            }
            else
            {
                // if NULL we remove a beacon
                beacons--;
            }

            // if the list still contains elements we are done,
            // otherwise we do some cleanup
            if (!vPd.empty() || beacons > 0)
                return;

            // can invalidate data that is going to be added
            // also allows the hook to store a list of erased entities
            // also notifies the relevant zones that we are not
            // interested in this cell any longer
            hook->LostInterestInCellBeforeCleanup(x, y);

            // cleanup
            EraseCellData(x, y);

            // can readd entities that just got erased
            hook->LostInterestInCellAfterCleanup(x, y);
        }

        void CellData::UpdateClientVisibleAfterMovement(IMPlayerData* pd, const glm::vec2& oldPos, const glm::vec2& newPos)
        {
            // all the zones we loop over are guaranteed in our cell data store
            for (const auto& x : pd->cells)
            {
                for (const auto& y : x.second)
                {
                    for (const auto& entity : *GetCellData(x.first, y.first))
                    {
                        // do not send state changes for self
                        // this is done before this function is called
                        if (entity.first == pd->GetClientId()->id)
                            continue;

                        // if the entity is in range of the old position the client already has the info
                        if (Shared::Util::IsInRange(entity.second.ef.pos, oldPos, Shared::Config::visionRadius))
                            continue;
                        // if the entity is not in range of the new position it should not get the info
                        if (!Shared::Util::IsInRange(entity.second.ef.pos, newPos, Shared::Config::visionRadius))
                            continue;

                        std::shared_ptr<Client::UpdatePlayerStateMessage> mess(
                            std::make_shared<Client::UpdatePlayerStateMessage>(entity.first, entity.second.ef, pd->GetLastMessageHandled(), entity.second.zonePhase));
                        mess->senderID = owner->GetID();
                        mess->receiverID = pd->GetClientId();
                        owner->SendMessagePersistent(mess);
                    }
                }
            }
        }

        void CellData::EraseCellData(const int cellX, const int cellY)
        {
            for (const auto& entity : *GetCellData(cellX, cellY))
            {
                // entities can only be in one cell at a time, so if they are
                // in the cell that is being erased we can also remove their
                // entity to cell mapping
                entityToCell.erase(entity.first);
            }

            cellInfo[cellX].erase(cellY);
            if (cellInfo[cellX].empty())
                cellInfo.erase(cellX);

            if (!clientLocation[cellX][cellY].empty())
            {
                SOLP_ERROR("There are still clients interested in a cell being erased... terminating...");
                std::terminate();
            }

            clientLocation[cellX].erase(cellY);
            if (clientLocation[cellX].empty())
                clientLocation.erase(cellX);

            if (clientBeaconsPerCell[cellX][cellY] > 0)
            {
                SOLP_ERROR("There are still clients interested in a cell being erased... terminating...");
                std::terminate();
            }

            clientBeaconsPerCell[cellX].erase(cellY);
            if (clientBeaconsPerCell[cellX].empty())
                clientBeaconsPerCell.erase(cellX);
        }

        std::unordered_map<int, IMEntityInfo>* CellData::GetCellData(int x, int y)
        {
            const auto& xCell = cellInfo.find(x);
            // if not found we are finished
            //TODO: handle updates addressed to a specific client
            if (xCell == cellInfo.end())
                return NULL;

            const auto& yCell = xCell->second.find(y);
            // if not found we are finished
            if (yCell == xCell->second.end())
                return NULL;

            return &(yCell->second);
        }

        std::list<IMPlayerData*>* CellData::ClientsInCell(int x, int y)
        {
            const auto& xMap = clientLocation.find(x);
            // if not found we are finished
            if (xMap == clientLocation.end())
                return NULL;

            const auto& yMap = xMap->second.find(y);
            // if not found we are finished
            if (yMap == xMap->second.end())
                return NULL;

            // otherwise we return a pointer to the stored list
            return &(yMap->second);
        }

        IMEntityInfo* CellData::GetEntityInfo(const int playerId)
        {
            auto found = entityToCell.find(playerId);
            if (found == entityToCell.end())
                return NULL;

            if (auto specificCellData = GetCellData(found->second.first, found->second.second))
            {
                auto foundPlayer = specificCellData->find(playerId);
                if (foundPlayer != specificCellData->end())
                    return &foundPlayer->second;
            }

            // if here we haven't found the cell or entity
            // this is unexpected behaviour... terminate
            SOLP_ERROR("Cell not found or entity in cell not found... terminating....");
            std::terminate();
        }

        void CellData::ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker, const glm::vec2& pos)
        {
            std::pair<int, int> cellCoords = Shared::Util::PositionToCellCoords(pos);

            const auto clients = ClientsInCell(cellCoords.first, cellCoords.second);

            if (!clients)
                return;

            for (auto client : *clients)
            {
                if (!Shared::Util::IsInRange(pos, client->GetPosition(),Shared::Config::visionRadius))
                    continue;

                std::shared_ptr<Shared::Message::Message> toSend(copyMaker());
                toSend->senderID = owner->GetID();
                toSend->receiverID = client->GetClientId();
                owner->SendMessagePersistent(toSend);
            }
        }

        void CellData::ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker,
            const glm::vec2& pos1, const glm::vec2& pos2)
        {
            std::pair<int, int> cellCoords1 = Shared::Util::PositionToCellCoords(pos1);
            const auto clients1 = ClientsInCell(cellCoords1.first, cellCoords1.second);

            std::set<IMPlayerData*> clients;
            if (clients1)
                clients.insert(clients1->begin(), clients1->end());

            std::pair<int, int> cellCoords2 = Shared::Util::PositionToCellCoords(pos2);
            if (cellCoords2.first != cellCoords1.first || cellCoords2.second != cellCoords1.second)
            {
                const auto clients2 = ClientsInCell(cellCoords2.first, cellCoords2.second);
                if (clients2)
                    clients.insert(clients2->begin(), clients2->end());
            }

            for (auto client : clients)
            {
                if (!Shared::Util::IsInRange(pos1, client->GetPosition(), Shared::Config::visionRadius) &&
                    !Shared::Util::IsInRange(pos2, client->GetPosition(), Shared::Config::visionRadius))
                    continue;

                std::shared_ptr<Shared::Message::Message> toSend(copyMaker());
                toSend->senderID = owner->GetID();
                toSend->receiverID = client->GetClientId();
                owner->SendMessagePersistent(toSend);
            }
        }
    }
}
