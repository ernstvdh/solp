#include "RouterMessage.h"

namespace Processing
{
    namespace Router
    {
        const std::vector<std::function<std::shared_ptr<RouterMessage>(const Shared::JSon::JSon&)>> RouterMessage::enumToMessageMap
        {
            ConnectToRouterMessage::ParseJSon,
            GetPlayerZoneMessage::ParseJSon,
            ReplyPlayerZoneMessage::ParseJSon
        };
    }
}
