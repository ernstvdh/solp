#include "Router.h"
#include "RouterId.h"
#include "Balancing/Entry/EntryId.h"
#include "Balancing/Entry/EntryMessage.h"

#include "Shared/Config.h"
#include "Shared/Debug.h"
#include "Shared/Util.h"
#include "Shared/Singleton.h"

#include <random>

namespace Processing
{
    namespace Router
    {
        PlayerData::PlayerData() : imdata(new IMPlayerData(this)) {}

        Router::Router(int lId) : engine(time(NULL)), cellData(new CellData(this)),
            zoneToCellData(new ZoneToCellData(this, cellData.get()))
        {
            id = std::make_shared<const RouterId>(lId, Shared::Config::localIP);
            monitorHook = std::make_shared<RouterServerHookImpl>(lId);
            Shared::Singleton::monitorHooks->Add(monitorHook);
        }

        Router::~Router()
        {
            // cleanup
            for (auto& client : connectedClients)
                delete client.second;
        }

        int Router::Run()
        {
            ListenOn(Shared::Id::ProcessType::ROUTER);
            AnnounceSelfToSelector();
            RegisterWithMonitor();

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    // Monitoring
                    monitorHook->receivedMessages++;

                    if (auto connect = std::dynamic_pointer_cast<ConnectToRouterMessage>(mess))
                        HandleConnectToRouterMessage(connect);
                    else if (auto zonephase = std::dynamic_pointer_cast<Processing::Zone::AddPlayerResponseMessage>(mess))
                        HandleAddPlayerResponseMessage(zonephase);
                    else if (auto initplayerstate = std::dynamic_pointer_cast<Processing::Zone::ZoneCellStateInitMessage>(mess))
                        HandleZoneCellStateInitMessage(initplayerstate);
                    else if (auto playerup = std::dynamic_pointer_cast<Processing::Zone::UpdatePlayerStateMessage>(mess))
                        HandlePlayerUpdateMessage(playerup);
                    else if (auto entitytransfer = std::dynamic_pointer_cast<Processing::Zone::EntityZoneTransferPublicMessage>(mess))
                        HandleZoneEntityTransferMessage(entitytransfer);
                    else if (auto playererase = std::dynamic_pointer_cast<Processing::Zone::ErasePlayerMessage>(mess))
                        HandleErasePlayerMessage(playererase);
                    else if (auto beacon = std::dynamic_pointer_cast<Processing::Zone::EntityBeaconMessage>(mess))
                        HandleEntityBeaconMessage(beacon);
                    else if (auto getplayerzone = std::dynamic_pointer_cast<GetPlayerZoneMessage>(mess))
                        HandleGetPlayerZoneMessage(getplayerzone);
                    else if (auto playeractionstart = std::dynamic_pointer_cast<Processing::Zone::ActionStartMessage>(mess))
                        HandleActionStartMessage(playeractionstart);
                    else if (auto playeractionfire = std::dynamic_pointer_cast<Processing::Zone::ActionFireMessage>(mess))
                        HandleActionFireMessage(playeractionfire);
                    else if (auto playeractionhit = std::dynamic_pointer_cast<Processing::Zone::ActionHitMessage>(mess))
                        HandleActionHitMessage(playeractionhit);
                }
            }

            return 0;
        }

        void Router::AnnounceSelfToSelector()
        {
            std::shared_ptr<BL::Entry::AddEntryMessage> addRouterMess(std::make_shared<BL::Entry::AddEntryMessage>());
            addRouterMess->routerId = std::static_pointer_cast<const RouterId>(id);
            addRouterMess->senderID = id;
            addRouterMess->receiverID = std::make_shared<const BL::Entry::EntryId>(Shared::Config::gLBIp);
            SendSingleMessageTo(addRouterMess);
        }

        void Router::HandleGetPlayerZoneMessage(std::shared_ptr<GetPlayerZoneMessage>& getplayerzone)
        {
            auto reply = Shared::Message::Message::MakeMessage<ReplyPlayerZoneMessage>(id, getplayerzone->senderID);
            reply->player = getplayerzone->player;

            auto controlsPlayer = connectedClients.find(getplayerzone->player);
            if (controlsPlayer != connectedClients.end())
            {
                reply->found = true;
                reply->zoneX = std::get<0>(controlsPlayer->second->currentZone);
                reply->zoneY = std::get<1>(controlsPlayer->second->currentZone);
                reply->zonePhase = std::get<2>(controlsPlayer->second->currentZone);
            }
            else
                reply->found = false;

            SendMessagePersistent(reply);
        }

        void Router::HandleConnectToRouterMessage(std::shared_ptr<ConnectToRouterMessage>& connect)
        {
            SOLP_DEBUG(DEBUG_ROUTER, "received connect to router message from %s", connect->senderID->Serialize().c_str());
            if (connect->senderID->type != Shared::Id::ProcessType::CLIENT)
            {
                SOLP_WARNING("Received connect to router message from non client:\n%s -> %s",
                    connect->senderID->Serialize().c_str(), connect->receiverID->Serialize().c_str());
                return;
            }

            // deleted in the destructor of Router
            PlayerData* pd = new PlayerData();
            pd->id = std::static_pointer_cast<const Client::ClientId>(connect->senderID);

            //TODO: make this persistent, read/store
            // fow now we just take a random position on the map
            if (connect->requestPosition)
                pd->pInfo.pos = connect->position;
            else
                pd->pInfo.pos = Shared::Util::GetRandomPositionOnMap(engine);

            auto zoneIdCoords = Shared::Util::PositionToZoneCoords(pd->pInfo.pos);

            std::get<0>(pd->currentZone) = zoneIdCoords.first;
            std::get<1>(pd->currentZone) = zoneIdCoords.second;
            std::get<2>(pd->currentZone) = std::numeric_limits<int>::max();
            
            connectedClients[pd->id->id] = pd;

            cellData->AddOwnClient(pd->imdata.get());
            zoneToCellData->AddOwnClient(pd->id->id, std::get<0>(pd->currentZone), std::get<1>(pd->currentZone), std::get<2>(pd->currentZone));

            auto addPlayerMessage(std::make_shared<Processing::Zone::AddPlayerMessage>(pd->id->id, pd->pInfo, 0, id));
            addPlayerMessage->senderID = id;
            addPlayerMessage->receiverID = std::shared_ptr<const Processing::Zone::ZoneId>(new Processing::Zone::ZoneId(
                std::get<0>(pd->currentZone), std::get<1>(pd->currentZone), 0));
            // We assume a link to the respective zone
            SOLP_DEBUG(DEBUG_ROUTER, "sending message to the zone to connect the player: %s", mess->Serialise().c_str());
            SendSingleMessageTo(addPlayerMessage);

            // Monitoring
            monitorHook->connectedClients++;
        }

        void Router::HandleAddPlayerResponseMessage(std::shared_ptr<Processing::Zone::AddPlayerResponseMessage>& zoneaddresponse)
        {
            auto controlsPlayer = connectedClients.find(zoneaddresponse->playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            zoneToCellData->EraseEntity(
                std::get<0>(controlsPlayer->second->currentZone),
                std::get<1>(controlsPlayer->second->currentZone),
                std::get<2>(controlsPlayer->second->currentZone),
                zoneaddresponse->playerId, true);

            std::get<2>(controlsPlayer->second->currentZone) = zoneaddresponse->phase;

            // claim entity
            SendPlayerClaimMessage(zoneaddresponse->playerId, controlsPlayer->second->pInfo.pos, std::get<2>(controlsPlayer->second->currentZone));
        }

        void Router::SendPlayerUpdateMessage(int playerId, const Processing::Zone::EntityInfo& pInfo, const uint64_t counter)
        {
            auto controlsPlayer = connectedClients.find(playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            auto mess(std::make_shared<Processing::Zone::UpdatePlayerStateMessage>(playerId, pInfo, counter));
            mess->senderID = id;
            mess->receiverID = std::shared_ptr<const Processing::Zone::ZoneId>(new Processing::Zone::ZoneId(
                std::get<0>(controlsPlayer->second->currentZone), std::get<1>(controlsPlayer->second->currentZone),
                std::get<2>(controlsPlayer->second->currentZone)));
            // We assume a link to the respective zone
            SOLP_DEBUG(DEBUG_ROUTER, "sending message to the zone to connect the player: %s", mess->Serialise().c_str());
            SendMessagePersistent(mess);
        }

        void Router::SendPlayerActionMessage(int playerId, const Processing::Zone::Action& action, const uint64_t counter)
        {
            auto controlsPlayer = connectedClients.find(playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            auto mess(std::make_shared<Processing::Zone::ActionStartMessage>(playerId, action, counter));
            mess->senderID = id;
            mess->receiverID = std::shared_ptr<const Processing::Zone::ZoneId>(new Processing::Zone::ZoneId(
                std::get<0>(controlsPlayer->second->currentZone), std::get<1>(controlsPlayer->second->currentZone),
                std::get<2>(controlsPlayer->second->currentZone)));
            // We assume a link to the respective zone
            SOLP_DEBUG(DEBUG_ROUTER, "sending message to the zone to let the player cast a spell: %s", mess->Serialise().c_str());

            SOLP_DEBUG(DEBUG_INTERACTION, "Router sending start action message.");

            SendMessagePersistent(mess);
        }

        void Router::SendPlayerActionFireMessage(int playerId, const Processing::Zone::Action& action, const uint64_t counter)
        {
            auto controlsPlayer = connectedClients.find(playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            auto mess(std::make_shared<Processing::Zone::ActionFireMessage>(playerId, action, counter));
            mess->senderID = id;
            mess->receiverID = std::shared_ptr<const Processing::Zone::ZoneId>(new Processing::Zone::ZoneId(
                std::get<0>(controlsPlayer->second->currentZone), std::get<1>(controlsPlayer->second->currentZone),
                std::get<2>(controlsPlayer->second->currentZone)));
            // We assume a link to the respective zone
            SOLP_DEBUG(DEBUG_ROUTER, "sending message to the zone to let the player fire a spell: %s", mess->Serialise().c_str());

            SOLP_DEBUG(DEBUG_INTERACTION, "Router sending fire action message.");

            SendMessagePersistent(mess);
        }

        // we open a link here because interest management works separately from our possible interest in
        // this zone, we cannot be sure interest management has a link to this zone and is less likely as soon
        // as connection delays start to increase
        // we close this link when we change the currentZone in HandleZoneEntityTransferMessage
        void Router::SendPlayerClaimMessage(int playerId, const glm::vec2& pos, const int zonePhase)
        {
            auto zoneIdCoords = Shared::Util::PositionToZoneCoords(pos);
            std::shared_ptr<const Shared::Id> receiver = std::shared_ptr<const Processing::Zone::ZoneId>(
                new Processing::Zone::ZoneId(zoneIdCoords.first, zoneIdCoords.second, zonePhase));

            auto claim = Shared::Message::Message::MakeMessage<Processing::Zone::ClaimEntityMessage>(id, receiver);
            claim->playerId = playerId;
            SendMessageTemporary(claim);
        }

        void Router::HandleZoneCellStateInitMessage(std::shared_ptr<Processing::Zone::ZoneCellStateInitMessage>& initzone)
        {
            // if not we receive this from an unexpected sender....
            if (auto zoneId = std::dynamic_pointer_cast<const Processing::Zone::ZoneId>(initzone->senderID))
            {
                zoneToCellData->AddCellData(initzone->cellX, initzone->cellY, zoneId->x, zoneId->y, zoneId->phase,
                                            initzone->numPhases, initzone->interestId, std::move(initzone->entityStates));
            }
            else
                SOLP_WARNING("Unexpected id (%s) is sending an state init message.", initzone->senderID->Serialize().c_str());
        }

        void Router::HandlePlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate)
        {
            if (playerupdate->senderID->type == Shared::Id::ProcessType::CLIENT)
                HandleClientPlayerUpdateMessage(playerupdate);
            else if (playerupdate->senderID->type == Shared::Id::ProcessType::CELL)
                HandleZonePlayerUpdateMessage(playerupdate);
            else
                SOLP_ERROR("Wrong sender type for update player state message.");
        }

        void Router::HandleActionStartMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart)
        {
            if (actionstart->senderID->type == Shared::Id::ProcessType::CLIENT)
                HandleActionStartClientMessage(actionstart);
            else if (actionstart->senderID->type == Shared::Id::ProcessType::CELL)
                HandleActionStartZoneMessage(actionstart);
            else
                SOLP_ERROR("Wrong sender type for action start message.");
        }

        void Router::HandleActionStartClientMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart)
        {
            //TODO: in a real scenario we wouldn't trust the sender, so we would have
            // to check if this playerId matches the sender id
            auto controlsPlayer = connectedClients.find(actionstart->playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            SendPlayerActionMessage(actionstart->playerId, actionstart->action, actionstart->aCounter);
            controlsPlayer->second->messagesWithoutConfirm.push_back(actionstart);

            if (controlsPlayer->second->messagesWithoutConfirm.size() > 50)
                SOLP_WARNING("PlayerId: %d, Messages without confirm: %d", actionstart->playerId, controlsPlayer->second->messagesWithoutConfirm.size());
        }

        //TODO: wait for possible movement updates before handling this (see ZoneToCellData)
        //TODO: store this data in CellData so that new clients connecting will see this state
        // for now we do not care enough so we just foward the information to the clients
        void Router::HandleActionStartZoneMessage(std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart)
        {
            // we can drop the messages that have been confirmed to be handled by a zone
            LastMessageConfirmedForPlayer(actionstart->playerId, actionstart->aCounter);

            if (IMEntityInfo* info = cellData->GetEntityInfo(actionstart->action.source))
            {
                cellData->ForwardMessageToInRange(std::bind(
                    &Shared::Message::Message::MakeCopy<Processing::Zone::ActionStartMessage>, actionstart), info->ef.pos);
            }
        }

        //TODO: see comments HandleActionStartZoneMessage
        void Router::HandleActionFireMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire)
        {
            if (actionfire->senderID->type == Shared::Id::ProcessType::CLIENT)
                HandleActionFireClientMessage(actionfire);
            else if (actionfire->senderID->type == Shared::Id::ProcessType::CELL)
                HandleActionFireZoneMessage(actionfire);
            else
                SOLP_ERROR("Wrong sender type for action fire message.");
        }

        //TODO: see comments HandleActionStartZoneMessage
        void Router::HandleActionFireClientMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire)
        {
            //TODO: in a real scenario we wouldn't trust the sender, so we would have
            // to check if this playerId matches the sender id
            auto controlsPlayer = connectedClients.find(actionfire->playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            SendPlayerActionFireMessage(actionfire->playerId, actionfire->fire, actionfire->aCounter);
            controlsPlayer->second->messagesWithoutConfirm.push_back(actionfire);

            if (controlsPlayer->second->messagesWithoutConfirm.size() > 50)
                SOLP_WARNING("PlayerId: %d, Messages without confirm: %d", actionfire->playerId, controlsPlayer->second->messagesWithoutConfirm.size());
        }

        //TODO: see comments HandleActionStartZoneMessage
        void Router::HandleActionFireZoneMessage(std::shared_ptr<Processing::Zone::ActionFireMessage>& actionfire)
        {
            // we can drop the messages that have been confirmed to be handled by a zone
            LastMessageConfirmedForPlayer(actionfire->playerId, actionfire->aCounter);

            if (IMEntityInfo* info = cellData->GetEntityInfo(actionfire->fire.source))
            {
                cellData->ForwardMessageToInRange(std::bind(
                    &Shared::Message::Message::MakeCopy<Processing::Zone::ActionFireMessage>, actionfire), info->ef.pos);
            }
        }

        //TODO: see comments HandleActionStartZoneMessage
        void Router::HandleActionHitMessage(std::shared_ptr<Processing::Zone::ActionHitMessage>& actionhit)
        {
            if (IMEntityInfo* info = cellData->GetEntityInfo(actionhit->hit.target))
            {
                cellData->ForwardMessageToInRange(std::bind(
                    &Shared::Message::Message::MakeCopy<Processing::Zone::ActionHitMessage>, actionhit), info->ef.pos);
            }
        }

        void Router::HandleErasePlayerMessage(std::shared_ptr<Processing::Zone::ErasePlayerMessage>& eraseplayer)
        {
            // if not we receive this from an unexpected sender....
            if (auto zoneId = std::dynamic_pointer_cast<const Processing::Zone::ZoneId>(eraseplayer->senderID))
                zoneToCellData->EraseEntity(zoneId->x, zoneId->y, zoneId->phase, eraseplayer->playerId, false);
            else
                SOLP_WARNING("Unexpected id (%s) is sending an update player state message.", eraseplayer->senderID->Serialize().c_str());
        }

        void Router::HandleEntityBeaconMessage(std::shared_ptr<Processing::Zone::EntityBeaconMessage>& beacon)
        {
            // we can drop the messages that have been confirmed to be handled by a zone
            LastMessageConfirmedForPlayer(beacon->playerId, beacon->aCounter);

            if (auto zoneId = std::dynamic_pointer_cast<const Processing::Zone::ZoneId>(beacon->senderID))
                zoneToCellData->UpdateCellData(zoneId->x, zoneId->y, zoneId->phase, beacon->playerId, std::move(beacon->pInfo), true);
            else
                SOLP_ERROR("Wrong sender type for player beacon message.");
        }

        void Router::HandleZonePlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate)
        {
            // we can drop the messages that have been confirmed to be handled by a zone
            LastMessageConfirmedForPlayer(playerupdate->playerId, playerupdate->aCounter);

            // if not we receive this from an unexpected sender....
            if (auto zoneId = std::dynamic_pointer_cast<const Processing::Zone::ZoneId>(playerupdate->senderID))
                zoneToCellData->UpdateCellData(zoneId->x, zoneId->y, zoneId->phase, playerupdate->playerId, std::move(playerupdate->pInfo), false);
            else
                SOLP_WARNING("Unexpected id (%s) is sending an update player state message.", playerupdate->senderID->Serialize().c_str());
        }

        void Router::HandleClientPlayerUpdateMessage(std::shared_ptr<Processing::Zone::UpdatePlayerStateMessage>& playerupdate)
        {
            //TODO: in a real scenario we wouldn't trust the sender, so we would have
            // to check if this playerId matches the sender id
            auto controlsPlayer = connectedClients.find(playerupdate->playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            SendPlayerUpdateMessage(playerupdate->playerId, playerupdate->pInfo, playerupdate->aCounter);
            controlsPlayer->second->messagesWithoutConfirm.push_back(playerupdate);

            if (controlsPlayer->second->messagesWithoutConfirm.size() > 50)
                SOLP_WARNING("PlayerId: %d, Messages without confirm: %d", playerupdate->playerId, controlsPlayer->second->messagesWithoutConfirm.size());
        }

        void Router::LastMessageConfirmedForPlayer(int playerId, const uint64_t lastMessageHandled, PlayerData* pd)
        {
            PlayerData* player = pd;

            // only if we own this entity we need to maintain a list of unhandled messages
            if (!player)
            {
                auto controlsPlayer = connectedClients.find(playerId);
                if (controlsPlayer == connectedClients.end())
                    return;

                player = controlsPlayer->second;
            }

            // we already received a message that up to this point has been handled
            // in this case we can just return
            if (player->lastConfirmedMessage >= lastMessageHandled)
                return;

            uint64_t toDelete = lastMessageHandled - player->lastConfirmedMessage;
            if (toDelete > player->messagesWithoutConfirm.size())
            {
                SOLP_ERROR("Deleting more messages than currently are in the queue... this should not happen... terminating...");
                std::terminate();
            }
            player->lastConfirmedMessage = lastMessageHandled;

            player->messagesWithoutConfirm.erase(
                player->messagesWithoutConfirm.begin(),
                player->messagesWithoutConfirm.begin() + toDelete);
        }

        void Router::HandleZoneEntityTransferMessage(std::shared_ptr<Processing::Zone::EntityZoneTransferPublicMessage>& entitytransfer)
        {
            SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Router handle entity transfer.\n%s", entitytransfer->Serialise().c_str());

            auto fromZoneId = std::dynamic_pointer_cast<const Processing::Zone::ZoneId>(entitytransfer->senderID);
            if (!fromZoneId)
                SOLP_ERROR("Wrong sender type for player transfer message.");

            // the router that controls this entity will always get a new state update
            // the other routers might not, so in case this zone transfer results in this
            // router not receiving the new update we make sure it updates the clients
            // and its internal state appropriately
            zoneToCellData->EraseEntity(fromZoneId->x, fromZoneId->y, fromZoneId->phase, entitytransfer->playerId, true);

            auto controlsPlayer = connectedClients.find(entitytransfer->playerId);
            if (controlsPlayer == connectedClients.end())
                return;

            // we can drop the messages that have been confirmed to be handled by a zone
            LastMessageConfirmedForPlayer(entitytransfer->playerId, entitytransfer->aCounter, controlsPlayer->second);

            // if not yet claimed we ignore this message in terms of currrent zone because we will still
            // get that message (guaranteed), otherwise we might end up changing the zone twice
            if (!entitytransfer->claimed)
                return;

            // only continue when this entity zone transfer is the one we are currently expecting
            // if not we are completely sure that the zones will resend this message as soon as needed
            if (fromZoneId->x != std::get<0>(controlsPlayer->second->currentZone) ||
                fromZoneId->y != std::get<1>(controlsPlayer->second->currentZone) ||
                fromZoneId->phase != std::get<2>(controlsPlayer->second->currentZone))
                return;

            // we claim the player on the new zone server if needed
            // also set the new zone coords
            const auto toZoneCoords(Shared::Util::PositionToZoneCoords(entitytransfer->to));
            if (fromZoneId->x != toZoneCoords.first || fromZoneId->y != toZoneCoords.second || fromZoneId->phase != entitytransfer->toPhase)
            {
                // here we remove the link we added in the previous SendPlayerClaimMessage call for this entity
                // we remove it because we change the currentZone here
                LinkToRemove(std::make_shared<const Processing::Zone::ZoneId>(
                    std::get<0>(controlsPlayer->second->currentZone),
                    std::get<1>(controlsPlayer->second->currentZone),
                    std::get<2>(controlsPlayer->second->currentZone)));

                std::get<0>(controlsPlayer->second->currentZone) = toZoneCoords.first;
                std::get<1>(controlsPlayer->second->currentZone) = toZoneCoords.second;
                std::get<2>(controlsPlayer->second->currentZone) = entitytransfer->toPhase;
                SendPlayerClaimMessage(entitytransfer->playerId, entitytransfer->to, entitytransfer->toPhase);
            }

            for (const auto& message : controlsPlayer->second->messagesWithoutConfirm)
            {
                if (const auto playerup = std::dynamic_pointer_cast<const Processing::Zone::UpdatePlayerStateMessage>(message))
                {
                    SendPlayerUpdateMessage(entitytransfer->playerId, playerup->pInfo, playerup->aCounter);
                }
                else if (const auto playeraction = std::dynamic_pointer_cast<const Processing::Zone::ActionStartMessage>(message))
                {
                    SendPlayerActionMessage(playeraction->playerId, playeraction->action, playeraction->aCounter);
                }
                else if (const auto playeractionfire = std::dynamic_pointer_cast<const Processing::Zone::ActionFireMessage>(message))
                {
                    SendPlayerActionFireMessage(playeractionfire->playerId, playeractionfire->fire, playeractionfire->aCounter);
                }
                else
                {
                    SOLP_ERROR("Message without confirm is not handled on entity transfer.... terminating....");
                    std::terminate();
                }
            }
        }

        void Router::RouterServerHookImpl::MonitorTick(int t)
        {
            tickInfo.push_back(std::tuple<int, uint64_t, uint64_t>(
                t, connectedClients.load(), receivedMessages.exchange(0)));
        }

        void Router::RouterServerHookImpl::StoreData()
        {
            std::fstream fs;
            fs.open(std::to_string(Shared::Id::ProcessType::ROUTER) + "_" + std::to_string(id) +
                "_" + Shared::Config::localIP + ".processing.router.solp.txt", std::fstream::out | std::fstream::trunc);

            fs << "tick\tclients\tmessages\n";
            for (const auto& info : tickInfo)
                fs << std::get<0>(info) << "\t" << std::get<1>(info) << "\t" << std::get<2>(info) << "\n";

            fs.close();
        }
    }
}
