#include <cstdint>
#include <vector>

#include "ApproximatedCost.h"

namespace Processing
{
    namespace ApproximatedCost
    {
        // the time to run this function is about the same
        // as the cost to run the trinitycore movement handler
        // https://github.com/TrinityCore/TrinityCore/blob/6.x/src/server/game/Handlers/MovementHandler.cpp#L260
        // estimated using the visual studio performance profiler
        void ZoneMovementHandler()
        {
            std::vector<uint64_t> temps;
            for (int i = 0; i < 5000; i++)
                temps.push_back(i);
        }

        // this is an approximation of the spell cast cost in trinitycore
        // the specific function approximated it prepare in Spell.cpp
        void SpellHandler()
        {
            std::vector<uint64_t> temps;
            for (int i = 0; i < 20000; i++)
                temps.push_back(i);
        }

        void SpellVisionCheck()
        {
            // so cheap that we do nothing
        }
    }
}
