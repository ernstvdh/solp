#ifndef SOLP_PROCESSING_ZONE_INTERNALENTITYINFO_H
#define SOLP_PROCESSING_ZONE_INTERNALENTITYINFO_H

#include "EntityInfo.h"

namespace Processing
{
    namespace Router
    {
        class RouterId;
    }

    namespace Zone
    {
        struct InternalEntityInfo : public EntityInfo
        {
            enum InternalEntityInfoState
            {
                CONFIRMED,
                TRANSFER,
                WAITING
            };

            InternalEntityInfo() {}
            InternalEntityInfo(const EntityInfo& ef) : EntityInfo(ef) {}

            InternalEntityInfoState transferState = InternalEntityInfoState::CONFIRMED;
            std::shared_ptr<const Processing::Router::RouterId> router = NULL;
            std::vector<ActionTag> tags;
            uint64_t currentActionId = 0;
            uint64_t lastHandledCounter = 0;

            InternalEntityInfo(const Shared::JSon::JSon& json) : EntityInfo(json)
            {
                // we do not transfer the router because the entity needs to be claimed
                // after a zone transfer, we also do not transfer the internal entity state
                // because is changes anyway after a transfer
                transferState = InternalEntityInfoState::CONFIRMED;
                GET_FROM_JSON_STRICT(tags);
                GET_FROM_JSON_STRICT(lastHandledCounter);
            }

            void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                EntityInfo::SerializeToJSon(json);
                ADD_TO_JSON(tags);
                ADD_TO_JSON(lastHandledCounter);
            }

            static InternalEntityInfo ParseJSon(const Shared::JSon::JSon& json)
            {
                return InternalEntityInfo(json);
            }
        };
    }
}

#endif
