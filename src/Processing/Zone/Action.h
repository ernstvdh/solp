#ifndef SOLP_PROCESSING_ZONE_ACTION_H
#define SOLP_PROCESSING_ZONE_ACTION_H

#include "glm/vec2.hpp"

#include "Shared/Serializable.h"

#include "ZoneId.h"

//TODO: REMOVE THIS
namespace Shared
{
    namespace Message
    {
        class Message;
    }
}

namespace Processing
{
    namespace Zone
    {
        enum ActionType
        {
            SPELL,

            ACTION_MAX
        };

        struct ActionTagId : public Shared::Serializable
        {
            ActionTagId() {}
            ActionTagId(const std::shared_ptr<const ZoneId>& zone, const uint64_t count) :
                zoneId(zone), counter(count) {}
            ActionTagId(const Shared::JSon::JSon& json)
            {
                GET_FROM_JSON_STRICT(zoneId);
                GET_FROM_JSON_STRICT(counter);
            }
            ~ActionTagId() {}

            std::shared_ptr<const ZoneId> zoneId;
            uint64_t counter;

            bool equals(const ActionTagId& that) const
            {
                if (that.counter != this->counter)
                    return false;

                if (!that.zoneId->equals(this->zoneId))
                    return false;

                return true;
            }

            void SerializeToJSon(Shared::JSon::JSon& json) const override
            {
                ADD_TO_JSON(zoneId);
                ADD_TO_JSON(counter);
            }

            static ActionTagId ParseJSon(const Shared::JSon::JSon& json)
            {
                return ActionTagId(json);
            }
        };

        struct ActionTag : public Shared::Serializable
        {
            ActionTag() : type(ActionType::ACTION_MAX) {}
            ActionTag(const Shared::JSon::JSon& json)
            {
                GET_FROM_JSON_STRICT(type);
                GET_FROM_JSON_STRICT(source);
                GET_FROM_JSON_STRICT(sourcePos);
                GET_FROM_JSON_STRICT(range);
                GET_FROM_JSON_STRICT(actionId);
            }
            ~ActionTag() {}

            ActionType type;
            int source;
            glm::vec2 sourcePos;
            double range;

            ActionTagId actionId;

            void SerializeToJSon(Shared::JSon::JSon& json) const override
            {
                ADD_TO_JSON(type);
                ADD_TO_JSON(source);
                ADD_TO_JSON(sourcePos);
                ADD_TO_JSON(range);
                ADD_TO_JSON(actionId);
            }

            static ActionTag ParseJSon(const Shared::JSon::JSon& json)
            {
                return ActionTag(json);
            }
        };

        struct Action : public Shared::Serializable
        {
            Action() : type(ActionType::ACTION_MAX), duration(0) {}
            Action(const Shared::JSon::JSon& json)
            {
                GET_FROM_JSON_STRICT(type);
                GET_FROM_JSON_STRICT(source);
                GET_FROM_JSON_STRICT(target);
                GET_FROM_JSON_STRICT(range);
                GET_FROM_JSON_STRICT(duration);
            }
            ~Action() {}

            ActionType type;
            int source;
            int target;
            //TODO: this should be e.g. a spell ID, for now just send action range around
            double range;
            uint64_t duration;

            void SerializeToJSon(Shared::JSon::JSon& json) const override
            {
                ADD_TO_JSON(type);
                ADD_TO_JSON(source);
                ADD_TO_JSON(target);
                ADD_TO_JSON(range);
                ADD_TO_JSON(duration);
            }

            static Action ParseJSon(const Shared::JSon::JSon& json)
            {
                return Action(json);
            }
        };
    }
}

#endif