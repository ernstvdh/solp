#ifndef SOLP_PROCESSING_ZONE_ZONE_H
#define SOLP_PROCESSING_ZONE_ZONE_H

#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <deque>

#include "Shared/Link/Moveable.h"
#include "ZoneMessage.h"
#include "InternalEntityInfo.h"
#include "TaggedEntities.h"
#include "Processing/Router/RouterId.h"
#include "Balancing/Tracker/TrackerMessage.h"

namespace Monitor
{
    namespace Zone
    {
        class Zone;
    }
}

namespace BL
{
    namespace Tracker
    {
        class TrackerId;
    }

    namespace Zone
    {
        class Zone;
    }
}

namespace Processing
{
    namespace Zone
    {
        /*
        A zone can have multiple phases for load balancing purposes, if some phases are dropped
        because the load drops they will stay active but idle. They do remain because some tags
        might still be referencing the particular phase (NOTE: this could also be solved by having
        the main phase, 0, register as the dropped phase IDs). The dropped phases will drop connections
        to other threads but will still respond to requests sent to them.
        */
        class Zone : public Shared::Link::Moveable
        {
        public:
            Zone(int lX, int lY, int lPhase, Shared::Link::Moveable::State st);
            ~Zone();

            int Run();


            InternalEntityInfo* GetEntity(const int playerId, bool includeInTransfer)
            {
                auto exists = entityMap.find(playerId);

                if (exists == entityMap.end())
                    return NULL;

                if (!includeInTransfer)
                {
                    // if the entity is in transfer now we technically are not the owner
                    //TODO: look at this, would be more efficient maybe to just store the message
                    // till we know if the transfer is a succes (although we should be optimistic
                    // and assume that most transfers succeed)
                    if (exists->second->transferState != InternalEntityInfo::InternalEntityInfoState::CONFIRMED)
                        return NULL;
                }

                return exists->second;
            }

            // WARNING: any movement related message needs to be send by BroadcastEntityState
            void ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker, const glm::vec2& pos);

            bool PhaseMoveEntity(const int playerId, const int toPhase, InternalEntityInfo* ef = NULL);
            void SetActivePhases(const int activePhases);

            // Serialization
            Zone(const Shared::JSon::JSon& json);
            void SerializeToJSon(Shared::JSon::JSon& json) const override;
            static std::shared_ptr<Zone> ParseJSon(const Shared::JSon::JSon& json);

        private:
            int ownX;
            int ownY;
            int ownPhase;

            void Init();

            void HandleUpdatePlayerMessage(const std::shared_ptr<UpdatePlayerStateMessage>& playerup);
            void HandleAddPlayerMessage(const std::shared_ptr<AddPlayerMessage>& addplayer);
            void HandleActionStartMessage(const std::shared_ptr<ActionStartMessage>& actionstart);
            void HandleActionFireMessage(const std::shared_ptr<ActionFireMessage>& actionfire);
            void HandleRouterConnectMessage(const std::shared_ptr<RouterConnectMessage>& router);
            void HandleRouterDisconnectMessage(const std::shared_ptr<RouterDisconnectMessage>& router);
            // WARNING: When using phases when the entity is declined in a zone and has to be sent back
            // to the original zone this send back has to take care of sending it to the correct phase
            void HandleEntityTransferMessage(const std::shared_ptr<EntityZoneTransferMessage>& transfer);
            void HandleClaimEntityMessage(const std::shared_ptr<ClaimEntityMessage>& claim);

            glm::vec2 GetBorderPoint(const glm::vec2& from, const glm::vec2& to, const std::shared_ptr<const ZoneId>& fromZone);
            void BroadcastEntityState(int playerId, const EntityInfo& info, const uint64_t counter);
            void BroadcastEntityState(int playerId, const EntityInfo& info, const glm::vec2& oldPos, const uint64_t counter);

            void ChangeEntityPosition(const int playerId, const glm::vec2& oldPos, const glm::vec2& newPos);
            void AddEntity(const int playerId, EntityInfo& entityInfo, const uint64_t counter, const bool phaseChange);
            void AddEntity(const int playerId, InternalEntityInfo& entityInfo, const bool phaseChange);
            void UniversalAddEntityCode(const int playerId, InternalEntityInfo* ef, const bool phaseChange);
            void RemoveEntity(const int playerId, const bool phaseChange);
            void UpdateEntity(const int playerId, InternalEntityInfo* entity, const glm::vec2& newPos);
            void UpdateEntity(const int playerId, InternalEntityInfo* entity, EntityInfo& newInfo);
            void ClaimEntity(const int playerId, std::shared_ptr<const Processing::Router::RouterId>& claimer);

            void SendBeaconIfNeeded(const int playerId, bool skipCheck);

            int cellOffsetX;
            int cellOffsetY;
            std::unordered_map<int, InternalEntityInfo*> entityMap;
            std::vector<std::vector<std::unordered_map<int, InternalEntityInfo*>>> cellToEntityMap;
            std::vector<std::vector<std::unordered_map<std::string,
                std::pair<std::pair<int, uint64_t>, std::shared_ptr<const Processing::Router::RouterId>>>>> connectedRoutersPerCell;

            std::unordered_map<int, std::deque<std::shared_ptr<EntityZoneTransferPublicMessage>>> zoneTransfers;

            TaggedEntities taggedEntities;

            // Monitoring
            std::shared_ptr<Monitor::Zone::Zone> monitor;
            //TODO: make this a unique_ptr, not supported yet by JSon wrapper
            std::shared_ptr<BL::Zone::Zone> loadBalancer;
        };
    }
}

#endif
