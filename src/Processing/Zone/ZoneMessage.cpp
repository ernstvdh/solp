#include "ZoneMessage.h"

namespace Processing
{
    namespace Zone
    {
        const std::vector<std::function<std::shared_ptr<ZoneMessage>(const Shared::JSon::JSon&)>> ZoneMessage::enumToMessageMap
        {
            ErasePlayerMessage::ParseJSon,
            AddPlayerMessage::ParseJSon,
            AddPlayerResponseMessage::ParseJSon,
            UpdatePlayerStateMessage::ParseJSon,
            RouterConnectMessage::ParseJSon,
            RouterDisconnectMessage::ParseJSon,
            ZoneCellStateInitMessage::ParseJSon,
            EntityZoneTransferMessage::ParseJSon,
            EntityZoneTransferPublicMessage::ParseJSon,
            ClaimEntityMessage::ParseJSon,
            EntityBeaconMessage::ParseJSon,
            ActionTagSetMessage::ParseJSon,
            ActionTagSetFailMessage::ParseJSon,
            ActionTagUseMessage::ParseJSon,
            ActionTagUseSuccessMessage::ParseJSon,
            ActionTagBeaconMessage::ParseJSon,
            ActionTagZoneAddMessage::ParseJSon,
            ActionTagZoneRemoveMessage::ParseJSon,
            ActionStartMessage::ParseJSon,
            ActionFireMessage::ParseJSon,
            ActionHitMessage::ParseJSon
        };
    }
}
