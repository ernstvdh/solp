#ifndef SOLP_PROCESSING_ZONE_TAGGEDENTITIES_H
#define SOLP_PROCESSING_ZONE_TAGGEDENTITIES_H

#include <queue>
#include <vector>

#include "Action.h"
#include "ZoneId.h"
#include "ZoneMessage.h"

#include "Shared/Serializable.h"

namespace BL
{
    namespace Tracker
    {
        class TrackerId;
        class UpdatePlayerTrackerMessage;
    }
}

namespace Processing
{
    namespace Router
    {
        class ReplyPlayerZoneMessage;
    }

    namespace Zone
    {
        struct InternalEntityInfo;
        class Zone;

        class TaggedEntities : public Shared::Serializable
        {
        public:
            TaggedEntities() {}
            TaggedEntities(Zone* creator) : owner(creator) {}
            ~TaggedEntities() {}

            // this function starts the action in terms of setting the proper state for the entity
            // we are handling, properly adding data to the targetted entity should be handled
            // by the private AddTagEntity function
            void StartAction(const int caster, InternalEntityInfo* const iInfo, const Action& action);
            // this function finishes the action in terms of setting the proper state for the entity
            // we are handling, properly triggering the action at the targetted entity should be handled
            // by the private UseTagEntity function
            void FinishAction(const int caster, InternalEntityInfo* const iInfo);

            void AddEntity(InternalEntityInfo* const added);
            void MoveEntity(const InternalEntityInfo* const mover, const glm::vec2& newPos);
            void RemoveEntity(const InternalEntityInfo* const remove);

            void HandleActionTagSetMessage(const std::shared_ptr<ActionTagSetMessage>& settag);
            void HandleActionTagSetFailMessage(const std::shared_ptr<ActionTagSetFailMessage>& settagfail);
            void HandleActionTagBeaconMessage(const std::shared_ptr<ActionTagBeaconMessage>& tagbeacon);
            void HandleActionTagUseMessage(const std::shared_ptr<ActionTagUseMessage>& usetag);
            void HandleActionTagUseSuccessMessage(const std::shared_ptr<ActionTagUseSuccessMessage>& usetagsuccess);

            void HandleActionTagZoneAddMessage(const std::shared_ptr<ActionTagZoneAddMessage>& zoneadd);
            void HandleActionTagZoneRemoveMessage(const std::shared_ptr<ActionTagZoneRemoveMessage>& zoneremove);

            void HandlePlayerUpdate(const std::shared_ptr<BL::Tracker::UpdatePlayerTrackerMessage>& playerupdate);
            void HandleReplyPlayerZoneMessage(const std::shared_ptr<Processing::Router::ReplyPlayerZoneMessage>& playerzone);

            // Serialization
            TaggedEntities(const Shared::JSon::JSon& json);
            void SerializeToJSon(Shared::JSon::JSon& json) const override;
            static TaggedEntities ParseJSon(const Shared::JSon::JSon& json);

            // tracker id (no need to make it all the time....)
            std::shared_ptr<const Shared::Id> tracker;

            void SetOwner(Zone* creator) { owner = creator; }

        private:
            enum ActionTagState
            {
                START,

                TAGGED
            };

            enum ActionState
            {
                STARTED,
                FIRED
            };

            struct InternalActionData : public Shared::Serializable
            {
                InternalActionData() {}

                Action action;
                ActionTag actionTag;
                std::vector<std::shared_ptr<const ZoneId>> zones;
                ActionTagState tagState;
                ActionState actionState;

                // Serialization
                InternalActionData(const Shared::JSon::JSon& json);
                void SerializeToJSon(Shared::JSon::JSon& json) const override;
                static InternalActionData ParseJSon(const Shared::JSon::JSon& json);
            };

            struct InternalRemoteEntityData : public Shared::Serializable
            {
                InternalRemoteEntityData() {}

                int interest = 0;

                std::shared_ptr<const Processing::Router::RouterId> routerId;

                //WARNING: Call AFTER adding the new action to the waiting actions
                bool IsSearching() { return waitingActions.size() > 1; }

                std::vector<uint64_t> waitingActions;

                // Serialization
                InternalRemoteEntityData(const Shared::JSon::JSon& json);
                void SerializeToJSon(Shared::JSon::JSon& json) const override;
                static InternalRemoteEntityData ParseJSon(const Shared::JSon::JSon& json);
            };

            struct UnusedRemoteEntityData
            {
            public:
                void Add(int entity, InternalRemoteEntityData&& remoteEntityData);
                bool Contains(int entity);
                InternalRemoteEntityData Restore(int entity);

            private:
                void Limit(size_t limit);

                std::unordered_map<int, InternalRemoteEntityData> toExpireRemoteEntityData1;
                std::unordered_map<int, InternalRemoteEntityData> toExpireRemoteEntityData2;
            };

            void AddTagEntity(const int fromEntity, const int toEntity, ActionTag& actionTag, bool retry);
            void UseTagEntity(const uint64_t id);
            void RemoveTagEntity();

            void GetPlayerZone(const int player, InternalRemoteEntityData* const playerData);

            // this function may be called twice with the same action tag id
            // the second time will not do anything guaranteed
            // (this can be used to broadcast tag use messages and still being
            // sure that it only works ones)
            bool ProcessActionHit(const Action& action, const ActionTagId& id, InternalEntityInfo* const iInfo);

            inline void AddTagToOwnedEntity(InternalEntityInfo* entity, ActionTag& actionTag);

            void SendCurrentActionStateToZone(const uint64_t actionId,
                const InternalActionData& action, const std::shared_ptr<const ZoneId>& zoneId);
            void SendCurrentActionStateToAllZones(const uint64_t actionId);

            void ReduceInterestInRemoteEntity(int entity, InternalRemoteEntityData* remoteEntity);

            uint64_t GetNextUniqueId();

            Zone* owner;
            uint64_t uniqueId = 0;

            std::unordered_map<uint64_t, InternalActionData> actionData;
            std::unordered_map<int, InternalRemoteEntityData> remoteEntityData;

            UnusedRemoteEntityData unusedRemoteEntityData;
        };
    }
}

#endif
