#include "TaggedEntities.h"
#include "Zone.h"

#include "Processing/Router/RouterMessage.h"
#include "Balancing/Tracker/TrackerMessage.h"

namespace Processing
{
    namespace Zone
    {
        void TaggedEntities::StartAction(const int caster, InternalEntityInfo* const iInfo, const Action& action)
        {
            ActionTag at;

            at.actionId.counter = GetNextUniqueId();
            at.actionId.zoneId = std::static_pointer_cast<const ZoneId>(owner->GetID());

            at.range = action.range;
            at.source = caster;
            at.sourcePos = iInfo->pos;
            at.type = action.type;

            InternalActionData& iad = actionData[at.actionId.counter];
            iad.action = action;
            iad.actionTag = at;
            iad.tagState = ActionTagState::START;
            iad.actionState = ActionState::STARTED;
            iInfo->currentActionId = at.actionId.counter;

            std::shared_ptr<ActionStartMessage> actionStart(std::make_shared<ActionStartMessage>(caster, action, iInfo->lastHandledCounter));
            owner->ForwardMessageToInRange(std::bind(
                &Shared::Message::Message::MakeCopy<ActionStartMessage>, actionStart), iInfo->pos);

            SOLP_DEBUG(DEBUG_INTERACTION, "Starting spell %d", at.actionId.counter);

            AddTagEntity(caster, action.target, at, false);
        }

        void TaggedEntities::FinishAction(const int caster, InternalEntityInfo* const iInfo)
        {
            InternalActionData& iad = actionData[iInfo->currentActionId];

            std::shared_ptr<ActionFireMessage> actionFire(std::make_shared<ActionFireMessage>(caster, iad.action, iInfo->lastHandledCounter));
            owner->ForwardMessageToInRange(std::bind(
                &Shared::Message::Message::MakeCopy<ActionFireMessage>, actionFire), iInfo->pos);

            iad.actionState = ActionState::FIRED;
            iInfo->currentActionId = 0;

            SOLP_DEBUG(DEBUG_INTERACTION, "Finishing spell %d", iad.actionTag.actionId.counter);

            UseTagEntity(iad.actionTag.actionId.counter);
        }

        void TaggedEntities::AddEntity(InternalEntityInfo* const added)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            std::vector<uint64_t> completedActions;

            for (auto& tag : added->tags)
            {
                // in this case we do not have to send a message but we can add
                // ourself as the target zone directly in memory
                if (tag.actionId.zoneId->equals(owner->GetID()))
                {
                    // we do not add ourself as target zone, it is easier
                    // to just check if the target is in this zone instead
                    // of comparing our local zone id with the target zone id

                    //TODO: handle actions that try to set a tag on this entity
                    // now we just wait for a tag set failure message from another zone

                    auto iad = actionData.find(tag.actionId.counter);
                    
                    if (iad != actionData.end())
                    {
                        // if the action state is fired, we have to handle tag use right here and now
                        // actually we have to really handle it after this loop otherwise we invalidate
                        // our tag iterator
                        if (iad->second.actionState == ActionState::FIRED)
                        {
                            completedActions.push_back(tag.actionId.counter);

                            // we can also reduce the interest in the remote entity in this case
                            // we can only do that when the state is fired because we know
                            // that the action will be completely finished by the end
                            // of this function and hence we are not interested any more
                            // in getting for example the zone of this entity
                            ReduceInterestInRemoteEntity(iad->second.action.target, NULL);
                        }
                    }
                }
                else
                {
                    owner->LinkTo(tag.actionId.zoneId);

                    auto zoneAdd(Shared::Message::Message::MakeMessage<ActionTagZoneAddMessage>(owner->GetID(), tag.actionId.zoneId));
                    zoneAdd->counter = tag.actionId.counter;

                    owner->SendMessagePersistent(zoneAdd);
                }
            }

            for (const uint64_t counter : completedActions)
            {
                InternalActionData& iad = actionData[counter];
                ProcessActionHit(iad.action, iad.actionTag.actionId, added);
                actionData.erase(counter);
            }

            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");
        }

        void TaggedEntities::MoveEntity(const InternalEntityInfo* const mover, const glm::vec2& newPos)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            for (const auto& tag : mover->tags)
            {
                if (tag.actionId.zoneId->equals(owner->GetID()))
                {
                    // in this case we do nothing because the targetting is internal
                }
                else
                {
                    auto remoteEntityMovement(Shared::Message::Message::MakeMessage<ActionTagBeaconMessage>(owner->GetID(), tag.actionId.zoneId));
                    remoteEntityMovement->counter = tag.actionId.counter;
                    remoteEntityMovement->pos = newPos;

                    owner->SendMessagePersistent(remoteEntityMovement);
                }
            }
        }

        void TaggedEntities::RemoveEntity(const InternalEntityInfo* const remove)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            for (const auto& tag : remove->tags)
            {
                if (tag.actionId.zoneId->equals(owner->GetID()))
                {
                    // we did not add this zone as the target zone for this action
                    // so we have nothing to do here
                }
                else
                {
                    auto zoneRemove(Shared::Message::Message::MakeMessage<ActionTagZoneRemoveMessage>(owner->GetID(), tag.actionId.zoneId));
                    zoneRemove->counter = tag.actionId.counter;

                    owner->SendMessagePersistent(zoneRemove);

                    owner->LinkToRemove(tag.actionId.zoneId);
                }
            }
        }

        void TaggedEntities::AddTagToOwnedEntity(InternalEntityInfo* entity, ActionTag& actionTag)
        {
            SOLP_DEBUG(DEBUG_INTERACTION, "Adding tag to entity for spell %d", actionTag.actionId.counter);

            if (!actionTag.actionId.zoneId->equals(owner->GetID()))
            {
                owner->LinkTo(actionTag.actionId.zoneId);

                auto zoneAdd(Shared::Message::Message::MakeMessage<ActionTagZoneAddMessage>(owner->GetID(), actionTag.actionId.zoneId));
                zoneAdd->counter = actionTag.actionId.counter;

                owner->SendMessagePersistent(zoneAdd);
            }

            entity->tags.push_back(actionTag);
        }

        void TaggedEntities::AddTagEntity(const int fromEntity, const int toEntity, ActionTag& actionTag, bool retry)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            InternalRemoteEntityData* remoteEntity;
            if (remoteEntityData.count(toEntity) > 0)
            {
                remoteEntity = &remoteEntityData[toEntity];
            }
            else if (retry)
            {
                SOLP_ERROR("Retrying to add a tag to a remote entry but it does not yet exist.. terminating..");
                std::terminate();
            }
            else if (unusedRemoteEntityData.Contains(toEntity))
            {
                //TODO: maybe ignore the zone that we think the remote entity is in?
                remoteEntity = &remoteEntityData[toEntity];
                *remoteEntity = unusedRemoteEntityData.Restore(toEntity);
            }
            else
            {
                // makes a new entry and gives the pointer
                remoteEntity = &remoteEntityData[toEntity];
            }

            // only increase interest if not retrying...
            if (!retry)
                remoteEntity->interest++;

            // if toEntity is owned by this zone we can easily assign the tag to this entity
            // we can set the tag and return
            if (InternalEntityInfo* toEntityInfo = owner->GetEntity(toEntity, false))
            {
                actionData[actionTag.actionId.counter].tagState = ActionTagState::TAGGED;

                AddTagToOwnedEntity(toEntityInfo, actionTag);
            }
            else
            {
                // on failure we clear the waiting actions list, so readd waiting if needed
                remoteEntity->waitingActions.push_back(actionTag.actionId.counter);

                // if already searching for the entity we can just return now
                if (remoteEntity->IsSearching())
                    return;

                // otherwise this is the first action interested in this remote entity
                // and we need to figure out in what zone this remote entity is
                GetPlayerZone(toEntity, remoteEntity);
            }
        }

        void TaggedEntities::GetPlayerZone(const int player, InternalRemoteEntityData* const playerData)
        {
            // if we already "know" the router id we can immediately ask the router the location
            // else we have to ask the tracker first what router is handling this entity
            if (playerData->routerId)
            {
                auto routerMessage = Shared::Message::Message::MakeMessage<Processing::Router::GetPlayerZoneMessage>(owner->GetID(), playerData->routerId);
                routerMessage->player = player;
                owner->SendSingleMessageTo(routerMessage);
            }
            else
            {
                auto trackerMessage = Shared::Message::Message::MakeMessage<BL::Tracker::RequestPlayerTrackerMessage>(owner->GetID(), tracker);
                trackerMessage->playerId = player;
                owner->SendMessagePersistent(trackerMessage);
            }
        }

        void TaggedEntities::UseTagEntity(const uint64_t id)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            InternalActionData& iad = actionData[id];
            int target = iad.action.target;

            // if target is owned by this zone we can easily remove the tag from this entity
            // we can remove the tag, announce the hit and return
            if (InternalEntityInfo* targetEntityInfo = owner->GetEntity(target, false))
            {
                ReduceInterestInRemoteEntity(target, NULL);

                ProcessActionHit(iad.action, ActionTagId(
                    std::static_pointer_cast<const ZoneId>(owner->GetID()), id), targetEntityInfo);
                actionData.erase(id);

                SOLP_DEBUG(DEBUG_INTERACTION, "Used spell %d", id);

                return;
            }

            // otherwise we need to let the target zone know that the spell is fired
            // and we will then wait for a reply from that zone before deleting the
            // action state
            iad.actionState = ActionState::FIRED;
            SendCurrentActionStateToAllZones(id);
        }

        bool TaggedEntities::ProcessActionHit(const Action& action, const ActionTagId& id, InternalEntityInfo* const iInfo)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            // we allow the case that the tag is already used
            size_t startSize = iInfo->tags.size();

            iInfo->tags.erase(std::remove_if(iInfo->tags.begin(), iInfo->tags.end(), [&](const ActionTag& tag)
            {
                return tag.actionId.equals(id);
            }), iInfo->tags.end());

            // if this is the case the tag is already used so return
            if (iInfo->tags.size() == startSize)
                return false;

            std::shared_ptr<ActionHitMessage> actionHit(std::make_shared<ActionHitMessage>(
                action.source, action.target, action));
            owner->ForwardMessageToInRange(std::bind(
                &Shared::Message::Message::MakeCopy<ActionHitMessage>, actionHit), iInfo->pos);

            SOLP_DEBUG(DEBUG_INTERACTION, "Spell %d hit target", id.counter);

            return true;
        }

        void TaggedEntities::HandleActionTagSetMessage(const std::shared_ptr<ActionTagSetMessage>& settag)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            // we own this entity
            if (InternalEntityInfo* toEntityInfo = owner->GetEntity(settag->playerId, false))
            {
                AddTagToOwnedEntity(toEntityInfo, settag->actionTag);

                // we can return here because we send a zone add message anyway
                // based on that information we can assume the set was a success
                return;
            }

            auto reply(Shared::Message::Message::MakeMessage<ActionTagSetFailMessage>(owner->GetID(), settag->senderID));
            reply->actionTagId = settag->actionTag.actionId;
            reply->playerId = settag->playerId;

            owner->SendMessagePersistent(reply);
        }

        void TaggedEntities::HandleActionTagSetFailMessage(const std::shared_ptr<ActionTagSetFailMessage>& settagfail)
        {
            auto action = actionData.find(settagfail->actionTagId.counter);

            // if we do not have action data for this action id something went wrong..
            // we only send the set tag message to one zone at a time, so
            // the action cannot finish while still receiving this message
            if (action == actionData.end())
            {
                SOLP_ERROR("Unexpectedly received action tag set fail message but the action already finished... terminating...");
                std::terminate();
            }

            // on failure just retry adding the tag to the target entity
            AddTagEntity(action->second.action.source, action->second.action.target, action->second.actionTag, true);
        }

        void TaggedEntities::HandleActionTagBeaconMessage(const std::shared_ptr<ActionTagBeaconMessage>& tagbeacon)
        {
            //TODO: add approximated cost for LOS check
        }

        void TaggedEntities::HandleActionTagUseMessage(const std::shared_ptr<ActionTagUseMessage>& usetag)
        {
            // if we do not handle this entity just ignore the message
            // this is no problem because the spell casting zone will
            // sent this message to any zone claiming to handle this entity
            auto target = owner->GetEntity(usetag->action.target, false);
            if (!target)
                return;

            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            // if process action hit returns false the tag was not available
            // in this case we do not send a success message (it most likely
            // has been sent before by another zone handling this entity)
            // IMPORTANT: if we do not have the tag it means we also do not
            // have a link to this zone for this particular tag so we should
            // not delete any link
            if (!ProcessActionHit(usetag->action, usetag->actionTagId, target))
                return;

            SOLP_DEBUG(DEBUG_INTERACTION, "Remote used spell %d", usetag->actionTagId.counter);

            auto reply(Shared::Message::Message::MakeMessage<ActionTagUseSuccessMessage>(owner->GetID(), usetag->senderID));
            reply->actionTagId = usetag->actionTagId;

            owner->SendMessagePersistent(reply);
            owner->LinkToRemove(usetag->senderID);
        }

        void TaggedEntities::HandleActionTagUseSuccessMessage(const std::shared_ptr<ActionTagUseSuccessMessage>& usetagsuccess)
        {
            ReduceInterestInRemoteEntity(actionData[usetagsuccess->actionTagId.counter].action.target, NULL);
            actionData.erase(usetagsuccess->actionTagId.counter);
        }

        void TaggedEntities::HandleActionTagZoneAddMessage(const std::shared_ptr<ActionTagZoneAddMessage>& zoneadd)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            auto action = actionData.find(zoneadd->counter);

            // if action is already finished we can just ignore this message
            if (action == actionData.end())
                return;

            // we expect this message to be sent by another zone
            if (zoneadd->senderID->type != Shared::Id::ProcessType::CELL)
            {
                SOLP_ERROR("Zone add message sent by a non zone process.... terminating....");
                std::terminate();
            }

            // if this is the first zone added to this action then we we know the tag has been set
            // so we can change the tagstate and continue with the action handling
            if (action->second.tagState == ActionTagState::START)
                action->second.tagState = ActionTagState::TAGGED;

            action->second.zones.push_back(std::static_pointer_cast<const ZoneId>(zoneadd->senderID));
            SendCurrentActionStateToZone(zoneadd->counter, action->second, std::static_pointer_cast<const ZoneId>(zoneadd->senderID));
        }

        void TaggedEntities::HandleActionTagZoneRemoveMessage(const std::shared_ptr<ActionTagZoneRemoveMessage>& zoneremove)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            auto action = actionData.find(zoneremove->counter);

            // if action is already finished we can just ignore this message
            if (action == actionData.end())
                return;

            // we expect this message to be sent by another zone
            if (zoneremove->senderID->type != Shared::Id::ProcessType::CELL)
            {
                SOLP_ERROR("Zone add message sent by a non zone process.... terminating....");
                std::terminate();
            }

            action->second.zones.erase(std::remove_if(
                action->second.zones.begin(), action->second.zones.end(), [&](const std::shared_ptr<const ZoneId>& zone)
            {
                if (zoneremove->senderID->equals(zone))
                    return true;

                return false;
            }), action->second.zones.end());
        }

        void TaggedEntities::SendCurrentActionStateToZone(const uint64_t actionId, const InternalActionData& action, const std::shared_ptr<const ZoneId>& zoneId)
        {
            switch (action.actionState)
            {
            case ActionState::FIRED:
                {
                    std::shared_ptr<ActionTagUseMessage> tagUse(std::make_shared<ActionTagUseMessage>(
                        action.action, ActionTagId(std::static_pointer_cast<const ZoneId>(owner->GetID()), actionId)));
                    tagUse->senderID = owner->GetID();
                    tagUse->receiverID = zoneId;
                    owner->SendMessagePersistent(tagUse);
                }

                break;
            case ActionState::STARTED:
            default:
                break;
            }
        }

        void TaggedEntities::SendCurrentActionStateToAllZones(const uint64_t actionId)
        {
            const InternalActionData& iad = actionData[actionId];

            for (const auto& zone : iad.zones)
                SendCurrentActionStateToZone(actionId, iad, zone);
        }

        uint64_t TaggedEntities::GetNextUniqueId()
        {
            if (uniqueId == std::numeric_limits<uint64_t>::max())
                uniqueId = 0;
            else
                uniqueId++;

            return uniqueId;
        }

        void TaggedEntities::ReduceInterestInRemoteEntity(int entity, InternalRemoteEntityData* remoteEntity)
        {
            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            InternalRemoteEntityData* changedEntity = remoteEntity;

            if (!changedEntity)
            {
                if (remoteEntityData.count(entity) > 0)
                {
                    changedEntity = &remoteEntityData[entity];
                }
                else
                {
                    SOLP_ERROR("Reducing interest in remote entity but we were not interested in it to begin with... terminating..");
                    std::terminate();
                }
            }

            changedEntity->interest--;
            // if lost interest we move the data to the cache from which it will be deleted eventually
            // if it is not restored before then
            if (changedEntity->interest <= 0)
            {
                changedEntity->waitingActions.clear();
                unusedRemoteEntityData.Add(entity, std::move(*changedEntity));
                remoteEntityData.erase(entity);
            }
        }

        //TODO: make sure that this method can deal with actions being already completed
        void TaggedEntities::HandlePlayerUpdate(const std::shared_ptr<BL::Tracker::UpdatePlayerTrackerMessage>& playerupdate)
        {
            auto remoteEntity = remoteEntityData.find(playerupdate->playerId);
            // if data does not exist any more we can appareantly just ignore this message
            if (remoteEntity == remoteEntityData.end())
                return;

            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            if (playerupdate->found)
            {
                remoteEntity->second.routerId = playerupdate->router;

                // only if we do not currently own the entity will we continue with this
                // zone request, otherwise we continue with this method and let the
                // actions be readded so they can immediately finish
                if (!owner->GetEntity(playerupdate->playerId, false))
                {
                    GetPlayerZone(playerupdate->playerId, &remoteEntity->second);
                    return;
                }
            }

            std::vector<uint64_t> currentWaiting;
            currentWaiting.swap(remoteEntity->second.waitingActions);

            for (const auto waiting : currentWaiting)
            {
                auto action = actionData.find(waiting);

                // the action can already be finished if the remote entity connected to this zone
                if (action == actionData.end())
                    continue;

                // on failure just retry adding the tag to the target entity
                AddTagEntity(action->second.action.source, action->second.action.target, action->second.actionTag, true);
            }
        }

        void TaggedEntities::HandleReplyPlayerZoneMessage(const std::shared_ptr<Processing::Router::ReplyPlayerZoneMessage>& playerzone)
        {
            auto remoteEntity = remoteEntityData.find(playerzone->player);
            // if data does not exist any more we can appareantly just ignore this message
            if (remoteEntity == remoteEntityData.end())
                return;

            SOLP_DEBUG(DEBUG_TAGGED_ENTITIES, "");

            if (!playerzone->found)
            {
                remoteEntity->second.routerId = NULL;

                // only if we do not currently own the entity will we continue with this
                // zone request, otherwise we continue with this method and let the
                // actions be readded so they can immediately finish
                if (!owner->GetEntity(playerzone->player, false))
                {
                    GetPlayerZone(playerzone->player, &remoteEntity->second);
                    return;
                }
            }
            else if (!owner->GetEntity(playerzone->player, false))
            {
                // this result is only interesting if we do not now own the target entity
                // if we do own the target entity we just will readd all the tags and be
                // done with it

                // otherwise we now send set tag messages for all the waiting actions
                // only do this when we don't know the zone a player is in yet

                std::shared_ptr<const ZoneId> remoteZoneId(std::make_shared<const ZoneId>(playerzone->zoneX, playerzone->zoneY, playerzone->zonePhase));

                for (const auto waiting : remoteEntity->second.waitingActions)
                {
                    auto action = actionData.find(waiting);

                    // the action can already be finished if the remote entity connected to this zone
                    // if we are here the entity actually has already left this zone...
                    if (action == actionData.end())
                        continue;

                    auto actionTagSetMessage(Shared::Message::Message::MakeMessage<ActionTagSetMessage>(
                        owner->GetID(), remoteZoneId));
                    actionTagSetMessage->playerId = playerzone->player;
                    actionTagSetMessage->actionTag = action->second.actionTag;

                    owner->SendSingleMessageTo(actionTagSetMessage);
                }
                remoteEntity->second.waitingActions.clear();

                return;
            }

            std::vector<uint64_t> currentWaiting;
            currentWaiting.swap(remoteEntity->second.waitingActions);

            for (const auto waiting : currentWaiting)
            {
                auto action = actionData.find(waiting);

                // the action can already be finished if the remote entity connected to this zone
                if (action == actionData.end())
                    continue;

                // on failure just retry adding the tag to the target entity
                AddTagEntity(action->second.action.source, action->second.action.target, action->second.actionTag, true);
            }
        }

        TaggedEntities::TaggedEntities(const Shared::JSon::JSon& json)
        {
            GET_FROM_JSON_STRICT(uniqueId);
            GET_FROM_JSON_STRICT(actionData);
        }

        void TaggedEntities::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(uniqueId);
            ADD_TO_JSON(actionData);
        }

        TaggedEntities TaggedEntities::ParseJSon(const Shared::JSon::JSon& json)
        {
            return TaggedEntities(json);
        }

        TaggedEntities::InternalActionData::InternalActionData(const Shared::JSon::JSon& json)
        {
            GET_FROM_JSON_STRICT(action);
            GET_FROM_JSON_STRICT(actionTag);
            GET_FROM_JSON_STRICT(zones);
            GET_FROM_JSON_STRICT(tagState);
            GET_FROM_JSON_STRICT(actionState);
        }

        void TaggedEntities::InternalActionData::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(action);
            ADD_TO_JSON(actionTag);
            ADD_TO_JSON(zones);
            ADD_TO_JSON(tagState);
            ADD_TO_JSON(actionState);
        }

        TaggedEntities::InternalActionData TaggedEntities::InternalActionData::ParseJSon(const Shared::JSon::JSon& json)
        {
            return InternalActionData(json);
        }

        TaggedEntities::InternalRemoteEntityData::InternalRemoteEntityData(const Shared::JSon::JSon& json)
        {
            GET_FROM_JSON_STRICT(interest);
            GET_FROM_JSON_STRICT(routerId);
            GET_FROM_JSON_STRICT(waitingActions);
        }

        void TaggedEntities::InternalRemoteEntityData::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(interest);
            ADD_TO_JSON(routerId);
            ADD_TO_JSON(waitingActions);
        }

        TaggedEntities::InternalRemoteEntityData TaggedEntities::InternalRemoteEntityData::ParseJSon(const Shared::JSon::JSon& json)
        {
            return InternalRemoteEntityData(json);
        }

        void TaggedEntities::UnusedRemoteEntityData::Add(int entity, InternalRemoteEntityData&& remoteEntityData)
        {
            toExpireRemoteEntityData1[entity] = std::move(remoteEntityData);

            //TODO: don't use a magic number for the limit on the expired cache
            Limit(200);
        }

        bool TaggedEntities::UnusedRemoteEntityData::Contains(int entity)
        {
            return toExpireRemoteEntityData1.count(entity) > 0 ||
                toExpireRemoteEntityData2.count(entity) > 0;
        }

        //TODO: also we might want to consider time till an entry expires
        TaggedEntities::InternalRemoteEntityData TaggedEntities::UnusedRemoteEntityData::Restore(int entity)
        {
            if (toExpireRemoteEntityData1.count(entity) > 0)
            {
                InternalRemoteEntityData res = std::move(toExpireRemoteEntityData1[entity]);
                toExpireRemoteEntityData1.erase(entity);

                return res;
            }
            else if (toExpireRemoteEntityData2.count(entity) > 0)
            {
                InternalRemoteEntityData res = std::move(toExpireRemoteEntityData2[entity]);
                toExpireRemoteEntityData2.erase(entity);

                return res;
            }
            else
            {
                SOLP_ERROR("Trying to restore remote entity data from cache but the entity does not exist... terminating...");
                std::terminate();
            }
        }

        void TaggedEntities::UnusedRemoteEntityData::Limit(size_t limit)
        {
            if (toExpireRemoteEntityData1.size() > (limit / 2))
            {
                toExpireRemoteEntityData1.swap(toExpireRemoteEntityData2);
                toExpireRemoteEntityData1.clear();
            }
        }
    }
}
