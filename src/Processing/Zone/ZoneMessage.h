#ifndef SOLP_PROCESSING_ZONE_ZONEMESSAGE_H
#define SOLP_PROCESSING_ZONE_ZONEMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

#include "InternalEntityInfo.h"
#include "ZoneId.h"

namespace Processing
{
    namespace Zone
    {
        class ZoneMessage : public Shared::Message::Message
        {
        public:
            enum ZoneMessageType
            {
                ERASE_PLAYER,
                ADD_PLAYER,
                ADD_PLAYER_RESPONSE,
                UPDATE_PLAYER,
                ROUTER_CONNECT,
                ROUTER_DISCONNECT,
                ZONE_CELL_STATE_INIT,
                ENTITY_ZONE_TRANSFER,
                ENTITY_ZONE_TRANSFER_PUBLIC,
                CLAIM_ENTITY,
                ENTITY_BEACON,

                ACTION_TAG_SET,
                ACTION_TAG_SET_FAIL,
                ACTION_TAG_USE,
                ACTION_TAG_USE_SUCCESS,
                ACTION_TAG_BEACON,
                ACTION_TAG_ZONE_ADD,
                ACTION_TAG_ZONE_REMOVE,

                ACTION_START,
                ACTION_FIRE,
                ACTION_HIT,

                MAX_VALUE
            };

            ZoneMessage()
            {
                processType = Shared::Id::ProcessType::CELL;
            }

            ZoneMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(zoneMessageType);
            }

            ZoneMessageType zoneMessageType;

            static std::shared_ptr<ZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                ZoneMessageType zmt;
                json.QueryStrict("zoneMessageType", zmt);

                if (zmt >= ZoneMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds zone message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[zmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(zoneMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<ZoneMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class ErasePlayerMessage : public ZoneMessage
        {
        public:
            ErasePlayerMessage()
            {
                zoneMessageType = ZoneMessageType::ERASE_PLAYER;
            }

            ErasePlayerMessage(int id)
            {
                zoneMessageType = ZoneMessageType::ERASE_PLAYER;
                playerId = id;
            }

            int playerId;

            ErasePlayerMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
            }

            static std::shared_ptr<ErasePlayerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ErasePlayerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
            }
        };

        class AddPlayerMessage : public ZoneMessage
        {
        public:
            AddPlayerMessage()
            {
                zoneMessageType = ZoneMessageType::ADD_PLAYER;
            }

            AddPlayerMessage(int id, const EntityInfo& entityInfo, const uint64_t counter, std::shared_ptr<const Shared::Id>& routerId)
            {
                zoneMessageType = ZoneMessageType::ADD_PLAYER;
                playerId = id;
                pInfo = entityInfo;
                aCounter = counter;
                router = routerId;
            }

            int playerId;
            EntityInfo pInfo;
            uint64_t aCounter;
            std::shared_ptr<const Shared::Id> router;

            AddPlayerMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(pInfo);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
                MESSAGE_GET_FROM_JSON_STRICT(router);
            }

            static std::shared_ptr<AddPlayerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<AddPlayerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(pInfo);
                MESSAGE_ADD_TO_JSON(aCounter);
                MESSAGE_ADD_TO_JSON(router);
            }
        };

        class AddPlayerResponseMessage : public ZoneMessage
        {
        public:
            AddPlayerResponseMessage()
            {
                zoneMessageType = ZoneMessageType::ADD_PLAYER_RESPONSE;
            }

            AddPlayerResponseMessage(int player, const int startphase)
            {
                zoneMessageType = ZoneMessageType::ADD_PLAYER_RESPONSE;
                playerId = player;
                phase = startphase;
            }

            int playerId;
            int phase;

            AddPlayerResponseMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(phase);
            }

            static std::shared_ptr<AddPlayerResponseMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<AddPlayerResponseMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(phase);
            }
        };

        class UpdatePlayerStateMessage : public ZoneMessage
        {
        public:
            UpdatePlayerStateMessage()
            {
                zoneMessageType = ZoneMessageType::UPDATE_PLAYER;
            }

            UpdatePlayerStateMessage(int id, const EntityInfo& entityInfo, const uint64_t counter)
            {
                zoneMessageType = ZoneMessageType::UPDATE_PLAYER;
                playerId = id;
                pInfo = entityInfo;
                aCounter = counter;
            }

            int playerId;
            EntityInfo pInfo;
            uint64_t aCounter;

            UpdatePlayerStateMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(pInfo);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
            }

            static std::shared_ptr<UpdatePlayerStateMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<UpdatePlayerStateMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(pInfo);
                MESSAGE_ADD_TO_JSON(aCounter);
            }
        };

        class RouterConnectMessage : public ZoneMessage
        {
        public:
            RouterConnectMessage()
            {
                zoneMessageType = ZoneMessageType::ROUTER_CONNECT;
            }

            RouterConnectMessage(const int lCellX, const int lCellY, const uint64_t id)
            {
                zoneMessageType = ZoneMessageType::ROUTER_CONNECT;
                cellX = lCellX;
                cellY = lCellY;
                interestId = id;
            }

            RouterConnectMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(cellX);
                MESSAGE_GET_FROM_JSON_STRICT(cellY);
                MESSAGE_GET_FROM_JSON_STRICT(interestId);
            }

            int cellX;
            int cellY;
            uint64_t interestId;

            static std::shared_ptr<RouterConnectMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RouterConnectMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(cellX);
                MESSAGE_ADD_TO_JSON(cellY);
                MESSAGE_ADD_TO_JSON(interestId);
            }
        };

        class RouterDisconnectMessage : public ZoneMessage
        {
        public:
            RouterDisconnectMessage()
            {
                zoneMessageType = ZoneMessageType::ROUTER_DISCONNECT;
            }

            RouterDisconnectMessage(const int lCellX, const int lCellY)
            {
                zoneMessageType = ZoneMessageType::ROUTER_DISCONNECT;
                cellX = lCellX;
                cellY = lCellY;
            }

            RouterDisconnectMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(cellX);
                MESSAGE_GET_FROM_JSON_STRICT(cellY);
            }

            int cellX;
            int cellY;

            static std::shared_ptr<RouterDisconnectMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RouterDisconnectMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(cellX);
                MESSAGE_ADD_TO_JSON(cellY);
            }
        };

        class ZoneCellStateInitMessage : public ZoneMessage
        {
        public:
            ZoneCellStateInitMessage()
            {
                zoneMessageType = ZoneMessageType::ZONE_CELL_STATE_INIT;
            }

            ZoneCellStateInitMessage(std::unordered_map<int, EntityInfo>&& state,
                const std::shared_ptr<const ZoneId>& zid, const int x, const int y, const uint64_t uniqueInterestId, const uint64_t numberOfPhases)
            {
                zoneMessageType = ZoneMessageType::ZONE_CELL_STATE_INIT;
                entityStates = std::move(state);
                zoneId = zid;
                interestId = uniqueInterestId;
                numPhases = numberOfPhases;
                cellX = x;
                cellY = y;
            }

            std::unordered_map<int, EntityInfo> entityStates;
            std::shared_ptr<const ZoneId> zoneId;
            int cellX;
            int cellY;
            uint64_t interestId;
            uint64_t numPhases;

            ZoneCellStateInitMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(entityStates);
                MESSAGE_GET_FROM_JSON_STRICT(zoneId);
                MESSAGE_GET_FROM_JSON_STRICT(cellX);
                MESSAGE_GET_FROM_JSON_STRICT(cellY);
                MESSAGE_GET_FROM_JSON_STRICT(interestId);
                MESSAGE_GET_FROM_JSON_STRICT(numPhases);
            }

            static std::shared_ptr<ZoneCellStateInitMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ZoneCellStateInitMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(entityStates);
                MESSAGE_ADD_TO_JSON(zoneId);
                MESSAGE_ADD_TO_JSON(cellX);
                MESSAGE_ADD_TO_JSON(cellY);
                MESSAGE_ADD_TO_JSON(interestId);
                MESSAGE_ADD_TO_JSON(numPhases);
            }
        };

        class EntityZoneTransferMessage : public ZoneMessage
        {
        public:
            EntityZoneTransferMessage()
            {
                zoneMessageType = ZoneMessageType::ENTITY_ZONE_TRANSFER;
            }

            EntityZoneTransferMessage(const glm::vec2& lFrom, const int lFromPhase, const glm::vec2& lTo,
                const InternalEntityInfo& entity, const int player, const bool lForward)
            {
                zoneMessageType = ZoneMessageType::ENTITY_ZONE_TRANSFER;
                from = lFrom;
                fromPhase = lFromPhase;
                to = lTo;
                info = entity;
                playerId = player;
                forward = lForward;
            }

            glm::vec2 from;
            int fromPhase;
            glm::vec2 to;
            InternalEntityInfo info;
            int playerId;
            bool forward;

            EntityZoneTransferMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(from);
                MESSAGE_GET_FROM_JSON_STRICT(fromPhase);
                MESSAGE_GET_FROM_JSON_STRICT(to);
                MESSAGE_GET_FROM_JSON_STRICT(info);
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(forward);
            }

            static std::shared_ptr<EntityZoneTransferMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntityZoneTransferMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(from);
                MESSAGE_ADD_TO_JSON(fromPhase);
                MESSAGE_ADD_TO_JSON(to);
                MESSAGE_ADD_TO_JSON(info);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(forward);
            }
        };

        class EntityZoneTransferPublicMessage : public ZoneMessage
        {
        public:
            EntityZoneTransferPublicMessage()
            {
                zoneMessageType = ZoneMessageType::ENTITY_ZONE_TRANSFER_PUBLIC;
            }

            EntityZoneTransferPublicMessage(const glm::vec2& lFrom, const glm::vec2& lTo, const int lToPhase, const int player, const uint64_t counter, const bool alreadyClaimed)
            {
                zoneMessageType = ZoneMessageType::ENTITY_ZONE_TRANSFER_PUBLIC;
                from = lFrom;
                to = lTo;
                toPhase = lToPhase;
                playerId = player;
                aCounter = counter;
                claimed = alreadyClaimed;
            }

            glm::vec2 from;
            glm::vec2 to;
            int toPhase;
            int playerId;
            uint64_t aCounter;
            bool claimed;

            EntityZoneTransferPublicMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(from);
                MESSAGE_GET_FROM_JSON_STRICT(to);
                MESSAGE_GET_FROM_JSON_STRICT(toPhase);
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
                MESSAGE_GET_FROM_JSON_STRICT(claimed);
            }

            static std::shared_ptr<EntityZoneTransferPublicMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntityZoneTransferPublicMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(from);
                MESSAGE_ADD_TO_JSON(to);
                MESSAGE_ADD_TO_JSON(toPhase);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(aCounter);
                MESSAGE_ADD_TO_JSON(claimed);
            }
        };

        class ClaimEntityMessage : public ZoneMessage
        {
        public:
            ClaimEntityMessage()
            {
                zoneMessageType = ZoneMessageType::CLAIM_ENTITY;
            }

            ClaimEntityMessage(int id)
            {
                zoneMessageType = ZoneMessageType::CLAIM_ENTITY;
                playerId = id;
            }

            int playerId;

            ClaimEntityMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
            }

            static std::shared_ptr<ClaimEntityMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ClaimEntityMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
            }
        };

        class EntityBeaconMessage : public ZoneMessage
        {
        public:
            EntityBeaconMessage()
            {
                zoneMessageType = ZoneMessageType::ENTITY_BEACON;
            }

            EntityBeaconMessage(int id, const EntityInfo& info, const uint64_t counter)
            {
                zoneMessageType = ZoneMessageType::ENTITY_BEACON;
                playerId = id;
                pInfo = info;
                aCounter = counter;
            }

            int playerId;
            EntityInfo pInfo;
            uint64_t aCounter;

            EntityBeaconMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(pInfo);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
            }

            static std::shared_ptr<EntityBeaconMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntityBeaconMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(pInfo);
                MESSAGE_ADD_TO_JSON(aCounter);
            }
        };

        class ActionTagSetMessage : public ZoneMessage
        {
        public:
            ActionTagSetMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_SET;
            }

            ActionTagSetMessage(int id, const ActionTag& at)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_SET;
                playerId = id;
                actionTag = at;
            }

            int playerId;
            ActionTag actionTag;

            ActionTagSetMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(actionTag);
            }

            static std::shared_ptr<ActionTagSetMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagSetMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(actionTag);
            }
        };

        class ActionTagSetFailMessage : public ZoneMessage
        {
        public:
            ActionTagSetFailMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_SET_FAIL;
            }

            ActionTagSetFailMessage(int id, const ActionTagId& atId)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_SET_FAIL;
                playerId = id;
                actionTagId = atId;
            }

            int playerId;
            ActionTagId actionTagId;

            ActionTagSetFailMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(actionTagId);
            }

            static std::shared_ptr<ActionTagSetFailMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagSetFailMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(actionTagId);
            }
        };

        class ActionTagUseMessage : public ZoneMessage
        {
        public:
            ActionTagUseMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_USE;
            }

            ActionTagUseMessage(const Action& ac, const ActionTagId& atId)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_USE;
                action = ac;
                actionTagId = atId;
            }

            Action action;
            ActionTagId actionTagId;

            ActionTagUseMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(action);
                MESSAGE_GET_FROM_JSON_STRICT(actionTagId);
            }

            static std::shared_ptr<ActionTagUseMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagUseMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(action);
                MESSAGE_ADD_TO_JSON(actionTagId);
            }
        };

        class ActionTagUseSuccessMessage : public ZoneMessage
        {
        public:
            ActionTagUseSuccessMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_USE_SUCCESS;
            }

            ActionTagUseSuccessMessage(const Action& ac, const ActionTagId& atId)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_USE_SUCCESS;
                actionTagId = atId;
            }

            ActionTagId actionTagId;

            ActionTagUseSuccessMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(actionTagId);
            }

            static std::shared_ptr<ActionTagUseSuccessMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagUseSuccessMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(actionTagId);
            }
        };

        class ActionTagBeaconMessage : public ZoneMessage
        {
        public:
            ActionTagBeaconMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_BEACON;
            }

            ActionTagBeaconMessage(const int uniqueZoneId, const glm::vec2& newPos)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_BEACON;
                counter = uniqueZoneId;
                pos = newPos;
            }

            uint64_t counter;
            glm::vec2 pos;

            ActionTagBeaconMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(counter);
                MESSAGE_GET_FROM_JSON_STRICT(pos);
            }

            static std::shared_ptr<ActionTagBeaconMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagBeaconMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(counter);
                MESSAGE_ADD_TO_JSON(pos);
            }
        };

        class ActionTagZoneAddMessage : public ZoneMessage
        {
        public:
            ActionTagZoneAddMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_ZONE_ADD;
            }

            ActionTagZoneAddMessage(const uint64_t actionid)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_ZONE_ADD;
                counter = actionid;
            }

            uint64_t counter;

            ActionTagZoneAddMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(counter);
            }

            static std::shared_ptr<ActionTagZoneAddMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagZoneAddMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(counter);
            }
        };

        class ActionTagZoneRemoveMessage : public ZoneMessage
        {
        public:
            ActionTagZoneRemoveMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_ZONE_REMOVE;
            }

            ActionTagZoneRemoveMessage(const uint64_t actionid)
            {
                zoneMessageType = ZoneMessageType::ACTION_TAG_ZONE_REMOVE;
                counter = actionid;
            }

            uint64_t counter;

            ActionTagZoneRemoveMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(counter);
            }

            static std::shared_ptr<ActionTagZoneRemoveMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionTagZoneRemoveMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(counter);
            }
        };

        class ActionStartMessage : public ZoneMessage
        {
        public:
            ActionStartMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_START;
            }

            ActionStartMessage(const int player, const Action& ac, const uint64_t counter)
            {
                zoneMessageType = ZoneMessageType::ACTION_START;
                playerId = player;
                action = ac;
                aCounter = counter;
            }

            int playerId;
            Action action;
            uint64_t aCounter;

            ActionStartMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(action);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
            }

            static std::shared_ptr<ActionStartMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionStartMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(action);
                MESSAGE_ADD_TO_JSON(aCounter);
            }
        };

        class ActionFireMessage : public ZoneMessage
        {
        public:
            ActionFireMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_FIRE;
            }

            ActionFireMessage(const int caster, const Action& action, const uint64_t counter)
            {
                zoneMessageType = ZoneMessageType::ACTION_FIRE;
                playerId = caster;
                fire = action;
                aCounter = counter;
            }

            int playerId;
            Action fire;
            uint64_t aCounter;

            ActionFireMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(fire);
                MESSAGE_GET_FROM_JSON_STRICT(aCounter);
            }

            static std::shared_ptr<ActionFireMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionFireMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(fire);
                MESSAGE_ADD_TO_JSON(aCounter);
            }
        };

        class ActionHitMessage : public ZoneMessage
        {
        public:
            ActionHitMessage()
            {
                zoneMessageType = ZoneMessageType::ACTION_HIT;
            }

            ActionHitMessage(const int casterPlayer, const int targetPlayer, const Action& action)
            {
                zoneMessageType = ZoneMessageType::ACTION_HIT;
                caster = casterPlayer;
                target = targetPlayer;
                hit = action;
            }

            int caster;
            int target;
            Action hit;

            ActionHitMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(caster);
                MESSAGE_GET_FROM_JSON_STRICT(target);
                MESSAGE_GET_FROM_JSON_STRICT(hit);
            }

            static std::shared_ptr<ActionHitMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ActionHitMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(caster);
                MESSAGE_ADD_TO_JSON(target);
                MESSAGE_ADD_TO_JSON(hit);
            }
        };
    }
}

#endif
