#include "glm/geometric.hpp"

#include "Zone.h"
#include "ZoneId.h"
#include "TaggedEntities.h"

#include "Balancing/Zone/Zone.h"
#include "Balancing/Tracker/TrackerId.h"
#include "Balancing/Zone/ZoneMessage.h"
#include "Processing/Router/RouterMessage.h"
#include "Monitoring/Zone/Zone.h"
#include "Monitoring/Server/ServerMessage.h"
#include "Processing/ApproximatedCost.h"
#include "Shared/Util.h"
#include "Shared/Config.h"
#include "Shared/Singleton.h"
#include "Shared/System/System.h"

namespace Processing
{
    namespace Zone
    {
        Zone::Zone(int lX, int lY, int lPhase, Shared::Link::Moveable::State st) :
            Shared::Link::Moveable(st), ownX(lX), ownY(lY), ownPhase(lPhase),
            cellOffsetX(Shared::Util::ZoneXToCellXCoord(lX)),
            cellOffsetY(Shared::Util::ZoneYToCellYCoord(lY)), taggedEntities(this)
        {
                Init();
        }

        Zone::~Zone()
        {
            // cleanup
            for (auto& entity : entityMap)
                delete entity.second;
        }

        void Zone::Init()
        {
            id = std::make_shared<const ZoneId>(ownX, ownY, ownPhase);

            // wait till we init the id because we immediately start sending messages
            loadBalancer = std::make_shared<BL::Zone::Zone>(ownX, ownY, ownPhase, this);

            int sizeX = Shared::Util::ZoneXToCellXCoord(ownX + 1) - cellOffsetX + 1;
            int sizeY = Shared::Util::ZoneYToCellYCoord(ownY + 1) - cellOffsetY + 1;
            connectedRoutersPerCell.resize(sizeX);
            for (auto& vec : connectedRoutersPerCell)
                vec.resize(sizeY);
            cellToEntityMap.resize(sizeX);
            for (auto& vec : cellToEntityMap)
                vec.resize(sizeY);
        }

        //TODO: if we make this moveable we should not link again to the neighbouring zones etc
        int Zone::Run()
        {
            monitor = std::make_shared<Monitor::Zone::Zone>(ownX, ownY, ownPhase);
            Shared::Singleton::monitorHooks->Add(monitor);

            // Link to the local tracker so that we can get the router handling a particular entity
            taggedEntities.tracker = std::make_shared<const BL::Tracker::TrackerId>(Shared::Config::localIP);
            LinkTo(taggedEntities.tracker);

            //TODO: announce all subcells of the zone (for the splitting cell load partitioning)
            // ListenOn also announces the ip the tracker
            //TODO: what happens when the zone is moved very quickly?
            ListenOn(Shared::Id::ProcessType::CELL);
            if (Shared::Util::IsZoneInDirection(ownX, ownY, 1, 0))
                LinkTo(std::make_shared<const ZoneId>(ownX + 1, ownY,  0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, -1, 0))
                LinkTo(std::make_shared<const ZoneId>(ownX - 1, ownY, 0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, 0, 1))
                LinkTo(std::make_shared<const ZoneId>(ownX, ownY + 1, 0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, 0, -1))
                LinkTo(std::make_shared<const ZoneId>(ownX, ownY - 1, 0));

            if (Shared::Util::IsZoneInDirection(ownX, ownY, 1, 1))
                LinkTo(std::make_shared<const ZoneId>(ownX + 1, ownY + 1, 0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, -1, -1))
                LinkTo(std::make_shared<const ZoneId>(ownX - 1, ownY - 1, 0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, -1, 1))
                LinkTo(std::make_shared<const ZoneId>(ownX - 1, ownY + 1, 0));
            if (Shared::Util::IsZoneInDirection(ownX, ownY, 1, -1))
                LinkTo(std::make_shared<const ZoneId>(ownX + 1, ownY - 1, 0));

            RegisterWithMonitor();

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                // possibly we have to let the load balancer know that it's event has expired and that we can rebalance again if needed
                loadBalancer->FinishLoadRebalance();

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    if (auto putplayerat = std::dynamic_pointer_cast<UpdatePlayerStateMessage>(mess))
                        HandleUpdatePlayerMessage(putplayerat);
                    else if (auto addplayer = std::dynamic_pointer_cast<AddPlayerMessage>(mess))
                        HandleAddPlayerMessage(addplayer);
                    else if (auto routerconnect = std::dynamic_pointer_cast<RouterConnectMessage>(mess))
                        HandleRouterConnectMessage(routerconnect);
                    else if (auto routerdisconnect = std::dynamic_pointer_cast<RouterDisconnectMessage>(mess))
                        HandleRouterDisconnectMessage(routerdisconnect);
                    else if (auto entitytransfer = std::dynamic_pointer_cast<EntityZoneTransferMessage>(mess))
                        HandleEntityTransferMessage(entitytransfer);
                    else if (auto claimEntity = std::dynamic_pointer_cast<ClaimEntityMessage>(mess))
                        HandleClaimEntityMessage(claimEntity);
                    else if (auto zoneload = std::dynamic_pointer_cast<Monitor::Server::HookIDLoadResMessage>(mess))
                        loadBalancer->HandleLoadMessage(zoneload);
                    else if (auto entityRemovedFromPhase = std::dynamic_pointer_cast<BL::Zone::EntityRemovedFromPhaseMessage>(mess))
                        loadBalancer->HandleEntityRemovedFromPhaseMessage(entityRemovedFromPhase);
                    else if (auto phasemoveentities = std::dynamic_pointer_cast<BL::Zone::PhaseMoveEntitiesMessage>(mess))
                        loadBalancer->HandlePhaseMoveEntitiesMessage(phasemoveentities);
                    else if (auto phasemoveconfirm = std::dynamic_pointer_cast<BL::Zone::PhaseMoveConfirmMessage>(mess))
                        loadBalancer->HandlePhaseMoveConfirmMessage(phasemoveconfirm);
                    else if (auto tagset = std::dynamic_pointer_cast<ActionTagSetMessage>(mess))
                        taggedEntities.HandleActionTagSetMessage(tagset);
                    else if (auto tagsetfail = std::dynamic_pointer_cast<ActionTagSetFailMessage>(mess))
                        taggedEntities.HandleActionTagSetFailMessage(tagsetfail);
                    else if (auto taguse = std::dynamic_pointer_cast<ActionTagUseMessage>(mess))
                        taggedEntities.HandleActionTagUseMessage(taguse);
                    else if (auto tagusesuccess = std::dynamic_pointer_cast<ActionTagUseSuccessMessage>(mess))
                        taggedEntities.HandleActionTagUseSuccessMessage(tagusesuccess);
                    else if (auto tagbeacon = std::dynamic_pointer_cast<ActionTagBeaconMessage>(mess))
                        taggedEntities.HandleActionTagBeaconMessage(tagbeacon);
                    else if (auto tagzoneadd = std::dynamic_pointer_cast<ActionTagZoneAddMessage>(mess))
                        taggedEntities.HandleActionTagZoneAddMessage(tagzoneadd);
                    else if (auto tagzoneremove = std::dynamic_pointer_cast<ActionTagZoneRemoveMessage>(mess))
                        taggedEntities.HandleActionTagZoneRemoveMessage(tagzoneremove);
                    else if (auto playeraction = std::dynamic_pointer_cast<ActionStartMessage>(mess))
                        HandleActionStartMessage(playeraction);
                    else if (auto actionfire = std::dynamic_pointer_cast<ActionFireMessage>(mess))
                        HandleActionFireMessage(actionfire);
                    else if (auto playerupdate = std::dynamic_pointer_cast<BL::Tracker::UpdatePlayerTrackerMessage>(mess))
                        taggedEntities.HandlePlayerUpdate(playerupdate);
                    else if (auto playerzone = std::dynamic_pointer_cast<Processing::Router::ReplyPlayerZoneMessage>(mess))
                        taggedEntities.HandleReplyPlayerZoneMessage(playerzone);
                }

                // If server wants to move this zone we stop our loop
                // and inform the server that we are ready for the move.
                if (PreparingForMove())
                {
                    IsReadyForMove();
                    break;
                }
            }

            return 0;
        }

        void Zone::AddEntity(const int playerId, EntityInfo& entityInfo, const uint64_t counter, const bool phaseChange)
        {
            // deleted in RemoveEntity
            InternalEntityInfo* ef = new InternalEntityInfo(entityInfo);
            ef->lastHandledCounter = counter;
            
            UniversalAddEntityCode(playerId, ef, phaseChange);
        }

        void Zone::AddEntity(const int playerId, InternalEntityInfo& entityInfo, const bool phaseChange)
        {
            // deleted in RemoveEntity
            InternalEntityInfo* ef = new InternalEntityInfo(entityInfo);
            
            UniversalAddEntityCode(playerId, ef, phaseChange);
        }

        void Zone::UniversalAddEntityCode(const int playerId, InternalEntityInfo* ef, const bool phaseChange)
        {
            SOLP_DEBUG(DEBUG_ZONE_ADD_REMOVE, "");

            entityMap.emplace(playerId, ef);
            std::pair<int, int> cellCoords = Shared::Util::PositionToCellCoords(ef->pos);
            cellToEntityMap[cellCoords.first - cellOffsetX][cellCoords.second - cellOffsetY][playerId] = ef;

            taggedEntities.AddEntity(ef);

            // Monitoring
            monitor->handledEntities++;

            // if this is a phase change it was coordinated by the loadbalancer, in which case we don't have to
            // add this entity to the load balancer (because it is already added)
            // if this is not phase 0 we are also certain the load balancer is behind the placement, again
            // we don't have to do anything
            // handle load balancer decision which phase this entity should be added to
            // we only move entities to a different phase if they have been added to a phase
            // other than 0, this should not happen anyway
            if (!phaseChange && ownPhase == 0)
            {
                // if we add a entity to phase 0 we have to check if the load balancer might want to
                // add this particular entity to another phase, in which case we immediately start a transfer
                int entityPhase = loadBalancer->AddEntityToZone(playerId);
                PhaseMoveEntity(playerId, entityPhase, ef);
            }

            SOLP_DEBUG(DEBUG_ZONE_ADD_REMOVE, "");
        }

        //TODO: if entities can be declined by a zone we have to handle the case that the entity is currently in transfer
        // we should then wait with our final confirm till either the entity has succesfully moved out of this phase
        // or when he was declined and we still move the entity to the new phase
        bool Zone::PhaseMoveEntity(const int playerId, const int toPhase, InternalEntityInfo* ef)
        {
            InternalEntityInfo* entityInfo = ef;
            if (!entityInfo)
                entityInfo = GetEntity(playerId, false);

            if (!entityInfo)
                return true;

            if (ownPhase != toPhase)
            {
                //TODO: fix this... now long casts could slow down load partitioning....
                // cannot phase change entity if it is casting something
                // the zone balancer has to wait for the cast to complete
                if (entityInfo->currentActionId != 0)
                    return false;

                std::shared_ptr<EntityZoneTransferMessage> entityTransfer(
                    Shared::Message::Message::MakeMessage<EntityZoneTransferMessage>(id,
                    std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, toPhase)));
                entityTransfer->from = entityInfo->pos;
                entityTransfer->fromPhase = ownPhase;
                entityTransfer->to = entityInfo->pos;
                entityTransfer->playerId = playerId;
                entityTransfer->forward = true;

                entityInfo->transferState = InternalEntityInfo::InternalEntityInfoState::TRANSFER;
                entityTransfer->info = *entityInfo;

                // in theory there is a stable link between all phases but this function might
                // be called before the other phase links to us
                SendSingleMessageTo(entityTransfer);
            }

            return true;
        }

        void Zone::RemoveEntity(const int playerId, const bool phaseChange)
        {
            SOLP_DEBUG(DEBUG_ZONE_ADD_REMOVE, "");

            auto entity = entityMap.find(playerId);
            if (entity == entityMap.end())
            {
                SOLP_ERROR("Removing entity from zone but the entity is not found in the zone...");
                return;
            }

            if (!phaseChange)
                loadBalancer->RemoveEntityFromZone(playerId);

            taggedEntities.RemoveEntity(entity->second);

            std::pair<int, int> cellCoords = Shared::Util::PositionToCellCoords(entity->second->pos);
            cellToEntityMap[cellCoords.first - cellOffsetX][cellCoords.second - cellOffsetY].erase(playerId);
            delete entity->second;
            entityMap.erase(entity);

            // Monitoring
            monitor->handledEntities--;

            SOLP_DEBUG(DEBUG_ZONE_ADD_REMOVE, "");
        }

        void Zone::UpdateEntity(const int playerId, InternalEntityInfo* entity, const glm::vec2& newPos)
        {
            //TODO: maybe this does not work (floats!) maybe compare with epsilon???
            if (newPos != entity->pos)
                taggedEntities.MoveEntity(entity, newPos);

            std::pair<int, int> oldCellCoords = Shared::Util::PositionToCellCoords(entity->pos);
            std::pair<int, int> newCellCoords = Shared::Util::PositionToCellCoords(newPos);

            entity->pos = newPos;

            if (oldCellCoords.first == newCellCoords.first &&
                oldCellCoords.second == newCellCoords.second)
                return;

            cellToEntityMap[newCellCoords.first - cellOffsetX][newCellCoords.second - cellOffsetY][playerId] = entity;
            cellToEntityMap[oldCellCoords.first - cellOffsetX][oldCellCoords.second - cellOffsetY].erase(playerId);
        }

        void Zone::UpdateEntity(const int playerId, InternalEntityInfo* entity, EntityInfo& newInfo)
        {
            UpdateEntity(playerId, entity, newInfo.pos);
            entity->moving = newInfo.moving;
            entity->ori = newInfo.ori;
        }

        void Zone::ClaimEntity(const int playerId, std::shared_ptr<const Processing::Router::RouterId>& claimer)
        {
            auto find = zoneTransfers.find(playerId);
            if (find != zoneTransfers.end())
            {
                // if there is a zoneTransfer entry for this player then we know
                // that he has already moved on to a different zone and we need to
                // let the connecting router know
                auto transfer = find->second.front();
                find->second.pop_front();
                // if the zone transfer queue is empty now, we can delete the whole queue
                if (find->second.empty())
                    zoneTransfers.erase(find);

                transfer->receiverID = claimer;
                transfer->claimed = true;
                SendMessagePersistent(transfer);
            }
            else
            {
                // in this case the entity we are claiming is handled by this zone
                // we can just store the given router id for the given player id
                if (InternalEntityInfo* info = GetEntity(playerId, true))
                {
                    info->router = claimer;
                }
                else
                {
                    SOLP_ERROR("Claiming entity in zone, but he does not exist... terminating...");
                    std::terminate();
                }

                // we need to send a beacon to the router if it is not connected to
                // the cell containing the entity
                SendBeaconIfNeeded(playerId, false);
            }
        }

        void Zone::SendBeaconIfNeeded(const int playerId, bool skipCheck)
        {
            if (InternalEntityInfo* info = GetEntity(playerId, true))
            {
                if (!info->router)
                    return;

                if (!skipCheck)
                {
                    // we check here if the router is connected to the cell the player is in
                    // if so we can return otherwise we send a beacon
                    auto cellCoords(Shared::Util::PositionToCellCoords(info->pos));

                    for (const auto& router : connectedRoutersPerCell[cellCoords.first - cellOffsetX][cellCoords.second - cellOffsetY])
                        if (router.second.second->equals(info->router))
                            return;
                }

                auto beacon(std::make_shared<EntityBeaconMessage>(playerId, *((EntityInfo*)info), info->lastHandledCounter));
                beacon->senderID = id;
                beacon->receiverID = info->router;
                SendMessagePersistent(beacon);
            }
        }

        void Zone::HandleAddPlayerMessage(const std::shared_ptr<AddPlayerMessage>& addplayer)
        {
            if (ownPhase == 0)
            {
                int entityPhase = loadBalancer->AddEntityToZone(addplayer->playerId);

                if (entityPhase != ownPhase)
                {
                    auto copy = Shared::Message::Message::MakeCopy<AddPlayerMessage>(addplayer);
                    copy->senderID = id;
                    copy->receiverID = std::make_shared<ZoneId>(ownX, ownY, entityPhase);
                    SendSingleMessageTo(copy);

                    return;
                }
            }

            // let the router know by which zone the client is handled
            auto response = Shared::Message::Message::MakeMessage<AddPlayerResponseMessage>(id, addplayer->router);
            response->phase = ownPhase;
            response->playerId = addplayer->playerId;
            SendSingleMessageTo(response);

            AddEntity(addplayer->playerId, addplayer->pInfo, addplayer->aCounter, true);

            BroadcastEntityState(addplayer->playerId, addplayer->pInfo, addplayer->aCounter);

            if (addplayer->pInfo.moving)
            {
                SOLP_ERROR("Unexpected state for player that has just been added to the zone... terminating...");
                std::terminate();
            }
        }

        void Zone::HandleUpdatePlayerMessage(const std::shared_ptr<UpdatePlayerStateMessage>& playerup)
        {
            // if already exists we move the player
            // else we update the information
            if (InternalEntityInfo* info = GetEntity(playerup->playerId, false))
            {
                // we shouldn't handle messages twice and only inorder
                if (playerup->aCounter != info->lastHandledCounter + 1)
                    return;
                info->lastHandledCounter++;

                if (info->moving)
                {
                    // add approximated cost of a movement handler that does something useful
                    ApproximatedCost::ZoneMovementHandler();

                    // we assume the client does not lie to us, this is safe to assume in a testing environment
                    // to better simulate the workload we might want to do safety checks here anyway?
                    // a good way of dealing with client server time deviations would be to maintain
                    // an offset measure that is maintained for the whole session, this offset should be stable
                    // depending on the client server latency this offset can be higher or lower.
                    // This however does not deal with temporarily network hiccups or overload on the server...

                    // if we moved outside of this cell we have to communicate with the moved to cell to see
                    // if this movement is possible, the last movement update is then broadcast by this cell
                    // this way the router will know the move was a success
                    auto zoneCoords = Shared::Util::PositionToZoneCoords(playerup->pInfo.pos);
                    if (!id->equals(std::make_shared<const ZoneId>(zoneCoords.first, zoneCoords.second, ownPhase)))
                    {
                        auto transferZoneCoords = Shared::Util::PositionsToFirstZone(info->pos, playerup->pInfo.pos);
                        std::shared_ptr<const ZoneId> transferId = std::make_shared<const ZoneId>(
                            transferZoneCoords.first, transferZoneCoords.second, 0);

                        // we already move the player to our border (which we "know" is possible)
                        // and we set state to transfer, if the transfer fails we should possibly
                        // apply some effects to the transfering entity based on his border position
                        glm::vec2 border(GetBorderPoint(info->pos, playerup->pInfo.pos, std::static_pointer_cast<const ZoneId>(id)));
                        
                        std::shared_ptr<EntityZoneTransferMessage> entityTransfer(
                            Shared::Message::Message::MakeMessage<EntityZoneTransferMessage>(id, transferId));
                        entityTransfer->from = info->pos;
                        entityTransfer->fromPhase = ownPhase;
                        entityTransfer->to = playerup->pInfo.pos;
                        entityTransfer->playerId = playerup->playerId;
                        entityTransfer->forward = true;

                        playerup->pInfo.pos = border;
                        // update local state with border position as new position
                        UpdateEntity(playerup->playerId, info, playerup->pInfo);
                        info->transferState = InternalEntityInfo::InternalEntityInfoState::TRANSFER;

                        // set the entity info in the entity transfer message after we updated our local state
                        entityTransfer->info = *info;
                        // set the real towards point properly here
                        entityTransfer->info.pos = entityTransfer->to;

                        SendMessagePersistent(entityTransfer);
                        return;
                    }
                }

                // the position should be the same if the existing state was not moving
                glm::vec2 oldPos = info->pos;

                // moves the player to the correct cell if needed
                UpdateEntity(playerup->playerId, info, playerup->pInfo);

                BroadcastEntityState(playerup->playerId, playerup->pInfo, oldPos, playerup->aCounter);
            }
            else
                SOLP_DEBUG(DEBUG_ZONE_HANDLER, "Received update state message, but zone does not own the mover (id: %d)...", playerup->playerId);
        }

        void Zone::HandleActionStartMessage(const std::shared_ptr<Processing::Zone::ActionStartMessage>& actionstart)
        {
            if (InternalEntityInfo* info = GetEntity(actionstart->playerId, false))
            {
                // we shouldn't handle messages twice and only inorder
                if (actionstart->aCounter != info->lastHandledCounter + 1)
                    return;
                info->lastHandledCounter++;

                taggedEntities.StartAction(actionstart->playerId, info, actionstart->action);

                ApproximatedCost::SpellHandler();
            }
            else
                SOLP_DEBUG(DEBUG_ZONE_HANDLER, "Received action start message, but zone does not own the caster (id: %d)...", actionstart->playerId);
        }

        void Zone::HandleActionFireMessage(const std::shared_ptr<ActionFireMessage>& actionfire)
        {
            if (InternalEntityInfo* info = GetEntity(actionfire->playerId, false))
            {
                // we shouldn't handle messages twice and only inorder
                if (actionfire->aCounter != info->lastHandledCounter + 1)
                    return;
                info->lastHandledCounter++;

                taggedEntities.FinishAction(actionfire->playerId, info);
                loadBalancer->EntityFinishAction(actionfire->playerId);
            }
            else
                SOLP_DEBUG(DEBUG_ZONE_HANDLER, "Received action fire message, but zone does not own the caster (id: %d)...", actionfire->playerId);
        }

        //TODO: when decline is possible we should deal with the case where we are pushing back to the initial
        // zone, in this case we should not push back to phase 0, but to the original phase
        void Zone::HandleEntityTransferMessage(const std::shared_ptr<EntityZoneTransferMessage>& transfer)
        {
            auto senderID = std::static_pointer_cast<const ZoneId>(transfer->senderID);

            // first check if this is the final destination of this entity
            auto toCoords = Shared::Util::PositionToZoneCoords(transfer->to);
            // if going forward we can always set the phase to our own because only our entry phase (0) can send
            // them here and we can never decline the entity
            // otherwise we use an unused phase because we never decline an entry so never will change the toId
            std::shared_ptr<const ZoneId> toId = std::make_shared<const ZoneId>(toCoords.first, toCoords.second, transfer->forward ? ownPhase : std::numeric_limits<int>::max());
            auto fromCoords = Shared::Util::PositionToZoneCoords(transfer->from);
            std::shared_ptr<const ZoneId> fromId = std::make_shared<const ZoneId>(fromCoords.first, fromCoords.second, transfer->fromPhase);

            bool phaseChange = (toCoords.first == fromCoords.first && toCoords.second == fromCoords.second);

            transfer->info.transferState = InternalEntityInfo::InternalEntityInfoState::CONFIRMED;
            transfer->info.router = NULL;
            transfer->info.currentActionId = 0;

            if (transfer->forward)
            {
                if (toId->equals(id))
                {
                    SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Zone transfer reached end point.");

                    // if this is the end point we send a confirmation to the originating zone
                    auto transferCopy(Shared::Message::Message::MakeCopy<EntityZoneTransferMessage>(transfer));
                    transferCopy->senderID = id;
                    transferCopy->receiverID = fromId;
                    transferCopy->forward = false;

                    SendSingleMessageTo(transferCopy);

                    if (GetEntity(transfer->playerId, true))
                    {
                        // this is possible in the following scenario R1 -> R2 -> R3 -> R1
                        // if the connection between R1 and R2 is slow the transfer from R3 to R1
                        // might arrive at R1 before the transfer from R1 to R2 is confirmed
                        // for now we just crash because the chances of this happening are really slim
                        //TODO: fix this properly
                        SOLP_ERROR("Zone recieves entity transfer but already has a state for this entity... terminating...");
                        std::terminate();
                    }
                    else
                    {
                        // and we add the entity to our state
                        AddEntity(transfer->playerId, transfer->info, phaseChange);
                    }

                    BroadcastEntityState(transfer->playerId, transfer->info, transfer->info.lastHandledCounter);

                    //TODO: If it is possible that the client lies possibly the EntityInfo.pos has to be changed
                    // to reflect the actual location the entity moves to
                }
                else
                {
                    SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Zone transfer reached transit point.");

                    // otherwise we still have to send this message forward.
                    // we don't have to do collision checks here, because
                    // the map is empty and entities cannot collide
                    
                    // we have to calculate the border point from the previous zone to this one
                    // then we can get the new zone to transfer this message to
                    glm::vec2 border(GetBorderPoint(transfer->from, transfer->to, std::static_pointer_cast<const ZoneId>(id)));
                    auto transferZoneCoords = Shared::Util::PositionsToFirstZone(border, transfer->to);
                    std::shared_ptr<const ZoneId> transferId = std::make_shared<const ZoneId>(
                        transferZoneCoords.first, transferZoneCoords.second, 0);

                    auto transferCopy(Shared::Message::Message::MakeCopy<EntityZoneTransferMessage>(transfer));
                    transferCopy->senderID = id;
                    transferCopy->receiverID = transferId;

                    SendMessagePersistent(transferCopy);
                }
            }
            else
            {
                // this can happen if the zone after this zone on the path
                // has declined access, in this case the location is on the
                // border between both zones. This zone becomes the owner
                // and should send this to the original zone.
                if (toId->equals(id))
                {
                    glm::vec2 border(GetBorderPoint(transfer->from, transfer->to, std::static_pointer_cast<const ZoneId>(id)));
                    transfer->info.pos = border;
                    transfer->to = border;

                    const auto exists = GetEntity(transfer->playerId, true);
                    if (exists == NULL)
                    {
                        AddEntity(transfer->playerId, transfer->info, phaseChange);
                    }
                    else if (fromId != id)
                    {
                        // not possible atm because other zones will never block a transfer
                        //TODO: fix this properly when needed
                        SOLP_ERROR("Zone recieves entity transfer but already has a state for this entity.... terminating...");
                        std::terminate();
                    }

                    // if this is the end point we send a confirmation to the originating zone,
                    // unless we are already at the originating zone
                    if (!fromId->equals(id))
                    {
                        SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Zone transfer is moving backwards and is now accepted in a closer zone.");

                        auto transferCopy(Shared::Message::Message::MakeCopy<EntityZoneTransferMessage>(transfer));
                        transferCopy->senderID = id;
                        transferCopy->receiverID = fromId;

                        SendSingleMessageTo(transferCopy);

                        BroadcastEntityState(transfer->playerId, transfer->info, transfer->info.lastHandledCounter);
                    }
                    else
                    {
                        //TODO: set proper transfer state for the denied entity
                        //TODO: update state of the entity (move to border)
                        // also handle delayed actions if they would be implemented
                        SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Zone transfer is moving backwards and is now accepted in the original zone.");

                        // if the router is not connected, it did not know of this transfer in the first
                        // place so we don't inform it of the zone transfer
                        // either way we inform all routers of the new state
                        BroadcastEntityState(transfer->playerId, transfer->info, transfer->from, transfer->info.lastHandledCounter);
                    }
                }
                else if (fromId->equals(id))
                {
                    SOLP_DEBUG(DEBUG_ZONE_TRANSFER, "Zone transfer starting point again.");

                    // let all routers (listening on this cell) know of the zone transfer
                    // if the owning router is not listening on this cell we still send it
                    // if there is no owning router we store the message for when the player
                    // is claimed by one
                    bool sentToOwner = false;
                    const auto exists = GetEntity(transfer->playerId, true);
                    if (exists == NULL)
                    {
                        SOLP_ERROR("Zone that should have an entity state for a player unexpectedly does not have it... terminating...");
                        std::terminate();
                    }

                    std::shared_ptr<EntityZoneTransferPublicMessage> publicEntityTransfer(
                        std::make_shared<EntityZoneTransferPublicMessage>(transfer->from, transfer->to, senderID->phase,
                        transfer->playerId, transfer->info.lastHandledCounter, exists->router ? true : false));
                    publicEntityTransfer->senderID = id;

                    auto fromCellCoords = Shared::Util::PositionToCellCoords(transfer->from);
                    for (auto router : connectedRoutersPerCell[fromCellCoords.first - cellOffsetX][fromCellCoords.second - cellOffsetY])
                    {
                        if (exists->router)
                            if (exists->router->equals(router.second.second))
                                sentToOwner = true;

                        auto transferCopy(Shared::Message::Message::MakeCopy<EntityZoneTransferPublicMessage>(publicEntityTransfer));
                        transferCopy->receiverID = router.second.second;
                        SendMessagePersistent(transferCopy);
                    }

                    // if sent to owner is true we can stop caring about this entity
                    // and we delete its state
                    // if the message has not yet been send to the router we check if a
                    // router has claimed this entity in which case we directly send the
                    // message otherwise we store it as a zone transfer
                    if (!sentToOwner)
                    {
                        // router is known
                        if (exists->router)
                        {
                            publicEntityTransfer->receiverID = exists->router;
                            SendMessagePersistent(publicEntityTransfer);
                        }
                        else
                        {
                            // router is unknown so we just store the entity zone transfer
                            zoneTransfers[transfer->playerId].push_back(publicEntityTransfer);
                        }
                    }

                    // we know that the entity is not any more in this zone
                    // so we remove its state from this zone
                    // if the x and y stayed the same we know it is only a phase change
                    RemoveEntity(transfer->playerId, phaseChange);
                }
                else
                    SOLP_WARNING("Unexpected zone received entity transfer message...");
            }
        }

        void Zone::HandleClaimEntityMessage(const std::shared_ptr<ClaimEntityMessage>& claim)
        {
            auto router = std::dynamic_pointer_cast<const Processing::Router::RouterId>(claim->senderID);

            // non router trying to claim entity.. this should not happen...
            if (!router)
            {
                SOLP_ERROR("Non router trying to claim entity... shutdown...");
                std::terminate();
            }

            // we expect the entity to be handled by this zone
            // if not ClaimEntity with CTD
            ClaimEntity(claim->playerId, router);
        }

        void Zone::HandleRouterConnectMessage(const std::shared_ptr<RouterConnectMessage>& router)
        {
            if (auto routerId = std::dynamic_pointer_cast<const Processing::Router::RouterId>(router->senderID))
            {
                connectedRoutersPerCell[router->cellX - cellOffsetX][router->cellY - cellOffsetY][router->senderID->Serialize()] =
                    std::pair<std::pair<int, uint64_t>, std::shared_ptr<const Processing::Router::RouterId>>(
                    std::pair<int, uint64_t>(loadBalancer->GetActivePhases(), router->interestId), routerId);

                std::unordered_map<int, EntityInfo> data;
                // fill data
                for (auto& entityDat : cellToEntityMap[router->cellX - cellOffsetX][router->cellY - cellOffsetY])
                    if (entityDat.second->transferState == InternalEntityInfo::InternalEntityInfoState::CONFIRMED)
                        data.emplace(entityDat.first, *((EntityInfo*)entityDat.second));

                std::shared_ptr<ZoneCellStateInitMessage> init(std::make_shared<ZoneCellStateInitMessage>(
                    std::move(data), std::static_pointer_cast<const ZoneId>(id), router->cellX, router->cellY,
                    router->interestId, loadBalancer->GetActivePhases()));
                init->senderID = id;
                init->receiverID = router->senderID;
                SendMessagePersistent(init);
            }
            else
            {
                SOLP_ERROR("Received a router connect message from a non router..... terminating....");
                std::terminate();
            }
        }

        void Zone::HandleRouterDisconnectMessage(const std::shared_ptr<RouterDisconnectMessage>& router)
        {
            connectedRoutersPerCell[router->cellX - cellOffsetX][router->cellY - cellOffsetY].erase(router->senderID->Serialize());
        }

        void Zone::SetActivePhases(const int activePhases)
        {
            int cellX = cellOffsetX;

            for (auto& routersX : connectedRoutersPerCell)
            {
                int cellY = cellOffsetY;

                for (auto& routersXY : routersX)
                {
                    for (auto& routerXY : routersXY)
                    {
                        int& currentKnownPhases = routerXY.second.first.first;

                        if (currentKnownPhases >= activePhases)
                            continue;

                        currentKnownPhases = activePhases;

                        auto phaseUpdate(Shared::Message::Message::MakeMessage<ZoneCellStateInitMessage>(id, routerXY.second.second));
                        phaseUpdate->zoneId = std::static_pointer_cast<const ZoneId>(id);
                        phaseUpdate->interestId = routerXY.second.first.second;
                        phaseUpdate->numPhases = activePhases;
                        phaseUpdate->cellX = cellX;
                        phaseUpdate->cellY = cellY;
                        SendMessagePersistent(phaseUpdate);
                    }

                    cellY++;
                }

                cellX++;
            }
        }

        //TODO: this can possibly go wrong when the line goes through a corner of the rectangle
        // if the distance traveled through this rectangle is really small this border point might
        // end up in a different zone....
        glm::vec2 Zone::GetBorderPoint(const glm::vec2& from, const glm::vec2& to, const std::shared_ptr<const ZoneId>& fromZone)
        {
            glm::vec2 posInZone = Shared::Util::PositionOnLineInZone(from, to, fromZone->x, fromZone->y);
            auto inZonePosZone = Shared::Util::PositionToZoneCoords(posInZone);
            if (inZonePosZone.first != fromZone->x || inZonePosZone.second != fromZone->y)
            {
                SOLP_ERROR("PositionOnLineInZone returned a position not in the requested zone.... terminating....");
                SOLP_ERROR("Moving from: (%f, %f), to: (%f, %f), in zone: (%d, %d).", from.x, from.y, to.x, to.y, fromZone->x, fromZone->y);
                std::terminate();
            }

            std::pair<int, int> nextZone = Shared::Util::PositionsToFirstZone(posInZone, to);
            glm::vec2 ori = to - from;
            glm::vec2 res;

            if (nextZone.first == fromZone->x)
            {
                if (nextZone.second == fromZone->y)
                {
                    SOLP_ERROR("Zone transfering to has same coordinates as our zone.... terminating....");
                    std::terminate();
                }

                float y = (nextZone.second > fromZone->y) ?
                    (static_cast<float>(nextZone.second * Shared::Config::minZoneEdgeLength) - Shared::Util::MapCoordEpsilon()) :
                    (static_cast<float>(fromZone->y * Shared::Config::minZoneEdgeLength) + Shared::Util::MapCoordEpsilon());

                float x = (((y - from.y) / ori.y) * ori.x) + from.x;
                res = glm::vec2(x, y);
            }
            else
            {
                float x = (nextZone.first > fromZone->x) ?
                    (static_cast<float>(nextZone.first * Shared::Config::minZoneEdgeLength) - Shared::Util::MapCoordEpsilon()) :
                    (static_cast<float>(fromZone->x * Shared::Config::minZoneEdgeLength) + Shared::Util::MapCoordEpsilon());

                float y = (((x - from.x) / ori.x) * ori.y) + from.y;
                res = glm::vec2(x, y);
            }

            // this function may fail when passing edges.... in this case just use a working point in the zone...
            auto resultZone = Shared::Util::PositionToZoneCoords(res);
            if (resultZone.first != fromZone->x || resultZone.second != fromZone->y)
                return posInZone;
            else
                return res;
        }

        void Zone::BroadcastEntityState(int playerId, const EntityInfo& info, const uint64_t counter)
        {
            std::shared_ptr<UpdatePlayerStateMessage> broadcast(std::make_shared<UpdatePlayerStateMessage>(playerId, info, counter));
            broadcast->senderID = id;

            std::shared_ptr<const Processing::Router::RouterId> owner;
            if (InternalEntityInfo* info = GetEntity(playerId, true))
                owner = info->router;

            bool sentToOwner = false;

            auto toCellCoords = Shared::Util::PositionToCellCoords(info.pos);
            for (auto router : connectedRoutersPerCell[toCellCoords.first - cellOffsetX][toCellCoords.second - cellOffsetY])
            {
                if (owner)
                    if (owner->equals(router.second.second))
                        sentToOwner = true;

                std::shared_ptr<UpdatePlayerStateMessage> copy(Shared::Message::Message::MakeCopy<UpdatePlayerStateMessage>(broadcast));
                copy->receiverID = router.second.second;
                SendMessagePersistent(copy);
            }

            if (!sentToOwner)
                SendBeaconIfNeeded(playerId, true);
        }

        void Zone::ForwardMessageToInRange(std::function<std::shared_ptr<Shared::Message::Message>()> copyMaker, const glm::vec2& pos)
        {
            auto toCellCoords = Shared::Util::PositionToCellCoords(pos);

            for (auto router : connectedRoutersPerCell[toCellCoords.first - cellOffsetX][toCellCoords.second - cellOffsetY])
            {
                std::shared_ptr<Shared::Message::Message> copy(copyMaker());
                copy->senderID = id;
                copy->receiverID = router.second.second;
                SendMessagePersistent(copy);
            }
        }

        void Zone::BroadcastEntityState(int playerId, const EntityInfo& info, const glm::vec2& oldPos, const uint64_t counter)
        {
            auto fromCellCoords = Shared::Util::PositionToCellCoords(oldPos);
            auto toCellCoords = Shared::Util::PositionToCellCoords(info.pos);

            // if same cell ignore the old pos
            if (fromCellCoords.first == toCellCoords.first && fromCellCoords.second == toCellCoords.second)
            {
                BroadcastEntityState(playerId, info, counter);
                return;
            }

            std::shared_ptr<UpdatePlayerStateMessage> broadcast(std::make_shared<UpdatePlayerStateMessage>(playerId, info, counter));
            broadcast->senderID = id;

            std::shared_ptr<const Processing::Router::RouterId> owner;
            if (InternalEntityInfo* info = GetEntity(playerId, true))
                owner = info->router;

            bool sentToOwner = false;

            for (auto router : connectedRoutersPerCell[toCellCoords.first - cellOffsetX][toCellCoords.second - cellOffsetY])
            {
                if (owner && owner->equals(router.second.second))
                    sentToOwner = true;

                std::shared_ptr<UpdatePlayerStateMessage> copy(Shared::Message::Message::MakeCopy<UpdatePlayerStateMessage>(broadcast));
                copy->receiverID = router.second.second;
                SendMessagePersistent(copy);
            }

            for (auto router : connectedRoutersPerCell[fromCellCoords.first - cellOffsetX][fromCellCoords.second - cellOffsetY])
            {
                // only send to routers not connected to the target cell
                if (connectedRoutersPerCell[toCellCoords.first - cellOffsetX][toCellCoords.second - cellOffsetY].find(router.first) !=
                    connectedRoutersPerCell[toCellCoords.first - cellOffsetX][toCellCoords.second - cellOffsetY].end())
                    continue;

                std::shared_ptr<ErasePlayerMessage> eraseMessage(std::make_shared<ErasePlayerMessage>(playerId));
                eraseMessage->senderID = id;
                eraseMessage->receiverID = router.second.second;
                SendMessagePersistent(eraseMessage);
            }

            if (!sentToOwner)
                SendBeaconIfNeeded(playerId, true);
        }

        Zone::Zone(const Shared::JSon::JSon& json) : Shared::Link::Moveable(json)
        {
            GET_FROM_JSON_STRICT(ownX);
            GET_FROM_JSON_STRICT(ownY);
            GET_FROM_JSON_STRICT(ownPhase);
            GET_FROM_JSON_STRICT(connectedRoutersPerCell);
            GET_FROM_JSON_STRICT(zoneTransfers);
            cellOffsetX = Shared::Util::ZoneXToCellXCoord(ownX);
            cellOffsetY = Shared::Util::ZoneXToCellXCoord(ownY);

            Init();

            GET_FROM_JSON_STRICT(taggedEntities);
            taggedEntities.SetOwner(this);

            std::unordered_map<int, InternalEntityInfo> data;
            GET_FROM_JSON_STRICT(data);

            GET_FROM_JSON_STRICT(loadBalancer);
            loadBalancer->SetOwner(this);
            loadBalancer->AfterMove();

            for (auto& entity : data)
            {
                InternalEntityInfo* ef = new InternalEntityInfo(std::move(entity.second));
                entityMap.emplace(entity.first, ef);
                std::pair<int, int> cellCoords = Shared::Util::PositionToCellCoords(ef->pos);
                cellToEntityMap[cellCoords.first - cellOffsetX][cellCoords.second - cellOffsetY][entity.first] = ef;

                // Monitoring
                monitor->handledEntities++;
            }
        }

        void Zone::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            Shared::Link::Linkable::SerializeToJSon(json);
            ADD_TO_JSON(ownX);
            ADD_TO_JSON(ownY);
            ADD_TO_JSON(ownPhase);
            ADD_TO_JSON(connectedRoutersPerCell);
            ADD_TO_JSON(zoneTransfers);
            ADD_TO_JSON(taggedEntities);

            std::unordered_map<int, InternalEntityInfo> data;
            for (auto& entity : entityMap)
                data.emplace(entity.first, std::move(*entity.second));

            ADD_TO_JSON(data);
            ADD_TO_JSON(loadBalancer);
        }

        std::shared_ptr<Zone> Zone::ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<Zone>(json);
        }
    }
}
