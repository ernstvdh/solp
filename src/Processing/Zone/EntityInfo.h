#ifndef SOLP_PROCESSING_ZONE_ENTITYINFO_H
#define SOLP_PROCESSING_ZONE_ENTITYINFO_H

#include "glm/vec2.hpp"

#include "Action.h"

#include "Shared/Serializable.h"

namespace Processing
{
    namespace Zone
    {
        struct EntityInfo : public Shared::Serializable
        {
            EntityInfo() : moving(false) {}
            EntityInfo(const glm::vec2& lPos, const glm::vec2& lOri, bool lMoving)
            {
                pos = lPos;
                ori = lOri;
                moving = lMoving;
            }
            EntityInfo(const Shared::JSon::JSon& json)
            {
                GET_FROM_JSON_STRICT(pos);
                GET_FROM_JSON_STRICT(ori);
                GET_FROM_JSON_STRICT(moving);
            }
            ~EntityInfo() {}

            glm::vec2 pos;
            glm::vec2 ori;
            bool moving;

            //TODO: what am I going to do with serialize? should use json IMO.
            std::string Serialize() const { return "NOT_IMPLEMENTED"; }

            void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ADD_TO_JSON(pos);
                ADD_TO_JSON(ori);
                ADD_TO_JSON(moving);
            }

            static EntityInfo ParseJSon(const Shared::JSon::JSon& json)
            {
                return EntityInfo(json);
            }
        };
    }
}

#endif