#include "Tracker.h"

#include "Shared/Config.h"
#include "Shared/Singleton.h"

#include <thread>
#include <algorithm>

namespace BL
{
    namespace Tracker
    {
        Tracker::Tracker()
        {
            id = std::make_shared<const TrackerId>(Shared::Config::localIP);

            isGlobalTracker = false;
            for (std::string& ip : Shared::Config::trackerIPs)
            {
                if (Shared::Config::localIP == ip)
                {
                    isGlobalTracker = true;
                    break;
                }
            }

            // we choose a random tracker if we are not one ourself
            if (!isGlobalTracker)
            {
                std::vector<std::string> trackers = Shared::Config::trackerIPs;
                std::random_shuffle(trackers.begin(), trackers.end());
                globalTrackerId = std::make_shared<const TrackerId>(trackers[0]);
            }

        }

        const std::string* Tracker::GetValue(const std::string& key) const
        {
            auto it = localMapping.find(key);

            if (it != localMapping.end())
                return &(it->second);
            else
                return NULL;
        }

        int Tracker::Run()
        {
            ListenOn(Shared::Id::ProcessType::ZONE_SERVER_TRACKER);
            RegisterWithMonitor();

            if (!isGlobalTracker)
                LinkTo(globalTrackerId);

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    if (auto request = std::dynamic_pointer_cast<RequestInfoTrackerMessage>(mess))
                        HandleRequest(request);
                    else if (auto update = std::dynamic_pointer_cast<UpdateTrackerMessage>(mess))
                        HandleUpdate(update);
                    else if (auto playerrequest = std::dynamic_pointer_cast<RequestPlayerTrackerMessage>(mess))
                        HandlePlayerRequest(playerrequest);
                    else if (auto playerupdate = std::dynamic_pointer_cast<UpdatePlayerTrackerMessage>(mess))
                        HandlePlayerUpdate(playerupdate);
                }
            }

            return 0;
        }

        void Tracker::HandleRequest(const std::shared_ptr<RequestInfoTrackerMessage>& request)
        {
            SOLP_DEBUG(DEBUG_TRACKER, "Ip for id (%s) requested of tracker.", request->requestedId->Serialize().c_str());

            // if we are not a global tracker we request the information from a random global tracker
            // we might still have the information in our cash, which we return if we have it
            if (!isGlobalTracker)
                SendMessagePersistent(request);

            // if we have the information we immediately reply otherwise we store the request
            if (const std::string* result = GetValue(request->requestedId->Serialize()))
            {
                std::shared_ptr<UpdateTrackerMessage> reply(std::make_shared<UpdateTrackerMessage>(request->requestedId, *result));
                reply->receiverID = request->senderID;
                reply->senderID = id;
                SendMessagePersistent(reply);
            }
            else
                toReplyTo[request->requestedId->Serialize()].push_back(request->senderID);
        }

        void Tracker::HandleUpdate(const std::shared_ptr<UpdateTrackerMessage>& update)
        {
            SOLP_DEBUG(DEBUG_TRACKER, "Received id to update (%s).", update->updateId->Serialize().c_str());

            localMapping[update->updateId->Serialize()] = update->newIp;

            // if we had open request answer them now
            auto it = toReplyTo.find(update->updateId->Serialize());
            if (it != toReplyTo.end())
            {
                for (std::shared_ptr<const Shared::Id>& requester : it->second)
                {
                    std::shared_ptr<UpdateTrackerMessage> reply(std::make_shared<UpdateTrackerMessage>(update->updateId, update->newIp));
                    reply->receiverID = requester;
                    reply->senderID = id;
                    SendMessagePersistent(reply);
                }

                toReplyTo.erase(it);
            }
        }

        void Tracker::HandlePlayerRequest(const std::shared_ptr<RequestPlayerTrackerMessage>& playerrequest)
        {
            auto find = playerToRouter.find(playerrequest->playerId);
            auto reply = Shared::Message::Message::MakeMessage<UpdatePlayerTrackerMessage>(id, playerrequest->senderID);
            reply->playerId = playerrequest->playerId;

            if (find == playerToRouter.end())
            {
                reply->found = false;
                reply->router = std::make_shared<const Processing::Router::RouterId>(0, "");
            }
            else
            {
                reply->found = true;
                reply->router = find->second;
            }

            SendSingleMessageTo(reply);
        }

        void Tracker::HandlePlayerUpdate(const std::shared_ptr<UpdatePlayerTrackerMessage>& playerupdate)
        {
            playerToRouter[playerupdate->playerId] = playerupdate->router;
        }
    }
}
