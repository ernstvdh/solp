#ifndef SOLP_BALANCING_TRACKER_TRACKER_H
#define SOLP_BALANCING_TRACKER_TRACKER_H

#include <unordered_map>
#include <utility>
#include <memory>
#include <mutex>

#include "TrackerId.h"
#include "TrackerMessage.h"

#include "Shared/Link/Linkable.h"
#include "Shared/Config.h"

namespace Processing
{
    namespace Router
    {
        class RouterId;
    }
}

namespace BL
{
    namespace Tracker
    {
        class Tracker : public Shared::Link::Linkable
        {
        public:
            Tracker();
            ~Tracker() {}

            int Run();

        private:
            void HandleRequest(const std::shared_ptr<RequestInfoTrackerMessage>& request);
            void HandleUpdate(const std::shared_ptr<UpdateTrackerMessage>& update);

            // This map contains keys of particular remotes and the
            // remote (ip, port). If this server is assigned to be a
            // tracker it will contain key value for all keys in the
            // distributed system, otherwise it is a cache.
            std::unordered_map<std::string, std::string> localMapping;

            // this map contains open requests
            std::unordered_map<std::string, std::vector<std::shared_ptr<const Shared::Id>>> toReplyTo;

            // this may only be called from the tracker thread, sequential use only
            // returns a reference because it will stay valid (single thread)
            const std::string* GetValue(const std::string& key) const;

            //TODO: store the number of cache hits/misses
            int hits;
            int misses;

            bool isGlobalTracker;

            std::shared_ptr<const TrackerId> globalTrackerId;

            //TODO: make entity finding distributed?????
            void HandlePlayerRequest(const std::shared_ptr<RequestPlayerTrackerMessage>& playerrequest);
            void HandlePlayerUpdate(const std::shared_ptr<UpdatePlayerTrackerMessage>& playerupdate);
            std::unordered_map<int, std::shared_ptr<const Processing::Router::RouterId>> playerToRouter;
        };
    }
}


#endif
