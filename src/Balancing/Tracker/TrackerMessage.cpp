#include "TrackerMessage.h"

namespace BL
{
    namespace Tracker
    {
        const std::vector<std::function<std::shared_ptr<TrackerMessage>(const Shared::JSon::JSon&)>> TrackerMessage::enumToMessageMap
        {
            UpdateTrackerMessage::ParseJSon,
            RequestInfoTrackerMessage::ParseJSon,
            UpdatePlayerTrackerMessage::ParseJSon,
            RequestPlayerTrackerMessage::ParseJSon
        };
    }
}
