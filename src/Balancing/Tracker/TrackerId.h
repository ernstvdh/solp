#ifndef SOLP_BALANCING_TRACKER_TRACKERID_H
#define SOLP_BALANCING_TRACKER_TRACKERID_H

#include "Shared/Id.h"

namespace BL
{
    namespace Tracker
    {
        class TrackerId : public Shared::IpId
        {
        public:
            TrackerId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::ZONE_SERVER_TRACKER) {}
            TrackerId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::ZONE_SERVER_TRACKER) {}
            ~TrackerId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<TrackerId>(json);
            }
        };
    }
}

#endif
