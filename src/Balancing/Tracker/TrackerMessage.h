#ifndef SOLP_BALANCING_TRACKER_TRACKERMESSAGE_H
#define SOLP_BALANCING_TRACKER_TRACKERMESSAGE_H

#include "Shared/Message/Message.h"

#include "Processing/Router/RouterId.h"

namespace BL
{
    namespace Tracker
    {
        class TrackerMessage : public Shared::Message::Message
        {
        public:
            enum TrackerMessageType
            {
                UPDATE,
                REQUEST,
                PLAYER_UPDATE,
                PLAYER_REQUEST,

                MAX_VALUE
            };

            TrackerMessage()
            {
                processType = Shared::Id::ProcessType::ZONE_SERVER_TRACKER;
            }

            TrackerMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(trackerMessageType);
            }

            TrackerMessageType trackerMessageType;

            static std::shared_ptr<TrackerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                TrackerMessageType tmt;
                json.QueryStrict("trackerMessageType", tmt);

                if (tmt >= TrackerMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds tracker message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[tmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(trackerMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<TrackerMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };


        class UpdateTrackerMessage : public TrackerMessage
        {
        public:
            UpdateTrackerMessage()
            {
                trackerMessageType = TrackerMessageType::UPDATE;
            }

            UpdateTrackerMessage(const std::shared_ptr<const Shared::Id>& id, const std::string& ip)
            {
                trackerMessageType = TrackerMessageType::UPDATE;
                updateId = id;
                newIp = ip;
            }

            UpdateTrackerMessage(const Shared::JSon::JSon& json) : TrackerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(updateId);
                MESSAGE_GET_FROM_JSON_STRICT(newIp);
            }

            std::shared_ptr<const Shared::Id> updateId;
            std::string newIp;

            static std::shared_ptr<UpdateTrackerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<UpdateTrackerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                TrackerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(updateId);
                MESSAGE_ADD_TO_JSON(newIp);
            }
        };

        class RequestInfoTrackerMessage : public TrackerMessage
        {
        public:
            RequestInfoTrackerMessage(const std::shared_ptr<const Shared::Id>& id) : requestedId(id)
            {
                trackerMessageType = TrackerMessageType::REQUEST;
            }

            RequestInfoTrackerMessage(const Shared::JSon::JSon& json) : TrackerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(requestedId);
            }

            std::shared_ptr<const Shared::Id> requestedId;

            static std::shared_ptr<RequestInfoTrackerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RequestInfoTrackerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                TrackerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(requestedId);
            }
        };

        class UpdatePlayerTrackerMessage : public TrackerMessage
        {
        public:
            UpdatePlayerTrackerMessage()
            {
                trackerMessageType = TrackerMessageType::PLAYER_UPDATE;
            }

            UpdatePlayerTrackerMessage(const int player, const std::shared_ptr<const Processing::Router::RouterId>& routerId, const bool foundPlayer)
            {
                trackerMessageType = TrackerMessageType::PLAYER_UPDATE;
                playerId = player;
                router = routerId;
                found = foundPlayer;
            }

            UpdatePlayerTrackerMessage(const Shared::JSon::JSon& json) : TrackerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(found);
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(router);
            }

            bool found;
            int playerId;
            std::shared_ptr<const Processing::Router::RouterId> router;

            static std::shared_ptr<UpdatePlayerTrackerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<UpdatePlayerTrackerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                TrackerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(found);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(router);
            }
        };

        class RequestPlayerTrackerMessage : public TrackerMessage
        {
        public:
            RequestPlayerTrackerMessage()
            {
                trackerMessageType = TrackerMessageType::PLAYER_REQUEST;
            }

            RequestPlayerTrackerMessage(const int pId) : playerId(pId)
            {
                trackerMessageType = TrackerMessageType::PLAYER_REQUEST;
            }

            RequestPlayerTrackerMessage(const Shared::JSon::JSon& json) : TrackerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
            }

            int playerId;

            static std::shared_ptr<RequestPlayerTrackerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RequestPlayerTrackerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                TrackerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
            }
        };
    }
}

#endif
