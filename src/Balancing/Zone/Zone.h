#ifndef SOLP_BALANCING_ZONE_ZONE_H
#define SOLP_BALANCING_ZONE_ZONE_H

#include <future>
#include <unordered_set>

#include "Shared/Serializable.h"

namespace Monitor
{
    namespace Server
    {
        class HookIDLoadResMessage;
        class ServerId;
    }
}

namespace Processing
{
    namespace Zone
    {
        class Zone;
    }
}

namespace BL
{
    namespace Zone
    {
        class LoadPartitioningPolicy;
        class EntityRemovedFromPhaseMessage;
        class PhaseMoveEntitiesMessage;
        class PhaseMoveConfirmMessage;

        class Zone : public Shared::Serializable
        {
        public:
            Zone(const int x, const int y, const int phase, Processing::Zone::Zone* creator);

            void SetOwner(Processing::Zone::Zone* creator) { owner = creator; }

            void HandleLoadMessage(const std::shared_ptr<Monitor::Server::HookIDLoadResMessage>& zoneload);
            void HandleEntityRemovedFromPhaseMessage(const std::shared_ptr<EntityRemovedFromPhaseMessage>& entityremoved);
            void HandlePhaseMoveEntitiesMessage(const std::shared_ptr<PhaseMoveEntitiesMessage>& phasemoveentities);
            void HandlePhaseMoveConfirmMessage(const std::shared_ptr<PhaseMoveConfirmMessage>& phasemoveconfirm);

            //WARNING: returns the phase the entity should be added to
            int AddEntityToZone(const int playerId);
            void RemoveEntityFromZone(const int playerId);
            void EntityFinishAction(const int playerId);

            void MoveEntitiesBetweenPhases(std::vector<std::vector<std::vector<int>>>& phaseEntityToPhase);
            int GetActivePhases() { return activePhases; }
            const std::vector<double>& GetPhaseLoad() { return phaseLoads; }
            const std::vector<int>& GetFreeCapacity() { return phaseEntityCapacity; }

            void FinishLoadRebalance();

            void AfterMove();

            // Serialization
            Zone(const Shared::JSon::JSon& json);
            void SerializeToJSon(Shared::JSon::JSon& json) const override;
            static std::shared_ptr<Zone> ParseJSon(const Shared::JSon::JSon& json);

        private:
            int ownX;
            int ownY;
            int ownPhase;

            std::shared_ptr<const Monitor::Server::ServerId> monitorId;;
            std::vector<bool> awaitingConfirm;

            int startedPhases = 1;
            int activePhases = 1;
            std::unordered_map<int, std::pair<int, int>> playersInZone;
            std::vector<std::unordered_set<int>> phaseData;
            std::unordered_map<int, int> waitingToBePhaseMoved;
            int phasesForThisMove = 0;
            bool loadRebalanceInProgress = false;
            std::future<void> rebalanceFuture;

            std::vector<double> phaseLoads;
            std::vector<int> phaseEntityCapacity;
            std::shared_ptr<LoadPartitioningPolicy> partitioningPolicy;

            Processing::Zone::Zone* owner;

            bool WaitingForConfirms() const;

            void MoveEntities(const int phases, const std::vector<std::vector<int>>& toMovePlayers);
            void FinishMovementIfCan();

            void AddActivePhase();
            // Call this function after moving entities out of the dropped phase
            void RemoveActivePhase();
            void AddStartedPhase();

            void Init();

            void AddMonitoringThresholds();
            void RemoveMonitoringThresholds();

            void HandlePhaseLoad();
            void HandleEntityLoad();
        };
    }
}


#endif
