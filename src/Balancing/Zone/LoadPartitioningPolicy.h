#ifndef SOLP_BALANCING_ZONE_LOADPARTITIONINGPOLICY_H
#define SOLP_BALANCING_ZONE_LOADPARTITIONINGPOLICY_H

#include <vector>
#include <memory>
#include <functional>
#include <unordered_set>

#define LOAD_PARTITIONING_POLICY(policyName, classContent)                                      \
    class policyName : public LoadPartitioningPolicy classContent;                              \
    LoadPartitioningPolicyBinding binding##policyName (std::string(#policyName), [](){ return std::make_shared<policyName>(); })

#define LOAD_PARTITIONING_POLICY_EXTEND(policyName, toExtend, classContent)                     \
    class policyName : public toExtend classContent;                                            \
    LoadPartitioningPolicyBinding binding##policyName (std::string(#policyName), [](){ return std::make_shared<policyName>(); })

namespace BL
{
    namespace Zone
    {
        class Zone;
        class LoadPartitioningPolicy
        {
        public:
            static void AddPolicy(const std::string& policyName, const std::function<std::shared_ptr<LoadPartitioningPolicy>()>& policy);
            static std::shared_ptr<LoadPartitioningPolicy> GetActivePolicy(Zone* creator);

            virtual int AddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData) = 0;
            virtual void RemoveEntity(const int playerId, const int currentPhase, std::vector<std::unordered_set<int>>& phaseData) = 0;
            virtual void ChangeNumberOfPhases(const int old, const int changed, std::vector<std::unordered_set<int>>& phaseData) = 0;

        protected:
            Zone* owner;

            void SetOwner(Zone* creator) { owner = creator; }
        };

        class LoadPartitioningPolicyBinding
        {
        public:
            LoadPartitioningPolicyBinding(const std::string& policyName, const std::function<std::shared_ptr<LoadPartitioningPolicy>()>& policy)
            {
                LoadPartitioningPolicy::AddPolicy(policyName, policy);
            }
        };
    }
}

#endif
