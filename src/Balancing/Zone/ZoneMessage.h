#ifndef SOLP_BALANCING_ZONE_ZONEMESSAGE_H
#define SOLP_BALANCING_ZONE_ZONEMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace BL
{
    namespace Zone
    {
        class ZoneMessage : public Shared::Message::Message
        {
        public:
            enum ZoneMessageType
            {
                ENTITY_REMOVED_FROM_PHASE,
                PHASE_MOVE_ENTITIES,
                PHASE_MOVE_CONFIRM,

                MAX_VALUE
            };

            ZoneMessage()
            {
                processType = Shared::Id::ProcessType::LB_CELL;
            }

            ZoneMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(zoneMessageType);
            }

            ZoneMessageType zoneMessageType;

            static std::shared_ptr<ZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                ZoneMessageType zmt;
                json.QueryStrict("zoneMessageType", zmt);

                if (zmt >= ZoneMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds server message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[zmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(zoneMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<ZoneMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class EntityRemovedFromPhaseMessage : public ZoneMessage
        {
        public:
            EntityRemovedFromPhaseMessage()
            {
                zoneMessageType = ZoneMessageType::ENTITY_REMOVED_FROM_PHASE;
            }

            EntityRemovedFromPhaseMessage(const int entity, const int removedFromPhase)
            {
                zoneMessageType = ZoneMessageType::ENTITY_REMOVED_FROM_PHASE;
                playerId = entity;
                phase = removedFromPhase;
            }

            EntityRemovedFromPhaseMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(playerId);
                MESSAGE_GET_FROM_JSON_STRICT(phase);
            }

            int playerId;
            int phase;

            static std::shared_ptr<EntityRemovedFromPhaseMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntityRemovedFromPhaseMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(playerId);
                MESSAGE_ADD_TO_JSON(phase);
            }
        };

        class PhaseMoveEntitiesMessage : public ZoneMessage
        {
        public:
            PhaseMoveEntitiesMessage()
            {
                zoneMessageType = ZoneMessageType::PHASE_MOVE_ENTITIES;
            }

            PhaseMoveEntitiesMessage(const int activePhases, std::vector<std::vector<int>>&& newPhasesForPlayers)
            {
                zoneMessageType = ZoneMessageType::PHASE_MOVE_ENTITIES;
                phases = activePhases;
                movePlayerToPhase = newPhasesForPlayers;
            }

            PhaseMoveEntitiesMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(phases);
                MESSAGE_GET_FROM_JSON_STRICT(movePlayerToPhase);
            }

            int phases;
            std::vector<std::vector<int>> movePlayerToPhase;

            static std::shared_ptr<PhaseMoveEntitiesMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<PhaseMoveEntitiesMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(phases);
                MESSAGE_ADD_TO_JSON(movePlayerToPhase);
            }
        };

        class PhaseMoveConfirmMessage : public ZoneMessage
        {
        public:
            PhaseMoveConfirmMessage()
            {
                zoneMessageType = ZoneMessageType::PHASE_MOVE_CONFIRM;
            }

            PhaseMoveConfirmMessage(const Shared::JSon::JSon& json) : ZoneMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(phases);
            }

            int phases;

            static std::shared_ptr<PhaseMoveConfirmMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<PhaseMoveConfirmMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ZoneMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(phases);
            }
        };
    }
}

#endif
