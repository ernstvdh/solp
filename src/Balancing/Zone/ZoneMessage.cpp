#include "ZoneMessage.h"

namespace BL
{
    namespace Zone
    {
        const std::vector<std::function<std::shared_ptr<ZoneMessage>(const Shared::JSon::JSon&)>> ZoneMessage::enumToMessageMap
        {
            EntityRemovedFromPhaseMessage::ParseJSon,
            PhaseMoveEntitiesMessage::ParseJSon,
            PhaseMoveConfirmMessage::ParseJSon
        };
    }
}
