#include <numeric>
#include <algorithm>

#include "Zone.h"
#include "ZoneId.h"
#include "ZoneMessage.h"
#include "LoadPartitioningPolicy.h"

#include "Shared/Config.h"
#include "Balancing/Server/ServerId.h"
#include "Balancing/Server/ServerMessage.h"
#include "Processing/Zone/Zone.h"
#include "Monitoring/Server/ServerId.h"
#include "Monitoring/Server/ServerMessage.h"

namespace BL
{
    namespace Zone
    {
        enum
        {
            LOAD_BASED,
            ENTITY_BASED
        };

        Zone::Zone(const int x, const int y, const int phase, Processing::Zone::Zone* creator) :
            ownX(x), ownY(y), ownPhase(phase), owner(creator), phaseLoads(1, 0.0),
            phaseEntityCapacity(1, Shared::Config::masterMaxEntityLoad), phaseData(1)
        {
            // this way 2 phases will always have a link between them, reducing interaction time between phases
            for (int i = 0; i < ownPhase; i++)
                owner->LinkTo(std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, i));

            Init();
        }

        void Zone::HandlePhaseLoad()
        {
            if (Shared::Config::loadAssessmentStrategy != LOAD_BASED)
                return;

            // wait till we are ready to handle this
            if (WaitingForConfirms())
                return;

            // currently already rebalancing... wait
            if (loadRebalanceInProgress)
                return;

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            if (std::any_of(phaseLoads.begin(), phaseLoads.begin() + activePhases,
                [](double i){ return i > Shared::Config::addPhaseThreshold; }))
            {
                if (activePhases < Shared::Config::maxPhasesPerZone)
                {
                    AddActivePhase();
                    partitioningPolicy->ChangeNumberOfPhases(activePhases - 1, activePhases, phaseData);
                }
                else if (std::any_of(phaseLoads.begin(), phaseLoads.begin() + activePhases,
                    [](double i){ return i > Shared::Config::maxLoadPerPhase; }))
                {
                    SOLP_ERROR("Load for zone (%d, %d, %d) exceeds maximum load without an option to add more threads....", ownX, ownY, ownPhase);
                }
            }
            else if (activePhases > 1)
            {
                double currentLoad = std::accumulate(phaseLoads.begin(), phaseLoads.begin() + activePhases, 0.0);
                if (currentLoad / (activePhases - 1.0) < Shared::Config::targetMergeLoad)
                {
                    partitioningPolicy->ChangeNumberOfPhases(activePhases, activePhases - 1, phaseData);
                    RemoveActivePhase();
                }
            }

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");
        }

        void Zone::HandleEntityLoad()
        {
            if (Shared::Config::loadAssessmentStrategy != ENTITY_BASED)
                return;

            // wait till we are ready to handle this
            if (WaitingForConfirms())
                return;

            // currently already rebalancing... wait
            if (loadRebalanceInProgress)
                return;

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            int freeCapacity = std::accumulate(phaseEntityCapacity.begin(), phaseEntityCapacity.begin() + activePhases, 0);
            if (freeCapacity <= 0)
            {
                if (activePhases < Shared::Config::maxPhasesPerZone)
                {
                    AddActivePhase();
                    partitioningPolicy->ChangeNumberOfPhases(activePhases - 1, activePhases, phaseData);
                }
            }
            else if (activePhases > 1)
            {
                int freeCapacityWithOneLess = freeCapacity - Shared::Config::slaveMaxEntityLoad;
                if (freeCapacityWithOneLess >= Shared::Config::targetMergeCapacity)
                {
                    partitioningPolicy->ChangeNumberOfPhases(activePhases, activePhases - 1, phaseData);
                    RemoveActivePhase();
                }
            }

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");
        }

        void Zone::HandleLoadMessage(const std::shared_ptr<Monitor::Server::HookIDLoadResMessage>& zoneload)
        {
            if (ownPhase != 0)
            {
                SOLP_ERROR("Received load of: %f for phase: %d", zoneload->currentLoad, std::static_pointer_cast<const Processing::Zone::ZoneId>(zoneload->id)->phase);
                auto loadMessage = Shared::Message::Message::MakeCopy<Monitor::Server::HookIDLoadResMessage>(zoneload);
                loadMessage->senderID = owner->GetID();
                loadMessage->receiverID = std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, 0);
                owner->SendMessagePersistent(loadMessage);
            }
            else
            {
                SOLP_ERROR("Received load of: %f for phase: %d", zoneload->currentLoad, std::static_pointer_cast<const Processing::Zone::ZoneId>(zoneload->id)->phase);

                phaseLoads[std::static_pointer_cast<const Processing::Zone::ZoneId>(zoneload->id)->phase] = zoneload->currentLoad;
                HandlePhaseLoad();

                SOLP_DEBUG(DEBUG_ZONE_LOAD, "");
            }
        }

        void Zone::HandleEntityRemovedFromPhaseMessage(const std::shared_ptr<EntityRemovedFromPhaseMessage>& entityremoved)
        {
            RemoveEntityFromZone(entityremoved->playerId);
        }

        void Zone::FinishLoadRebalance()
        {
            if (ownPhase == 0)
            {
                loadRebalanceInProgress = false;
                HandlePhaseLoad();
                HandleEntityLoad();
            }
        }

        void Zone::MoveEntitiesBetweenPhases(std::vector<std::vector<std::vector<int>>>& phaseEntityToPhase)
        {
            // we don't want to immediately handle load changes when moving is in progress
            // or is just finished because we might still have old load data...
            loadRebalanceInProgress = true;
            
            std::shared_ptr<Shared::Link::LinkEventOnExpire> tempRealTimeout = owner->GetOnExpireLinkEvent();
            std::shared_ptr<std::shared_ptr<Shared::Link::LinkEventOnExpire>> timeoutDeleteWrapper(
                std::make_shared<std::shared_ptr<Shared::Link::LinkEventOnExpire>>(tempRealTimeout));

            rebalanceFuture = std::async(std::launch::async, [timeoutDeleteWrapper]
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(2000));
                timeoutDeleteWrapper->reset();
            });

            if (ownPhase != 0)
            {
                SOLP_ERROR("Only phase 0 should call this function because only phase 0 should an active load partitioning policy.... terminating...");
                std::terminate();
            }

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            for (int fromPhase = 0; fromPhase < phaseEntityToPhase.size(); fromPhase++)
            {
                for (int toPhase = 0; toPhase < phaseEntityToPhase[fromPhase].size(); toPhase++)
                {
                    phaseEntityCapacity[fromPhase] -= phaseEntityToPhase[fromPhase][toPhase].size();
                    phaseEntityCapacity[toPhase] += phaseEntityToPhase[fromPhase][toPhase].size();

                    for (const int player : phaseEntityToPhase[fromPhase][toPhase])
                    {
                        playersInZone[player].second = toPhase;
                    }
                }
            }

            for (int i = 1; i < activePhases; i++)
            {
                std::shared_ptr<PhaseMoveEntitiesMessage> moveEntities(std::make_shared<PhaseMoveEntitiesMessage>(activePhases, std::move(phaseEntityToPhase[i])));
                moveEntities->senderID = owner->GetID();
                moveEntities->receiverID = std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, i);
                // at this point we might not have a link yet to the new phase, so make it ourself if necessary
                owner->SendSingleMessageTo(moveEntities);
            }

            MoveEntities(activePhases, phaseEntityToPhase[0]);
        }

        void Zone::MoveEntities(const int phases, const std::vector<std::vector<int>>& toMovePlayers)
        {
            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            if (awaitingConfirm.empty())
                awaitingConfirm = std::vector<bool>(phases, false);
            awaitingConfirm[ownPhase] = true;
            phasesForThisMove = phases;

            for (int toPhase = 0; toPhase < phases; toPhase++)
                for (const int playerToMove : toMovePlayers[toPhase])
                    if (!owner->PhaseMoveEntity(playerToMove, toPhase))
                        waitingToBePhaseMoved[playerToMove] = toPhase;

            FinishMovementIfCan();
        }

        void Zone::FinishMovementIfCan()
        {
            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            if (waitingToBePhaseMoved.empty())
            {
                // we start with 1 because we wait with the confirm message to phase 1 till we have received the confirms
                // from all the phases, this way phase 0 will know every phase is finished before initiating a new move
                for (int i = 1; i < phasesForThisMove; i++)
                {
                    // we shouldn't send the confirm to ourself
                    if (i == ownPhase)
                        continue;

                    auto mess = Shared::Message::Message::MakeMessage<PhaseMoveConfirmMessage>(owner->GetID(),
                        std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, i));
                    mess->phases = phasesForThisMove;
                    owner->SendSingleMessageTo(mess);
                }
            }

            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");
        }

        void Zone::EntityFinishAction(const int playerId)
        {
            auto found = waitingToBePhaseMoved.find(playerId);
            if (found != waitingToBePhaseMoved.end())
            {
                SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

                if (!owner->PhaseMoveEntity(playerId, found->second))
                {
                    SOLP_ERROR("Action was just finished but still cannot move entity? this should not happen... terminating...");
                    std::terminate();
                }

                waitingToBePhaseMoved.erase(playerId);

                // now we have to check if we can finally finish the movement
                FinishMovementIfCan();
            }
        }

        bool Zone::WaitingForConfirms() const
        {
            if (awaitingConfirm.empty())
                return false;

            auto stillUnconfirmed = std::find(std::begin(awaitingConfirm), std::end(awaitingConfirm), false);
            return stillUnconfirmed != std::end(awaitingConfirm);
        }

        void Zone::HandlePhaseMoveEntitiesMessage(const std::shared_ptr<PhaseMoveEntitiesMessage>& phasemoveentities)
        {
            MoveEntities(phasemoveentities->phases, phasemoveentities->movePlayerToPhase);
        }

        void Zone::HandlePhaseMoveConfirmMessage(const std::shared_ptr<PhaseMoveConfirmMessage>& phasemoveconfirm)
        {
            auto otherPhaseId = std::static_pointer_cast<const Processing::Zone::ZoneId>(phasemoveconfirm->senderID);

            // this is possible when this phase has not yet received the move message from phase 0
            if (awaitingConfirm.empty())
                awaitingConfirm = std::vector<bool>(phasemoveconfirm->phases, false);
            awaitingConfirm[otherPhaseId->phase] = true;
            phasesForThisMove = phasemoveconfirm->phases;

            if (WaitingForConfirms())
                return;

            awaitingConfirm.clear();

            if (ownPhase != 0)
            {
                auto mess = Shared::Message::Message::MakeMessage<PhaseMoveConfirmMessage>(owner->GetID(),
                    std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, 0));
                mess->phases = phasesForThisMove;
                owner->SendSingleMessageTo(mess);
            }
            else
            {
                HandlePhaseLoad();
                HandleEntityLoad();
            }
        }

        int Zone::AddEntityToZone(const int playerId)
        {
            auto foundPlayer = playersInZone.find(playerId);
            if (foundPlayer != playersInZone.end())
            {
                foundPlayer->second.first++;
                return foundPlayer->second.second;
            }
            else
            {
                int result = partitioningPolicy->AddEntity(playerId, phaseData);

                auto& data = playersInZone[playerId];
                data.first++;
                data.second = result;

                phaseEntityCapacity[result]--;

                if (Shared::Config::loadAssessmentStrategy == ENTITY_BASED)
                    HandleEntityLoad();

                return result;
            }
        }

        void Zone::RemoveEntityFromZone(const int playerId)
        {
            if (ownPhase != 0)
            {
                auto entityRemovedMessage = std::make_shared<EntityRemovedFromPhaseMessage>(playerId, ownPhase);
                entityRemovedMessage->senderID = owner->GetID();
                entityRemovedMessage->receiverID = std::make_shared<Processing::Zone::ZoneId>(ownX, ownY, 0);
                owner->SendMessagePersistent(entityRemovedMessage);
            }
            else
            {
                int count = --(playersInZone[playerId].first);
                int phase = playersInZone[playerId].second;
                if (count <= 0)
                {
                    playersInZone.erase(playerId);
                    partitioningPolicy->RemoveEntity(playerId, phase, phaseData);
                }

                phaseEntityCapacity[phase]++;

                if (Shared::Config::loadAssessmentStrategy == ENTITY_BASED)
                    HandleEntityLoad();
            }
        }

        void Zone::AddActivePhase()
        {
            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            if (activePhases >= Shared::Config::maxPhasesPerZone)
                return;

            if (activePhases >= startedPhases)
                AddStartedPhase();

            phaseEntityCapacity[activePhases] = Shared::Config::slaveMaxEntityLoad;
            activePhases++;

            owner->SetActivePhases(activePhases);
        }

        void Zone::RemoveActivePhase()
        {
            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            if (activePhases <= 1)
                return;

            activePhases--;

            owner->SetActivePhases(activePhases);
        }

        void Zone::AddStartedPhase()
        {
            SOLP_DEBUG(DEBUG_ZONE_LOAD, "");

            std::shared_ptr<BL::Server::InitZoneMessage> assign(
                Shared::Message::Message::MakeMessage<BL::Server::InitZoneMessage>(
                owner->GetID(), std::make_shared<const BL::Server::ServerId>(Shared::Config::localIP)));
            assign->zoneId = std::make_shared<const Processing::Zone::ZoneId>(ownX, ownY, startedPhases);

            owner->SendSingleMessageTo(assign);

            startedPhases++;
            phaseLoads.resize(startedPhases, 0.0);
            phaseEntityCapacity.resize(startedPhases, Shared::Config::slaveMaxEntityLoad);
        }

        void Zone::AfterMove()
        {
            if (Shared::Config::loadAssessmentStrategy == LOAD_BASED)
            {
                // after movement we have to unlink from the previous monitor and link to the local one
                // but first we have to unsubscribe to the thresholds for this ID on the old server
                RemoveMonitoringThresholds();
                owner->LinkToRemove(monitorId);
            }

            Init();
        }

        void Zone::Init()
        {
            if (Shared::Config::loadAssessmentStrategy == LOAD_BASED)
            {
                monitorId = std::make_shared<const Monitor::Server::ServerId>(Shared::Config::localIP);
                owner->LinkTo(monitorId);

                AddMonitoringThresholds();
            }

            if (ownPhase == 0)
                partitioningPolicy = LoadPartitioningPolicy::GetActivePolicy(this);
        }

        void Zone::AddMonitoringThresholds()
        {
            std::vector<double> thresholds{ Shared::Config::maxLoadPerPhase };
            for (double toAdd = Shared::Config::addPhaseThreshold; toAdd > 0.0; toAdd -= Shared::Config::zoneLoadMonitoringStepSize)
                thresholds.push_back(toAdd);

            auto loadMonitorMessage = std::make_shared<Monitor::Server::HookIDLoadMessage>(std::move(thresholds), owner->GetID(), true);
            loadMonitorMessage->senderID = owner->GetID();
            loadMonitorMessage->receiverID = monitorId;
            owner->SendMessagePersistent(loadMonitorMessage);
        }

        void Zone::RemoveMonitoringThresholds()
        {
            auto loadMonitorMessage = std::make_shared<Monitor::Server::HookIDLoadMessage>(std::vector<double>(), owner->GetID(), false);
            loadMonitorMessage->senderID = owner->GetID();
            loadMonitorMessage->receiverID = monitorId;
            owner->SendMessagePersistent(loadMonitorMessage);
        }

        Zone::Zone(const Shared::JSon::JSon& json)
        {
            GET_FROM_JSON_STRICT(ownX);
            GET_FROM_JSON_STRICT(ownY);
            GET_FROM_JSON_STRICT(ownPhase);
            if (Shared::Config::loadAssessmentStrategy == LOAD_BASED)
                GET_FROM_JSON_STRICT(monitorId);
            GET_FROM_JSON_STRICT(awaitingConfirm);
            GET_FROM_JSON_STRICT(waitingToBePhaseMoved);
            GET_FROM_JSON_STRICT(phasesForThisMove);

            if (ownPhase == 0)
            {
                GET_FROM_JSON_STRICT(startedPhases);
                GET_FROM_JSON_STRICT(activePhases);

                GET_FROM_JSON_STRICT(playersInZone);
                GET_FROM_JSON_STRICT(phaseData);
                GET_FROM_JSON_STRICT(phaseLoads);
                GET_FROM_JSON_STRICT(phaseEntityCapacity);
            }
        }

        void Zone::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(ownX);
            ADD_TO_JSON(ownY);
            ADD_TO_JSON(ownPhase);
            if (Shared::Config::loadAssessmentStrategy == LOAD_BASED)
                ADD_TO_JSON(monitorId);
            ADD_TO_JSON(awaitingConfirm);
            ADD_TO_JSON(waitingToBePhaseMoved);
            ADD_TO_JSON(phasesForThisMove);

            if (ownPhase == 0)
            {
                ADD_TO_JSON(startedPhases);
                ADD_TO_JSON(activePhases);

                ADD_TO_JSON(playersInZone);
                ADD_TO_JSON(phaseData);
                ADD_TO_JSON(phaseLoads);
                ADD_TO_JSON(phaseEntityCapacity);
            }
        }

        std::shared_ptr<Zone> Zone::ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<Zone>(json);
        }
    }
}
