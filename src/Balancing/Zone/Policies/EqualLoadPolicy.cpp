#include <numeric>
#include <algorithm>

#include "../Zone.h"
#include "../LoadPartitioningPolicy.h"

#include "Shared/Config.h"

namespace BL
{
    namespace Zone
    {
        LOAD_PARTITIONING_POLICY(EqualLoadPolicy,
        {
            virtual int AddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData) override
            {
                const auto& loads = owner->GetPhaseLoad();
                const auto result = std::min_element(std::begin(loads), std::begin(loads) + phaseData.size());
                const int phase = std::distance(std::begin(loads), result);
                phaseData[phase].insert(playerId);

                return phase;
            }

            virtual void RemoveEntity(const int playerId, const int currentPhase, std::vector<std::unordered_set<int>>& phaseData) override
            {
                phaseData[currentPhase].erase(playerId);
            }

            virtual void ChangeNumberOfPhases(const int old, const int changed, std::vector<std::unordered_set<int>>& phaseData) override
            {
                if (changed < old)
                {
                    std::vector<std::vector<std::vector<int>>> result(old, std::vector<std::vector<int>>(old));

                    // in this case we need to move entities from the removed phases to the remaining phases
                    const int toMove = std::accumulate(phaseData.begin() + changed, phaseData.end(), 0,
                        [](int res, const std::unordered_set<int>& data) { return res + data.size(); });

                    const auto& loads = owner->GetPhaseLoad();
                    // make a fair estimate of how much additional load any one phase can handle
                    // taking into account load after the phase add threshold will only result in more phases being added
                    double load = std::accumulate(loads.begin(), loads.begin() + changed, 0.0, [](double res, double data)
                    {
                        if (Shared::Config::addPhaseThreshold < data)
                            return res;
                        else
                            return res + (Shared::Config::addPhaseThreshold - data);
                    });

                    int moveTo = 0;
                    int movedPlayersToThisPhase = 0;
                    const double loadPerEntity = load / toMove;
                    for (int moveFrom = changed; moveFrom < old; moveFrom++)
                    {
                        for (const int entity : phaseData[moveFrom])
                        {
                            while (moveTo + 1 < changed)
                            {
                                if (movedPlayersToThisPhase * loadPerEntity >= Shared::Config::addPhaseThreshold - loads[moveTo])
                                {
                                    moveTo++;
                                    movedPlayersToThisPhase = 0;
                                }
                                else
                                    break;
                            }

                            result[moveFrom][moveTo].push_back(entity);
                            movedPlayersToThisPhase++;

                            // we can safely change the phaseData for the remaining phases
                            // because we don't loop over those
                            phaseData[moveTo].insert(entity);
                        }
                    }

                    owner->MoveEntitiesBetweenPhases(result);

                    phaseData.resize(changed);
                }
                else
                {
                    phaseData.resize(changed);

                    std::vector<std::vector<std::vector<int>>> result(changed, std::vector<std::vector<int>>(changed));

                    // in this case we need to move entities from all current phases to the new phase
                    const int numberOfEntities = std::accumulate(phaseData.begin(), phaseData.end(), 0,
                        [](int res, const std::unordered_set<int>& data) { return res + data.size(); });

                    const auto& loads = owner->GetPhaseLoad();
                    // make a fair estimate of how much additional load any one phase can handle
                    double totalLoad = std::accumulate(loads.begin(), loads.begin() + old, 0.0, [](double res, double data)
                    {
                        return res + data;
                    });

                    const double loadPerEntity = totalLoad / numberOfEntities;
                    const double aimLoadPerPhase = totalLoad / changed;
                    const double aimNumberOfEntities = aimLoadPerPhase / loadPerEntity;

                    std::vector<int> excessEntities(changed, 0);
                    for (int moveFrom = 0; moveFrom < changed; moveFrom++)
                        excessEntities[moveFrom] = phaseData[moveFrom].size() - aimNumberOfEntities;

                    int moveTo = 0;
                    // here we only loop over the old phases because we do not want to move entities from
                    // the new phases to to the old phases
                    for (int moveFrom = 0; moveFrom < old; moveFrom++)
                    {
                        while (!phaseData[moveFrom].empty())
                        {
                            if (moveTo >= changed)
                                break;

                            if (excessEntities[moveFrom] <= 0)
                                break;

                            while (true)
                            {
                                if (excessEntities[moveTo] < 0)
                                {
                                    auto toMove = std::begin(phaseData[moveFrom]);
                                    result[moveFrom][moveTo].push_back(*toMove);
                                    phaseData[moveTo].insert(*toMove);
                                    excessEntities[moveTo]++;

                                    phaseData[moveFrom].erase(toMove);
                                    excessEntities[moveFrom]--;
                                    
                                }
                                else
                                {
                                    moveTo++;
                                    if (moveTo < changed)
                                        continue;
                                }

                                break;
                            }
                        }
                    }

                    owner->MoveEntitiesBetweenPhases(result);
                }
            }
        });
    }
}
