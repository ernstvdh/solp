#include <numeric>
#include <algorithm>

#include "../Zone.h"
#include "../LoadPartitioningPolicy.h"

#include "Shared/Config.h"

namespace BL
{
    namespace Zone
    {
        LOAD_PARTITIONING_POLICY(EqualEntitiesPolicy,
        {
            virtual int AddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData) override
            {
                const auto& capacity = owner->GetFreeCapacity();
                const auto result = std::max_element(std::begin(capacity), std::begin(capacity) + phaseData.size());
                const int phase = std::distance(std::begin(capacity), result);
                phaseData[phase].insert(playerId);

                return phase;
            }

            virtual void RemoveEntity(const int playerId, const int currentPhase, std::vector<std::unordered_set<int>>& phaseData) override
            {
                phaseData[currentPhase].erase(playerId);
            }

            virtual void ChangeNumberOfPhases(const int old, const int changed, std::vector<std::unordered_set<int>>& phaseData) override
            {
                if (changed < old)
                {
                    std::vector<std::vector<std::vector<int>>> result(old, std::vector<std::vector<int>>(old));

                    // in this case we need to move entities from the removed phases to the remaining phases
                    const int toMove = std::accumulate(phaseData.begin() + changed, phaseData.end(), 0,
                        [](int res, const std::unordered_set<int>& data) { return res + data.size(); });

                    const auto& capacity = owner->GetFreeCapacity();
                    int moveTo = 0;
                    int movedPlayersToThisPhase = 0;
                    for (int moveFrom = changed; moveFrom < old; moveFrom++)
                    {
                        for (const int entity : phaseData[moveFrom])
                        {
                            while (moveTo + 1 < changed)
                            {
                                if (movedPlayersToThisPhase >= capacity[moveTo])
                                {
                                    moveTo++;
                                    movedPlayersToThisPhase = 0;
                                }
                                else
                                    break;
                            }

                            if (moveTo >= changed)
                            {
                                SOLP_ERROR("Reducing number of phases but not enough room in remaining phases to handle all entities... terminating...");
                                std::terminate();
                                break;
                            }

                            result[moveFrom][moveTo].push_back(entity);
                            movedPlayersToThisPhase++;

                            // we can safely change the phaseData for the remaining phases
                            // because we don't loop over those
                            phaseData[moveTo].insert(entity);
                        }
                    }

                    owner->MoveEntitiesBetweenPhases(result);
                }

                phaseData.resize(changed);
            }
        });
    }
}
