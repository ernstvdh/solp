#include <numeric>

#include "metis.h"

#include "../Zone.h"
#include "../LoadPartitioningPolicy.h"

#include "Shared/CSR.h"
#include "Shared/Config.h"
#include "Shared/Singleton.h"

#define COM ,

namespace BL
{
    namespace Zone
    {
        int LowestLoadAddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData, Zone* owner)
        {
            const auto& capacity = owner->GetFreeCapacity();
            const auto result = std::max_element(std::begin(capacity), std::begin(capacity) + phaseData.size());
            const int phase = std::distance(std::begin(capacity), result);
            phaseData[phase].insert(playerId);

            return phase;
        }

        int HighestFriendScoreAddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData, Zone* owner)
        {
            std::vector<double> scores(phaseData.size(), 0.0);
            std::unordered_set<int>& playerFriends = Shared::Singleton::playerRelations[playerId];
            int numberOfFriends = playerFriends.size();
            const auto& capacity = owner->GetFreeCapacity();
            auto minmax = std::minmax_element(std::begin(capacity), std::begin(capacity) + phaseData.size());
            int diff = *minmax.second - *minmax.first;

            for (int i = 0; i < phaseData.size(); i++)
            {
                //TODO: if iterating over all the friends is cheaper we should do that...
                int socialScore = std::accumulate(std::begin(phaseData[i]), std::end(phaseData[i]), 0, [&](int current, int entity)
                {
                    return current + playerFriends.count(entity);
                });

                double totalScore = static_cast<double>(socialScore) / static_cast<double>(numberOfFriends);

                // if the difference in load is too small we ignore
                //TODO: maybe change this???
                if (Shared::Config::churnSocialConsiderLoad && diff > (Shared::Config::slaveMaxEntityLoad / 10))
                {
                    totalScore *= Shared::Config::churnSocialRelationWeight;
                    totalScore += (1.0 - Shared::Config::churnSocialRelationWeight) * static_cast<double>(*minmax.second - capacity[i]) / diff;
                }

                scores[i] = totalScore;
            }

            double highestScore = scores[0];
            int highestFreeCap = capacity[0];
            int selectedPhase = 0;
            for (int i = 1; i < phaseData.size(); i++)
            {
                // either the old highest score is lower than the score of this phase
                // or it is the same but this phase has a higher capacity
                // if either is true we select this phase
                if (highestScore < scores[i] || (highestScore == scores[i] && highestFreeCap < capacity[i]))
                {
                    selectedPhase = i;
                    highestFreeCap = capacity[i];
                    highestScore = scores[i];
                }
            }

            phaseData[selectedPhase].insert(playerId);

            return selectedPhase;
        }

        std::pair<std::vector<std::vector<std::vector<int>>>, int>&& Partition(const int addedplayer, const int old, const int changed, std::vector<std::unordered_set<int>>& phaseData, Zone* owner)
        {
            std::pair<std::vector<std::vector<std::vector<int>>>, int> result(
                std::vector<std::vector<std::vector<int>>>(changed, std::vector<std::vector<int>>(old)), 0);
            std::unordered_set<int> entitiesInZone;

            std::vector<int> phasestoPartition;
            int newNumberOfPhasesToConsider = changed;
            int oldNumberOfPhasesToConsider = old;
            if (Shared::Config::repartitionSocialOnlyOverloaded && changed > old)
            {
                //TODO: implement this check for cpu load based thresholding... now only support entity based thresholds
                const auto& capacity = owner->GetFreeCapacity();
                for (int i = 0; i < phaseData.size(); i++)
                    if (capacity[i] <= Shared::Config::repartitionOverloadedCapacity)
                        phasestoPartition.push_back(i);

                oldNumberOfPhasesToConsider = phasestoPartition.size();
                newNumberOfPhasesToConsider = changed - old + phasestoPartition.size();
            }
            else
            {
                for (int i = 0; i < old; i++)
                    phasestoPartition.push_back(i);
            }

            Shared::CSR<idx_t, idx_t, int> csr;
            for (int phase : phasestoPartition)
            {
                for (int entity : phaseData[phase])
                {
                    csr.AddVertex(entity);
                    entitiesInZone.insert(entity);
                }
            }

            if (addedplayer >= 0)
            {
                csr.AddVertex(addedplayer);
                entitiesInZone.insert(addedplayer);
            }

            if (Shared::Config::repartitionSocialPhaseWeightDivision > 0)
            {
                for (int phase : phasestoPartition)
                {
                    for (int fromEntity : phaseData[phase])
                    {
                        std::vector<int> toAddEdge;
                        std::vector<idx_t> toAddCost;

                        for (int toEntity : Shared::Singleton::playerRelations[fromEntity])
                        {
                            if (entitiesInZone.count(toEntity) <= 0)
                                continue;

                            toAddEdge.push_back(toEntity);
                            toAddCost.push_back(static_cast<idx_t>(Shared::Config::repartitionSocialPhaseWeightDivision));
                        }

                        for (int otherEntityInPhase : phaseData[phase])
                        {
                            // ignore self
                            if (otherEntityInPhase == fromEntity)
                                continue;

                            // if already added because of the relation we continue here as well
                            if (Shared::Singleton::playerRelations[fromEntity].count(otherEntityInPhase) > 0)
                                continue;

                            toAddEdge.push_back(otherEntityInPhase);
                            // we use integer weights so Shared::Config::repartitionSocialPhaseWeightDivision is actually the multiplier
                            // for the friends...
                            //TODO: avoid magic variable
                            toAddCost.push_back(1);
                        }

                        csr.AddEdgesDirected(fromEntity, toAddEdge, toAddCost);
                    }
                }
            }
            else
            {
                for (int phase : phasestoPartition)
                {
                    for (int fromEntity : phaseData[phase])
                    {
                        std::vector<int> toAddEdge;

                        for (int toEntity : Shared::Singleton::playerRelations[fromEntity])
                        {
                            if (entitiesInZone.count(toEntity) <= 0)
                                continue;

                            toAddEdge.push_back(toEntity);
                        }

                        csr.AddEdgesDirected(fromEntity, toAddEdge);
                    }
                }
            }

            if (addedplayer >= 0)
            {
                std::vector<int> toAddEdge;

                for (int toEntity : Shared::Singleton::playerRelations[addedplayer])
                {
                    if (entitiesInZone.count(toEntity) <= 0)
                        continue;

                    toAddEdge.push_back(toEntity);
                }

                if (Shared::Config::repartitionSocialPhaseWeightDivision > 0)
                {
                    std::vector<idx_t> toAddCost(toAddEdge.size(), static_cast<idx_t>(Shared::Config::repartitionSocialPhaseWeightDivision));
                    csr.AddEdgesDirected(addedplayer, toAddEdge, toAddCost);
                }
                else
                    csr.AddEdgesDirected(addedplayer, toAddEdge);
            }

            idx_t options[METIS_NOPTIONS];
            METIS_SetDefaultOptions(options);
            options[METIS_OPTION_PTYPE] = METIS_PTYPE_KWAY;
            options[METIS_OPTION_NUMBERING] = 0;
            options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
            // should be atleast 1, but is unused because we set tpwgts to be NULL
            // and we don't set weights (ncon per vertex) for the vertices
            idx_t numBalancingContraints = 1;
            idx_t numVertices = csr.GetSize();
            idx_t numOfPartitions = newNumberOfPhasesToConsider;
            std::vector<idx_t> partitionResult(numVertices, 0);
            idx_t edgeCut = 0;
            if (numOfPartitions > 1)
            {
                int error = METIS_PartGraphKway(&numVertices, &numBalancingContraints, csr.GetRawVertices(), csr.GetRawEdges(),
                    csr.GetRawVertexWeights(), NULL, csr.GetRawEdgeWeights(),
                    &numOfPartitions, NULL, NULL, options, &edgeCut, partitionResult.data());
            }

            std::vector<int> newPhaseToPhaseMapping(newNumberOfPhasesToConsider, -1);

            {
                std::vector<std::vector<int>> moveMatrix(oldNumberOfPhasesToConsider, std::vector<int>(newNumberOfPhasesToConsider, 0));
                int i = 0;
                for (int phaseToConsider = 0; phaseToConsider < oldNumberOfPhasesToConsider; phaseToConsider++)
                {
                    for (int entity : phaseData[phasestoPartition[phaseToConsider]])
                    {
                        moveMatrix[phaseToConsider][partitionResult[i]]++;
                        i++;
                    }
                }

                struct HighestMoveScore
                {
                    int score = -1;
                    int from = 0;
                    int to = 0;
                };

                for (int round = 0; round < std::min(oldNumberOfPhasesToConsider, newNumberOfPhasesToConsider); round++)
                {
                    HighestMoveScore highestMoveScore;

                    for (int fromPhase = 0; fromPhase < std::min(oldNumberOfPhasesToConsider, newNumberOfPhasesToConsider); fromPhase++)
                    {
                        auto it = std::max_element(std::begin(moveMatrix[fromPhase]), std::end(moveMatrix[fromPhase]));
                        if (highestMoveScore.score < *it)
                        {
                            highestMoveScore.score = *it;
                            highestMoveScore.from = fromPhase;
                            highestMoveScore.to = std::distance(std::begin(moveMatrix[fromPhase]), it);
                        }
                    }

                    newPhaseToPhaseMapping[highestMoveScore.to] = phasestoPartition[highestMoveScore.from];

                    for (int fromPhase = 0; fromPhase < std::min(oldNumberOfPhasesToConsider, newNumberOfPhasesToConsider); fromPhase++)
                    {
                        if (highestMoveScore.from == fromPhase)
                            std::fill(std::begin(moveMatrix[fromPhase]), std::end(moveMatrix[fromPhase]), -1);
                        else
                            moveMatrix[fromPhase][highestMoveScore.to] = -1;
                    }
                }

                if (newNumberOfPhasesToConsider > oldNumberOfPhasesToConsider)
                {
                    std::unordered_set<int> phases;
                    for (int oldPhase : phasestoPartition)
                        phases.insert(oldPhase);

                    for (int newPhase = old; newPhase < changed; newPhase++)
                        phases.insert(newPhase);

                    for (int newPhase : newPhaseToPhaseMapping)
                    if (newPhase >= 0)
                        phases.erase(newPhase);

                    for (int& phase : newPhaseToPhaseMapping)
                    {
                        if (phase < 0)
                        {
                            auto it = phases.begin();
                            phase = *it;
                            phases.erase(it);
                        }
                    }
                }
            }

            // IMPORTANT at this point the iteration order of the unordered_sets in phaseData is still the same
            // as at the start of the function:
            // http://stackoverflow.com/questions/18301302/is-forauto-i-unordered-map-guaranteed-to-have-the-same-order-every-time
            {
                int i = 0;
                for (int moveFrom : phasestoPartition)
                {
                    for (int entity : phaseData[moveFrom])
                    {
                        int moveTo = newPhaseToPhaseMapping[partitionResult[i]];
                        i++;

                        if (moveFrom == moveTo)
                            continue;

                        result.first[moveFrom][moveTo].push_back(entity);
                    }
                }
            }

            // IMPORTANT here we edit the phaseData unordered_sets so we can not iterate over the sets in the same order anymore
            for (int moveFrom = 0; moveFrom < old; moveFrom++)
            {
                for (int moveTo = 0; moveTo < changed; moveTo++)
                {
                    auto& fromVec = phaseData[moveFrom];
                    const std::vector<int>& toMove = result.first[moveFrom][moveTo];

                    for (int removeEntity : toMove)
                        fromVec.erase(removeEntity);
                }
            }

            phaseData.resize(changed);

            for (int moveFrom = 0; moveFrom < old; moveFrom++)
            {
                for (int moveTo = 0; moveTo < changed; moveTo++)
                {
                    auto& toVec = phaseData[moveTo];
                    const auto& toMove = result.first[moveFrom][moveTo];
                    toVec.insert(std::begin(toMove), std::end(toMove));
                }
            }

            if (addedplayer >= 0)
                result.second = newPhaseToPhaseMapping[partitionResult.back()];

            return std::move(result);
        }

        LOAD_PARTITIONING_POLICY(SocialWeightPolicy,
        {
            virtual int AddEntity(const int playerId, std::vector<std::unordered_set<int>>& phaseData) override
            {
                if (Shared::Config::churnSocialConsiderRelations && Shared::Config::churnSocialConsiderLoad)
                {
                    return HighestFriendScoreAddEntity(playerId, phaseData, owner);
                }
                else if (Shared::Config::churnSocialConsiderRelations)
                {
                    return HighestFriendScoreAddEntity(playerId, phaseData, owner);
                }
                else if (Shared::Config::churnSocialConsiderLoad)
                {
                    return LowestLoadAddEntity(playerId, phaseData, owner);
                }
                else if (Shared::Config::churnSocialRepartition)
                {
                    std::pair<std::vector<std::vector<std::vector<int>>> COM int> newAssignment = Partition(playerId, phaseData.size(), phaseData.size(), phaseData, owner);
                    owner->MoveEntitiesBetweenPhases(newAssignment.first);

                    phaseData[newAssignment.second].insert(playerId);
                    return newAssignment.second;
                }
                else
                {
                    SOLP_ERROR("Social weight policy is incorrectly configured... terminating...");
                    std::terminate();

                    return 0;
                }
            }

            virtual void RemoveEntity(const int playerId, const int currentPhase, std::vector<std::unordered_set<int>>& phaseData) override
            {
                phaseData[currentPhase].erase(playerId);
            }

            virtual void ChangeNumberOfPhases(const int old, const int changed, std::vector<std::unordered_set<int>>& phaseData) override
            {
                std::pair<std::vector<std::vector<std::vector<int>>> COM int> newAssignment = Partition(-1, old, changed, phaseData, owner);
                owner->MoveEntitiesBetweenPhases(newAssignment.first);
            }
        });
    }
}
