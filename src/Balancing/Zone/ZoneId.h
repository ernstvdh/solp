#ifndef SOLP_BALANCING_ZONE_ZONEID_H
#define SOLP_BALANCING_ZONE_ZONEID_H

#include "Shared/Id.h"

namespace BL
{
    namespace Zone
    {
        class ZoneId : public Shared::Id
        {
        public:
            ZoneId(int lX, int lY, int lPhase) : Shared::Id(Shared::Id::ProcessType::LB_CELL), x(lX), y(lY), phase(lPhase) {}
            ZoneId(const Shared::JSon::JSon& json) : Shared::Id(Shared::Id::ProcessType::LB_CELL)
            {
                GET_FROM_JSON_STRICT(x);
                GET_FROM_JSON_STRICT(y);
                GET_FROM_JSON_STRICT(phase);
            }
            ~ZoneId() {}

            int x;
            int y;
            int phase;

            std::string Serialize() const
            {
                return std::to_string(type) + "_" + std::to_string(x) + "_" + std::to_string(y) + "_" + std::to_string(phase);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ADD_TO_JSON(type);
                ADD_TO_JSON(x);
                ADD_TO_JSON(y);
                ADD_TO_JSON(phase);
            }

            bool equals(const std::shared_ptr<const Id>& that) const
            {
                if (type != that->type)
                    return false;

                std::shared_ptr<const ZoneId> otherId(std::static_pointer_cast<const ZoneId>(that));

                if (x != otherId->x)
                    return false;

                if (y != otherId->y)
                    return false;

                if (phase != otherId->phase)
                    return false;

                return true;
            }

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ZoneId>(json);
            }
        };
    }
}

#endif
