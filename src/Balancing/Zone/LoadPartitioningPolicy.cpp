#include <mutex>
#include <unordered_map>

#include "Shared/Debug.h"
#include "Shared/Config.h"
#include "LoadPartitioningPolicy.h"

namespace BL
{
    namespace Zone
    {
        std::unordered_map<std::string, std::function<std::shared_ptr<LoadPartitioningPolicy>()>>& GetPolicies()
        {
            static std::unordered_map<std::string, std::function<std::shared_ptr<LoadPartitioningPolicy>()>> policies;
            return policies;
        }

        std::mutex& GetMutex()
        {
            static std::mutex mutex;
            return mutex;
        }

        void LoadPartitioningPolicy::AddPolicy(const std::string& policyName, const std::function<std::shared_ptr<LoadPartitioningPolicy>()>& policy)
        {
            std::lock_guard<std::mutex> lck(GetMutex());

            GetPolicies()[policyName] = policy;
        }

        std::shared_ptr<LoadPartitioningPolicy> LoadPartitioningPolicy::GetActivePolicy(Zone* creator)
        {
            auto& policies = GetPolicies();
            auto policy = policies.find(Shared::Config::zoneLoadPartitioningPolicy);

            if (policy == policies.end())
            {
                SOLP_ERROR("Active zone load partitioning policy does not exist... terminating...");
                std::terminate();
            }
            else
            {
                auto activePolicy = policy->second();
                activePolicy->SetOwner(creator);
                return activePolicy;
            }
        }
    }
}
