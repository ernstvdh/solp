#ifndef SOLP_BALANCING_GLOBAL_GLOBALID_H
#define SOLP_BALANCING_GLOBAL_GLOBALID_H

#include "Shared/Id.h"

namespace BL
{
    namespace Global
    {
        class GlobalId : public Shared::IpId
        {
        public:
            GlobalId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::LB_GLOBAL) {}
            GlobalId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::LB_GLOBAL) {}
            ~GlobalId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<GlobalId>(json);
            }
        };
    }
}

#endif
