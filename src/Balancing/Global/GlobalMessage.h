#ifndef SOLP_BALANCING_GLOBAL_GLOBALMESSAGE_H
#define SOLP_BALANCING_GLOBAL_GLOBALMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace BL
{
    namespace Global
    {
        class GlobalMessage : public Shared::Message::Message
        {
        public:
            enum GlobalMessageType
            {
                REGISTER,

                MAX_VALUE
            };

            GlobalMessage()
            {
                processType = Shared::Id::ProcessType::LB_GLOBAL;
            }

            GlobalMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(globalMessageType);
            }

            GlobalMessageType globalMessageType;

            static std::shared_ptr<GlobalMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                GlobalMessageType gmt;
                json.QueryStrict("globalMessageType", gmt);

                if (gmt >= GlobalMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds global message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[gmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(globalMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<GlobalMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class RegisterServerMessage : public GlobalMessage
        {
        public:
            RegisterServerMessage()
            {
                globalMessageType = GlobalMessageType::REGISTER;
            }

            RegisterServerMessage(const Shared::JSon::JSon& json) : GlobalMessage(json) {}

            static std::shared_ptr<RegisterServerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RegisterServerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                GlobalMessage::SerializeToJSon(json);
            }
        };
    }
}

#endif
