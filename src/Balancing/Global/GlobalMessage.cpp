#include "GlobalMessage.h"

namespace BL
{
    namespace Global
    {
        const std::vector<std::function<std::shared_ptr<GlobalMessage>(const Shared::JSon::JSon&)>> GlobalMessage::enumToMessageMap
        {
            RegisterServerMessage::ParseJSon
        };
    }
}
