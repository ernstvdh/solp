#ifndef SOLP_BALANCING_GLOBAL_GLOBAL_H
#define SOLP_BALANCING_GLOBAL_GLOBAL_H

#include "GlobalMessage.h"
#include "Balancing/Server/ServerId.h"

#include "Shared/Link/Linkable.h"

namespace BL
{
    namespace Global
    {
        class Global : public Shared::Link::Linkable
        {
        public:
            Global();
            ~Global() {}
            int Run();

        private:
            std::vector<std::shared_ptr<const BL::Server::ServerId>> registeredServers;

            int ready;

            void Prepare();
            void Initialize();
            void StartEntryServer();

            void HandleRegisterServerMessage(std::shared_ptr<RegisterServerMessage>& regi);
        };
    }
}

#endif
