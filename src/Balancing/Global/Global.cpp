#include <cmath>

#include "metis.h"

#include "Global.h"
#include "GlobalId.h"

#include "Balancing/Entry/EntryId.h"
#include "Balancing/Entry/EntryMessage.h"
#include "Balancing/Server/ServerMessage.h"

#include "Shared/CSR.h"
#include "Shared/Config.h"
#include "Shared/Singleton.h"

namespace BL
{
    namespace Global
    {
        Global::Global()
        {
            ready = 0;
            id = std::make_shared<const GlobalId>(Shared::Config::localIP);
        }

        int Global::Run()
        {
            ListenOn(Shared::Id::ProcessType::LB_GLOBAL);
            RegisterWithMonitor();

            while (true)
            {
                Prepare();

                if (Shared::Singleton::shutdown.load())
                    break;

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage(500))
                {
                    if (auto regi = std::dynamic_pointer_cast<RegisterServerMessage>(mess))
                        HandleRegisterServerMessage(regi);
                }
            }

            return 0;
        }

        void Global::Prepare()
        {
            // 10 * +-500 miliseconds delay before we divide the cells
            if (ready <= 10)
            {
                if ((ready % 2 == 0))
                    SOLP_WARNING("Starting cell distributing in %d seconds..", (5 - ready / 2));

                ready++;

                if (ready > 10)
                    Initialize();
            }
        }

        void Global::StartEntryServer()
        {
            SendSingleMessageTo(Shared::Message::Message::MakeMessage<BL::Entry::StartEntryServerMessage>(
                id, std::make_shared<const BL::Entry::EntryId>(Shared::Config::gLBIp)));
        }

        void Global::Initialize()
        {
            int toDivideBetween = (int)registeredServers.size();
            if (toDivideBetween <= 0)
            {
                SOLP_ERROR("No servers have registered themselves with the global load balancer... terminating...");
                std::terminate();
            }

            StartEntryServer();

            int cellsX = std::ceil((double)Shared::Config::mapX / (double)Shared::Config::minZoneEdgeLength);
            int cellsY = std::ceil((double)Shared::Config::mapY / (double)Shared::Config::minZoneEdgeLength);
            int totalCells = cellsX * cellsY;

            // make our CSR
            Shared::CSR<idx_t, idx_t, uint64_t> csr;
            // add our vertices (cell coords)
            for (uint64_t x = 0; x < cellsX; x++)
                for (uint64_t y = 0; y < cellsY; y++)
                    csr.AddVertex(x * cellsY + y);
            // add all the required edges
            struct offsetdata { int x, y, cost; };
            std::vector<offsetdata> edges = {
                { 1, 1, 1 },
                { 1, 0, 4 },
                { 1, -1, 1 },
                { 0, 1, 4 },
                { 0, -1, 4 },
                { -1, 1, 1 },
                { -1, 0, 4 },
                { -1, -1, 1 }
            };

            std::vector<uint64_t> toAddEdge(8);
            std::vector<idx_t> toAddCost(8);
            for (uint64_t x = 0; x < cellsX; x++)
            {
                for (uint64_t y = 0; y < cellsY; y++)
                {
                    toAddEdge.clear();
                    toAddCost.clear();

                    for (const auto& offset : edges)
                    {
                        if (x + offset.x < 0 || x + offset.x >= cellsX)
                            continue;

                        if (y + offset.y < 0 || y + offset.y >= cellsY)
                            continue;

                        toAddEdge.push_back((x + offset.x) * cellsY + (y + offset.y));
                        toAddCost.push_back(offset.cost);
                    }

                    csr.AddEdgesDirected(x * cellsY + y, toAddEdge, toAddCost);
                }
            }

            idx_t options[METIS_NOPTIONS];
            METIS_SetDefaultOptions(options);
            options[METIS_OPTION_PTYPE] = METIS_PTYPE_KWAY;
            options[METIS_OPTION_NUMBERING] = 0;
            options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
            // should be atleast 1, but is unused because we set tpwgts to be NULL
            // and we don't set weights (ncon per vertex) for the vertices
            idx_t numBalancingContraints = 1;
            idx_t numVertices = csr.GetSize();
            idx_t numOfPartitions = toDivideBetween;
            std::vector<idx_t> result(numVertices, 0);
            idx_t edgeCut = 0;
            if (toDivideBetween > 1)
            {
                int error = METIS_PartGraphKway(&numVertices, &numBalancingContraints, csr.GetRawVertices(), csr.GetRawEdges(),
                    csr.GetRawVertexWeights(), NULL, csr.GetRawEdgeWeights(),
                    &numOfPartitions, NULL, NULL, options, &edgeCut, result.data());
            }

            for (int i = 0; i < result.size(); i++)
            {
                int x = i % cellsX;
                int y = i / cellsX;

                std::shared_ptr<BL::Server::InitZoneMessage> assign(
                    Shared::Message::Message::MakeMessage<BL::Server::InitZoneMessage>(id, registeredServers[result[i]]));
                assign->zoneId = std::make_shared<const Processing::Zone::ZoneId>(x, y, 0);

                SendSingleMessageTo(assign);
            }
        }

        void Global::HandleRegisterServerMessage(std::shared_ptr<RegisterServerMessage>& regi)
        {
            if (auto serverid = std::dynamic_pointer_cast<const BL::Server::ServerId>(regi->senderID))
            {
                registeredServers.push_back(serverid);

                if (ready > 10)
                    SOLP_WARNING("Server registering after zone assignment... probably best to restart...");
            }
            else
                SOLP_WARNING("Received register server message from a different id than BL::Server::ServerId.");
        }
    }
}
