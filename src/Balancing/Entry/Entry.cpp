#include "Entry.h"
#include "EntryId.h"

#include "Shared/Config.h"
#include "Shared/Singleton.h"

#include "Client/ClientId.h"
#include "Balancing/Tracker/TrackerId.h"
#include "Balancing/Tracker/TrackerMessage.h"

namespace BL
{
    namespace Entry
    {
        Entry::Entry() : engine(time(NULL))
        {
            ready = false;
            id = std::make_shared<const EntryId>(Shared::Config::localIP);
        }

        int Entry::Run()
        {
            ListenOn(Shared::Id::ProcessType::ROUTER_SELECTOR);
            RegisterWithMonitor();

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    if (auto request = std::dynamic_pointer_cast<RequestEntryMessage>(mess))
                        HandleRequest(request);
                    else if (auto add = std::dynamic_pointer_cast<AddEntryMessage>(mess))
                        HandleAddEntry(add);
                    else if (auto start = std::dynamic_pointer_cast<StartEntryServerMessage>(mess))
                        HandleStartAssignment(start);
                }
            }

            return 0;
        }

        void Entry::HandleRequest(const std::shared_ptr<RequestEntryMessage>& request)
        {
            SOLP_DEBUG(DEBUG_BL_ENTRY, "Processing request message...");
            if (!ready || routers.empty())
            {
                tempStore.push_back(request);
                return;
            }

            //TODO: pick a low load node
            std::uniform_int_distribution<int> distribution(0, routers.size() - 1);
            int routerNumber = distribution(engine);
            std::shared_ptr<EntryResultMessage> reply(std::make_shared<EntryResultMessage>(routers[routerNumber]));
            reply->receiverID = request->senderID;
            reply->senderID = id;
            SendMessagePersistent(reply);

            // inform all trackers of the router that handles the player
            // we do this so that the zone's have a way to find the location
            // of players in different zones
            //TODO: rethink this system, is seems slow and like a HACK
            // note that the zones should be able to directly find players
            // in zones within a set action range, this makes finding players
            // through the tracker less used (which is better)
            //TODO: implement direct player finding in adjacent zones
            // we only expect this message from clients
            if (auto clientId = std::dynamic_pointer_cast<const Client::ClientId>(request->senderID))
            {
                std::shared_ptr<BL::Tracker::UpdatePlayerTrackerMessage> announce(
                    std::make_shared<BL::Tracker::UpdatePlayerTrackerMessage>(clientId->id, routers[routerNumber], true));
                announce->senderID = id;

                for (auto tracker : Shared::Config::trackerIPs)
                {
                    std::shared_ptr<const BL::Tracker::TrackerId> remoteId(std::make_shared<const BL::Tracker::TrackerId>(tracker));
                    std::shared_ptr<BL::Tracker::UpdatePlayerTrackerMessage> copy(
                        Shared::Message::Message::MakeCopy<BL::Tracker::UpdatePlayerTrackerMessage>(announce));
                    copy->receiverID = remoteId;
                    SendSingleMessageTo(copy);
                }
            }
            else
                SOLP_WARNING("Received router request from a non client... this should not happen...");
        }

        void Entry::HandleStartAssignment(const std::shared_ptr<StartEntryServerMessage>& start)
        {
            ready = true;

            if (!routers.empty())
            {
                for (auto& temp : tempStore)
                    HandleRequest(temp);

                tempStore.clear();
            }
        }

        void Entry::HandleAddEntry(const std::shared_ptr<AddEntryMessage>& add)
        {
            routers.push_back(add->routerId);

            if (ready)
            {
                for (auto& temp : tempStore)
                    HandleRequest(temp);

                tempStore.clear();
            }
        }
    }
}
