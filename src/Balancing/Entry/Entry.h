#ifndef SOLP_BALANCING_ENTRY_ENTRY_H
#define SOLP_BALANCING_ENTRY_ENTRY_H

#include <random>

#include "Shared/Link/Linkable.h"
#include "Processing/Router/RouterId.h"
#include "EntryMessage.h"

namespace BL
{
    namespace Entry
    {
        class Entry : public Shared::Link::Linkable
        {
        public:
            Entry();
            ~Entry() {}

            int Run();

        private:
            void HandleRequest(const std::shared_ptr<RequestEntryMessage>& request);
            void HandleAddEntry(const std::shared_ptr<AddEntryMessage>& add);
            void HandleStartAssignment(const std::shared_ptr<StartEntryServerMessage>& start);

            bool ready;
            std::vector<std::shared_ptr<RequestEntryMessage>> tempStore;

            std::vector<std::shared_ptr<const Processing::Router::RouterId>> routers;

            std::default_random_engine engine;
        };
    }
}

#endif
