#ifndef SOLP_BALANCING_ENTRY_ENTRYMESSAGE_H
#define SOLP_BALANCING_ENTRY_ENTRYMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"
#include "Processing/Router/RouterId.h"

namespace BL
{
    namespace Entry
    {
        class EntryMessage : public Shared::Message::Message
        {
        public:
            enum EntryMessageType
            {
                REQUEST,
                RESULT,
                ADD,
                START_ENTRY_SERVER,

                MAX_VALUE
            };

            EntryMessage()
            {
                processType = Shared::Id::ProcessType::ROUTER_SELECTOR;
            }

            EntryMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(entryMessageType);
            }

            EntryMessageType entryMessageType;

            static std::shared_ptr<EntryMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                EntryMessageType emt;
                json.QueryStrict("entryMessageType", emt);

                if (emt >= EntryMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds entry message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[emt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(entryMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<EntryMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };


        class EntryResultMessage : public EntryMessage
        {
        public:
            EntryResultMessage()
            {
                entryMessageType = EntryMessageType::RESULT;
            }

            EntryResultMessage(const std::shared_ptr<const Processing::Router::RouterId>& id)
            {
                entryMessageType = EntryMessageType::RESULT;
                routerId = id;
            }

            EntryResultMessage(const Shared::JSon::JSon& json) : EntryMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(routerId);
            }

            std::shared_ptr<const Processing::Router::RouterId> routerId;

            static std::shared_ptr<EntryResultMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntryResultMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                EntryMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(routerId);
            }
        };

        class RequestEntryMessage : public EntryMessage
        {
        public:
            RequestEntryMessage()
            {
                entryMessageType = EntryMessageType::REQUEST;
            }

            RequestEntryMessage(const Shared::JSon::JSon& json) : EntryMessage(json) {}

            static std::shared_ptr<RequestEntryMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<RequestEntryMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                EntryMessage::SerializeToJSon(json);
            }
        };

        class AddEntryMessage : public EntryMessage
        {
        public:
            AddEntryMessage()
            {
                entryMessageType = EntryMessageType::ADD;
            }

            AddEntryMessage(const std::shared_ptr<const Processing::Router::RouterId>& id)
            {
                entryMessageType = EntryMessageType::ADD;
                routerId = id;
            }

            AddEntryMessage(const Shared::JSon::JSon& json) : EntryMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(routerId);
            }

            std::shared_ptr<const Processing::Router::RouterId> routerId;

            static std::shared_ptr<AddEntryMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<AddEntryMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                EntryMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(routerId);
            }
        };

        class StartEntryServerMessage : public EntryMessage
        {
        public:
            StartEntryServerMessage()
            {
                entryMessageType = EntryMessageType::START_ENTRY_SERVER;
            }

            StartEntryServerMessage(const Shared::JSon::JSon& json) : EntryMessage(json) {}

            static std::shared_ptr<StartEntryServerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<StartEntryServerMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                EntryMessage::SerializeToJSon(json);
            }
        };
    }
}

#endif
