#ifndef SOLP_BALANCING_ENTRY_ENTRYID_H
#define SOLP_BALANCING_ENTRY_ENTRYID_H

#include "Shared/Id.h"

namespace BL
{
    namespace Entry
    {
        class EntryId : public Shared::IpId
        {
        public:
            EntryId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::ROUTER_SELECTOR) {}
            EntryId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::ROUTER_SELECTOR) {}
            ~EntryId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<EntryId>(json);
            }
        };
    }
}

#endif
