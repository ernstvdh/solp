#include "EntryMessage.h"

namespace BL
{
    namespace Entry
    {
        const std::vector<std::function<std::shared_ptr<EntryMessage>(const Shared::JSon::JSon&)>> EntryMessage::enumToMessageMap
        {
            RequestEntryMessage::ParseJSon,
            EntryResultMessage::ParseJSon,
            AddEntryMessage::ParseJSon,
            StartEntryServerMessage::ParseJSon
        };
    }
}
