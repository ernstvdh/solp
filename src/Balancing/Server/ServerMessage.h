#ifndef SOLP_BALANCING_SERVER_SERVERMESSAGE_H
#define SOLP_BALANCING_SERVER_SERVERMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

#include "Processing/Zone/Zone.h"
#include "Processing/Zone/ZoneId.h"

namespace BL
{
    namespace Server
    {
        class ServerMessage : public Shared::Message::Message
        {
        public:
            enum ServerMessageType
            {
                INIT_ZONE,
                TRANSFER_ZONE,

                MAX_VALUE
            };

            ServerMessage()
            {
                processType = Shared::Id::ProcessType::LB_SERVER;
            }

            ServerMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(serverMessageType);
            }

            ServerMessageType serverMessageType;

            static std::shared_ptr<ServerMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                ServerMessageType smt;
                json.QueryStrict("serverMessageType", smt);

                if (smt >= ServerMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds server message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[smt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(serverMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<ServerMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class InitZoneMessage : public ServerMessage
        {
        public:
            InitZoneMessage()
            {
                serverMessageType = ServerMessageType::INIT_ZONE;
            }

            InitZoneMessage(const std::shared_ptr<const Processing::Zone::ZoneId>& id)
            {
                serverMessageType = ServerMessageType::INIT_ZONE;
                zoneId = id;
            }

            InitZoneMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(zoneId);
            }

            std::shared_ptr<const Processing::Zone::ZoneId> zoneId;

            static std::shared_ptr<InitZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<InitZoneMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(zoneId);
            }
        };

        class TransferZoneMessage : public ServerMessage
        {
        public:
            TransferZoneMessage()
            {
                serverMessageType = ServerMessageType::TRANSFER_ZONE;
            }

            TransferZoneMessage(const std::shared_ptr<Processing::Zone::Zone>& zoneToTransfer)
            {
                serverMessageType = ServerMessageType::INIT_ZONE;
                zone = zoneToTransfer;
            }

            TransferZoneMessage(const Shared::JSon::JSon& json) : ServerMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(zone);
            }

            std::shared_ptr<Processing::Zone::Zone> zone;

            static std::shared_ptr<TransferZoneMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<TransferZoneMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ServerMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(zone);
            }
        };
    }
}

#endif
