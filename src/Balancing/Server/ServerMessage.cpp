#include "ServerMessage.h"

namespace BL
{
    namespace Server
    {
        const std::vector<std::function<std::shared_ptr<ServerMessage>(const Shared::JSon::JSon&)>> ServerMessage::enumToMessageMap
        {
            InitZoneMessage::ParseJSon,
            TransferZoneMessage::ParseJSon
        };
    }
}
