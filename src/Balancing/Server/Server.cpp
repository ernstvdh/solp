#include <future>

#include "Server.h"
#include "ServerId.h"

#include "Shared/Config.h"
#include "Shared/Singleton.h"
#include "Processing/Zone/Zone.h"
#include "Balancing/Global/GlobalId.h"
#include "Balancing/Global/GlobalMessage.h"

namespace BL
{
    namespace Server
    {
        Server::Server()
        {
            id = std::make_shared<const ServerId>(Shared::Config::localIP);
        }

        int Server::Run()
        {
            ListenOn(Shared::Id::ProcessType::LB_SERVER);
            AnnounceToGlobalLoadBalancer();
            RegisterWithMonitor();

            while (true)
            {
                if (Shared::Singleton::shutdown.load())
                    break;

                auto found = std::find_if(zones.begin(), zones.end(),
                    [](const std::pair<std::shared_ptr<Processing::Zone::Zone>, std::thread>& zone)
                {
                    return zone.first->IsReadyForMove();
                });

                //TODO: send to the server that can handle the additional load
                if (found != zones.end())
                {

                }

                while (std::shared_ptr<Shared::Message::Message> mess = PopMessage())
                {
                    if (auto initzone = std::dynamic_pointer_cast<InitZoneMessage>(mess))
                        HandleInitZoneMessage(initzone);
                    else if (auto transferzone = std::dynamic_pointer_cast<TransferZoneMessage>(mess))
                        HandleZoneTransferMessage(transferzone);
                }
            }

            for (auto& zone : zones)
                zone.second.join();

            return 0;
        }

        void Server::HandleInitZoneMessage(const std::shared_ptr<InitZoneMessage>& initzone)
        {
            std::shared_ptr<Processing::Zone::Zone> zone(std::make_shared<Processing::Zone::Zone>(
                initzone->zoneId->x, initzone->zoneId->y, initzone->zoneId->phase, Shared::Link::Moveable::State::RUN));

            SOLP_DEBUG(DEBUG_BL_SERVER, "Received message: %s", initzone->Serialise().c_str());
            auto startZone = [](std::shared_ptr<Processing::Zone::Zone> zone)
            {
                zone->Run();
            };

            SOLP_DEBUG(DEBUG_BL_SERVER, "Starting zone (%s) in %s.", initzone->zoneId->Serialize().c_str(), id->Serialize().c_str());
            zones.emplace_back(zone, std::thread(std::bind(startZone, zone)));
            
        }

        void Server::HandleZoneTransferMessage(const std::shared_ptr<TransferZoneMessage>& zonetransfer)
        {
            SOLP_DEBUG(DEBUG_BL_SERVER, "Received message: %s", zonetransfer->Serialise().c_str());
            auto startZone = [](std::shared_ptr<Processing::Zone::Zone> zone)
            {
                zone->Run();
            };

            SOLP_DEBUG(DEBUG_BL_SERVER, "Transfered zone (%s) from %s to %s.",
                zonetransfer->zone->GetID()->Serialize().c_str(), zonetransfer->senderID->Serialize().c_str(), id->Serialize().c_str());
            zones.emplace_back(zonetransfer->zone, std::thread(std::bind(startZone, zonetransfer->zone)));
        }

        void Server::AnnounceToGlobalLoadBalancer()
        {
            SOLP_DEBUG(DEBUG_BL_SERVER, "");
            std::shared_ptr<BL::Global::RegisterServerMessage> registerSelfMess(
                Shared::Message::Message::MakeMessage<BL::Global::RegisterServerMessage>(id,
                std::make_shared<const BL::Global::GlobalId>(Shared::Config::gLBIp)));
            SendSingleMessageTo(registerSelfMess);
        }
    }
}
