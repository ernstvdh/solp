#ifndef SOLP_BALANCING_SERVER_SERVER_H
#define SOLP_BALANCING_SERVER_SERVER_H

#include "ServerMessage.h"

#include "Shared/Link/Linkable.h"

namespace BL
{
    namespace Server
    {
        class Server : public Shared::Link::Linkable
        {
        public:
            Server();
            ~Server() {}

            int Run();

        private:
            void HandleInitZoneMessage(const std::shared_ptr<InitZoneMessage>& initzone);
            void HandleZoneTransferMessage(const std::shared_ptr<TransferZoneMessage>& zonetransfer);

            void AnnounceToGlobalLoadBalancer();

            std::vector<std::pair<std::shared_ptr<Processing::Zone::Zone>, std::thread>> zones;
        };
    }
}

#endif
