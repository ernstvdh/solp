#ifndef SOLP_BALANCING_SERVER_SERVERID_H
#define SOLP_BALANCING_SERVER_SERVERID_H

#include "Shared/Id.h"

namespace BL
{
    namespace Server
    {
        class ServerId : public Shared::IpId
        {
        public:
            ServerId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::LB_SERVER) {}
            ServerId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::LB_SERVER) {}
            ~ServerId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ServerId>(json);
            }
        };
    }
}

#endif
