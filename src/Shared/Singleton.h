#ifndef SOLP_SHARED_SINGLETON_H
#define SOLP_SHARED_SINGLETON_H

#include <list>
#include <atomic>
#include <vector>
#include <unordered_set>

#include "Shared/Link/LinkEventListener.h"
#include "Monitoring/Server/ServerHook.h"

namespace Shared
{
    //TODO: now old link event expires are kept till shutdown...
    // maybe do a cleanup every now and then?
    // or release hook?
    class ShutdownHook
    {
        std::mutex mutex;
        std::list<std::shared_ptr<Shared::Link::LinkEventOnExpire>> onExpire;
        bool shutdown;

    public:
        ShutdownHook() : shutdown(false) {}

        void Shutdown()
        {
            std::lock_guard<std::mutex> lck(mutex);
            shutdown = true;
            onExpire.clear();
        }

        void Add(std::shared_ptr<Shared::Link::LinkEventOnExpire> hook)
        {
            std::lock_guard<std::mutex> lck(mutex);
            if (!shutdown)
                onExpire.push_back(hook);
        }
    };

    struct Singleton
    {
        static std::atomic_bool shutdown;
        static std::unique_ptr<ShutdownHook> shutdownHooks;
        static std::unique_ptr<Monitor::Server::ServerHook> monitorHooks;
        static std::unique_ptr<Shared::Link::LinkEventTimed> timedEvents;
        static uint64_t processStartTime;
        static std::vector<std::unordered_set<int>> playerRelations;
    };
}

#endif
