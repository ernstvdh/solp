#include <algorithm>

#include "Config.h"
#include "Shared/Id.h"
#include "Shared/Util.h"
#include "Shared/Debug.h"
#include "Shared/JSon/JSon.h"
#include "Shared/System/Files.h"

namespace Shared
{
    std::string Config::localIP = "";

    std::string Config::gLBIp = "";
    int Config::startPort = 0;
    std::vector<int> Config::typePorts = std::vector<int>(Shared::Id::ProcessType::NOTHING_AND_MAX_VALUE);
    std::vector<std::string> Config::trackerIPs = std::vector<std::string>();
    std::vector<std::string> Config::clientIPs = std::vector<std::string>();
    std::vector<std::string> Config::routerIPs = std::vector<std::string>();
    uint64_t Config::serverMonitorIntervalInSec = 0;
    uint64_t Config::serverShutdownTimeInSec = 0;
    uint64_t Config::connectionTimeoutInSec = 0;
    uint64_t Config::numClients = 0;
    bool Config::clientsOnServer = false;
    bool Config::zonesWithRouter = false;
    uint64_t Config::mapX = 0.0;
    uint64_t Config::mapY = 0.0;
    float Config::walkingDistancePerSec = 0.0;
    double Config::visionRadius = 0.0;
    double Config::actionRadius = 0.0;
    double Config::cellPreloadDistance = 0.0;
    double Config::cellUnloadDistance = 0.0;
    uint64_t Config::clientAIUpdatesPerSec = 0;
    uint64_t Config::minCellEdgeLength = 0;
    uint64_t Config::minZoneEdgeLength = 0;
    uint64_t Config::routersPerMachine = 0;
    uint64_t Config::networkThreadsPerMachine = 0;
    bool Config::recordClients = false;
    std::string Config::clientAIName = "";
    uint64_t Config::targetInteractionsPerPause = 0;
    bool Config::loadPlayerRelations = false;
    bool Config::targetSelectionPreferFriends = false;
    bool Config::targetSelectionIgnoreNonFriends = false;
    bool Config::targetSelectionOnlyOwnPhase = false;
    int Config::friendSelectionModifier = 0;
    int Config::maxPhasesPerZone = 0;
    double Config::targetMergeLoad = 0.0;
    double Config::maxLoadPerPhase = 0.0;
    double Config::addPhaseThreshold = 0.0;
    int Config::masterMaxEntityLoad = 0;
    int Config::slaveMaxEntityLoad = 0;
    int Config::targetMergeCapacity = 0;
    double Config::loadMonitoringDeviation = 0.0;
    double Config::zoneLoadMonitoringStepSize = 0.0;
    int Config::loadAssessmentStrategy = 0;
    std::string Config::zoneLoadPartitioningPolicy = "";
    bool Config::churnSocialConsiderRelations = false;
    bool Config::churnSocialConsiderLoad = true;
    bool Config::churnSocialRepartition = false;
    double Config::churnSocialRelationWeight = 0.5;
    bool Config::repartitionSocialOnlyOverloaded = false;
    uint64_t Config::repartitionOverloadedCapacity = 0;
    uint64_t Config::repartitionSocialPhaseWeightDivision = 0;

    // TODO: check for existance of keys
    int Config::LoadConfig(bool everything)
    {
        std::string jsonData = "";
        if (!Shared::System::ReadFile("config.json", jsonData))
        {
            SOLP_ERROR("opening config file failed");
            return 1;
        }

        Shared::JSon::JSon json;
        if (!json.Parse(jsonData))
        {
            SOLP_ERROR("failed to parse config file");
            return 1;
        }

        SOLP_DEBUG(DEBUG_CONFIG, "Config file content:\n%s", json.Serialise().c_str());

        // if we didn't get these as arguments we ready them from the config
        // we handle this now as program arguments
        if (everything)
        {
            SOLP_DEBUG(DEBUG_CONFIG, "Reading everything from config.");

            if (json.Query("globalIP", Config::gLBIp) != Shared::JSon::QueryResult::SUCCESS)
                return 1;

            if (json.Query("trackerIPs", Config::trackerIPs) != Shared::JSon::QueryResult::SUCCESS)
                return 1;

            if (Config::trackerIPs.size() < 1)
                return 1;

            if (json.Query("clientIPs", Config::clientIPs) != Shared::JSon::QueryResult::SUCCESS)
                return 1;

            if (Config::clientIPs.size() < 1)
                return 1;

            if (json.Query("routerIPs", Config::routerIPs) != Shared::JSon::QueryResult::SUCCESS)
                return 1;

            if (Config::routerIPs.size() < 1)
                return 1;
        }
        else
            SOLP_DEBUG(DEBUG_CONFIG, "Not reading everything from config.");


        if (json.Query("PORT_START", Config::startPort) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        typedef std::pair<int, std::string> idToString;
        std::vector<idToString> processToPort;

        processToPort.push_back(idToString(Shared::Id::ProcessType::CELL, "CELL_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::ROUTER, "ROUTER_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::LB_GLOBAL, "LB_GLOBAL_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::LB_SERVER, "LB_SERVER_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::LB_CELL, "LB_CELL_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::ZONE_SERVER_TRACKER, "ZONE_SERVER_TRACKER_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::ROUTER_SELECTOR, "ROUTER_SELECTOR_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::DATABASE, "DATABASE_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::PING_PONG, "PING_PONG_PORT"));
        processToPort.push_back(idToString(Shared::Id::ProcessType::SERVER_MONITOR, "SERVER_MONITOR_PORT"));

        for (int i = 0; i < processToPort.size(); i++)
        {
            if (json.Query(processToPort[i].second, Config::typePorts[processToPort[i].first]) != Shared::JSon::QueryResult::SUCCESS)
                return 1;
        }

        if (json.Query("serverMonitorIntervalInSec", Config::serverMonitorIntervalInSec) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("serverShutdownTimeInSec", Config::serverShutdownTimeInSec) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("connectionTimeoutInSec", Config::connectionTimeoutInSec) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("numClients", Config::numClients) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("routersPerMachine", Config::routersPerMachine) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("networkThreadsPerMachine", Config::networkThreadsPerMachine) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("clientsOnServer", Config::clientsOnServer) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("zonesWithRouter", Config::zonesWithRouter) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("mapX", Config::mapX) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("mapY", Config::mapY) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("walkingDistancePerSec", Config::walkingDistancePerSec) != Shared::JSon::QueryResult::SUCCESS)
            return 1;
        else if (walkingDistancePerSec >= (float)Config::mapX / 2.0f)
            if (walkingDistancePerSec >= (float)Config::mapY / 2.0f)
                return 1;

        if (json.Query("visionRadius", Config::visionRadius) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("actionRadius", Config::actionRadius) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("cellPreloadDistance", Config::cellPreloadDistance) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("cellUnloadDistance", Config::cellUnloadDistance) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::cellUnloadDistance < Config::cellPreloadDistance)
            {
                SOLP_ERROR("The cellUnloadDistance is smaller than the cellPreloadDistance, this is not allowed... terminating...");
                std::terminate();
            }
        }
        else
            return 1;

        if (json.Query("clientAIUpdatesPerSec", Config::clientAIUpdatesPerSec) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("minCellEdgeLength", Config::minCellEdgeLength) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("minZoneEdgeLength", Config::minZoneEdgeLength) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("recordClients", Config::recordClients) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("clientAIName", Config::clientAIName) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("targetInteractionsPerPause", Config::targetInteractionsPerPause) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("loadPlayerRelations", Config::loadPlayerRelations) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("targetSelectionPreferFriends", Config::targetSelectionPreferFriends) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (!Config::loadPlayerRelations)
            {
                SOLP_ERROR("Should load player relations if relation based target selection is enabled...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("targetSelectionIgnoreNonFriends", Config::targetSelectionIgnoreNonFriends) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("targetSelectionOnlyOwnPhase", Config::targetSelectionOnlyOwnPhase) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("friendSelectionModifier", Config::friendSelectionModifier) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::targetSelectionPreferFriends && !Config::targetSelectionIgnoreNonFriends)
            {
                if (std::abs(Config::friendSelectionModifier) < 2)
                {
                    SOLP_ERROR("If targetSelectionPreferFriends is enabled the friend selection modifier should be in the valid range < -1 or > 1.");
                    return 1;
                }
            }
        }
        else
            return 1;

        if (json.Query("maxPhasesPerZone", Config::maxPhasesPerZone) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("maxLoadPerPhase", Config::maxLoadPerPhase) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::maxLoadPerPhase < 0.1)
            {
                SOLP_ERROR("The maxLoadPerPhase is smaller than 0.1 which makes no sense... terminating...");
                return 1;
            }
            else if (Config::maxLoadPerPhase < 0.5)
            {
                SOLP_WARNING("The maxLoadPerPhase is set lower than 0.5, this is probably a mistake...");
            }
            else if (Config::maxLoadPerPhase > 1.0)
            {
                SOLP_ERROR("The maxLoadPerPhase is larger than 1.0 but should be in the range (0.1 - 1.0)... terminating...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("addPhaseThreshold", Config::addPhaseThreshold) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::addPhaseThreshold < 0.1)
            {
                SOLP_ERROR("The addPhaseThreshold is smaller than 0.1 which makes no sense... terminating...");
                return 1;
            }
            else if (Config::addPhaseThreshold < 0.3)
            {
                SOLP_WARNING("The addPhaseThreshold is set lower than 0.3, this is probably a mistake...");
            }
            else if (Config::addPhaseThreshold > 1.0)
            {
                SOLP_ERROR("The addPhaseThreshold is larger than 1.0 but should be in the range (0.1 - 1.0)... terminating...");
                return 1;
            }
            else if (Config::addPhaseThreshold >= Config::maxLoadPerPhase)
            {
                SOLP_ERROR("The addPhaseThreshold is larger than the maxLoadPerPhase... terminating...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("targetMergeLoad", Config::targetMergeLoad) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::targetMergeLoad < 0.2)
            {
                SOLP_WARNING("The targetMergeLoad is set lower than 0.2, this is probably a mistake...");
            }
            else if (Config::targetMergeLoad > Config::addPhaseThreshold)
            {
                SOLP_ERROR("The targetMergeLoad is greater than the addPhaseThreshold, this should not be... terminating...");
                return 1;
            }
            else if (Config::targetMergeLoad > 1.0)
            {
                SOLP_ERROR("The targetMergeLoad is larger than 1.0 but should be in the range (0.2 - 1.0)... terminating...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("masterMaxEntityLoad", Config::masterMaxEntityLoad) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("slaveMaxEntityLoad", Config::slaveMaxEntityLoad) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("targetMergeCapacity", Config::targetMergeCapacity) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("loadMonitoringDeviation", Config::loadMonitoringDeviation) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::loadMonitoringDeviation < 0.01 || Config::loadMonitoringDeviation > 0.1)
            {
                SOLP_ERROR("The loadMonitoringDeviation is not in the range (0.01 - 0.1) ... terminating...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("zoneLoadMonitoringStepSize", Config::zoneLoadMonitoringStepSize) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (Config::zoneLoadMonitoringStepSize < 0.0 || Config::zoneLoadMonitoringStepSize > 1.0)
            {
                SOLP_ERROR("The zoneLoadMonitoringStepSize is not in the range (0.0 - 1.0), 0.0 to disable ... terminating...");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("loadAssessmentStrategy", Config::loadAssessmentStrategy) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("zoneLoadPartitioningPolicy", Config::zoneLoadPartitioningPolicy) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("churnSocialConsiderLoad", Config::churnSocialConsiderLoad) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("churnSocialConsiderRelations", Config::churnSocialConsiderRelations) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("churnSocialRepartition", Config::churnSocialRepartition) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (!Config::churnSocialConsiderLoad && !Config::churnSocialConsiderRelations && !Config::churnSocialRepartition)
            {
                SOLP_ERROR("Zone load partitioning policy requires churnSocialConsiderLoad, churnSocialConsiderRelations or churnSocialRepartition to be true");
                return 1;
            }
        }
        return 1;

        if (json.Query("churnSocialRelationWeight", Config::churnSocialRelationWeight) == Shared::JSon::QueryResult::SUCCESS)
        {
            if (!(Config::churnSocialRelationWeight > 0.0 && Config::churnSocialRelationWeight < 1.0))
            {
                SOLP_ERROR("churnSocialRelationWeight needs to be in the range (0.0, 1.0)");
                return 1;
            }
        }
        else
            return 1;

        if (json.Query("repartitionSocialOnlyOverloaded", Config::repartitionSocialOnlyOverloaded) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("repartitionOverloadedCapacity", Config::repartitionOverloadedCapacity) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        if (json.Query("repartitionSocialPhaseWeightDivision", Config::repartitionSocialPhaseWeightDivision) != Shared::JSon::QueryResult::SUCCESS)
            return 1;

        return 0;
    }
}
