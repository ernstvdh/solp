#include <fstream>

#include "Files.h"

namespace Shared
{
    namespace System
    {
        bool ReadFile(const std::string& filename, std::string& result)
        {
            std::ifstream ifs;
            ifs.open(filename, std::ifstream::in);

            if (!ifs.is_open())
                return false;

            while (!ifs.eof())
            {
                std::string line;
                ifs >> line;
                result += line;
            }

            return true;
        }

        bool WriteFile(const std::string& filename, const std::string& toWrite)
        {
            std::fstream fs;
            fs.open(filename, std::fstream::out | std::fstream::trunc);

            if (!fs.is_open())
                return false;

            fs << toWrite;

            return true;
        }
    }
}
