#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>

#include "Shared/System/System.h"

namespace Shared
{
    namespace System
    {
        uint64_t GetThreadId()
        {
            return GetCurrentThreadId();
        }

        uint64_t GetProcessId()
        {
            return GetCurrentProcessId();
        }

        // implementation taken from the Windows Dev Center
        // Traversing the Thread List
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms686852%28v=vs.85%29.aspx
        std::forward_list<uint64_t> GetThreadIdsOfPId(const uint64_t pId)
        {
            std::forward_list<uint64_t> res;

            HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
            THREADENTRY32 te32;

            // Take a snapshot of all running threads  
            hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
            if (hThreadSnap == INVALID_HANDLE_VALUE)
                return res;

            // Fill in the size of the structure before using it. 
            te32.dwSize = sizeof(THREADENTRY32);

            // Retrieve information about the first thread,
            // and exit if unsuccessful
            if (!Thread32First(hThreadSnap, &te32))
            {
                // Must clean up the snapshot object!
                CloseHandle(hThreadSnap);     
                return res;
            }

            // Now walk the thread list of the system,
            // and display information about each thread
            // associated with the specified process
            do
            {
                if (te32.th32OwnerProcessID == pId)
                {
                    res.push_front(te32.th32ThreadID);
                }
            }
            while (Thread32Next(hThreadSnap, &te32));

            //  Don't forget to clean up the snapshot object.
            CloseHandle(hThreadSnap);
            return res;
        }

        // http://www.drdobbs.com/windows/win32-performance-measurement-options/184416651
        // https://msdn.microsoft.com/en-us/library/ms683237%28VS.85%29.aspx
        bool GetThreadTimesForId(const uint64_t threadId, uint64_t& utime, uint64_t& stime, uint64_t& start)
        {
            FILETIME creation, exit, lstime, lutime;
            HANDLE handle = OpenThread(THREAD_QUERY_INFORMATION, FALSE, threadId);
            if (!handle)
                return false;

            bool res = GetThreadTimes(handle, &creation, &exit, &lstime, &lutime);

            if (res)
            {
                // this takes care of endianness
                utime = lutime.dwHighDateTime;
                utime = utime << 32;
                utime += lutime.dwLowDateTime;

                stime = lstime.dwHighDateTime;
                stime = stime << 32;
                stime += lstime.dwLowDateTime;

                start = creation.dwHighDateTime;
                start = start << 32;
                start += creation.dwLowDateTime;

                // convert from 100ns to 1microseconds timestep
                utime /= 10;
                stime /= 10;
            }

            CloseHandle(handle);
            return res;
        }

        bool GetProcessTimesForCurrent(uint64_t& utime, uint64_t& stime, uint64_t& start)
        {
            FILETIME creation, exit, lstime, lutime;
            HANDLE handle = GetCurrentProcess();
            if (!handle)
                return false;

            bool res = GetProcessTimes(handle, &creation, &exit, &lstime, &lutime);

            if (res)
            {
                // this takes care of endianness
                utime = lutime.dwHighDateTime;
                utime = utime << 32;
                utime += lutime.dwLowDateTime;

                stime = lstime.dwHighDateTime;
                stime = stime << 32;
                stime += lstime.dwLowDateTime;

                start = creation.dwHighDateTime;
                start = start << 32;
                start += creation.dwLowDateTime;

                // convert from 100ns to 1microseconds timestep
                utime /= 10;
                stime /= 10;
            }

            CloseHandle(handle);
            return res;
        }
    }
}
