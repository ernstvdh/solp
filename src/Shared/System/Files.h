#ifndef SOLP_SHARED_SYSTEM_FILES_H
#define SOLP_SHARED_SYSTEM_FILES_H

#include <string>

namespace Shared
{
    namespace System
    {
        bool ReadFile(const std::string& filename, std::string& result);
        bool WriteFile(const std::string& filename, const std::string& toWrite);
    }
}

#endif
