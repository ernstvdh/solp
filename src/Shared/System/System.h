#ifndef SOLP_SHARED_SYSTEM_SYSTEM_H
#define SOLP_SHARED_SYSTEM_SYSTEM_H

#include <cstdint>
#include <forward_list>

namespace Shared
{
    namespace System
    {
        uint64_t GetThreadId();
        uint64_t GetProcessId();
        std::forward_list<uint64_t> GetThreadIdsOfPId(const uint64_t pId);

        // returns utime and stime in microseconds since thread start
        // the start time is guaranteed to be the same for the same thread
        // each time this function is called, but does not mean anything
        // start together with the threadId uniquely identifies a thread
        bool GetThreadTimesForId(const uint64_t threadId, uint64_t& utime, uint64_t& stime, uint64_t& start);
        bool GetProcessTimesForCurrent(uint64_t& utime, uint64_t& stime, uint64_t& start);
    }
}

#endif
