#include <string>
#include <limits>
#include <fstream>

#include <sys/types.h>
#include <sys/syscall.h>
#include <dirent.h>
#include <unistd.h>

#include "Shared/System/System.h"
#include "Shared/Debug.h"

namespace Shared
{
    namespace System
    {
        uint64_t GetThreadId()
        {
            return syscall(SYS_gettid);
        }

        uint64_t GetProcessId()
        {
            return getpid();
        }

        std::forward_list<uint64_t> GetThreadIdsOfPId(const uint64_t pId)
        {
            std::forward_list<uint64_t> res;

            std::string procPath = "/proc/" + std::to_string(pId) + "/task/";
            auto dir = opendir(procPath.c_str());

            if (NULL == dir)
            {
                SOLP_ERROR("Could not open directory with path: %s", procPath.c_str());
                return res;
            }

            auto entity = readdir(dir);

            while (entity != NULL)
            {
                // filter out the current dir . and the parent dir ..
                if (entity->d_type == DT_DIR && entity->d_name[0] != '.')
                    res.push_front(std::stoull(std::string(entity->d_name)));

                entity = readdir(dir);
            }

            closedir(dir);

            return res;
        }

        bool ReadTimesFromFile(const std::string& file, uint64_t& utime, uint64_t& stime, uint64_t& start)
        {
            std::ifstream ifs;
            ifs.open(file, std::ifstream::in);

            if (!ifs.good())
                return false;

            // loop (from 1) to the utime (14) and stime (15) and then starttime (22)
            for (int i = 1; i < 14; i++)
                ifs.ignore(std::numeric_limits<std::streamsize>::max(), ' ');
            if (!ifs.good())
                return false;

            ifs >> utime;
            if (!ifs.good())
                return false;

            ifs >> stime;
            if (!ifs.good())
                return false;

            // we start @ 15 because we still have to read the trailing whitespace of stime
            for (int i = 15; i < 22; i++)
                ifs.ignore(std::numeric_limits<std::streamsize>::max(), ' ');
            if (!ifs.good())
                return false;

            ifs >> start;
            if (!ifs.good())
                return false;

            // convert utime and stime from _SC_CLK_TCK to microseconds
            const uint64_t ticksPerSecond = sysconf(_SC_CLK_TCK);
            // first convert to number of ticks per microsecond
            utime *= 1000000;
            stime *= 1000000;
            // now ticksPerSecond will result in the number of microseconds
            utime /= ticksPerSecond;
            stime /= ticksPerSecond;
            // now the time is in microseconds

            return true;
        }

        bool GetThreadTimesForId(const uint64_t threadId, uint64_t& utime, uint64_t& stime, uint64_t& start)
        {
            std::string file = "/proc/" + std::to_string(GetProcessId()) + "/task/" + std::to_string(threadId) + "/stat";
            
            return ReadTimesFromFile(file, utime, stime, start);
        }

        bool GetProcessTimesForCurrent(uint64_t& utime, uint64_t& stime, uint64_t& start)
        {
            std::string file = "/proc/" + std::to_string(GetProcessId()) + "/stat";

            return ReadTimesFromFile(file, utime, stime, start);
        }
    }
}
