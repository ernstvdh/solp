#ifndef SOLP_SHARED_CSR_H
#define SOLP_SHARED_CSR_H

#include <vector>
#include <algorithm>

namespace Shared
{
    // IT: type of the index we store mapping vertex <-> edge, preferred type is size_t
    // WT: type of the weights
    // ET: type of the external identifier of vertices
    template <typename IT, typename WT, typename ET>
    class CSR
    {
    public:
        CSR() : xadj(1, 0) {}

        void AddVertex(const ET id)
        {
            // possibly loss of information, but important to realize that so no cast
            IT internalId = internalToExternalId.size();
            internalToExternalId.push_back(id);

            externalToInternalId.emplace(id, internalId);

            // add last element to our vertex vector, this basically means we have a
            // vertex without any edges because they both have the same value
            xadj.push_back(xadj[internalId]);
        }

        // can only add edges from an existing starting vertex to and existing end vertex
        // undefined behaviour if either of the two does not exist yet
        // ITER: the type (of the edges) should be iterable (have a size, begin, end function)
        template <typename ITER>
        void AddEdgesDirected(const ET from, const ITER& to)
        {
            IT fromInternalId = externalToInternalId[from];

            std::vector<IT> internalTo(to.size());
            std::transform(to.begin(), to.end(), internalTo.begin(),
                [this](const ET externalId){ return externalToInternalId[externalId]; });

            // add the edges after the last edge already assigned to the from vertex
            // and before the first edge from the vertex after us
            adjncy.insert(adjncy.begin() + xadj[fromInternalId + 1], internalTo.begin(), internalTo.end());

            // move end/begin points of the edge lists for the vertices after our vertex
            // we move this last so we have cheap access to the previous adjncy position of
            // the vertex after the one we are adding edges to now
            std::for_each(xadj.begin() + fromInternalId + 1, xadj.end(), [&](IT& pos){ pos += to.size(); });
        }

        // can only add edges from an existing starting vertex to and existing end vertex
        // undefined behaviour if either of the two does not exist yet
        // EITER: the type (of the edges) should be iterable (have a size, begin, end function)
        // WITER: the type (of the weights) should be iterable (have a size, begin, end function)
        template <typename EITER, typename WITER>
        void AddEdgesDirected(const ET from, const EITER& to, const WITER& weights)
        {
            IT fromInternalId = externalToInternalId[from];

            // add the edge weights after the last edge weight already assigned to the from vertex
            // and before the first edge weight from the vertex after us
            adjwgt.insert(adjwgt.begin() + xadj[fromInternalId + 1], weights.begin(), weights.end());

            AddEdgesDirected(from, to);
        }

        IT* GetRawVertices() { return xadj.data(); }
        IT* GetRawEdges() { return adjncy.data(); }
        IT GetSize() { return xadj.size() - 1; }

        // returns NULL if unweighted graph
        WT* GetRawVertexWeights()
        {
            if (vwgt.empty())
                return NULL;

            return vwgt.data();
        }

        // returns NULL if unweighted graph
        WT* GetRawEdgeWeights()
        {
            if (adjwgt.empty())
                return NULL;

            return adjwgt.data();
        }

    private:
        std::vector<IT> xadj;
        std::vector<IT> adjncy;

        // weights (can be empty if it is an unweighted graphs)
        std::vector<WT> vwgt;
        std::vector<WT> adjwgt;

        // mapping from and to internal CSR id and external id
        std::vector<ET> internalToExternalId;
        std::unordered_map<ET, IT> externalToInternalId;
    };
}

#endif
