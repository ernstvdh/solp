#include <algorithm>

#include "Util.h"
#include "glm/geometric.hpp"

namespace Shared
{
    namespace Util
    {
        std::pair<int, int> PositionToZoneCoords(const glm::vec2& pos)
        {
            const int x = static_cast<int>(pos.x / Shared::Config::minZoneEdgeLength);
            const int y = static_cast<int>(pos.y / Shared::Config::minZoneEdgeLength);

            return std::pair<int, int>(x, y);
        }

        std::pair<int, int> PositionToCellCoords(const glm::vec2& pos)
        {
            const int x = static_cast<int>(pos.x / Shared::Config::minCellEdgeLength);
            const int y = static_cast<int>(pos.y / Shared::Config::minCellEdgeLength);

            return std::pair<int, int>(x, y);
        }

        int ZoneXToCellXCoord(int zoneX)
        {
            return static_cast<int>((zoneX * Shared::Config::minZoneEdgeLength) / Shared::Config::minCellEdgeLength);
        }

        int ZoneYToCellYCoord(int zoneY)
        {
            return static_cast<int>((zoneY * Shared::Config::minZoneEdgeLength) / Shared::Config::minCellEdgeLength);
        }

        int CellXToZoneXCoord(int cellX)
        {
            return static_cast<int>((cellX * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength);
        }

        int CellYToZoneYCoord(int cellY)
        {
            return static_cast<int>((cellY * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength);
        }

        std::pair<int, int> PositionsToFirstZone(const glm::vec2& from, const glm::vec2& to)
        {
            std::pair<int, int> res;

            glm::vec2 ori = glm::normalize(to - from);

            const int fromx = static_cast<int>(from.x / Shared::Config::minZoneEdgeLength);
            const int fromy = static_cast<int>(from.y / Shared::Config::minZoneEdgeLength);

            float xRelativeDistance;
            if (ori.x >= 0.0f)
                xRelativeDistance = ((fromx + 1) * static_cast<int>(Shared::Config::minZoneEdgeLength)) - from.x;
            else
                xRelativeDistance = from.x - (fromx * static_cast<int>(Shared::Config::minZoneEdgeLength));

            float yRelativeDistance;
            if (ori.y >= 0.0f)
                yRelativeDistance = ((fromy + 1) * static_cast<int>(Shared::Config::minZoneEdgeLength)) - from.y;
            else
                yRelativeDistance = from.y - (fromy * static_cast<int>(Shared::Config::minZoneEdgeLength));

            xRelativeDistance /= std::abs(ori.x);
            yRelativeDistance /= std::abs(ori.y);

            if (xRelativeDistance <= yRelativeDistance)
            {
                res.first = ori.x < 0 ? fromx - 1 : fromx + 1;
                res.second = fromy;
            }
            else
            {
                res.first = fromx;
                res.second = ori.y < 0 ? fromy - 1 : fromy + 1;
            }

            return res;
        }

        glm::vec2 PositionOnLineInZone(const glm::vec2& from, const glm::vec2& to, const int zoneX, const int zoneY)
        {
            auto fromZone = PositionToZoneCoords(from);
            if (fromZone.first == zoneX && fromZone.second == zoneY)
                return from;

            auto toZone = PositionToZoneCoords(to);
            if (toZone.first == zoneX && toZone.second == zoneY)
                return to;

            double edgeTop = (zoneY + 1) * Shared::Config::minZoneEdgeLength;
            double edgeBottom = zoneY * Shared::Config::minZoneEdgeLength;
            double edgeLeft = zoneX * Shared::Config::minZoneEdgeLength;
            double edgeRight = (zoneX + 1) * Shared::Config::minZoneEdgeLength;

            bool hitEdgeTop = false;
            bool hitEdgeBottom = false;
            bool hitEdgeLeft = false;
            bool hitEdgeRight = false;
            // we check through which 2 edges the lines goes
            glm::vec2 ori = to - from;
            double distance = 0.0;
            double hit = 0.0;

            // avoid divide by zero, also note that we cannot hit the left and right
            // edge if our x ori is zero, this means that we don't move horizontally
            if (ori.x != 0.0f)
            {
                distance = (edgeLeft - from.x) / ori.x;
                hit = (distance * ori.y) + from.y;
                if (hit >= edgeBottom && hit < edgeTop)
                    hitEdgeLeft = true;

                distance = (edgeRight - from.x) / ori.x;
                hit = (distance * ori.y) + from.y;
                if (hit >= edgeBottom && hit < edgeTop)
                    hitEdgeRight = true;
            }

            // avoid divide by zero, also note that we cannot hit the top and bottom
            // edge if our y ori is zero, this means that we don't move vertically
            if (ori.y != 0.0f)
            {
                distance = (edgeBottom - from.y) / ori.y;
                hit = (distance * ori.x) + from.x;
                if (hit >= edgeLeft && hit < edgeRight)
                    hitEdgeBottom = true;

                distance = (edgeTop - from.y) / ori.y;
                hit = (distance * ori.x) + from.x;
                if (hit >= edgeLeft && hit < edgeRight)
                    hitEdgeTop = true;
            }

            // if hitting bottom and top we pick an y value in between
            if (hitEdgeTop && hitEdgeBottom)
            {
                double y = (double)Shared::Config::minZoneEdgeLength * 0.5 + edgeBottom;
                distance = (y - from.y) / ori.y;
                hit = (distance * ori.x) + from.x;

                return glm::vec2(hit, y);
            }

            // if hitting left and right we pick an x value in between
            if (hitEdgeLeft && hitEdgeRight)
            {
                double x = (double)Shared::Config::minZoneEdgeLength * 0.5 + edgeLeft;
                distance = (x - from.x) / ori.x;
                hit = (distance * ori.y) + from.y;

                return glm::vec2(x, hit);
            }

            glm::vec2 res;
            if (hitEdgeLeft)
            {
                distance = (edgeLeft - from.x) / ori.x;
                hit = (distance * ori.y) + from.y;

                res = glm::vec2(edgeLeft, hit);
            }
            else  if (hitEdgeBottom)
            {
                distance = (edgeBottom - from.y) / ori.y;
                hit = (distance * ori.x) + from.x;

                res = glm::vec2(hit, edgeBottom);
            }
            else if (hitEdgeRight)
            {
                double x = edgeRight - MapCoordEpsilon();
                distance = (x - from.x) / ori.x;
                hit = (distance * ori.y) + from.y;
                
                res = glm::vec2(x, hit);
            }
            else
            {
                double y = edgeTop - MapCoordEpsilon();
                distance = (y - from.y) / ori.y;
                hit = (distance * ori.x) + from.x;

                res = glm::vec2(hit, y);
            }

            // making sure that the resulting coordinates are within the zone borders...
            // if not we slightly move the path to really go through this zone
            auto inZonePosZone = Shared::Util::PositionToZoneCoords(res);
            if (inZonePosZone.first > zoneX)
                res.x = edgeRight - MapCoordEpsilon();
            else if (inZonePosZone.first < zoneX)
                res.x = edgeLeft;

            if (inZonePosZone.second > zoneY)
                res.y = edgeTop - MapCoordEpsilon();
            else if (inZonePosZone.second < zoneY)
                res.y = edgeBottom;

            return res;
        }

        bool IsZoneInDirection(int cx, int cy, int dx, int dy)
        {
            if ((cx + dx) * Shared::Config::minZoneEdgeLength >= Shared::Config::mapX)
                return false;
            if ((cx + dx) * Shared::Config::minZoneEdgeLength < 0)
                return false;
            if ((cy + dy) * Shared::Config::minZoneEdgeLength >= Shared::Config::mapY)
                return false;
            if ((cy + dy) * Shared::Config::minZoneEdgeLength < 0)
                return false;

            return true;
        }

        std::unordered_map<int, std::unordered_map<int, bool>> ZonesCoveredByCell(int x, int y)
        {
            std::unordered_map<int, std::unordered_map<int, bool>> res;

            // if borders match we have to substract 1 from the endx and endy
            // because we do not really overlap with that zone
            uint64_t startx, starty, endx, endy;
            startx = (x * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength;
            starty = (y * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength;
            endx = ((x + 1) * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength;
            if (((x + 1) * Shared::Config::minCellEdgeLength) % Shared::Config::minZoneEdgeLength == 0)
                endx--;
            if (endx * Shared::Config::minZoneEdgeLength >= Shared::Config::mapX)
                endx = (double)Shared::Config::mapX - 0.5 / Shared::Config::minZoneEdgeLength;

            endy = ((y + 1) * Shared::Config::minCellEdgeLength) / Shared::Config::minZoneEdgeLength;
            if (((y + 1) * Shared::Config::minCellEdgeLength) % Shared::Config::minZoneEdgeLength == 0)
                endy--;
            if (endy * Shared::Config::minZoneEdgeLength >= Shared::Config::mapY)
                endy = (double)Shared::Config::mapY - 0.5 / Shared::Config::minZoneEdgeLength;

            for (int i = startx; i <= endx; i++)
                for (int j = starty; j <= endy; j++)
                    res[i][j] = true;

            return res;
        }

        bool CellInRange(const glm::vec2& pos, double range, int cellx, int celly)
        {
            int closestCornerX = (int)(pos.x / (double)Shared::Config::minCellEdgeLength + 0.5) <= cellx ? cellx : cellx + 1;
            int closestCornerY = (int)(pos.y / (double)Shared::Config::minCellEdgeLength + 0.5) <= celly ? celly : celly + 1;
            if (IsInRange(closestCornerX * Shared::Config::minCellEdgeLength,
                closestCornerY * Shared::Config::minCellEdgeLength,
                pos.x, pos.y, range))
                return true;

            return false;
        }

        int CellCornersInRange(const glm::vec2& pos, double range, int cellx, int celly)
        {
            int res = 0;

            if (IsInRange(cellx * Shared::Config::minCellEdgeLength,
                celly * Shared::Config::minCellEdgeLength,
                pos.x, pos.y, Shared::Config::visionRadius))
                res++;

            if (IsInRange((cellx + 1) * Shared::Config::minCellEdgeLength,
                celly * Shared::Config::minCellEdgeLength,
                pos.x, pos.y, Shared::Config::visionRadius))
                res++;

            if (IsInRange(cellx * Shared::Config::minCellEdgeLength,
                (celly + 1) * Shared::Config::minCellEdgeLength,
                pos.x, pos.y, Shared::Config::visionRadius))
                res++;

            if (IsInRange((cellx + 1) * Shared::Config::minCellEdgeLength,
                (celly + 1) * Shared::Config::minCellEdgeLength,
                pos.x, pos.y, Shared::Config::visionRadius))
                res++;

            return res;
        }

        bool ZoneInRange(const glm::vec2& pos, double range, int zonex, int zoney)
        {
            int closestCornerX = (int)(pos.x / (double)Shared::Config::minZoneEdgeLength + 0.5) <= zonex ? zonex : zonex + 1;
            int closestCornerY = (int)(pos.y / (double)Shared::Config::minZoneEdgeLength + 0.5) <= zoney ? zoney : zoney + 1;
            if (IsInRange(closestCornerX * Shared::Config::minZoneEdgeLength,
                closestCornerY * Shared::Config::minZoneEdgeLength,
                pos.x, pos.y, range))
                return true;

            return false;
        }

        std::unordered_map<int, std::unordered_map<int, bool>> CellCoordsInRange(const glm::vec2& pos, double range)
        {
            // only consider cells within the map
            std::unordered_map<int, std::unordered_map<int, bool>> res;
            int startx = (pos.x - range < 0.0 ? 0.0 : pos.x - range) / Shared::Config::minCellEdgeLength;
            int starty = (pos.y - range < 0.0 ? 0.0 : pos.y - range) / Shared::Config::minCellEdgeLength;
            // the cell border is an int so we safely substract 0.5 from the max border to avoid getting at the top and right side
            int endx = (pos.x + range >= Shared::Config::mapX ? (double)Shared::Config::mapX - 0.5 : pos.x + range) / Shared::Config::minCellEdgeLength;
            int endy = (pos.y + range >= Shared::Config::mapY ? (double)Shared::Config::mapY - 0.5 : pos.y + range) / Shared::Config::minCellEdgeLength;

            for (int x = startx; x <= endx; x += 1)
            {
                for (int y = starty; y <= endy; y += 1)
                {
                    // cells on the x or y line are in range anyway otherwise they wouldn't be within startx <-> endx
                    if (x == (int)(pos.x / Shared::Config::minCellEdgeLength) || y == (int)(pos.y / Shared::Config::minCellEdgeLength))
                    {
                        res[x][y] = true;
                        continue;
                    }

                    if (CellInRange(pos, range, x, y))
                        res[x][y] = true;
                }
            }

            return res;
        }

        std::unordered_map<int, std::unordered_map<int, bool>> ZoneCoordsInRange(const glm::vec2& pos, double range)
        {
            // only consider cells within the map
            std::unordered_map<int, std::unordered_map<int, bool>> res;
            int startx = (pos.x - range < 0.0 ? 0.0 : pos.x - range) / Shared::Config::minZoneEdgeLength;
            int starty = (pos.y - range < 0.0 ? 0.0 : pos.y - range) / Shared::Config::minZoneEdgeLength;
            // the cell border is an int so we safely substract 0.5 from the max border to avoid getting at the top and right side
            int endx = (pos.x + range >= Shared::Config::mapX ? (double)Shared::Config::mapX - 0.5 : pos.x + range) / Shared::Config::minZoneEdgeLength;
            int endy = (pos.y + range >= Shared::Config::mapY ? (double)Shared::Config::mapY - 0.5 : pos.y + range) / Shared::Config::minZoneEdgeLength;

            for (int x = startx; x <= endx; x += 1)
            {
                for (int y = starty; y <= endy; y += 1)
                {
                    // cells on the x or y line are in range anyway otherwise they wouldn't be within startx <-> endx
                    if (x == (int)(pos.x / Shared::Config::minZoneEdgeLength) || y == (int)(pos.y / Shared::Config::minZoneEdgeLength))
                    {
                        res[x][y] = true;
                        continue;
                    }

                    if (ZoneInRange(pos, range, x, y))
                        res[x][y] = true;
                }
            }

            return res;
        }

        void Diff2DMap(std::unordered_map<int, std::unordered_map<int, bool>>& oldMap,
            std::unordered_map<int, std::unordered_map<int, bool>>& newMap)
        {
            for (auto& x : newMap)
            {
                const auto& xFind = oldMap.find(x.first);
                if (xFind != oldMap.end())
                {
                    for (auto& y : x.second)
                    {
                        const auto& yFind = xFind->second.find(y.first);
                        if (yFind != xFind->second.end())
                            xFind->second.erase(y.first);
                        else
                            oldMap[x.first][y.first] = false;
                    }
                }
                else
                {
                    for (auto& y : x.second)
                        oldMap[x.first][y.first] = false;
                }
            }
        }
    }
}
