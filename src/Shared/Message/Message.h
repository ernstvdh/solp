#ifndef SOLP_SHARED_MESSAGE_MESSAGE_H
#define SOLP_SHARED_MESSAGE_MESSAGE_H

#include <string>
#include <thread>
#include <vector>
#include <functional>

#include "Shared/Id.h"
#include "Shared/JSon/JSon.h"

#define MESSAGE_ADD_TO_JSON(c) json.Add(#c, c)
#define MESSAGE_GET_FROM_JSON_STRICT(c) json.QueryStrict(#c, c)

namespace Shared
{
    namespace Message
    {
        class Message : public Serializable
        {
        public:
            virtual ~Message() {}
            Message() {}

            Message(const Shared::JSon::JSon& json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(senderID);
                MESSAGE_GET_FROM_JSON_STRICT(receiverID);
                MESSAGE_GET_FROM_JSON_STRICT(processType);
                MESSAGE_GET_FROM_JSON_STRICT(outMessNum);
                MESSAGE_GET_FROM_JSON_STRICT(inMessNum);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                MESSAGE_ADD_TO_JSON(senderID);
                MESSAGE_ADD_TO_JSON(receiverID);
                MESSAGE_ADD_TO_JSON(processType);
                MESSAGE_ADD_TO_JSON(outMessNum);
                MESSAGE_ADD_TO_JSON(inMessNum);
            }

            static std::shared_ptr<Message> ParseJSon(const Shared::JSon::JSon& json);
            static std::shared_ptr<Message> ParseMessage(const char* str);

            template <typename T>
            static std::shared_ptr<T> MakeMessage(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& receiverId)
            {
                std::shared_ptr<T> temp(std::make_shared<T>());
                temp->senderID = ownId;
                temp->receiverID = receiverId;
                return temp;
            }

            template <typename T>
            static std::shared_ptr<T> MakeCopy(const std::shared_ptr<T>& toCopy)
            {
                return std::shared_ptr<T>(new T(*(toCopy.get())));
            }

            std::string Serialise()
            {
                Shared::JSon::JSon json;
                SerializeToJSon(json);
                return json.Serialise();
            }

            std::shared_ptr<const Shared::Id> senderID;
            std::shared_ptr<const Shared::Id> receiverID;
            Shared::Id::ProcessType processType;
            uint32_t outMessNum;
            uint32_t inMessNum;
        };

        struct MessageContainer
        {
            MessageContainer(std::shared_ptr<Shared::Message::Message> lMess) : mess(lMess) {}
            ~MessageContainer() {}

            std::shared_ptr<Shared::Message::Message> mess;
        };
    }
}

#endif
