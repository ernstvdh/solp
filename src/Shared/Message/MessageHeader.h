#ifndef SOLP_SHARED_MESSAGE_MESSAGEHEADER_H
#define SOLP_SHARED_MESSAGE_MESSAGEHEADER_H

#include <string>
#include <vector>
#include <stdint.h>

namespace Shared
{
    namespace Message
    {
        struct MessageHeader
        {
            uint32_t value;
            MessageHeader(uint32_t init) : value(init) {}
            MessageHeader(std::string& init)
            {
                value = StringToValue(init);
            }

            MessageHeader(std::vector<char>& init)
            {
                value = VectorToValue(init);
            }

            static const size_t Size()
            {
                return 4;
            }

            std::string GetString()
            {
                return ValueToString();
            }

            std::vector<char> GetVector()
            {
                return ValueToVector();
            }

            std::vector<char> GetRoomyVector()
            {
                return ValueToVector(true);
            }

            // give as input a string with 4 bytes of free space
            static void InsertHeader(std::string& str)
            {
                uint32_t val = str.size() - 4;
                for (int i = 0; i < 4; i++)
                    str[i] = (val >> (i * 8) & 0xFF);
            }

        private:
            std::string ValueToString()
            {
                std::vector<char> chars(ValueToVector());
                return std::string(chars.begin(), chars.end());
            }

            std::vector<char> ValueToVector(bool roomy = false)
            {
                std::vector<char> chars(roomy ? (4 + value) : 4);

                for (int i = 0; i < 4; i++)
                    chars[i] = (value >> (i * 8) & 0xFF);

                return chars;
            }

            uint32_t StringToValue(std::string& init)
            {
                std::vector<char> chars(4);
                for (int i = 0; i < 4; i++)
                    chars[i] = init[i];

                return VectorToValue(chars);
            }

            uint32_t VectorToValue(std::vector<char>& chars)
            {
                uint32_t temp = 0;
                for (int i = 3; i >= 0; i--)
                    temp |= ((unsigned char)chars[i] << (i * 8));

                return temp;
            }
        };
    }
}

#endif
