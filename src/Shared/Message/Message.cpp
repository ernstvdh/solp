#include "Shared/Debug.h"
#include "Shared/Net/ConnectionMessage.h"
#include "Shared/PingPong/PingPongMessage.h"
#include "Shared/Link/LinkMessage.h"
#include "Balancing/Tracker/TrackerMessage.h"
#include "Balancing/Entry/EntryMessage.h"
#include "Balancing/Global/GlobalMessage.h"
#include "Balancing/Server/ServerMessage.h"
#include "Balancing/Zone/ZoneMessage.h"
#include "Processing/Router/RouterMessage.h"
#include "Processing/Zone/ZoneMessage.h"
#include "Client/ClientMessage.h"
#include "Monitoring/Server/ServerMessage.h"

namespace Shared
{
    namespace Message
    {
        // this method should never be called and is only a placeholder
        std::shared_ptr<Message> PathDoesNotExist(const Shared::JSon::JSon& json)
        {
            SOLP_ERROR("unexepected message translation path for message\n%s\n... aborting....",
                json.Serialise().c_str());
            std::terminate();
        }

        // add all message translation paths here
        const std::vector<std::function<std::shared_ptr<Message>(const Shared::JSon::JSon&)>> enumToMessageMap
        {
            Client::ClientMessage::ParseJSon,
            Processing::Zone::ZoneMessage::ParseJSon,
            Processing::Router::RouterMessage::ParseJSon,
            BL::Global::GlobalMessage::ParseJSon,
            BL::Server::ServerMessage::ParseJSon,
            BL::Zone::ZoneMessage::ParseJSon,
            BL::Tracker::TrackerMessage::ParseJSon,
            BL::Entry::EntryMessage::ParseJSon,
            PathDoesNotExist,
            Shared::PingPong::PingPongMessage::ParseJSon,
            Monitor::Server::ServerMessage::ParseJSon,
            Shared::Link::LinkMessage::ParseJSon,
            Shared::Net::ConnectionMessage::ParseJSon
        };

        std::shared_ptr<Message> Message::ParseJSon(const Shared::JSon::JSon& json)
        {
            Shared::Id::ProcessType pt;
            json.QueryStrict("processType", pt);

            if (pt >= Shared::Id::ProcessType::NOTHING_AND_MAX_VALUE)
            {
                SOLP_ERROR("received out of bounds process type, aborting...");
                std::terminate();
            }

            return enumToMessageMap[pt](json);
        }

        std::shared_ptr<Message> Message::ParseMessage(const char* str)
        {
            Shared::JSon::JSon json;
            if (!json.Parse(str))
            {
                SOLP_ERROR("received corrupt message, could not parse to json\n%s", json.GetError().c_str());
                std::terminate();
            }

            return Message::ParseJSon(json);
        }
    }
}
