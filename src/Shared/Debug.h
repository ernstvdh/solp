#ifndef SOLP_SHARED_DEBUG_H
#define SOLP_SHARED_DEBUG_H

#if defined ( WIN32 )
#define __func__ __FUNCTION__
#endif

#define DEBUG_CONNECTION                0
#define DEBUG_CONNECTION_LISTENER       0
#define DEBUG_CONNECTION_MGR            0
#define DEBUG_SOLP_COMMAND_LINE_ARG     0
#define DEBUG_SOLP                      1
#define DEBUG_CONFIG                    0
#define DEBUG_LINKABLE                  0
#define DEBUG_LINK                      0
#define DEBUG_STREAM_RESUMPTION         0
#define DEBUG_STREAM_RESET              0
#define DEBUG_LINKMGR                   0
#define DEBUG_LINKLISTENER              0
#define DEBUG_ACCEPTEDLINK              0
#define DEBUG_PINGPONG                  0
#define DEBUG_ROUTER                    0
#define DEBUG_BL_SERVER                 0
#define DEBUG_BL_ENTRY                  0
#define DEBUG_CLIENT                    0
#define DEBUG_TRACKER                   0
#define DEBUG_ZONE_TRANSFER             0
#define DEBUG_CONNECTION_LINK           0
#define DEBUG_MONITOR_SERVER            0
#define DEBUG_DELETE_CALLS              0
#define DEBUG_CELL_DATA                 0
#define DEBUG_ZONE_TO_CELL_DATA         0
#define DEBUG_INTERACTION               0
#define DEBUG_ZONE_LOAD                 0
#define DEBUG_ZONE_HANDLER              0
#define DEBUG_TAGGED_ENTITIES           0
#define DEBUG_ZONE_ADD_REMOVE           0
#define DEBUG_ZONE_GENERAL              0

#define CONCAT(a,b) a##b

#define SOLP_DEBUG(enabled, message, ...) CONCAT(SOLP_DEBUG,enabled)(message, ##__VA_ARGS__)
#define SOLP_DEBUG0(...) do {} while(false)
#define SOLP_DEBUG1(message, ...) printf("%s:%d - %s\nDEBUG: " message "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#define SOLP_WARNING(message, ...) printf("%s:%d - %s\nWARNING: " message "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#define SOLP_ERROR(message, ...) printf("%s:%d - %s\nERROR: " message "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#endif
