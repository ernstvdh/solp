#ifndef SOLP_SHARED_CONFIG_H
#define SOLP_SHARED_CONFIG_H

#include <fstream>
#include <vector>
#include <cstdint>

namespace Shared
{
    class Config
    {
    public:
        static std::string localIP;

        static std::string gLBIp;

        static int startPort;
        static std::vector<int> typePorts;
        static std::vector<std::string> trackerIPs;
        static std::vector<std::string> clientIPs;
        static std::vector<std::string> routerIPs;

        static uint64_t serverMonitorIntervalInSec;
        static uint64_t serverShutdownTimeInSec;
        static uint64_t connectionTimeoutInSec;
        static uint64_t numClients;
        static uint64_t mapX;
        static uint64_t mapY;
        static uint64_t minZoneEdgeLength;
        static uint64_t minCellEdgeLength;
        static uint64_t routersPerMachine;
        static uint64_t networkThreadsPerMachine;

        static bool recordClients;
        static std::string clientAIName;
        static uint64_t targetInteractionsPerPause;
        static bool loadPlayerRelations;
        static bool targetSelectionPreferFriends;
        static bool targetSelectionIgnoreNonFriends;
        static bool targetSelectionOnlyOwnPhase;
        static int friendSelectionModifier;

        static bool clientsOnServer;
        static bool zonesWithRouter;

        static float walkingDistancePerSec;
        static double visionRadius;
        static double actionRadius;
        static double cellPreloadDistance;
        static double cellUnloadDistance;
        static uint64_t clientAIUpdatesPerSec;

        static int maxPhasesPerZone;
        static double targetMergeLoad;
        static double maxLoadPerPhase;
        static double addPhaseThreshold;
        static int masterMaxEntityLoad;
        static int slaveMaxEntityLoad;
        static int targetMergeCapacity;

        static double loadMonitoringDeviation;
        static double zoneLoadMonitoringStepSize;

        static int loadAssessmentStrategy;
        static std::string zoneLoadPartitioningPolicy;

        static bool churnSocialConsiderRelations;
        static bool churnSocialConsiderLoad;
        static bool churnSocialRepartition;
        static double churnSocialRelationWeight;
        static bool repartitionSocialOnlyOverloaded;
        static uint64_t repartitionOverloadedCapacity;
        static uint64_t repartitionSocialPhaseWeightDivision;

        static int LoadConfig(bool everything);
    };
}

#endif
