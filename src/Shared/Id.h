#ifndef SOLP_SHARED_ID_H
#define SOLP_SHARED_ID_H

#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <unordered_map>

#include "Serializable.h"
#include "Shared/Debug.h"

namespace Shared
{
    class Id : public Serializable
    {
    public:
        // if this enum is changed we also have to change the message parsing tree and
        // the Id parsing tree
        // also note that if the id of a particular process type changes the processing
        // scripts might also require that same change...
        enum ProcessType
        {
            CLIENT,
            CELL,
            ROUTER,
            LB_GLOBAL,
            LB_SERVER,
            LB_CELL,
            ZONE_SERVER_TRACKER,
            ROUTER_SELECTOR,
            DATABASE,
            PING_PONG,
            SERVER_MONITOR,
            // the connection manager/a link is always listening
            // but for the sake of addressability add it here
            LINK,
            CONTROL,

            // nothing should be the last type
            // this value is also the maximum number
            // of process types (can be used to initialize
            // for example an array)
            NOTHING_AND_MAX_VALUE
        };

        Id(ProcessType t) : type(t) {}
        virtual ~Id() {}

        // returns a key string of the id
        std::string Key() const
        {
            return Serialize();
        }

        virtual std::string Serialize() const = 0;
        virtual void SerializeToJSon(Shared::JSon::JSon& json) const = 0;

        virtual bool equals(const std::shared_ptr<const Id>& that) const = 0;

        virtual bool IsIpId() const { return false; }

        static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
        {
            ProcessType pType;
            json.QueryStrict("type", pType);

            if (pType > NOTHING_AND_MAX_VALUE)
            {
                pType = NOTHING_AND_MAX_VALUE;
                SOLP_ERROR("Unknown process type given to parser.");
            }

            return enumToIdMap[pType](json);
        }

        static const std::vector<std::function<std::shared_ptr<Id>(const Shared::JSon::JSon&)>> enumToIdMap;
        static std::shared_ptr<Id> PathDoesNotExist(const Shared::JSon::JSon& json);

        const ProcessType type;
    };

    class IpId : public Id
    {
    public:
        IpId(const std::string& lIp, ProcessType t) : Id(t), ip(lIp) {}
        IpId(const Shared::JSon::JSon& json, ProcessType t) : Id(t)
        {
            GET_FROM_JSON_STRICT(ip);
        }

        virtual ~IpId() {}

        std::string ip;

        virtual std::string Serialize() const
        {
            return std::to_string(type) + "_" + ip;
        }

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(type);
            ADD_TO_JSON(ip);
        }

        virtual bool IsIpId() const { return true; }

        virtual bool equals(const std::shared_ptr<const Id>& that) const
        {
            if (type != that->type)
                return false;

            std::shared_ptr<const IpId> otherId(std::static_pointer_cast<const IpId>(that));

            if (ip != otherId->ip)
                return false;

            return true;
        }
    };
}

#endif
