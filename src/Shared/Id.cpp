#include "Id.h"
#include "Processing/Zone/ZoneId.h"
#include "Processing/Router/RouterId.h"
#include "Balancing/Global/GlobalId.h"
#include "Balancing/Server/ServerId.h"
#include "Balancing/Zone/ZoneId.h"
#include "Balancing/Tracker/TrackerId.h"
#include "Balancing/Entry/EntryId.h"
#include "Shared/PingPong/PingPongId.h"
#include "Shared/Link/LinkId.h"
#include "Shared/Net/ConnectionId.h"
#include "Client/ClientId.h"
#include "Monitoring/Server/ServerId.h"

namespace Shared
{
    std::shared_ptr<Id> Id::PathDoesNotExist(const Shared::JSon::JSon& json)
    {
        SOLP_ERROR("unexepected id translation path, aborting..");
        std::terminate();
    }

    const std::vector<std::function<std::shared_ptr<Id>(const Shared::JSon::JSon&)>> Id::enumToIdMap
    {
        Client::ClientId::ParseJSon,
        Processing::Zone::ZoneId::ParseJSon,
        Processing::Router::RouterId::ParseJSon,
        BL::Global::GlobalId::ParseJSon,
        BL::Server::ServerId::ParseJSon,
        BL::Zone::ZoneId::ParseJSon,
        BL::Tracker::TrackerId::ParseJSon,
        BL::Entry::EntryId::ParseJSon,
        PathDoesNotExist,
        Shared::PingPong::PingPongId::ParseJSon,
        Monitor::Server::ServerId::ParseJSon,
        Shared::Link::LinkId::ParseJSon,
        Shared::Net::ConnectionId::ParseJSon
    };
}
