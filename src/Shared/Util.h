#ifndef SOLP_SHARED_UTIL_H
#define SOLP_SHARED_UTIL_H

#include <cmath>
#include <random>
#include <unordered_map>

#include "glm/vec2.hpp"

#include "Config.h"

namespace Shared
{
    namespace Util
    {
        inline float MapCoordEpsilon()
        {
            return std::numeric_limits<float>::epsilon() * (
                (Shared::Config::mapX > Shared::Config::mapY ? Shared::Config::mapX : Shared::Config::mapY) + 1);
        }

        inline glm::vec2 GetRandomPositionOnMap(std::default_random_engine& engine)
        {
            std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
            return glm::vec2(((float)Shared::Config::mapX - MapCoordEpsilon()) * distribution(engine),
                ((float)Shared::Config::mapY - MapCoordEpsilon()) * distribution(engine));
        }

        std::pair<int, int> PositionToZoneCoords(const glm::vec2& pos);
        std::pair<int, int> PositionToCellCoords(const glm::vec2& pos);
        int ZoneXToCellXCoord(int zoneX);
        int ZoneYToCellYCoord(int zoneY);
        int CellXToZoneXCoord(int cellX);
        int CellYToZoneYCoord(int cellY);
        // this method assumes that the from and to coords are not in the same zone
        std::pair<int, int> PositionsToFirstZone(const glm::vec2& from, const glm::vec2& to);
        // this method assumes that part of the line from -> to is in this zone
        glm::vec2 PositionOnLineInZone(const glm::vec2& from, const glm::vec2& to, const int zoneX, const int zoneY);

        bool IsZoneInDirection(int cx, int cy, int dx, int dy);

        std::unordered_map<int, std::unordered_map<int, bool>> ZonesCoveredByCell(int x, int y);
        // for now we use a square range + euclidian
        //TODO: optimize?
        //TODO: if the interest management cell size becomes dynamic we should change this method
        bool CellInRange(const glm::vec2& pos, double range, int cellx, int celly);
        int CellCornersInRange(const glm::vec2& pos, double range, int cellx, int celly);
        std::unordered_map<int, std::unordered_map<int, bool>> CellCoordsInRange(const glm::vec2& pos, double range);
        bool ZoneInRange(const glm::vec2& pos, double range, int zonex, int zoney);
        std::unordered_map<int, std::unordered_map<int, bool>> ZoneCoordsInRange(const glm::vec2& pos, double range);

        // Returns in the given oldMap
        // if the bool is false then this cell was not present in the oldMap
        // if the bool is true then it was present in the oldMap but is not present in the newMap
        // coordinates that are both in the old and the new map are removed
        void Diff2DMap(std::unordered_map<int, std::unordered_map<int, bool>>& oldMap,
            std::unordered_map<int, std::unordered_map<int, bool>>& newMap);

        inline double CalculateDistance(const glm::vec2& pos1, const glm::vec2& pos2)
        {
            return std::sqrt(std::pow(pos1.x - pos2.x, 2.0) + std::pow(pos1.y - pos2.y, 2.0));
        }

        inline bool IsInRange(const glm::vec2& pos1, const glm::vec2& pos2, double range)
        {
            return (std::pow(pos1.x - pos2.x, 2.0) + std::pow(pos1.y - pos2.y, 2.0)) < std::pow(range, 2.0);
        }

        inline bool IsInRange(const double pos1x, const double pos1y, const double pos2x, const double pos2y, double range)
        {
            return (std::pow(pos1x - pos2x, 2.0) + std::pow(pos1y - pos2y, 2.0)) < std::pow(range, 2.0);
        }

        // thanks stackoverflow :)
        // http://stackoverflow.com/questions/7007802/erase-specific-elements-in-stdmap
        template <typename Map, typename F>
        void map_erase_if(Map& m, F pred)
        {
            typename Map::iterator i = m.begin();
            while ((i = std::find_if(i, m.end(), pred)) != m.end())
                m.erase(i++);
        }
    }
}

#endif
