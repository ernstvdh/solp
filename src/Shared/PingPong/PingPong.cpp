#include <thread>
#include <memory>
#include <algorithm>

#include "PingPong.h"
#include "PingPongId.h"
#include "PingPongMessage.h"
#include "Shared/Link/Link.h"
#include "Shared/Link/LinkEventListener.h"
#include "Shared/Net/ConnectionId.h"
#include "Shared/Config.h"
#include "Shared/Debug.h"
#include "Shared/Singleton.h"
#include "Shared/Net/ConnectionMgr.h"

namespace Shared
{
    namespace PingPong
    {
        int PingPong::Run()
        {
            std::shared_ptr<Shared::Link::LinkEventListener> eventListener = std::make_shared<Shared::Link::LinkEventListener>();
            Shared::Singleton::shutdownHooks->Add(std::make_shared<Shared::Link::LinkEventOnExpire>(eventListener));

            auto connectionListener = Shared::Net::ConnectionMgr::singleton->ListenOn(
                Shared::Config::typePorts[Shared::Id::ProcessType::PING_PONG], shared_from_this());

            // wait for shutdown
            if (eventListener->Poll())
                return 0;

            return 0;
        }

        std::shared_ptr<Shared::Net::ConnectionOwner> PingPong::GetConnectionOwner()
        {
            auto shared = std::make_shared<PingPongConnectionOwner>(shared_from_this());

            {
                std::lock_guard<std::mutex> lck(connectionMutex);
                connections.push_back(shared);
            }

            return shared;
        }

        std::string PingPong::GetIP()
        {
            // we utilise the tracker ip list to ping them
            // based on their response we will know our ip
            // randomize try order
            std::vector<std::string> trackers = Config::trackerIPs;
            std::random_shuffle(trackers.begin(), trackers.end());

            std::shared_ptr<Shared::Link::LinkEventListener> el(std::make_shared<Shared::Link::LinkEventListener>());
            std::vector<std::shared_ptr<PingPongConnectionOwner>> tempConnectionOwners;

            for (int i = 0; i < trackers.size(); i++)
            {
                SOLP_DEBUG(DEBUG_PINGPONG, "Pinging tracker: %s", trackers[i].c_str());
                std::shared_ptr<const Shared::Id> otherId(std::make_shared<const PingPongId>(trackers[i]));
                std::shared_ptr<PingPongConnectionOwner> connectionOwner(std::make_shared<PingPongConnectionOwner>());
                std::shared_ptr<Shared::Net::Connection> con(Shared::Net::ConnectionMgr::singleton->ConnectTo(
                    trackers[i], Shared::Config::typePorts[Shared::Id::ProcessType::PING_PONG], connectionOwner));
                connectionOwner->SetEventListener(el);
                tempConnectionOwners.push_back(connectionOwner);

                for (auto temp : tempConnectionOwners)
                    temp->SendMessage(Shared::Message::Message::MakeMessage<PingMessage>(otherId, otherId));

                // now we start the waiting game
                // if we do not get a response within ARBITRARY NUMBER HERE
                // 5 seconds we try with a next tracker if available
                // we keep polling the trackers we pinged already in case they respond
                // the last tracker gets an additional 10 seconds
                int ms = ((i + 1) == trackers.size()) ? 15000 : 5000;
                el->Poll(ms);
                 
                for (const auto& owner : tempConnectionOwners)
                    if (owner->hasRemoteIp)
                        return owner->remoteIP;
            }

            return "failure";
        }

        bool PingPong::Ping(std::string& remoteIP)
        {
            std::shared_ptr<Shared::Link::LinkEventListener> el(std::make_shared<Shared::Link::LinkEventListener>());
            std::shared_ptr<PingPongConnectionOwner> connectionOwner(std::make_shared<PingPongConnectionOwner>());
            std::shared_ptr<const Shared::Id> otherId(std::make_shared<const PingPongId>(remoteIP));
            std::shared_ptr<Shared::Net::Connection> con(Shared::Net::ConnectionMgr::singleton->ConnectTo(
                remoteIP, Shared::Config::typePorts[Shared::Id::ProcessType::PING_PONG], connectionOwner));
            connectionOwner->SetEventListener(el);
            connectionOwner->SendMessage(Shared::Message::Message::MakeMessage<PingMessage>(otherId, otherId));

            el->Poll(10000);
            if (connectionOwner->hasRemoteIp)
                return true;

            return false;
        }

        void PingPong::ConnectionAborted(Shared::Net::Connection* con)
        {
            std::lock_guard<std::mutex> lck(connectionMutex);

            auto found = std::find_if(connections.begin(), connections.end(), [con](std::shared_ptr<PingPongConnectionOwner>& owner)
            {
                return owner->HasConnection(con);
            });
            if (found != connections.end())
                connections.erase(found);
        }

        PingPongConnectionOwner::PingPongConnectionOwner(std::shared_ptr<PingPong> creator) : owner(creator), hasRemoteIp(false) {}

        bool PingPongConnectionOwner::PushMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            if (auto ping = std::dynamic_pointer_cast<PingMessage>(mess))
            {
                SOLP_DEBUG(DEBUG_PINGPONG, "Received PING message on incoming link.");
                std::shared_ptr<PongMessage> pongMessage = Shared::Message::Message::MakeMessage<PongMessage>(mess->receiverID, mess->senderID);

                pongMessage->ip = connection->GetID()->ip;

                if (pongMessage->ip == "failure")
                    return true;

                SendMessage(pongMessage);
            }
            else if (auto pong = std::dynamic_pointer_cast<PongMessage>(mess))
            {
                hasRemoteIp = true;
                remoteIP = pong->ip;

                SOLP_DEBUG(DEBUG_PINGPONG, "Received PONG message on incoming link.");
                if (eventListener)
                    eventListener->AddEvent(NULL);
            }
            else
                SOLP_WARNING("received an unexpected message in ping pong service..");

            return true;
        }

        void PingPongConnectionOwner::Aborted(Shared::Net::Connection::ABORTED_CODE code, Shared::Net::Connection* pointer)
        {
            // only if the owner is set it is an incoming connection
            // in which case we need to know when the remote destroys the connection
            // so that we can cleanup our ping pong connection storage
            // if it is an outgoing connection we want to retry connecting
            // and sending the message
            if (owner)
            {
                owner->ConnectionAborted(pointer);
            }
            else
            {
                std::shared_ptr<const Shared::Id> otherId(std::make_shared<const PingPongId>(connection->GetID()->ip));
                connection = Shared::Net::ConnectionMgr::singleton->ConnectTo(
                    connection->GetID()->ip, Shared::Config::typePorts[Shared::Id::ProcessType::PING_PONG], shared_from_this());
                
                SendMessage(Shared::Message::Message::MakeMessage<PingMessage>(otherId, otherId));
            }
        }

        void PingPongConnectionOwner::SetConnection(std::shared_ptr<Shared::Net::Connection> con)
        {
            connection = con;
        }

        bool PingPongConnectionOwner::HasConnection(Shared::Net::Connection* pointer)
        {
            // might not be set yet
            if (!connection)
                return false;

            return connection.get() == pointer;
        }

        void PingPongConnectionOwner::SetEventListener(std::shared_ptr<Shared::Link::LinkEventListener> el)
        {
            eventListener = el;
        }

        void PingPongConnectionOwner::SendMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            connection->PushMessage(mess);
        }
    }
}
