#ifndef SOLP_SHARED_PINGPONG_PINGPONGID_H
#define SOLP_SHARED_PINGPONG_PINGPONGID_H

#include "Shared/Id.h"

namespace Shared
{
    namespace PingPong
    {
        class PingPongId : public Shared::IpId
        {
        public:
            // we add a random number to decrease the likeliness that two hosts have the same name
            PingPongId(const std::string& lIp = (std::string("unknown") + std::to_string(rand()))) :
                Shared::IpId(lIp, Shared::Id::ProcessType::PING_PONG) {}
            PingPongId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::PING_PONG) {}
            ~PingPongId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<PingPongId>(json);
            }
        };
    }
}

#endif
