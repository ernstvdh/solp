#include "PingPongMessage.h"

namespace Shared
{
    namespace PingPong
    {
        const std::vector<std::function<std::shared_ptr<PingPongMessage>(const Shared::JSon::JSon&)>> PingPongMessage::enumToMessageMap
        {
            PingMessage::ParseJSon,
            PongMessage::ParseJSon
        };
    }
}
