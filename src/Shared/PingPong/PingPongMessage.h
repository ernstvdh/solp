#ifndef SOLP_SHARED_PINGPONG_PINGPONGMESSAGE_H
#define SOLP_SHARED_PINGPONG_PINGPONGMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace Shared
{
    namespace PingPong
    {
        class PingPongMessage : public Shared::Message::Message
        {
        public:
            enum PingPongMessageType
            {
                PING,
                PONG,

                MAX_VALUE
            };

            PingPongMessage()
            {
                processType = Shared::Id::ProcessType::PING_PONG;
            }

            PingPongMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(pingPongMessageType);
            }

            PingPongMessageType pingPongMessageType;

            static std::shared_ptr<PingPongMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                PingPongMessageType ppmt;
                json.QueryStrict("pingPongMessageType", ppmt);

                if (ppmt >= PingPongMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds connection message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[ppmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(pingPongMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<PingPongMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };


        class PingMessage : public PingPongMessage
        {
        public:
            PingMessage()
            {
                pingPongMessageType = PingPongMessageType::PING;
            }

            PingMessage(const Shared::JSon::JSon& json) : PingPongMessage(json) {}

            static std::shared_ptr<PingMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<PingMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                PingPongMessage::SerializeToJSon(json);
            }
        };


        class PongMessage : public PingPongMessage
        {
        public:
            PongMessage()
            {
                pingPongMessageType = PingPongMessageType::PONG;
            }

            PongMessage(std::string& tIp)
            {
                ip = tIp;
                pingPongMessageType = PingPongMessageType::PONG;
            }

            std::string ip;

            PongMessage(const Shared::JSon::JSon& json) : PingPongMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(ip);
            }

            static std::shared_ptr<PongMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<PongMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                PingPongMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(ip);
            }
        };
    }
}

#endif
