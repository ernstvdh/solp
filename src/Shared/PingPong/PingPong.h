#ifndef SOLP_SHARED_PINGPONG_PINGPONG_H
#define SOLP_SHARED_PINGPONG_PINGPONG_H

#include <string>

#include "Shared/Link/LinkEventListener.h"
#include "Shared/Net/ConnectionOwner.h"
#include "Shared/Net/ConnectionListener.h"

namespace Shared
{
    namespace PingPong
    {
        class PingPong;

        class PingPongConnectionOwner : public Shared::Net::ConnectionOwner, public std::enable_shared_from_this<PingPongConnectionOwner>
        {
        public:
            PingPongConnectionOwner() : hasRemoteIp(false) {}
            PingPongConnectionOwner(std::shared_ptr<PingPong> creator);

            void SetEventListener(std::shared_ptr<Shared::Link::LinkEventListener> el);

            void SendMessage(std::shared_ptr<Shared::Message::Message> mess);

            bool PushMessage(std::shared_ptr<Shared::Message::Message> mess) override;

            void Aborted(Shared::Net::Connection::ABORTED_CODE code, Shared::Net::Connection* pointer) override;

            void SetConnection(std::shared_ptr<Shared::Net::Connection> con) override;

            bool HasConnection(Shared::Net::Connection* pointer);

            std::shared_ptr<Shared::Link::LinkEventListener> eventListener;
            std::shared_ptr<Shared::Net::Connection> connection;
            std::shared_ptr<PingPong> owner;
            std::string remoteIP;
            bool hasRemoteIp;
        };

        class PingPong : public Shared::Net::ConnectionListenerOwner, public std::enable_shared_from_this<PingPong>
        {
        public:
            // by lack of a easy/quick alternative, use a static pingpong to
            // make it accessible anywhere in the program
            static std::shared_ptr<Shared::PingPong::PingPong> singleton;

            PingPong() {}
            ~PingPong() {}

            int Run();

            // returns after a maximum of 10 seconds
            bool Ping(std::string& remoteIP);
            // returns after a maximum of 5 * #trackers + 10 seconds
            std::string GetIP();

            void ConnectionAborted(Shared::Net::Connection* con);
            std::shared_ptr<Shared::Net::ConnectionOwner> GetConnectionOwner() override;

        private:
            std::mutex connectionMutex;
            std::vector<std::shared_ptr<PingPongConnectionOwner>> connections;
        };
    }
}

#endif
