#include "JSon.h"

namespace Shared
{
    namespace JSon
    {
        JSon::JSon()
        {
            data.SetObject();
        }

        // this is retarded, don't use it, or reimplement
        // it does however allow for compilation
        JSon::JSon(const JSon& json)
        {
            Parse(json.Serialise());
        }

        std::string JSon::Serialise() const
        {
            rapidjson::StringBuffer buffer;
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            this->data.Accept(writer);
            return buffer.GetString();
        }

        bool JSon::Parse(const std::string& toParse)
        {
            data.Parse<rapidjson::kParseFullPrecisionFlag>(toParse.c_str());

            return !data.HasParseError();
        }

        bool JSon::Parse(const char* toParse)
        {
            data.Parse<rapidjson::kParseFullPrecisionFlag>(toParse);

            return !data.HasParseError();
        }

        std::string JSon::GetError() const
        {
            rapidjson::ParseErrorCode pec = data.GetParseError();
            return std::string("error: ") + std::string(rapidjson::GetParseError_En(pec));
        }
    }
}
