#ifndef SOLP_SHARED_JSON_JSON_H
#define SOLP_SHARED_JSON_JSON_H

#include <list>
#include <vector>
#include <deque>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <type_traits>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/error/en.h"

#include "glm/vec2.hpp"

#include "Shared/Debug.h"

namespace Shared
{
    class Serializable;

    namespace JSon
    {
        enum QueryResult
        {
            SUCCESS,
            KEY_NOT_FOUND,
            WRONG_TYPE,
            UNKNOWN_TYPE_ASKED,

            MAX_RESULTS
        };

        template<typename T>
        struct QueryData
        {
            QueryResult result;
            std::string key;
            T data;

            inline operator bool()
            {
                return (result == QueryResult::SUCCESS);
            }
        };

        class JSon
        {
        public:

            JSon();
            // do not use this constructor
            JSon(const JSon& json);
            ~JSon() {}

            std::string Serialise() const;
            bool Parse(const std::string& toParse);
            bool Parse(const char* toParse);
            std::string GetError() const;

            template <typename T>
            void Add(const char* key, const T& value)
            {
                AddToResult(key, std::move(GetRapidValue<T>(value)));
            }

            template <typename T>
            void Add(const char* key, const std::vector<T>& value)
            {
                rapidjson::Value val;
                val.SetArray();

                buildingValVector.push_back(&val);

                for (const auto& temp : value)
                    Add(key, temp);

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename T>
            void Add(const char* key, const std::unordered_set<T>& value)
            {
                rapidjson::Value val;
                val.SetArray();

                buildingValVector.push_back(&val);

                for (const auto& temp : value)
                    Add(key, temp);

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename T>
            void Add(const char* key, const std::deque<T>& value)
            {
                rapidjson::Value val;
                val.SetArray();

                buildingValVector.push_back(&val);

                for (const auto& temp : value)
                    Add(key, temp);

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename T>
            void Add(const char* key, const std::list<T>& value)
            {
                rapidjson::Value val;
                val.SetArray();

                buildingValVector.push_back(&val);

                for (const auto& temp : value)
                    Add(key, temp);

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename K, typename V>
            void Add(const char* key, const std::unordered_map<K, V>& value)
            {
                rapidjson::Value val;
                val.SetArray();

                buildingValVector.push_back(&val);

                for (const auto& temp : value)
                {
                    Add(key, temp.first);
                    Add(key, temp.second);
                }

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename FIRST, typename SECOND>
            void Add(const char* key, const std::pair<FIRST, SECOND>& value)
            {
                rapidjson::Value val;
                val.SetObject();

                buildingValVector.push_back(&val);

                Add("first", value.first);
                Add("second", value.second);

                buildingValVector.pop_back();

                AddToResult(key, std::move(val));
            }

            template <typename T>
            inline T QueryStrict(const std::string key, T& value) const
            {
                return QueryStrict(key.c_str(), value);
            }

            template <typename T>
            void QueryStrict(const char* key, T& value) const
            {
                QueryResult qr = Query(key, value);
                if (qr == QueryResult::SUCCESS)
                    return;

                SOLP_ERROR("strict json query failed with query error code: %d\nwhile querying on key: %s", (int)qr, key);
                std::terminate();
            }

            template <typename T>
            inline QueryResult Query(const std::string key, T& value) const
            {
                return Query(key.c_str(), value);
            }

            template <typename T>
            QueryResult Query(const char* key, T& value) const
            {
                QueryResult temp;
                const rapidjson::Value* val = GetMember(key, temp);
                if (temp == QueryResult::KEY_NOT_FOUND)
                    return temp;

                return QueryInput(value, val);
            }

        private:

            std::vector<rapidjson::Value*> buildingValVector;
            mutable std::vector<const rapidjson::Value*> readingValVector;

            void AddToResult(const char* key, rapidjson::Value&& value)
            {
                if (buildingValVector.empty())
                    data.AddMember(rapidjson::StringRef(key), value, data.GetAllocator());
                else
                {
                    if (buildingValVector[buildingValVector.size() - 1]->IsArray())
                        buildingValVector[buildingValVector.size() - 1]->PushBack(value, data.GetAllocator());
                    else
                        buildingValVector[buildingValVector.size() - 1]->AddMember(rapidjson::StringRef(key), value, data.GetAllocator());
                }
            }

            const rapidjson::Value* GetMember(const char* key, QueryResult& error) const
            {
                rapidjson::Value::ConstMemberIterator mi = readingValVector.empty() ?
                    data.FindMember(key) : readingValVector[readingValVector.size() - 1]->FindMember(key);
                
                if (mi == (readingValVector.empty() ? data.MemberEnd() : readingValVector[readingValVector.size() - 1]->MemberEnd()))
                {
                    error = QueryResult::KEY_NOT_FOUND;
                    return NULL;
                }

                error = QueryResult::SUCCESS;
                return &(mi->value);
            }

            template <typename T>
            rapidjson::Value GetRapidValue(const typename std::enable_if<std::is_convertible<T,
                std::shared_ptr<const Shared::Serializable>>::value, T>::type& value)
            {
                rapidjson::Value val;
                val.SetObject();

                buildingValVector.push_back(&val);

                value->SerializeToJSon(*this);

                buildingValVector.pop_back();

                return val;
            }

            template <typename T>
            rapidjson::Value GetRapidValue(const typename std::enable_if<
                std::is_base_of<Shared::Serializable, T>::value, T>::type& value)
            {
                rapidjson::Value val;
                val.SetObject();

                buildingValVector.push_back(&val);

                value.SerializeToJSon(*this);

                buildingValVector.pop_back();

                return val;
            }

            template <typename T>
            rapidjson::Value GetRapidValue(const typename std::enable_if<std::is_same<std::string, T>::value, T>::type& value)
            {
                rapidjson::Value res;
                res.SetString(rapidjson::StringRef(value.c_str()), data.GetAllocator());
                return res;
            }

            template <typename T>
            rapidjson::Value GetRapidValue(const typename std::enable_if<std::is_arithmetic<T>::value, T>::type& value)
            {
                return rapidjson::Value(value);
            }

            template <typename T>
            rapidjson::Value GetRapidValue(const typename std::enable_if<std::is_enum<T>::value, T>::type& value)
            {
                return rapidjson::Value((typename std::underlying_type<T>::type)value);
            }

            template <typename T>
            QueryResult RapidValToNative(T& value, const rapidjson::Value* current) const
            {
                return QueryResult::UNKNOWN_TYPE_ASKED;
            }


            template <typename T>
            QueryResult QueryInput(T& value, const rapidjson::Value* current) const
            {
                return ToType<T>(value, current);
            }

            template <typename T>
            QueryResult QueryInput(std::vector<T>& value, const rapidjson::Value* current) const
            {
                if (!current->IsArray())
                    return QueryResult::WRONG_TYPE;

                QueryResult qr;
                T toAdd;
                for (rapidjson::Value::ConstValueIterator itr = current->Begin(); itr != current->End(); ++itr)
                {
                    qr = QueryInput(toAdd, itr);
                    if (qr != QueryResult::SUCCESS)
                        return qr;
                    value.push_back(std::move(toAdd));
                }

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult QueryInput(std::unordered_set<T>& value, const rapidjson::Value* current) const
            {
                if (!current->IsArray())
                    return QueryResult::WRONG_TYPE;

                QueryResult qr;
                T toAdd;
                for (rapidjson::Value::ConstValueIterator itr = current->Begin(); itr != current->End(); ++itr)
                {
                    qr = QueryInput(toAdd, itr);
                    if (qr != QueryResult::SUCCESS)
                        return qr;
                    value.insert(std::move(toAdd));
                }

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult QueryInput(std::deque<T>& value, const rapidjson::Value* current) const
            {
                if (!current->IsArray())
                    return QueryResult::WRONG_TYPE;

                QueryResult qr;
                T toAdd;
                for (rapidjson::Value::ConstValueIterator itr = current->Begin(); itr != current->End(); ++itr)
                {
                    qr = QueryInput(toAdd, itr);
                    if (qr != QueryResult::SUCCESS)
                        return qr;
                    value.push_back(std::move(toAdd));
                }

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult QueryInput(std::list<T>& value, const rapidjson::Value* current) const
            {
                if (!current->IsArray())
                    return QueryResult::WRONG_TYPE;

                QueryResult qr;
                T toAdd;
                for (rapidjson::Value::ConstValueIterator itr = current->Begin(); itr != current->End(); ++itr)
                {
                    qr = QueryInput(toAdd, itr);
                    if (qr != QueryResult::SUCCESS)
                        return qr;
                    value.push_back(std::move(toAdd));
                }

                return QueryResult::SUCCESS;
            }

            template <typename FIRST, typename SECOND>
            inline QueryResult QueryInput(std::pair<FIRST, SECOND>& value, const rapidjson::Value* current) const
            {
                if (!current->IsObject())
                    return QueryResult::WRONG_TYPE;

                readingValVector.push_back(current);

                QueryResult qr = Query("first", value.first);
                if (qr == QueryResult::SUCCESS)
                    qr = Query("second", value.second);

                readingValVector.pop_back();

                return qr;
            }

            template <typename K, typename V>
            QueryResult QueryInput(std::unordered_map<K, V>& value, const rapidjson::Value* current) const
            {
                if (!current->IsArray())
                    return QueryResult::WRONG_TYPE;

                // if the array contains an uneven 
                if (current->Size() % 2)
                    return QueryResult::WRONG_TYPE;

                // make sure we don't have to reallocate all the time
                value.reserve(current->Size() / 2);

                K k;
                V v;

                bool readingKey = true;
                QueryResult qr;

                for (rapidjson::Value::ConstValueIterator itr = current->Begin(); itr != current->End(); ++itr)
                {
                    if (readingKey)
                    {
                        qr = QueryInput(k, itr);
                        if (qr != QueryResult::SUCCESS)
                            return qr;

                        readingKey = false;
                    }
                    else
                    {
                        qr = QueryInput(v, itr);
                        if (qr != QueryResult::SUCCESS)
                            return qr;

                        value.emplace(k, std::move(v));
                        readingKey = true;
                    }
                }

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult ToType(typename std::enable_if<std::is_convertible<T,
                std::shared_ptr<const Shared::Serializable>>::value, T>::type& value, const rapidjson::Value* current) const
            {
                if (!current->IsObject())
                    return QueryResult::WRONG_TYPE;

                readingValVector.push_back(current);

                value = std::static_pointer_cast<typename T::element_type>(T::element_type::ParseJSon(*this));

                readingValVector.pop_back();

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult ToType(typename std::enable_if<
                std::is_base_of<Shared::Serializable, T>::value, T>::type& value, const rapidjson::Value* current) const
            {
                if (!current->IsObject())
                    return QueryResult::WRONG_TYPE;

                readingValVector.push_back(current);

                value = T::ParseJSon(*this);

                readingValVector.pop_back();

                return QueryResult::SUCCESS;
            }

            template <typename T>
            QueryResult ToType(typename std::enable_if<std::is_same<std::string, T>::value, T>::type& value,
                const rapidjson::Value* current) const
            {
                return RapidValToNative(value, current);
            }

            template <typename T>
            QueryResult ToType(typename std::enable_if<std::is_arithmetic<T>::value, T>::type& value, const rapidjson::Value* current) const
            {
                return RapidValToNative(value, current);
            }

            template <typename T>
            QueryResult ToType(typename std::enable_if<std::is_enum<T>::value, T>::type& value, const rapidjson::Value* current) const
            {
                typename std::underlying_type<T>::type temp;
                QueryResult qr = RapidValToNative(temp, current);
                if (qr != QueryResult::SUCCESS)
                    return qr;

                value = (T)temp;
                return qr;
            }

            rapidjson::Document data;
        };

        template <>
        inline void JSon::Add<glm::vec2>(const char* key, const glm::vec2& value)
        {
            rapidjson::Value val;
            val.SetObject();
            buildingValVector.push_back(&val);

            Add("x", value.x);
            Add("y", value.y);

            buildingValVector.pop_back();

            AddToResult(key, std::move(val));
        }

        template <>
        inline QueryResult JSon::QueryInput<glm::vec2>(glm::vec2& value, const rapidjson::Value* current) const
        {
            if (!current->IsObject())
                return QueryResult::WRONG_TYPE;

            readingValVector.push_back(current);

            QueryResult qr = Query("x", value.x);
            if (qr == QueryResult::SUCCESS)
                qr = Query("y", value.y);

            readingValVector.pop_back();

            return qr;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<std::string>(std::string& value, const rapidjson::Value* current) const
        {
            if (!current->IsString())
                return QueryResult::WRONG_TYPE;

            value = current->GetString();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<uint64_t>(uint64_t& value, const rapidjson::Value* current) const
        {
            if (!current->IsUint64())
                return QueryResult::WRONG_TYPE;

            value = current->GetUint64();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<uint32_t>(uint32_t& value, const rapidjson::Value* current) const
        {
            if (!current->IsUint64())
                return QueryResult::WRONG_TYPE;

            if (current->GetUint64() > UINT32_MAX)
                return QueryResult::WRONG_TYPE;

            value = (uint32_t)current->GetUint64();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<int>(int& value, const rapidjson::Value* current) const
        {
            if (!current->IsInt())
                return QueryResult::WRONG_TYPE;

            value = current->GetInt();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<bool>(bool& value, const rapidjson::Value* current) const
        {
            if (!current->IsBool())
                return QueryResult::WRONG_TYPE;

            value = current->GetBool();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<double>(double& value, const rapidjson::Value* current) const
        {
            if (!current->IsDouble())
                return QueryResult::WRONG_TYPE;

            value = current->GetDouble();
            return QueryResult::SUCCESS;
        }

        template <>
        inline QueryResult JSon::RapidValToNative<float>(float& value, const rapidjson::Value* current) const
        {
            if (!current->IsDouble())
                return QueryResult::WRONG_TYPE;

            value = (float)(current->GetDouble());
            return QueryResult::SUCCESS;
        }
    }
}

#endif
