#include "ConnectionMessage.h"

namespace Shared
{
    namespace Net
    {
        const std::vector<std::function<std::shared_ptr<ConnectionMessage>(const Shared::JSon::JSon&)>> ConnectionMessage::enumToMessageMap
        {
            ConnectionInitMessage::ParseJSon
        };
    }
}
