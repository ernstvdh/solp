#ifndef SOLP_SHARED_NET_CONNECTIONID_H
#define SOLP_SHARED_NET_CONNECTIONID_H

#include "Shared/Id.h"

namespace Shared
{
    namespace Net
    {
        class ConnectionId : public Shared::IpId
        {
        public:
            ConnectionId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::CONTROL) {}
            ConnectionId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::CONTROL) {}
            ~ConnectionId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ConnectionId>(json);
            }
        };
    }
}

#endif
