#ifndef SOLP_SHARED_NET_CONNECTION_H
#define SOLP_SHARED_NET_CONNECTION_H

#include <mutex>
#include <queue>
#include <vector>
#include <memory>
#include <atomic>

#include "asio.hpp"

#include "ConnectionId.h"
#include "ConnectionMessage.h"
#include "Shared/Message/MessageHeader.h"

namespace Shared
{
    namespace Net
    {
        class ConnectionOwner;

        class ConnectionSocket
        {
        public:
            ConnectionSocket(asio::ip::tcp::socket* sock, bool open) :
                socket(sock), isOpen(open), isClosed(false), canPush(open) {}
            
            void Open() { isOpen = true; }
            void Error() { isOpen = false; }
            void Close();
            bool IsOpen() { return isOpen; }
            bool CanPush() { return canPush; }

            std::unique_ptr<asio::ip::tcp::socket> socket;
            bool canPush;

        private:
            bool isOpen;
            bool isClosed;
        };

        class Connection : public std::enable_shared_from_this<Connection>
        {
        public:
            enum ABORTED_CODE
            {
                FAILED_CONNECT,
                REMOTE_DISCONNECT,
                WRITE_FAILED
            };

            // only use this constructor with a pre initialized socket
            // this means that we can immediately put messages on the wire
            static std::shared_ptr<Connection> MakeConnectionFromSocket(
                std::shared_ptr<ConnectionOwner> owner, asio::ip::tcp::socket* sock);
            static std::shared_ptr<Connection> MakeConnectionFromIpPort(std::shared_ptr<ConnectionOwner> owner,
                const std::string& adress, const int port, asio::io_service* ioService);

            ~Connection();

            // this function should push messages directly on the wire
            // only if the connection is not initialized we store
            // the messages temporarily
            bool PushMessage(std::shared_ptr<Shared::Message::Message> mess);

            std::shared_ptr<const ConnectionId> GetID() { return id; }

            // threadsafe but should still only be used from the network thread
            void SetOwner(std::shared_ptr<ConnectionOwner> owner) { listener = owner; }

        private:
            Connection(std::weak_ptr<ConnectionOwner> owner, asio::ip::tcp::socket* sock);
            Connection(std::weak_ptr<ConnectionOwner> owner, asio::io_service* ioService);
            void OpenConnection(const std::string& adress, const int port);
            void CloseConnection();

            std::shared_ptr<const ConnectionId> id;

            // socket is always unique for connection
            std::shared_ptr<ConnectionSocket> socket;
            asio::io_service* ioService;
            asio::ip::tcp::endpoint endpoint;

            void PushToLine(std::shared_ptr<Shared::Message::Message> mess);

            std::queue<std::shared_ptr<Shared::Message::Message>> waitingQueue;
            void PushQueueToLine();

            std::weak_ptr<ConnectionOwner> listener;

            void ConnectHandler(const std::error_code& ec);
            void WriteHandler(std::shared_ptr<std::vector<char>> data,
                const asio::error_code& ec, std::size_t bytesTransferred);

            // if we initialize the connection ourself
            void StartReading();
            void StartReadingHeader();
            void ReadHeader(std::shared_ptr<std::vector<char>> data,
                const asio::error_code& ec, std::size_t bytesTransferred);
            void StartReadingContent(uint32_t bytes);
            void ReadContent(std::shared_ptr<std::vector<char>> data,
                const asio::error_code& ec, std::size_t bytesTransferred);
        };
    }
}

#endif
