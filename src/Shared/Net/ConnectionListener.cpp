#include "ConnectionListener.h"
#include "ConnectionMgr.h"

#include "Shared/Debug.h"

/*
http://www.bogotobogo.com/cplusplus/Boost/boost_AsynchIO_asio_tcpip_socket_server_client_timer_bind_handler_multithreading_synchronizing_network_D.php
*/

namespace Shared
{
    namespace Net
    {
        ConnectionListener::ConnectionListener(std::weak_ptr<ConnectionListenerOwner> listener,
            int port, asio::io_service* lIoService) :
            ioService(lIoService), owner(listener),
                endPoint(new asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)),
                acceptor(asio::ip::tcp::acceptor(*lIoService, *endPoint)) {}

        ConnectionListener::~ConnectionListener()
        {
            asio::error_code ec;
            acceptor.close(ec);
        }

        std::shared_ptr<ConnectionListener> ConnectionListener::MakeConnectionListener(
            std::weak_ptr<ConnectionListenerOwner> listener, int port, asio::io_service* lIoService)
        {
            auto res = std::shared_ptr<ConnectionListener>(new ConnectionListener(listener, port, lIoService));
            res->StartListening();
            return res;
        }

        void ConnectionListener::StartListening()
        {
            SOLP_DEBUG(DEBUG_CONNECTION_LISTENER, "start listening");
            asio::ip::tcp::socket* socket = new asio::ip::tcp::socket(*ConnectionMgr::singleton->GetAvailableIoService());
            std::weak_ptr<ConnectionListener> weak = shared_from_this();
            asio::ip::tcp::endpoint* ep = endPoint;
            acceptor.async_accept(*socket, *endPoint, [socket, weak, ep](const std::error_code& ec)
            {
                if (auto shared = weak.lock())
                {
                    shared->AcceptHandler(socket, ec);
                }
                else
                {
                    if (!ec)
                    {
                        asio::error_code shutdownEc;
                        socket->shutdown(asio::ip::tcp::socket::shutdown_both, shutdownEc);

                        if (shutdownEc)
                            SOLP_WARNING("Error code returned by socket shutdown.. this should only rarely happen and mostly during shutdown..");
                    }

                    socket->close();
                    delete socket;
                    delete ep;
                }
            });
        }

        void ConnectionListener::AcceptHandler(asio::ip::tcp::socket* socket, const std::error_code& ec)
        {
            auto shared = owner.lock();

            if (!ec && shared)
            {
                SOLP_DEBUG(DEBUG_CONNECTION_LISTENER, "Accepted connection.");
                std::shared_ptr<Connection> con = Connection::MakeConnectionFromSocket(
                    shared->GetConnectionOwner(), socket);
            }
            else
            {
                if (ec)
                    SOLP_WARNING("Connection not accepted because of error: %i - %s", ec.value(), ec.message().c_str());

                socket->shutdown(asio::ip::tcp::socket::shutdown_both);
                socket->close();
                delete socket;
            }

            StartListening();
        }
    }
}
