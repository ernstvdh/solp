#ifndef SOLP_SHARED_NET_CONNECTIONLISTENER_H
#define SOLP_SHARED_NET_CONNECTIONLISTENER_H

#include <mutex>
#include <memory>
#include <vector>

#include "asio.hpp"

#include "Connection.h"
#include "ConnectionMessage.h"
#include "ConnectionOwner.h"

namespace Shared
{
    namespace Net
    {
        class Connection;

        class ConnectionListener : public std::enable_shared_from_this<ConnectionListener>
        {
        public:
            ~ConnectionListener();

            static std::shared_ptr<ConnectionListener> MakeConnectionListener(
                std::weak_ptr<ConnectionListenerOwner> listener, int port, asio::io_service* lIoService);

        private:
            ConnectionListener(std::weak_ptr<ConnectionListenerOwner> listener, int port, asio::io_service* lIoService);

            asio::io_service* ioService;
            asio::ip::tcp::endpoint* endPoint;
            asio::ip::tcp::acceptor acceptor;

            std::weak_ptr<ConnectionListenerOwner> owner;

            void StartListening();
            void AcceptHandler(asio::ip::tcp::socket* socket, const std::error_code& ec);
        };
    }
}

#endif
