#ifndef SOLP_SHARED_NET_CONNECTIONMGR_H
#define SOLP_SHARED_NET_CONNECTIONMGR_H

#include <memory>
#include <atomic>
#include <vector>

#include "asio.hpp"

namespace Shared
{
    namespace Net
    {
        class Connection;
        class ConnectionOwner;
        class ConnectionListener;
        class DummyConnectionListener;
        class ConnectionListenerOwner;

        class ConnectionMgr
        {
        public:
            // by lack of a easy/quick alternative, use a static connectionmgr to
            // make it accessible anywhere in the program
            static ConnectionMgr* singleton;

            ConnectionMgr();
            ~ConnectionMgr() { ioServiceRunners.clear(); }

            std::shared_ptr<ConnectionListener> ListenOn(const int port,
                std::shared_ptr<ConnectionListenerOwner> listener);

            std::shared_ptr<Connection> ConnectTo(const std::string& ip, const int port,
                std::shared_ptr<ConnectionOwner> listener);

            asio::io_service* GetAvailableIoService();

            // with the current implementations this function is blocking waiting
            // for incomming messages/connections
            void Init();
            int Run();
            void Stop();

            std::vector<uint64_t> threadIds;
            std::vector<uint64_t> startTimes;

        private:
            std::atomic<int> nextIoService;

            std::vector<std::unique_ptr<asio::io_service>> ioServices;
            std::vector<asio::io_service::work> ioServiceRunners;
        };
    }
}

#endif
