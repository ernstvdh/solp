#ifndef SOLP_SHARED_NET_CONNECTIONOWNER_H
#define SOLP_SHARED_NET_CONNECTIONOWNER_H

#include "Connection.h"

namespace Shared
{
    namespace Net
    {
        class ConnectionOwner
        {
        public:
            ConnectionOwner() {}
            virtual ~ConnectionOwner() {}

            virtual bool PushMessage(std::shared_ptr<Shared::Message::Message> mess) = 0;

            virtual void Aborted(Connection::ABORTED_CODE code, Connection* pointer) = 0;

            virtual void SetConnection(std::shared_ptr<Connection> con) = 0;
        };

        class ConnectionListenerOwner
        {
        public:
            ConnectionListenerOwner() {}
            virtual ~ConnectionListenerOwner() {}

            virtual std::shared_ptr<ConnectionOwner> GetConnectionOwner() = 0;
        };
    }
}

#endif
