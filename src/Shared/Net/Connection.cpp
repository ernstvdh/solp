#include "Connection.h"
#include "ConnectionOwner.h"
#include "Shared/Debug.h"

namespace Shared
{
    namespace Net
    {
        void ConnectionSocket::Close()
        {
            if (!isClosed)
            {
                asio::error_code ec;
                if (IsOpen())
                    socket->shutdown(asio::ip::tcp::socket::shutdown_both, ec);

                if (ec)
                    SOLP_WARNING("Error code returned by socket shutdown.. this should only rarely happen and mostly during shutdown..");

                socket->close();
                isOpen = false;
                isClosed = true;
            }
        }

        std::shared_ptr<Connection> Connection::MakeConnectionFromSocket(
            std::shared_ptr<ConnectionOwner> owner, asio::ip::tcp::socket* sock)
        {
            auto conn = std::shared_ptr<Connection>(new Connection(owner, sock));
            // we set the connection in the owner before we open the connection
            // otherwise callbacks can be called on the owner without it knowing
            // of the connection....
            owner->SetConnection(conn);
            conn->StartReading();
            return conn;
        }

        std::shared_ptr<Connection> Connection::MakeConnectionFromIpPort(std::shared_ptr<ConnectionOwner> owner,
            const std::string& adress, const int port, asio::io_service* ioService)
        {
            auto conn = std::shared_ptr<Connection>(new Connection(owner, ioService));
            // we set the connection in the owner before we open the connection
            // otherwise callbacks can be called on the owner without it knowing
            // of the connection....
            owner->SetConnection(conn);
            conn->OpenConnection(adress, port);
            return conn;
        }

        Connection::Connection(std::weak_ptr<ConnectionOwner> owner, asio::ip::tcp::socket* sock) :
            id(std::make_shared<const ConnectionId>(sock->remote_endpoint().address().to_string())),
            listener(owner), socket(new ConnectionSocket(sock, true))
        {
            sock->set_option(asio::ip::tcp::no_delay(true));
            endpoint = sock->remote_endpoint();
            ioService = &sock->get_io_service();
        }

        Connection::Connection(std::weak_ptr<ConnectionOwner> owner, asio::io_service* lIoService) :
            ioService(lIoService), socket(new ConnectionSocket(new asio::ip::tcp::socket(*lIoService), false)), listener(owner){}

        Connection::~Connection()
        {
            SOLP_DEBUG(DEBUG_DELETE_CALLS, "Deleted %p", (void*)this);

            CloseConnection();
        }

        void Connection::OpenConnection(const std::string& adress, const int port)
        {
            endpoint = asio::ip::tcp::endpoint(asio::ip::address::from_string(adress), port);
            id = std::make_shared<const ConnectionId>(endpoint.address().to_string());
            socket->socket->open(asio::ip::tcp::v4());
            socket->socket->set_option(asio::ip::tcp::no_delay(true));

            // the tracker is our resolver, use that if necessary
            /*asio::ip::tcp::resolver resolver(*ioService);
            asio::ip::tcp::resolver::query query(adress, std::to_string(port));
            asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);*/

            std::weak_ptr<Connection> weak = shared_from_this();
            std::shared_ptr<ConnectionSocket> sock = socket;
            socket->socket->async_connect(endpoint, [weak, sock](const std::error_code& ec)
            {
                if (ec)
                    sock->Error();

                if (auto shared = weak.lock())
                    shared->ConnectHandler(ec);
                else
                    sock->Close();
            });
        }

        void Connection::CloseConnection()
        {
            // make sure close is called from the network thread
            auto sock = socket;
            ioService->dispatch([sock]()
            {
                sock->Close();
            });
        }

        bool Connection::PushMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            std::weak_ptr<Connection> weak = shared_from_this();
            auto sock = socket;
            ioService->dispatch([weak, sock, mess]()
            {
                if (auto shared = weak.lock())
                {
                    if (!sock->CanPush())
                    {
                        // if we can not push we store the message in a queue
                        shared->waitingQueue.push(mess);
                    }
                    else
                    {
                        // line is free so we push the message on it
                        shared->PushToLine(mess);
                    }
                }
            });

            return true;
        }

        void Connection::PushToLine(std::shared_ptr<Shared::Message::Message> mess)
        {
            if (!socket->IsOpen())
                return;

            socket->canPush = false;

            //todo header size and add
            std::string ser(mess->Serialise());
            Shared::Message::MessageHeader header(ser.length());
            std::shared_ptr<std::vector<char>> data(
                std::make_shared<std::vector<char>>(header.GetRoomyVector()));

            for (int i = 0; i < ser.length(); i++)
                (*data)[i + Shared::Message::MessageHeader::Size()] = ser[i];

            std::weak_ptr<Connection> weak = shared_from_this();
            std::shared_ptr<ConnectionSocket> sock = socket;
            asio::async_write(*socket->socket, asio::buffer(*data), [data, weak, sock]
                (const asio::error_code& ec, std::size_t bytesTransferred)
            {
                if (ec)
                    sock->Error();

                if (auto shared = weak.lock())
                    shared->WriteHandler(data, ec, bytesTransferred);
                else
                    sock->Close();
            });
        }

        void Connection::PushQueueToLine()
        {
            SOLP_DEBUG(DEBUG_CONNECTION, "");
            if (!waitingQueue.empty())
            {
                auto mess(waitingQueue.front());
                waitingQueue.pop();
                PushToLine(mess);
            }  
        }

        void Connection::ConnectHandler(const std::error_code& ec)
        {
            SOLP_DEBUG(DEBUG_CONNECTION, "entering connecthandler");
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Entered ConnectHandler with remote on port (%d).", endpoint.port());

            if (ec)
            {
                SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Entered ConnectHandler with error (%s) remote on port (%d).", ec.message().c_str(), endpoint.port());

                socket->Close();
                
                if (auto owner = listener.lock())
                    owner->Aborted(ABORTED_CODE::FAILED_CONNECT, this);
            }
            else
            {
                SOLP_DEBUG(DEBUG_CONNECTION, "actually succeeded in connecting");
                SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Connected with remote on port (%d).", endpoint.port());
                socket->Open();
                socket->canPush = true;
                PushQueueToLine();
                StartReading();
            }
        }

        void Connection::WriteHandler(std::shared_ptr<std::vector<char>> data,
            const asio::error_code& ec, std::size_t bytesTransferred)
        {
            // we are not doing anything here so no need to lock
            if (ec)
            {
                //TODO: better error handling
                socket->Close();

                if (auto owner = listener.lock())
                    owner->Aborted(ABORTED_CODE::WRITE_FAILED, this);
            }
            else
            {
                SOLP_DEBUG(DEBUG_CONNECTION, "written %d bytes to the line\n", bytesTransferred);
                if (socket->IsOpen())
                {
                    socket->canPush = true;
                    PushQueueToLine();
                }
            }
        }

        void Connection::StartReading()
        {
            if (!socket->IsOpen())
            {
                SOLP_ERROR("reading on not connected socket... terminating...");
                std::terminate();
            }

            // start by reading a header
            StartReadingHeader();
        }

        void Connection::StartReadingHeader()
        {
            if (!socket->IsOpen())
                return;

            SOLP_DEBUG(DEBUG_CONNECTION, "start reading header");
            // header size is 
            std::shared_ptr<std::vector<char>> data(std::make_shared<std::vector<char>>(
                Shared::Message::MessageHeader::Size()));

            std::weak_ptr<Connection> weak = shared_from_this();
            std::shared_ptr<ConnectionSocket> sock = socket;
            asio::async_read(*socket->socket, asio::buffer(*data), [data, weak, sock]
                (const asio::error_code& ec, std::size_t bytesTransferred)
            {
                if (ec)
                    sock->Error();

                if (auto shared = weak.lock())
                    shared->ReadHeader(data, ec, bytesTransferred);
                else
                    sock->Close();
            });
        }

        void Connection::ReadHeader(std::shared_ptr<std::vector<char>> data,
            const asio::error_code& ec, std::size_t bytesTransferred)
        {
            if (ec)
            {
                socket->Close();

                //TODO: better error handling
                if (auto owner = listener.lock())
                    owner->Aborted(ABORTED_CODE::REMOTE_DISCONNECT, this);
            }
            else
            {
                SOLP_DEBUG(DEBUG_CONNECTION, "actually doing some header reading");
                uint32_t contentSize = Shared::Message::MessageHeader(*data).value;
                // when we have read a header start reading the content
                StartReadingContent(contentSize);
            }
        }

        void Connection::StartReadingContent(uint32_t bytes)
        {
            if (!socket->IsOpen())
                return;

            SOLP_DEBUG(DEBUG_CONNECTION, "started reading content of %d bytes", bytes);
            // we add 1 byte for the null termination of the string
            std::shared_ptr<std::vector<char>> data(std::make_shared<std::vector<char>>(bytes + 1));

            std::weak_ptr<Connection> weak = shared_from_this();
            std::shared_ptr<ConnectionSocket> sock = socket;
            // don't try to read the last byte, we add it ourself
            asio::async_read(*socket->socket, asio::buffer(*data, bytes), [data, weak, sock]
                (const asio::error_code& ec, std::size_t bytesTransferred)
            {
                if (ec)
                    sock->Error();

                if (auto shared = weak.lock())
                    shared->ReadContent(data, ec, bytesTransferred);
                else
                    sock->Close();
            });
        }

        void Connection::ReadContent(std::shared_ptr<std::vector<char>> data,
            const asio::error_code& ec, std::size_t bytesTransferred)
        {
            SOLP_DEBUG(DEBUG_CONNECTION, "entered read content");

            if (ec)
            {
                //TODO: better error handling
                socket->Close();

                if (auto owner = listener.lock())
                    owner->Aborted(ABORTED_CODE::REMOTE_DISCONNECT, this);
            }
            else
            {
                SOLP_DEBUG(DEBUG_CONNECTION, "actually reading content of %d bytes", bytesTransferred);
                // we initialized the vector with 1 additional byte in which we can store the null termination
                // this means that the internal data of the vector becomes a valid c str
                (*data.get())[bytesTransferred] = '\0';
                std::shared_ptr<Shared::Message::Message> mess = Shared::Message::Message::ParseMessage(data->data());

                if (auto owner = listener.lock())
                    owner->PushMessage(mess);
                else
                    SOLP_DEBUG(DEBUG_CONNECTION, "Connection (id (%s) port (%d)) reading content\n%s\nbut the owner is already deleted??",
                    id->Serialize().c_str(), endpoint.port(), mess->Serialise().c_str());

                // finally start listening again
                StartReadingHeader();
            }
        }
    }
}
