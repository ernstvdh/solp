#include <thread>
#include <future>

#include "ConnectionMgr.h"
#include "ConnectionListener.h"
#include "Connection.h"
#include "ConnectionId.h"
#include "ConnectionOwner.h"

#include "Shared/Config.h"
#include "Shared/System/System.h"

namespace Shared
{
    namespace Net
    {
        ConnectionMgr::ConnectionMgr() : nextIoService(0) {}

        std::shared_ptr<ConnectionListener> ConnectionMgr::ListenOn(const int port,
            std::shared_ptr<ConnectionListenerOwner> listener)
        {
            auto connListener = ConnectionListener::MakeConnectionListener(listener, port, GetAvailableIoService());
            return connListener;
        }

        std::shared_ptr<Connection> ConnectionMgr::ConnectTo(
            const std::string& ip, const int port, std::shared_ptr<ConnectionOwner> listener)
        {
            return Connection::MakeConnectionFromIpPort(listener, ip, port, GetAvailableIoService());
        }

        asio::io_service* ConnectionMgr::GetAvailableIoService()
        {
            // we don't really if two requests at the same time get the same ioService assigned
            int next = nextIoService.load();

            if (next + 1 >= Shared::Config::networkThreadsPerMachine)
                nextIoService.store(0);
            else
                nextIoService.store(next + 1);

            return ioServices[next].get();
        }

        void ConnectionMgr::Init()
        {
            threadIds.resize(Shared::Config::networkThreadsPerMachine, 0);
            startTimes.resize(Shared::Config::networkThreadsPerMachine, 0);

            for (int i = 0; i < Shared::Config::networkThreadsPerMachine; i++)
            {
                ioServices.emplace_back(new asio::io_service());
                // the dummy connection listeners make sure the run doesn't finish
                // untill the ioServices are stopped
                ioServiceRunners.emplace_back(*ioServices[i].get());
            }
        }

        int ConnectionMgr::Run()
        {
            std::vector<std::thread> networkHandlers;
            for (int i = 0; i < ioServices.size(); i++)
            {
                networkHandlers.push_back(std::thread([i, this]() mutable
                    {
                        threadIds[i] = Shared::System::GetThreadId();
                        uint64_t utime;
                        uint64_t stime;
                        uint64_t startTi;
                        Shared::System::GetThreadTimesForId(threadIds[i], utime, stime, startTi);
                        startTimes[i] = startTi;

                        asio::error_code ec;
                        ioServices[i]->run(ec);
                    })
                );
            }

            for (std::thread& thread : networkHandlers)
                thread.join();

            return 0;
        }

        void ConnectionMgr::Stop()
        {
            for (auto& ioService : ioServices)
                ioService->stop();
        }
    }
}
