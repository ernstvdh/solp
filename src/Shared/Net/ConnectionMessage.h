#ifndef SOLP_SHARED_NET_CONNECTIONMESSAGE_H
#define SOLP_SHARED_NET_CONNECTIONMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace Shared
{
    namespace Net
    {
        class ConnectionMessage : public Shared::Message::Message
        {
        public:
            enum ConnectionMessageType
            {
                INIT,

                MAX_VALUE
            };

            ConnectionMessage()
            {
                processType = Shared::Id::ProcessType::CONTROL;
            }

            ConnectionMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(connectionMessageType);
            }

            ConnectionMessageType connectionMessageType;

            static std::shared_ptr<ConnectionMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                ConnectionMessageType cmt;
                json.QueryStrict("connectionMessageType", cmt);

                if (cmt >= ConnectionMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds connection message type, aborting...\n");
                    std::terminate();
                }

                return enumToMessageMap[cmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(connectionMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<ConnectionMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };


        class ConnectionInitMessage : public ConnectionMessage
        {
        public:
            ConnectionInitMessage(bool real)
            {
                connectionMessageType = ConnectionMessageType::INIT;
                realNewMessage = real;
            }

            ConnectionInitMessage(const Shared::JSon::JSon& json) : ConnectionMessage(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(realNewMessage);
            }

            bool realNewMessage;

            static std::shared_ptr<ConnectionInitMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ConnectionInitMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                ConnectionMessage::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(realNewMessage);
            }
        };
    }
}

#endif
