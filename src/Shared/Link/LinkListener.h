#ifndef SOLP_SHARED_LINK_LINKLISTENER_H
#define SOLP_SHARED_LINK_LINKLISTENER_H

#include "Shared/Id.h"
#include "Shared/Net/ConnectionOwner.h"
#include "Shared/Net/ConnectionListener.h"

#include "Shared/TimedStorage.h"

namespace Shared
{
    namespace Link
    {
        class Link;
        class LinkableIncomingContainer;
        class LinkListener;

        class TemporaryConnectionOwner : public Shared::Net::ConnectionOwner
        {
        public:
            TemporaryConnectionOwner(std::shared_ptr<LinkListener> ll) : linkListener(ll) {}
            ~TemporaryConnectionOwner() {}

            bool PushMessage(std::shared_ptr<Shared::Message::Message> mess);

            // we don't really do anything with an aborted...
            // the connection has not been accepted anyway
            void Aborted(Shared::Net::Connection::ABORTED_CODE code, Shared::Net::Connection* pointer) {}

            void SetConnection(std::shared_ptr<Shared::Net::Connection> con) { conn = con; }

            std::shared_ptr<Shared::Net::Connection> conn;
            std::weak_ptr<LinkListener> linkListener;
        };

        class LinkListener : public Shared::Net::ConnectionListenerOwner,
            public std::enable_shared_from_this<LinkListener>
        {
        public:
            ~LinkListener() {}

            static std::shared_ptr<LinkListener> MakeLinkListener(Id::ProcessType type);

            void AddAcceptingLinkable(std::shared_ptr<LinkableIncomingContainer> linkable,
                std::shared_ptr<const Shared::Id> acceptingId);
            void RemoveAcceptingLinkable(const std::shared_ptr<const Shared::Id>& id);

            bool AddLinkFromTo(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> link);
            bool AddConnectionFromTo(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& otherId,
                std::shared_ptr<Shared::Net::Connection> connection);

            std::shared_ptr<Shared::Net::ConnectionOwner> GetConnectionOwner();

        private:
            LinkListener(Id::ProcessType type) : listenToType(type), timedConnectionStorage(10) {}

            Id::ProcessType listenToType;
            std::shared_ptr<Shared::Net::ConnectionListener> remoteListener;

            Shared::TimedStorage<std::shared_ptr<TemporaryConnectionOwner>> timedConnectionStorage;

            std::mutex listenerMutex;
            std::unordered_map<std::string, std::weak_ptr<LinkableIncomingContainer>> listeningLinkable;
        };
    }
}

#endif
