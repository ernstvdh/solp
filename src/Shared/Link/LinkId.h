#ifndef SOLP_SHARED_LINK_LINKID_H
#define SOLP_SHARED_LINK_LINKID_H

#include "Shared/Id.h"

namespace Shared
{
    namespace Link
    {
        class LinkId : public Shared::IpId
        {
        public:
            LinkId(const std::string& lIp) : Shared::IpId(lIp, Shared::Id::ProcessType::LINK) {}
            LinkId(const Shared::JSon::JSon& json) : Shared::IpId(json, Shared::Id::ProcessType::LINK) {}
            ~LinkId() {}

            static std::shared_ptr<Id> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<LinkId>(json);
            }
        };
    }
}

#endif
