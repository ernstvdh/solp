#ifndef SOLP_SHARED_LINK_MOVEABLE_H
#define SOLP_SHARED_LINK_MOVEABLE_H

#include <memory>
#include <atomic>
#include <future>

#include "Linkable.h"

namespace Shared
{
    namespace Link
    {
        class Moveable : public Linkable
        {
        public:
            enum State
            {
                INIT,
                RUN,
                GET_STATE,
                MOVEABLE
            };

            Moveable(bool run);
            Moveable(const Shared::JSon::JSon& json);
            virtual ~Moveable() = 0;

            void Run() { moveState.store(RUN); }
            void PrepareState(std::shared_ptr<LinkEventListener> listener);
            bool IsReadyForMove();

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const = 0;

        protected:
            bool PreparingForMove();
            void ReadyForMove();

            std::atomic<uint32_t> moveState;

        private:
            std::shared_ptr<LinkEventListener> moveListener;
        };
    }
}

#endif
