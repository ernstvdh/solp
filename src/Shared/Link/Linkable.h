#ifndef SOLP_SHARED_LINK_LINKABLE_H
#define SOLP_SHARED_LINK_LINKABLE_H

#include <map>
#include <memory>

#include "boost/lockfree/queue.hpp"

#include "Shared/Id.h"

namespace Shared
{
    namespace Message
    {
        class Message;
    }

    namespace Net
    {
        class Connection;
    }

    namespace Link
    {
        class Link;
        class Linkable;
        class LinkRequest;
        class LinkListener;
        class LinkEventListener;
        class LinkEventOnExpire;

        class LinkableIncomingContainer
        {
            friend class Linkable;

        private:
            struct LinkContainer
            {
                LinkContainer(std::shared_ptr<const Shared::Id> cId, std::weak_ptr<Link> cLink,
                std::shared_ptr<LinkRequest> rq) : id(cId), link(cLink), request(rq) {}

                std::shared_ptr<const Shared::Id> id;
                std::weak_ptr<Link> link;
                std::shared_ptr<LinkRequest> request;
            };

            struct ConnectionContainer
            {
                ConnectionContainer(std::shared_ptr<const Shared::Id> cId,
                std::shared_ptr<Shared::Net::Connection> cConn) :
                id(cId), connection(cConn) {}

                std::shared_ptr<const Shared::Id> id;
                std::shared_ptr<Shared::Net::Connection> connection;
            };

            boost::lockfree::queue<LinkContainer*, boost::lockfree::fixed_sized<false>> links;
            boost::lockfree::queue<ConnectionContainer*, boost::lockfree::fixed_sized<false>> connections;

        public:
            LinkableIncomingContainer(std::shared_ptr<LinkEventListener>& listener) :
                links(0), connections(0), eventListener(listener) {}
            ~LinkableIncomingContainer();

            bool AddIncomingLink(const std::shared_ptr<const Shared::Id>& from,
                std::shared_ptr<Link> link, std::shared_ptr<LinkRequest> request);
            bool AddIncomingConnection(const std::shared_ptr<const Shared::Id>& from,
                std::shared_ptr<Shared::Net::Connection> connection);

        private:
            std::shared_ptr<LinkEventListener> eventListener;
        };

        class Linkable : public Serializable
        {
        public:
            Linkable();
            Linkable(const Shared::JSon::JSon& json);
            virtual ~Linkable() = 0;

            // WARNING! any process we send a message to must either be an ipid or
            // must announce itself to the tracker!
            // options for sending a message
            // this method only sends one message and expects nothing back
            // it "opens" a link and afterwards "closes" it
            void SendSingleMessageTo(std::shared_ptr<Shared::Message::Message> mess);
            // WARNING! any process we send a message to must either be an ipid or
            // must announce itself to the tracker!
            // this method sends a message to some id and keeps the link open
            // because it expects a message back, it should close the link afterwards
            void SendMessageTemporary(std::shared_ptr<Shared::Message::Message> mess);
            // WARNING! any process we send a message to must either be an ipid or
            // must announce itself to the tracker!
            // this method sends a message over a pre created link and will error
            // if no link exists yet, this is the preferred way to handle long running links
            void SendMessagePersistent(std::shared_ptr<Shared::Message::Message> mess);

            // the below methods are not using locking
            void LinkTo(const std::shared_ptr<const Shared::Id>& otherId);
            void LinkToRemove(const std::shared_ptr<const Shared::Id>& otherId);
            // WARNING! ListenOn only works if this process is either an ipid or
            // if this process announces its ip to the tracker!
            void ListenOn(Id::ProcessType pType);
            void ListenOnRemove(Id::ProcessType pType);
            std::shared_ptr<Shared::Message::Message> PopMessage(uint64_t timeoutInMs = 0);

            std::shared_ptr<const Shared::Id> GetID() { return id; }

            std::string GetRemoteIpOfId(const std::shared_ptr<const Shared::Id>& remoteId);

            std::shared_ptr<LinkEventOnExpire> GetOnExpireLinkEvent();
            void SetTimedLinkEvent(uint64_t timeInMs);

            // Serialization
            virtual void SerializeToJSon(Shared::JSon::JSon& json) const override;

        protected:
            void RegisterWithMonitor();

            std::shared_ptr<const Shared::Id> id;
            std::shared_ptr<LinkEventListener> eventListener;

        private:
            void Init();
            void AnnounceToTrackers();

            bool HandleNewLinksAndConnections();

            std::unordered_map<std::string, std::pair<int, std::shared_ptr<Link>>> links;
            std::unordered_map<int, std::shared_ptr<LinkListener>> linkListeners;
            std::shared_ptr<LinkableIncomingContainer> incomings;

            // we don't have to serialize this variable (and SHOULD not) because
            // it will always be empty when we move (we exit PopMessage with a NULL message)
            std::shared_ptr<Link> lastPoppedLink;
        };
    }
}

#endif
