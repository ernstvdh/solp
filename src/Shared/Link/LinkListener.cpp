#include "LinkListener.h"
#include "LinkMessage.h"
#include "Linkable.h"
#include "Link.h"
#include "LinkId.h"
#include "Shared/Config.h"
#include "Shared/Debug.h"
#include "Shared/Net/ConnectionMgr.h"

namespace Shared
{
    namespace Link
    {
        bool TemporaryConnectionOwner::PushMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            SOLP_DEBUG(DEBUG_LINKLISTENER, "Pushing Message to TemporaryConnectionOwner.");

            if (std::shared_ptr<LinkingToMessage> linking = std::dynamic_pointer_cast<LinkingToMessage>(mess))
            {
                // link listener might already be expired... we need to lock it before we continue
                if (auto shared = linkListener.lock())
                    shared->AddConnectionFromTo(linking->senderID, linking->receiverID, conn);

                // if the linking was succesfull we do not need to manage the connection anymore
                // if it wasn't succesfull for whatever reason we still drop the connection
                conn.reset();
            }
            else
            {
                SOLP_WARNING("Received a non linking to message as a first message.. this should probably not happen...?");
            }

            return true;
        }

        std::shared_ptr<LinkListener> LinkListener::MakeLinkListener(Id::ProcessType type)
        {
            auto temp = std::shared_ptr<LinkListener>(new LinkListener(type));
            temp->remoteListener = Shared::Net::ConnectionMgr::singleton->ListenOn(
                Shared::Config::typePorts[type], temp);
            return temp;
        }

        std::shared_ptr<Shared::Net::ConnectionOwner> LinkListener::GetConnectionOwner()
        {
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Getting a temporary connection owner for incoming connection to linklistener of type %d", (int)listenToType);

            std::shared_ptr<TemporaryConnectionOwner> tco =
                std::make_shared<TemporaryConnectionOwner>(shared_from_this());
            timedConnectionStorage.Add(tco);
            
            return tco;
        }

        bool LinkListener::AddLinkFromTo(const std::shared_ptr<const Shared::Id>& ownId,
            const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> link)
        {
            std::lock_guard<std::mutex> lck(listenerMutex);

            auto find = listeningLinkable.find(otherId->Serialize());
            if (find != listeningLinkable.end())
            {
                if (auto shared = find->second.lock())
                {
                    // first we need to set the linkrequest for the outgoing
                    // otherwise the other thread may be to quick and try to
                    // set itself as outgoing link
                    // if the incoming does not accept the request the outgoing
                    // will retry anyway in PopMessage
                    std::shared_ptr<LinkRequest> request(std::make_shared<LinkRequest>(link));
                    link->SetOutgoingLinkRequest(request);
                    shared->AddIncomingLink(ownId, link, request);
                    return true;
                }
                else
                    listeningLinkable.erase(find);
            }

            return false;
        }

        bool LinkListener::AddConnectionFromTo(const std::shared_ptr<const Shared::Id>& ownId,
            const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Shared::Net::Connection> connection)
        {
            SOLP_DEBUG(DEBUG_LINKLISTENER, "Adding connection from %s to %s.", ownId->Serialize().c_str(), otherId->Serialize().c_str());

            std::lock_guard<std::mutex> lck(listenerMutex);

            auto find = listeningLinkable.find(otherId->Serialize());

            if (find == listeningLinkable.end())
                return false;

            if (auto shared = find->second.lock())
            {
                // with a delayed response the owner of the connection is set
                return shared->AddIncomingConnection(ownId, connection);
            }
            else
            {
                listeningLinkable.erase(find);
                return false;
            }
        }

        void LinkListener::AddAcceptingLinkable(std::shared_ptr<LinkableIncomingContainer> linkable,
            std::shared_ptr<const Shared::Id> acceptingId)
        {
            std::lock_guard<std::mutex> incLck(listenerMutex);
            
            if (listeningLinkable.find(acceptingId->Serialize()) != listeningLinkable.end())
            {
                SOLP_ERROR("Same ID is listening twice... this should not happen... terminating....");
                std::terminate();
            }

            listeningLinkable[acceptingId->Serialize()] = linkable;
        }

        void LinkListener::RemoveAcceptingLinkable(const std::shared_ptr<const Shared::Id>& id)
        {
            std::lock_guard<std::mutex> incLck(listenerMutex);

            listeningLinkable.erase(id->Serialize());
        }
    }
}
