#include "Shared/Config.h"
#include "LinkMgr.h"
#include "Linkable.h"
#include "LinkMessage.h"
#include "LinkEventListener.h"
#include "Shared/Net/ConnectionMgr.h"
#include "Balancing/Tracker/TrackerMessage.h"

namespace Shared
{
    namespace Link
    {
        std::shared_ptr<LinkListener> LinkMgr::ListenOn(Id::ProcessType type,
            std::shared_ptr<LinkableIncomingContainer> listener, std::shared_ptr<const Shared::Id> listeningId)
        {
            SOLP_DEBUG(DEBUG_LINKMGR, "Entering ListenOn with process type %d.", type);

            std::unique_lock<std::mutex> lock(listeningMutex);

            bool exists = (listening.count(type) > 0);

            std::shared_ptr<LinkListener> ll;

            // if it does exist but has expired we need to create a new listener
            if (exists)
            {
                std::weak_ptr<LinkListener> temp = listening[type];
                ll = temp.lock();
                if (temp.expired())
                {
                    exists = false;
                    listening.erase(type);
                }
            }

            if (!exists)
            {
                SOLP_DEBUG(DEBUG_LINKMGR, "Making new LinkListener;.");
                ll = LinkListener::MakeLinkListener(type);
                listening.emplace(type, ll);
            }

            lock.unlock();
            
            SOLP_DEBUG(DEBUG_LINKMGR, "Setting Listener.");
            ll->AddAcceptingLinkable(listener, listeningId);

            //TODO: maybe we have to lock this... not sure, we probably should as soon as more
            // threads are going to listen on the tracker port (such as monitoring)
            // initialize our link with the tracker as soon as it is operational
            //TODO: trackerCommunicator should just be initialized and the tracker can
            // connect with it when it is ready....
            if (type == Id::ProcessType::ZONE_SERVER_TRACKER && trackerCommunicator == NULL)
            {
                trackerCommunicator = std::make_shared<TrackerCommunicator>(
                    std::make_shared<const BL::Tracker::TrackerId>(Shared::Config::localIP));

                InternalLink(trackerCommunicator->GetID(), trackerCommunicator->GetRemoteID(),
                    trackerCommunicator);
            }

            return ll;
        }

        bool LinkMgr::LinkTo(const std::shared_ptr<const Shared::Id>& ownId,
            const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> listener)
        {
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Id (%s) is linking to id (%s).", ownId->Serialize().c_str(), otherId->Serialize().c_str());
            // ping pong should never resolve
            // and should never link internally
            std::promise<std::function<bool()>> promise;
            if (otherId->IsIpId())
            {
                SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Linking to remote ID (%s) is ipid.", otherId->Serialize().c_str());

                std::shared_ptr<const IpId> temp(std::static_pointer_cast<const IpId>(otherId));
                std::unique_ptr<std::function<bool()>> ptr(new std::function<bool()>(
                    std::bind(&LinkMgr::ContinueLinking, this, ownId, otherId, listener, temp->ip)));
                listener->NSLookupFinished(std::move(ptr), true);

                return true;
            }
            else
            {
                trackerCommunicator->RequestIpForId(otherId, listener, 
                    std::bind(&LinkMgr::ContinueLinking, this, ownId, otherId, listener, std::placeholders::_1));

                return false;
            }
        }

        bool LinkMgr::ContinueLinking(const std::shared_ptr<const Shared::Id>& ownId,
            const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> listener, const std::string& ip)
        {
            const int port = Config::typePorts[otherId->type];

            if (ip == Config::localIP)
            {
                return InternalLink(ownId, otherId, listener);
            }
            else
            {
                SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Making connection to remote ID (%s).", otherId->Serialize().c_str());
                auto conn = Shared::Net::ConnectionMgr::singleton->ConnectTo(ip, port, listener);
                conn->PushMessage(Shared::Message::Message::MakeMessage<LinkingToMessage>(ownId, otherId));
                return true;
            }
        }

        bool LinkMgr::InternalLink(const std::shared_ptr<const Shared::Id>& ownId,
            const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> listener)
        {
            std::lock_guard<std::mutex> lock(listeningMutex);

            if (listening.count(otherId->type) <= 0)
                return false;

            if (auto shared = listening[otherId->type].lock())
                return shared->AddLinkFromTo(ownId, otherId, listener);

            return false;
        }

        LinkMgr::TrackerCommunicator::TrackerCommunicator(std::shared_ptr<const BL::Tracker::TrackerId> tid) :
            Link(std::make_shared<const LinkId>("trackerCommunicator"), tid, std::make_shared<LinkEventListener>()) {}

        void LinkMgr::TrackerCommunicator::RequestIpForId(const std::shared_ptr<const Shared::Id>& otherId,
            const std::shared_ptr<Link>& listener,
            std::function<bool(const std::string& ip)>&& continuation)
        {
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Linking to remote ID (%s) is not an ipid.", otherId->Serialize().c_str());

            std::shared_ptr<BL::Tracker::RequestInfoTrackerMessage> mess =
                std::make_shared<BL::Tracker::RequestInfoTrackerMessage>(otherId);
            mess->senderID = ownId;
            mess->receiverID = remoteId;

            std::lock_guard<std::mutex> lck(updateRequestsMutex);
            openRequests[otherId->Key()].emplace_back(std::move(continuation), listener);
            
            SendMess(mess);
        }

        bool LinkMgr::TrackerCommunicator::PushMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            // we push and pop link messages so we get the expected behaviour....
            // we have to use the same lock that is used in RequestIpOfId because
            // PopMessage and SendMess should not be called concurrently
            if (mess->processType == Shared::Id::ProcessType::LINK)
            {
                std::lock_guard<std::mutex> lck(updateRequestsMutex);
                messages.push(new Shared::Message::MessageContainer(mess));
                if (Link::PopMessage() != NULL)
                    SOLP_WARNING("We only expect messages to be pushed to the real link when they are handled by popmessage...");
            }

            //TODO: handle stream resumption........
            if (auto update = std::dynamic_pointer_cast<BL::Tracker::UpdateTrackerMessage>(mess))
            {
                std::lock_guard<std::mutex> lck(updateRequestsMutex);

                auto requesters = openRequests.find(update->updateId->Key());

                if (requesters != openRequests.end())
                {
                    for (auto& request : requesters->second)
                    {
                        std::unique_ptr<std::function<bool()>> ptr(new std::function<bool()>(
                            std::bind(std::get<0>(request), update->newIp)));
                        std::get<1>(request)->NSLookupFinished(std::move(ptr), false);
                    }

                    openRequests.erase(requesters);
                }
            }

            return true;
        }
    }
}
