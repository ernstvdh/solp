#ifndef SOLP_SHARED_LINK_LINKMGR_H
#define SOLP_SHARED_LINK_LINKMGR_H

#include <future>
#include <tuple>

#include "Shared/Id.h"

#include "Link.h"
#include "LinkId.h"
#include "LinkListener.h"

#include "Balancing/Tracker/TrackerId.h"

namespace Shared
{
    namespace Link
    {
        class LinkMgr
        {
        private:
            // only one instance of tracker is pushing messages so that does not have to be thread safe
            // multiple threads may be adding request (so that combined with push message should be thread safe)
            class TrackerCommunicator : public Link
            {
            public:
                TrackerCommunicator(std::shared_ptr<const BL::Tracker::TrackerId> tid);
                ~TrackerCommunicator() {}

                // immediately handle pushmessage
                bool PushMessage(std::shared_ptr<Shared::Message::Message> mess);

                void RequestIpForId(const std::shared_ptr<const Shared::Id>& otherId,
                    const std::shared_ptr<Link>& listener,
                    std::function<bool(const std::string& ip)>&& continuation);

            private:
                std::mutex updateRequestsMutex;
                std::unordered_map<std::string, std::vector<std::tuple<
                    std::function<bool(const std::string& ip)>,
                    std::shared_ptr<Link>
                    >>> openRequests;
            };

        public:
            static LinkMgr* singleton;

            LinkMgr() {}
            ~LinkMgr() {}

            std::shared_ptr<LinkListener> ListenOn(Id::ProcessType type,
                std::shared_ptr<LinkableIncomingContainer> listener,
                std::shared_ptr<const Shared::Id> listeningId);

            bool LinkTo(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> listener);

            bool IsTrackerReady() { return trackerCommunicator != NULL; }

        private:
            // we need a link to the tracker to be able to request information
            std::shared_ptr<TrackerCommunicator> trackerCommunicator;

            std::unordered_map<int, std::weak_ptr<LinkListener>> listening;
            std::mutex listeningMutex;

            // we directly talk to the LinkListener to register our outgoing
            // link with an internal link
            bool InternalLink(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& otherId, std::shared_ptr<Link> listener);

            bool ContinueLinking(const std::shared_ptr<const Shared::Id>& ownId,
                const std::shared_ptr<const Shared::Id>& otherId,
                std::shared_ptr<Link> listener, const std::string& ip);
        };
    }
}

#endif
