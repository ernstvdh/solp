#include "Link.h"
#include "LinkMgr.h"
#include "LinkMessage.h"
#include "LinkEventListener.h"

#include "Shared/Debug.h"

namespace Shared
{
    namespace Link
    {
        LinkRequest::~LinkRequest()
        {
            if (auto shared = requester.lock())
                shared->ExpiredLinkRequest();
        }

        Link::~Link()
        {
            // we know for sure no messages can be pushed to the queue now because
            // the link is only deleted when nobody has a shared ptr to it
            Shared::Message::MessageContainer* temp = NULL;
            while (messages.pop(temp))
                delete temp;
        }

        void Link::InitOutgoing()
        {
            SOLP_DEBUG(DEBUG_LINK, "");
            SOLP_DEBUG(DEBUG_STREAM_RESET, "Init link from: %s to: %s",
                ownId->Serialize().c_str(), remoteId->Serialize().c_str());

            switch (outgoingStatus.load())
            {
            case Status::NO_OUTGOING:
                // if LinkTo returns true we didn't have to do a nsLookup so we can immediately call
                // get on the nsLookupContinuation future, which will give either true or false as result
                // otherwise we are in a NS_LOOKUP state
                // we set the outgoingStatus to NS_LOOKUP first because of callback hell
                // as soon as ExecuteContinuation is called we set it back to NO_OUTGOING...
                outgoingStatus.store(Status::NS_LOOKUP);
                nsLookupContinuation.reset();
                if (LinkMgr::singleton->LinkTo(ownId, remoteId, shared_from_this()))
                    ExecuteContinuation();

                // the state should never be NO_OUTGOING
                if (outgoingStatus.load() == Status::NO_OUTGOING)
                {
                    SOLP_ERROR("Unexpected outgoing state... terminating...");
                    std::terminate();
                }

                break;
            case Status::NS_LOOKUP:
            case Status::LINK_LISTENING:
            case Status::LINK_NOT_LISTENING:
            case Status::CONNECTION_LISTENING:
            case Status::CONNECTION_NOT_LISTENING:
                if (streamResumptionData.Resetting())
                {
                    streamResumptionData.resettingSelf = false;

                    if (!streamResumptionData.sentReset)
                        PushOutgoingMessagesToOut();
                }
                else
                {
                    SOLP_ERROR("Trying to init a outgoinglink but it already has a state... terminating...");
                    std::terminate();
                }
                break;
            default:
                SOLP_ERROR("Unknown outgoing link status encountered.... terminating....");
                std::terminate();
                break;
            }
        }

        // maybe we have to maintain the state that we are killing it?
        // otherwise maybe the external link might still be connecting when we are killing it
        // possibly addd at all points where we check the outgoingStatus also this special state?
        void Link::KillOutgoing()
        {
            SOLP_DEBUG(DEBUG_LINK, "");
            SOLP_DEBUG(DEBUG_STREAM_RESET, "Kill link from: %s to: %s",
                ownId->Serialize().c_str(), remoteId->Serialize().c_str());
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Kill link from: %s to: %s",
                ownId->Serialize().c_str(), remoteId->Serialize().c_str());

            switch (outgoingStatus.load())
            {
            case Status::NO_OUTGOING:
                // This is not possible because if we are switching to this state we
                // immediately reinit the state which results in either
                // CONNECTION_NOT_LISTENING or LINK_NOT_LISTENING
                SOLP_ERROR("Trying to kill a outgoinglink but it has no outgoing state... terminating...");
                std::terminate();
                break;
            case Status::NS_LOOKUP:
            case Status::LINK_LISTENING:
            case Status::LINK_NOT_LISTENING:
            case Status::CONNECTION_LISTENING:
            case Status::CONNECTION_NOT_LISTENING:
                //TODO: we don't need to reset if we never have sent anything....
                // Add special handling for this scenario....
                if (streamResumptionData.resettingSelf)
                    break;

                SOLP_DEBUG(DEBUG_STREAM_RESET, "Reset by kill....");
                streamResumptionData.resettingSelf = true;
                SendResetIfNeeded();
                break;
            default:
                SOLP_ERROR("Unknown outgoing link status encountered.... terminating....");
                std::terminate();
                break;
            }
        }

        void Link::ExecuteContinuation()
        {
            // if the continuation returns false we are stuck in the NO_OUTGOING state
            // to make things a bit easier we just set the state to
            // LINK_NOT_LISTENING, this is basically also true because
            // false can only be returned for localIP's
            // in this state it will retry connecting in PopMessage
            // when the linkRequest is found to be expired (it is never initialized)
            outgoingStatus.store(Status::NO_OUTGOING);
            if (!(*nsLookupContinuation)())
            {
                outgoingStatus.store(Status::LINK_NOT_LISTENING);
                std::shared_ptr<LinkRequest> request(std::make_shared<LinkRequest>(shared_from_this()));
                std::shared_ptr<std::shared_ptr<LinkRequest>> temporary(std::make_shared<std::shared_ptr<LinkRequest>>(request));
                // set our own weak ptr so that we can check the life of the fake request
                linkRequest = request;
                nsLookupFuture = std::async(std::launch::async, [temporary]
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(20));
                    temporary->reset();
                });
            }

            // the state should never be NO_OUTGOING
            if (outgoingStatus.load() == Status::NO_OUTGOING)
            {
                SOLP_ERROR("Unexpected outgoing state... terminating...");
                std::terminate();
            }

            nsLookupContinuation.reset();
        }

        //TODO: Returning false to a connection makes no sense....
        // it can't do anything with that, so connections should be able to push
        // till the connection is really aborted, in any case closing
        // has to be in sync with the connection closing
        bool Link::PushMessage(std::shared_ptr<Shared::Message::Message> mess)
        {
            SOLP_DEBUG(DEBUG_LINK, "Pushing message (%s) to (%s).", mess->Serialise().c_str(), ownId->Serialize().c_str());

            messages.push(new Shared::Message::MessageContainer(mess));

            eventListener->AddEvent(shared_from_this());
            return true;
        }

        std::shared_ptr<Shared::Message::Message> Link::PopMessage()
        {
            Shared::Message::MessageContainer* temp = NULL;
            if (messages.pop(temp))
            {
                // NULL is pushed to the queue if the connection was aborted
                // it can be either the outgoing connection or the incoming connection
                // the handling the outgoing connection is easy because we are
                // maintaining its state ourself, the incoming might end up with state problems....
                if (temp == NULL)
                {
                    HandleNULLMessage();
                    return NULL;
                }

                std::shared_ptr<Shared::Message::Message> mess(temp->mess);
                delete temp;

                if (mess->processType == Shared::Id::ProcessType::LINK)
                {
                    switch (std::static_pointer_cast<LinkMessage>(mess)->linkMessageType)
                    {
                    case LinkMessage::LinkMessageType::I_AM_LISTENING:
                        HandleIAmListeningMessage(std::static_pointer_cast<IAmListeningMessage>(mess));
                        return NULL;
                    case LinkMessage::LinkMessageType::ACK:
                        UpdateOutgoingStreamResumption(std::static_pointer_cast<AckLinkMessage>(mess));
                        return NULL;
                    case LinkMessage::LinkMessageType::RESET:
                        HandleResetLinkMessage(std::static_pointer_cast<ResetLinkMessage>(mess));
                        return NULL;
                    case LinkMessage::LinkMessageType::CONFIRM:
                        HandleResetConfirmMessage(std::static_pointer_cast<ResetConfirmMessage>(mess));
                        return NULL;
                    case LinkMessage::LinkMessageType::LINKING_TO:
                        SOLP_ERROR("Received LINKING_TO message in Link... should not happen... terminating...");
                        std::terminate();
                    default:
                        break;
                    }
                }

                if (!ExpectedMessageForStreamResumption(mess))
                    return NULL;

                UpdateStreamResumption(mess);

                SOLP_DEBUG(DEBUG_LINK, "Popping message (%s).", mess->Serialise().c_str());

                return mess;
            }
            else
            {
                SOLP_ERROR("Popping message on link, but no message found, this is not possible.... terminating...");
                std::terminate();
            }

            return HandleEmptyIncQueue();
        }

        void Link::FlushStreamResumptionIfNeeded()
        {
            if (messages.empty())
                FlushStreamResumption();
        }

        bool Link::SendMess(std::shared_ptr<Shared::Message::Message> mess, int flag)
        {
            SOLP_DEBUG(DEBUG_LINK, "Sending message (%s).", mess->Serialise().c_str());

            bool sent = false;

            if (flag & SendFlag::STREAM)
                SetStreamResumption(mess);

            if (!streamResumptionData.sentReset || flag & SendFlag::RESET)
            {
                if (!sent && outgoingStatus.load() & ~Status::CANNOT_SEND)
                {
                    if (outgoingStatus.load() == Status::LINK_LISTENING)
                    {
                        if (auto shared = outgoingLink.lock())
                        {
                            if (!shared->PushMessage(mess))
                            {
                                outgoingStatus.store(Status::NO_OUTGOING);
                                InitOutgoing();
                            }
                            else
                            {
                                SOLP_DEBUG(DEBUG_LINK, "Sending message on outgoing link.");
                                sent = true;
                            }
                        }
                        else
                        {
                            outgoingStatus.store(Status::NO_OUTGOING);
                            InitOutgoing();
                        }
                    }
                    else if (outgoingStatus.load() == Status::CONNECTION_LISTENING)
                    {
                        if (!outgoingConnection->PushMessage(mess))
                        {
                            // this is handled by the NULL message that is pushed
                            // we should not change the state here...
                        }
                        else
                        {
                            SOLP_DEBUG(DEBUG_LINK, "Sending message on outgoing connection.");
                            sent = true;
                        }
                    }
                }

                if (!sent && incomingStatus.load() & ~Status::CANNOT_SEND)
                {
                    if (incomingStatus.load() == Status::LINK_LISTENING)
                    {
                        if (auto shared = incomingLink.lock())
                        {
                            if (!shared->PushMessage(mess))
                            {
                                // this is handled by the NULL message that is pushed
                                // we should not change the state here...
                            }
                            else
                            {
                                SOLP_DEBUG(DEBUG_LINK, "Sending message on incoming link.");
                                sent = true;
                            }
                        }
                        else
                            incomingStatus.store(Status::NO_OUTGOING);
                    }
                    else if (incomingStatus.load() == Status::CONNECTION_LISTENING)
                    {
                        if (!incomingConnection->PushMessage(mess))
                        {
                            // this is handled by the NULL message that is pushed
                            // we should not change the state here...
                        }
                        else
                        {
                            SOLP_DEBUG(DEBUG_LINK, "Sending message on incoming connection.");
                            sent = true;
                        }
                    }
                }
            }

            if (!sent)
            {
                // if it is not sent and we don't have an incoming link/connection
                // messages have to wait for us to make a connection with the remote.
                // Or as expected the remote reconnects with this link and we can push
                // the messages, the original connector has to make sure it maintains
                // the connection until all messages are handled.

                if (flag & SendFlag::QUEUE)
                {
                    SOLP_DEBUG(DEBUG_LINK, "Message queued because no connection/link is working.\nOutgoing State: %d, Incoming State: %d",
                        outgoingStatus.load(), incomingStatus.load());
                    outgoingMessages.push_back(mess);
                }
                else
                    SOLP_DEBUG(DEBUG_LINK, "Message not sent and also not queued because of sendflag.");

                return false;
            }

            return true;
        }

        void Link::PushOutgoingMessagesToOut()
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            while (!outgoingMessages.empty())
            {
                if (!SendMess(outgoingMessages.front(), SendFlag::NONE))
                {
                    outgoingMessages.pop_back();
                    break;
                }
                else
                    outgoingMessages.pop_front();
            }
        }

        void Link::Aborted(Shared::Net::Connection::ABORTED_CODE code, Shared::Net::Connection* pointer)
        {
            SOLP_DEBUG(DEBUG_CONNECTION_LINK, "Aborted received in link with ownID (%s) and remoteID (%s).",
                ownId->Serialize().c_str(), remoteId->Serialize().c_str());
            SOLP_DEBUG(DEBUG_LINK, "");

            switch (code)
            {
            case Shared::Net::Connection::ABORTED_CODE::FAILED_CONNECT:
                if (incomingConnection.get() == pointer)
                {
                    SOLP_ERROR("Received failed connect error code in incomingLink... this makes no sense... terminate...");
                    std::terminate();
                }
                else if (outgoingConnection.get() == pointer)
                {
                    if (outgoingStatus.load() != Status::CONNECTION_NOT_LISTENING)
                    {
                        SOLP_ERROR("Received failed connect error code but link has unexpected state (%d)... terminate...",
                            outgoingStatus.load());
                        std::terminate();
                    }

                    abortedConnections.push(pointer);
                    messages.push(NULL);
                    eventListener->AddEvent(shared_from_this());
                }

                break;
            case Shared::Net::Connection::ABORTED_CODE::WRITE_FAILED:
            case Shared::Net::Connection::ABORTED_CODE::REMOTE_DISCONNECT:
            {
                abortedConnections.push(pointer);
                messages.push(NULL);
                eventListener->AddEvent(shared_from_this());
                break;
            }
            default:
                SOLP_ERROR("Unhandled connection error code.. terminating....");
                std::terminate();
                break;
            }
        }

        void Link::AbortedLink(Link* pointer)
        {
            abortedLinks.push(pointer);
            messages.push(NULL);
            eventListener->AddEvent(shared_from_this());
        }

        void Link::ExpiredLinkRequest()
        {
            if (outgoingStatus.load() == Status::LINK_NOT_LISTENING)
            {
                messages.push(NULL);
                eventListener->AddEvent(shared_from_this());
            }
        }

        void Link::SetConnection(std::shared_ptr<Shared::Net::Connection> out)
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            if (outgoingStatus.load() == Status::NO_OUTGOING)
            {
                outgoingConnection = out;
                outgoingStatus.store(Status::CONNECTION_NOT_LISTENING);
            }
            else
            {
                SOLP_ERROR("Setting an incoming connection but the state is unexpected..... terminating...");
                std::terminate();
            }
        }

        void Link::SetOutgoingLinkRequest(std::shared_ptr<LinkRequest> out)
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            if (outgoingStatus.load() == Status::NO_OUTGOING)
            {
                linkRequest = out;
                outgoingStatus.store(Status::LINK_NOT_LISTENING);
            }
            else
            {
                SOLP_ERROR("Setting an outgoing link request but the state is unexpected..... terminating...");
                std::terminate();
            }
        }

        bool Link::SetOutgoingLink(std::weak_ptr<Link> out)
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            if (outgoingStatus.load() == Status::LINK_NOT_LISTENING)
            {
                outgoingLink = out;
                return true;
            }
            
            // we return if the adding of the outgoing link was a success
            // if not the remote will never be informed that it has a
            // dead incoming link
            return false;
        }

        void Link::SetIncConnection(std::shared_ptr<Shared::Net::Connection> out)
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            // we have to wait with sending the IAmListeningMessage till we are here
            // because now we are sure the owner of the connection is set to be this link
            std::shared_ptr<Shared::Message::Message> listening(
                Shared::Message::Message::MakeMessage<IAmListeningMessage>(ownId, remoteId));
            SetStreamResumption(listening);
            out->PushMessage(listening);

            switch (incomingStatus.load())
            {
            case Status::NO_OUTGOING:
                incomingConnection = out;
                incomingStatus.store(Status::CONNECTION_LISTENING);
                PushOutgoingMessagesToOut();
                break;
            case Status::LINK_LISTENING:
                //TODO: does this really work in all cases? what if connections/links arive
                // in the wrong order? Will that break everything?
                incomingConnection = out;
                incomingStatus.store(Status::CONNECTION_LISTENING);
                incomingLink.reset();
                break;
            case Status::CONNECTION_LISTENING:
                incomingConnection = out;
                break;
            default:
                SOLP_ERROR("Setting incoming link but this link is in unexpected state.. terminating...");
                std::terminate();
                break;
            }

            SendResetIfNeeded();
            ResendUnconfirmed();
        }

        //TODO: the stream resumption should make sure all messages are always received...
        // looking at the switch case for Status::LINK_LISTENING this should not happen but
        // to be sure stream resumption should make sure both receiver and sender are
        // on the same page
        void Link::SetIncLink(std::weak_ptr<Link> out)
        {
            SOLP_DEBUG(DEBUG_LINK, "");

            if (auto shared = out.lock())
            {
                std::shared_ptr<Shared::Message::Message> listening(
                    Shared::Message::Message::MakeMessage<IAmListeningMessage>(ownId, remoteId));
                SetStreamResumption(listening);
                shared->PushMessage(listening);
            }

            switch (incomingStatus.load())
            {
            case Status::NO_OUTGOING:
                incomingLink = out;
                incomingStatus.store(Status::LINK_LISTENING);
                PushOutgoingMessagesToOut();
                break;
            case Status::LINK_LISTENING:
                // we can safely replace the old link because the status can only be changed by linkable
                incomingLink = out;
                // this case should however never happen, because we are replacing a link with a link....
                SOLP_WARNING("Setting the incoming link while we already are in this state... this should not happen...");
                break;
            case Status::CONNECTION_LISTENING:
                incomingLink = out;
                incomingStatus.store(Status::LINK_LISTENING);
                incomingConnection.reset();
                break;
            default:
                SOLP_ERROR("Setting incoming link but this link is in unexpected state.. terminating...");
                std::terminate();
                break;
            }

            SendResetIfNeeded();
            ResendUnconfirmed();
        }

        void Link::NSLookupFinished(std::unique_ptr<std::function<bool()>>&& continuation, bool immediate)
        {
            nsLookupContinuation = std::move(continuation);

            if (!immediate)
            {
                messages.push(NULL);
                eventListener->AddEvent(shared_from_this());
            }
        }

        bool Link::IsFinished()
        {
            if (!messages.empty())
                return false;

            if (streamResumptionData.Resetting())
                return false;

            if (!outgoingMessages.empty())
                return false;

            if (!streamResumptionData.IsFinished())
                return false;

            if (!streamResumptionData.messageQueue.empty())
                return false;

            if (incomingStatus.load() & ~Status::NO_OUTGOING)
                return false;

            return true;
        }

        std::string Link::GetIpOfRemote()
        {
            SOLP_DEBUG(DEBUG_LINK, "Getting ip for remote %s.", remoteId->Serialize().c_str());

            if (outgoingStatus.load() & Status::CONNECTION_STATUS)
                return outgoingConnection->GetID()->ip;

            if (incomingStatus.load() & Status::CONNECTION_STATUS)
                return incomingConnection->GetID()->ip;

            return "failed";
        }

        void Link::HandleNULLMessage()
        {
            // we are only allowed to handle one failure each time the handler is called
            // if we handle all at the same time we might end up with the wrong state
            // see comment at the end of this function
            Shared::Net::Connection* connPointer = NULL;
            while (abortedConnections.pop(connPointer))
            {
                //TODO: remove this check... we never know when the same
                // pointer will occur....
                if (outgoingConnection.get() == connPointer)
                {
                    outgoingConnection.reset();

                    if (outgoingStatus.load() & Status::CONNECTION_STATUS)
                    {
                        outgoingStatus.store(Status::NO_OUTGOING);
                        InitOutgoing();
                    }
                }
                
                if (incomingConnection.get() == connPointer)
                {
                    incomingConnection.reset();

                    if (incomingStatus.load() & Status::CONNECTION_STATUS)
                        incomingStatus.store(Status::NO_OUTGOING);
                }

                // we handled one failure so we return
                // otherwise we have the risk of handling multiple....
                return;
            }

            Link* linkPointer = NULL;
            while (abortedLinks.pop(linkPointer))
            {
                if (auto shared = incomingLink.lock())
                {
                    if (shared.get() == linkPointer)
                        if (incomingStatus.load() & Status::LINK_STATUS)
                            incomingStatus.store(Status::NO_OUTGOING);
                }
                else
                {
                    incomingLink.reset();
                    
                    if (incomingStatus.load() & Status::LINK_STATUS)
                        incomingStatus.store(Status::NO_OUTGOING);
                }

                // again we return to only handle one failure...
                return;
            }

            if (outgoingStatus.load() == Status::NS_LOOKUP)
            {
                // if set we also know the continuation is ready
                if (nsLookupContinuation)
                {
                    // in executecontinuation we set the state to
                    // NO_OUTGOING and we continue with the continuation
                    ExecuteContinuation();
                    return;
                }
            }

            // we do this check at the very end because we have to be sure
            // we are not handling a NULL message for a different failure
            // such as incoming connection disconnect
            // if we handle it earlier we might by accident delete the
            // outgoing status of waiting for a link, while actually
            // we did receive a IAmListening message, but we just did not
            // handle it yet
            if (outgoingStatus.load() == Status::LINK_NOT_LISTENING)
            {
                // if our request has been deleted the system we try to connect to
                // has moved and we should retry connecting
                if (linkRequest.expired())
                {
                    outgoingStatus.store(Status::NO_OUTGOING);
                    InitOutgoing();
                    return;
                }
            }
        }

        void Link::HandleIAmListeningMessage(const std::shared_ptr<IAmListeningMessage>& mess)
        {
            if (outgoingStatus.load() == Status::CONNECTION_NOT_LISTENING)
            {
                outgoingStatus.store(Status::CONNECTION_LISTENING);
            }
            else if (outgoingStatus.load() == Status::LINK_NOT_LISTENING)
            {
                outgoingStatus.store(Status::LINK_LISTENING);
            }
            else
                return;

            HandleStreamResumptionIAmListening(mess);

            PushOutgoingMessagesToOut();
        }

        void Link::HandleResetLinkMessage(const std::shared_ptr<ResetLinkMessage>& mess)
        {
            SOLP_DEBUG(DEBUG_STREAM_RESET, "Handle reset message at: %s from: %s",
            ownId->Serialize().c_str(), remoteId->Serialize().c_str());

            // if we have an outgoing we can always let the remote reset itself
            if (outgoingStatus.load() & ~Status::NO_OUTGOING)
            {
                SendMess(Shared::Message::Message::MakeMessage<ResetConfirmMessage>(ownId, remoteId), SendFlag::RESET);
                streamResumptionData.in = StreamResumptionStorage::NOT_INIT;
                streamResumptionData.lastAcknowledged = StreamResumptionStorage::NOT_INIT;
            }
            else
            {
                // otherwise we might be resetting without an outgoing...
                // in this case we use the incoming link/connection...
                // so we have to finish our own reset before we let the remote reset...
                // if we are already in the resetting state we don't have to do anything
                // otherwise we have to start our own reset
                if (!streamResumptionData.Resetting())
                {
                    streamResumptionData.remoteWaitingForReset = true;
                    streamResumptionData.resettingSelf = true;
                    SendResetIfNeeded();
                }
            }
        }

        void Link::HandleResetConfirmMessage(const std::shared_ptr<ResetConfirmMessage>& mess)
        {
            SOLP_DEBUG(DEBUG_STREAM_RESET, "Handle link reset confirm at: %s from: %s",
            ownId->Serialize().c_str(), remoteId->Serialize().c_str());

            // this message can be received multiple times so we need to check
            // if the reset is still in progress
            if (streamResumptionData.sentReset)
            {
                // if the remote was waiting for a reset confirm message we
                // can safely sent it now....
                if (streamResumptionData.remoteWaitingForReset)
                {
                    SendMess(Shared::Message::Message::MakeMessage<ResetConfirmMessage>(ownId, remoteId), SendFlag::RESET);
                    streamResumptionData.in = StreamResumptionStorage::NOT_INIT;
                    streamResumptionData.lastAcknowledged = StreamResumptionStorage::NOT_INIT;

                    streamResumptionData.remoteWaitingForReset = false;
                }

                streamResumptionData.sentReset = false;

                // if we are still resetting self then we actually drop
                // outgoing link or connection...
                if (streamResumptionData.resettingSelf)
                {
                    if (outgoingStatus.load() & Status::LINK_STATUS)
                    {
                        if (auto shared = outgoingLink.lock())
                            shared->AbortedLink(this);
                    }
                    else
                        outgoingConnection.reset();

                    outgoingStatus.store(Status::NO_OUTGOING);
                    streamResumptionData.resettingSelf = false;
                }

                // we couldn't send while we were waiting for this confirm
                // so if we have any messages in the queue we push them now
                PushOutgoingMessagesToOut();
            }
        }

        bool Link::ExpectedMessageForStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess)
        {
            SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "Checking message: %s.", mess->Serialise().c_str());

            if (!IsStreamResumptionMessage(mess))
            {
                SOLP_ERROR("Unhandled non stream resumption message detected... terminating....");
                std::terminate();
            }

            if (mess->outMessNum == StreamResumptionStorage::GetNextNumber(streamResumptionData.in))
            {
                SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "Accepted message.");
                streamResumptionData.in = StreamResumptionStorage::GetNextNumber(streamResumptionData.in);
                return true;
            }

            SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "Rejected message.");
            return false;
        }

        void Link::UpdateStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess)
        {
            // Note: this function is only called for incoming messages
            streamResumptionData.in = mess->outMessNum;

            UpdateOutgoingStreamResumption(mess);
        }

        void Link::UpdateOutgoingStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess)
        {
            // if we are resetting our stream resumption counter we have to wait with further processing
            // till we have a confirmation, otherwise data from before the reset might 
            if (streamResumptionData.sentReset)
                return;

            // first check if we can just delete the whole messageQueue
            if (mess->inMessNum == streamResumptionData.out)
                streamResumptionData.messageQueue.clear();

            while (!streamResumptionData.messageQueue.empty())
            {
                if (StreamResumptionStorage::CircularLargerThan(streamResumptionData.messageQueue.front()->outMessNum, mess->inMessNum))
                    return;

                SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "Removing message from streamResumptionData queue.");
                streamResumptionData.messageQueue.pop_front();
            }

            SendResetIfNeeded();
        }

        void Link::SendResetIfNeeded()
        {
            if (!streamResumptionData.resettingSelf)
                return;

            SOLP_DEBUG(DEBUG_STREAM_RESET, "Entered send reset at link from: %s to: %s",
            ownId->Serialize().c_str(), remoteId->Serialize().c_str());

            if (streamResumptionData.messageQueue.empty())
            {
                SOLP_DEBUG(DEBUG_STREAM_RESET, "Send reset at link from: %s to: %s",
                ownId->Serialize().c_str(), remoteId->Serialize().c_str());

                if (!streamResumptionData.sentReset)
                {
                    streamResumptionData.out = StreamResumptionStorage::NOT_INIT;
                    streamResumptionData.sentReset = true;
                }

                SendMess(Shared::Message::Message::MakeMessage<ResetLinkMessage>(ownId, remoteId), SendFlag::RESET);
            }
        }

        void Link::SetStreamResumption(std::shared_ptr<Shared::Message::Message>& mess)
        {
            if (IsStreamResumptionMessage(mess))
            {
                streamResumptionData.out = StreamResumptionStorage::GetNextNumber(streamResumptionData.out);
                streamResumptionData.messageQueue.push_back(mess);
            }

            mess->inMessNum = streamResumptionData.in;
            // we are not sure this message arrives but if it does not it means
            // we are going to reconnect anyway and we have to resync...
            streamResumptionData.lastAcknowledged = streamResumptionData.in;
            mess->outMessNum = streamResumptionData.out;
        }

        void Link::HandleStreamResumptionIAmListening(const std::shared_ptr<IAmListeningMessage>& mess)
        {
            UpdateOutgoingStreamResumption(mess);

            ResendUnconfirmed();
        }

        void Link::ResendUnconfirmed()
        {
            SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "");

            for (auto resend : streamResumptionData.messageQueue)
                SendMess(resend, SendFlag::QUEUE);
        }

        void Link::FlushStreamResumption()
        {
            // if all acks have been piggy backed we can skip the flush
            if (streamResumptionData.lastAcknowledged == streamResumptionData.in)
                return;

            SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "");

            // otherwise we send an ack mess
            std::shared_ptr<AckLinkMessage> ack = Shared::Message::Message::MakeMessage<AckLinkMessage>(ownId, remoteId);
            SendMess(ack, SendFlag::STREAM | SendFlag::RESET);
        }

        bool Link::IsStreamResumptionMessage(const std::shared_ptr<Shared::Message::Message>& mess)
        {
            if (mess->processType == Shared::Id::ProcessType::LINK)
            {
                SOLP_DEBUG(DEBUG_STREAM_RESUMPTION, "Not a stream resumption message: %s", mess->Serialise().c_str());
                return false;
            }

            return true;
        }

        Link::Link(const Shared::JSon::JSon& json) : incomingStatus(NO_OUTGOING),
            messages(0), abortedConnections(0), abortedLinks(0)
        {
            GET_FROM_JSON_STRICT(ownId);
            GET_FROM_JSON_STRICT(remoteId);
            GET_FROM_JSON_STRICT(outgoingMessages);

            int outgoingState = 0;
            GET_FROM_JSON_STRICT(outgoingState);
            outgoingStatus.store(outgoingState);
            
            GET_FROM_JSON_STRICT(streamResumptionData);
        }

        void Link::InitAfterMove(std::shared_ptr<LinkEventListener>& listener)
        {
            eventListener = listener;

            if (outgoingStatus.load() != NO_OUTGOING)
            {
                outgoingStatus.store(NO_OUTGOING);
                InitOutgoing();
            }
        }

        void Link::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(ownId);
            ADD_TO_JSON(remoteId);
            ADD_TO_JSON(outgoingMessages);

            int outgoingState = outgoingStatus.load();
            ADD_TO_JSON(outgoingState);
            ADD_TO_JSON(streamResumptionData);
        }

        std::shared_ptr<Link> Link::ParseJSon(const Shared::JSon::JSon& json)
        {
            return std::make_shared<Link>(json);
        }

        Link::StreamResumptionStorage::StreamResumptionStorage(const Shared::JSon::JSon& json)
        {
            GET_FROM_JSON_STRICT(out);
            GET_FROM_JSON_STRICT(in);
            GET_FROM_JSON_STRICT(lastAcknowledged);
            GET_FROM_JSON_STRICT(messageQueue);
            GET_FROM_JSON_STRICT(sentReset);
            GET_FROM_JSON_STRICT(resettingSelf);
            GET_FROM_JSON_STRICT(remoteWaitingForReset);
        }

        void Link::StreamResumptionStorage::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(out);
            ADD_TO_JSON(in);
            ADD_TO_JSON(lastAcknowledged);
            ADD_TO_JSON(messageQueue);
            ADD_TO_JSON(sentReset);
            ADD_TO_JSON(resettingSelf);
            ADD_TO_JSON(remoteWaitingForReset);
        }

        Link::StreamResumptionStorage Link::StreamResumptionStorage::ParseJSon(const Shared::JSon::JSon& json)
        {
            return StreamResumptionStorage(json);
        }
    }
}
