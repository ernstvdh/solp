#include "LinkMessage.h"

namespace Shared
{
    namespace Link
    {
        const std::vector<std::function<std::shared_ptr<LinkMessage>(const Shared::JSon::JSon&)>> LinkMessage::enumToMessageMap
        {
            LinkingToMessage::ParseJSon,
            IAmListeningMessage::ParseJSon,
            AckLinkMessage::ParseJSon,
            ResetLinkMessage::ParseJSon,
            ResetConfirmMessage::ParseJSon
        };
    }
}
