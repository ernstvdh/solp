#include <algorithm>

#include "LinkMgr.h"
#include "Linkable.h"
#include "LinkEventListener.h"

#include "Balancing/Tracker/TrackerId.h"
#include "Balancing/Tracker/TrackerMessage.h"
#include "Monitoring/Server/ServerId.h"
#include "Monitoring/Server/ServerMessage.h"

#include "Shared/Config.h"
#include "Shared/Debug.h"
#include "Shared/Singleton.h"
#include "Shared/System/System.h"

//TODO: Add support for localhost (instead of IP: localhost) should then be reset after/during/before move of the linkable
namespace Shared
{
    namespace Link
    {
        Linkable::Linkable()
        {
            Init();
        }

        void Linkable::Init()
        {
            eventListener = std::make_shared<LinkEventListener>();
            incomings = std::make_shared<LinkableIncomingContainer>(eventListener);
            Shared::Singleton::shutdownHooks->Add(GetOnExpireLinkEvent());
        }

        //TODO: fix this for moveable? We now give warnings for a case that is perfectly fine if we allow linkable to move...
        //TODO: force programmer with maybe a pure virtual function to think about what links to remove in the case of moveable???
        Linkable::~Linkable()
        {
            for (auto incoming : linkListeners)
            {
                incoming.second->RemoveAcceptingLinkable(id);

                if (!Shared::Singleton::shutdown.load())
                    SOLP_WARNING("Linkable is being destroyed with an link listener still open. Should only happen during shutdown.");
            }

            for (auto link : links)
                if (!link.second.second->IsFinished())
                    if (!Shared::Singleton::shutdown.load())
                        SOLP_WARNING("Linkable is being destroyed with a link still busy. Should only happen during shutdown.");
        }

        void Linkable::SendSingleMessageTo(std::shared_ptr<Shared::Message::Message> mess)
        {
            SOLP_DEBUG(DEBUG_LINKABLE, "Calling SendSingleMessageTo.");

            SendMessageTemporary(mess);

            LinkToRemove(mess->receiverID);
        }

        void Linkable::SendMessageTemporary(std::shared_ptr<Shared::Message::Message> mess)
        {
            LinkTo(mess->receiverID);

            SendMessagePersistent(mess);
        }

        void Linkable::SendMessagePersistent(std::shared_ptr<Shared::Message::Message> mess)
        {
            auto it = links.find(mess->receiverID->Serialize());

            // if found
            if (it != links.end())
                it->second.second->SendMess(mess);
            else
                SOLP_ERROR("we expected a link to exists for message (%s) but it doesnt....", mess->Serialise().c_str());
        }

        void Linkable::LinkTo(const std::shared_ptr<const Shared::Id>& otherId)
        {
            SOLP_DEBUG(DEBUG_LINKABLE, "Calling LinkTo with remote id (%s).", otherId->Serialize().c_str());

            auto it = links.find(otherId->Serialize());

            // if found
            if (it != links.end())
            {
                // if we never initialized the outgoing we do it now
                // this can occur if this link was established by the remote
                // also init the link when we are resetting it
                if (it->second.first <= 0)
                    it->second.second->InitOutgoing();

                it->second.first++;
            }
            else
            {
                links.emplace(otherId->Serialize(), std::pair<int, std::shared_ptr<Link>>(
                    1, std::make_shared<Link>(id, otherId, eventListener)));
                links[otherId->Serialize()].second->InitOutgoing();
            }
        }

        void Linkable::LinkToRemove(const std::shared_ptr<const Shared::Id>& otherId)
        {
            SOLP_DEBUG(DEBUG_LINKABLE, "Entering LinkToRemove with remote id (%s).", otherId->Serialize().c_str());

            auto it = links.find(otherId->Serialize());

            // if found
            if (it != links.end())
            {
                it->second.first--;
                if (it->second.first <= 0)
                    it->second.second->KillOutgoing();
            }
            else
                SOLP_ERROR("trying to remove non existing link.");
        }

        void Linkable::ListenOn(Id::ProcessType pType)
        {
            if (linkListeners.count(pType))
            {
                SOLP_ERROR("Trying to listen more than once one a link?? Terminating...");
                std::terminate();
            }

            // if everything burns and crashes here it is because the linkable object is not a shared_ptr....
            linkListeners[pType] = LinkMgr::singleton->ListenOn(pType, incomings, id);

            // if we are not an ipid we have to announce our ip to the tracker
            if (!std::dynamic_pointer_cast<const Shared::IpId>(id))
                AnnounceToTrackers();
        }

        void Linkable::ListenOnRemove(Id::ProcessType pType)
        {
            if (linkListeners.count(pType) == 0)
            {
                SOLP_ERROR("Trying to remove listen type but it does not exist?? Terminating...");
                std::terminate();
            }

            linkListeners[pType]->RemoveAcceptingLinkable(id);
        }

        std::shared_ptr<Shared::Message::Message> Linkable::PopMessage(uint64_t timeoutInMs)
        {
            while (true)
            {
                if (lastPoppedLink)
                {
                    lastPoppedLink->FlushStreamResumptionIfNeeded();
                    lastPoppedLink = NULL;
                }

                if (auto shared = eventListener->Poll(timeoutInMs))
                {
                    lastPoppedLink = shared;
                    if (auto mess = shared->PopMessage())
                        return mess;
                }
                else
                {
                    for (auto it = links.begin(); it != links.end();)
                    {
                        if (it->second.first <= 0)
                        {
                            if (it->second.second->IsFinished())
                            {
                                SOLP_DEBUG(DEBUG_LINKABLE, "Erasing link at %s with remoteid: %s.",
                                    id->Serialize().c_str(), it->second.second->remoteId->Serialize().c_str());
                                it = links.erase(it);
                                continue;
                            }
                        }

                        it++;
                    }

                    if (HandleNewLinksAndConnections())
                        continue;

                    return NULL;
                }
            }
        }

        void Linkable::AnnounceToTrackers()
        {
            std::shared_ptr<BL::Tracker::UpdateTrackerMessage> announce(
                std::make_shared<BL::Tracker::UpdateTrackerMessage>(id, Shared::Config::localIP));
            announce->senderID = id;

            for (auto tracker : Shared::Config::trackerIPs)
            {
                std::shared_ptr<const BL::Tracker::TrackerId> remoteId(std::make_shared<const BL::Tracker::TrackerId>(tracker));
                std::shared_ptr<BL::Tracker::UpdateTrackerMessage> copy(
                    Shared::Message::Message::MakeCopy<BL::Tracker::UpdateTrackerMessage>(announce));
                copy->receiverID = remoteId;
                SendSingleMessageTo(copy);
            }
        }

        void Linkable::RegisterWithMonitor()
        {
            std::shared_ptr<Monitor::Server::AnnounceThreadIdMessage> announce(
                Shared::Message::Message::MakeMessage<Monitor::Server::AnnounceThreadIdMessage>(
                id, std::make_shared<const Monitor::Server::ServerId>(Shared::Config::localIP)));
            announce->threadId = Shared::System::GetThreadId();

            uint64_t utime, stime;
            if (!Shared::System::GetThreadTimesForId(announce->threadId, utime, stime, announce->startTime))
            {
                SOLP_ERROR("Getting thread start time failed even when calling from own thread... terminating...");
                std::terminate();
            }

            SendSingleMessageTo(announce);
        }

        LinkableIncomingContainer::~LinkableIncomingContainer()
        {
            // we know for sure no new links/connections can be pushed to the queue now because
            // the container is only deleted when nobody has a shared ptr to it
            LinkContainer* link = NULL;
            while (links.pop(link))
                delete link;

            ConnectionContainer* conn = NULL;
            while (connections.pop(conn))
                delete conn;
        }

        bool LinkableIncomingContainer::AddIncomingLink(const std::shared_ptr<const Shared::Id>& from,
            std::shared_ptr<Link> link, std::shared_ptr<LinkRequest> request)
        {
            SOLP_DEBUG(DEBUG_LINKABLE, "Adding incoming link from %s.", from->Serialize().c_str());

            //TODO: returning false wouldn't change the behaviour? so maybe change that?

            links.push(new LinkContainer(from, link, request));

            eventListener->AddEvent(NULL);
            return true;
        }

        bool LinkableIncomingContainer::AddIncomingConnection(const std::shared_ptr<const Shared::Id>& from,
            std::shared_ptr<Shared::Net::Connection> connection)
        {
            SOLP_DEBUG(DEBUG_LINKABLE, "Adding incoming connection from %s.", from->Serialize().c_str());

            //TODO: return false when the linkable is being closed
            // if we return false here no messages will be sent, reducing overall load

            connections.push(new ConnectionContainer(from, connection));

            eventListener->AddEvent(NULL);
            return true;
        }

        std::string Linkable::GetRemoteIpOfId(const std::shared_ptr<const Shared::Id>& remoteId)
        {
            auto it = links.find(remoteId->Serialize());

            if (it != links.end())
                return it->second.second->GetIpOfRemote();
            else
                return "failure";
        }

        bool Linkable::HandleNewLinksAndConnections()
        {
            if (!incomings->links.empty())
            {
                LinkableIncomingContainer::LinkContainer* link = NULL;
                while (incomings->links.pop(link))
                {
                    if (auto shared = link->link.lock())
                    {
                        auto currentLink = links.find(link->id->Serialize());
                        // if does not exist yet
                        if (currentLink == links.end())
                            links.emplace(link->id->Serialize(), std::pair<int, std::shared_ptr<Link>>(
                            0, std::make_shared<Link>(id, link->id, eventListener)));

                        // if setting the outgoing link fails we know that it is a dead
                        // end and we should not set the inc link, otherwise 
                        if (shared->SetOutgoingLink(links[link->id->Serialize()].second))
                        {
                            links[link->id->Serialize()].second->SetIncLink(link->link);
                        }
                    }
                    delete link;

                    return true;
                }
            }

            if (!incomings->connections.empty())
            {
                LinkableIncomingContainer::ConnectionContainer* conn = NULL;
                while (incomings->connections.pop(conn))
                {
                    auto currentLink = links.find(conn->id->Serialize());
                    // if does not exist yet
                    if (currentLink == links.end())
                        links.emplace(conn->id->Serialize(), std::pair<int, std::shared_ptr<Link>>(
                            0, std::make_shared<Link>(id, conn->id, eventListener)));

                    conn->connection->SetOwner(links[conn->id->Serialize()].second);
                    links[conn->id->Serialize()].second->SetIncConnection(conn->connection);
                    
                    delete conn;

                    return true;
                }
            }

            return false;
        }

        std::shared_ptr<LinkEventOnExpire> Linkable::GetOnExpireLinkEvent()
        {
            return std::make_shared<LinkEventOnExpire>(eventListener);
        }

        void Linkable::SetTimedLinkEvent(uint64_t timeInMs)
        {
            Shared::Singleton::timedEvents->AddTimedEventFor(timeInMs, eventListener);
        }

        Linkable::Linkable(const Shared::JSon::JSon& json)
        {
            Init();
            GET_FROM_JSON_STRICT(id);
            GET_FROM_JSON_STRICT(links);

            for (auto& link : links)
                link.second.second->InitAfterMove(eventListener);
        }

        void Linkable::SerializeToJSon(Shared::JSon::JSon& json) const
        {
            ADD_TO_JSON(id);
            ADD_TO_JSON(links);
        }
    }
}
