#ifndef SOLP_SHARED_LINK_LINKEVENTLISTENER_H
#define SOLP_SHARED_LINK_LINKEVENTLISTENER_H

#include <chrono>
#include <future>
#include <memory>
#include <vector>
#include <unordered_map>
#include <thread>

#include "boost/lockfree/queue.hpp"

#include "Shared/Debug.h"

namespace Shared
{
    namespace Link
    {
        class Link;

        class LinkEventListener
        {
        public:
            LinkEventListener() : eventLocation(0)
            {
                eventCount.store(0);
            }

            ~LinkEventListener()
            {
                std::weak_ptr<Link>* ptr = NULL;

                while (eventLocation.pop(ptr))
                    if (ptr)
                        delete ptr;
            }

            void AddEvent(std::shared_ptr<Link> location)
            {
                if (location)
                    eventLocation.push(new std::weak_ptr<Link>(location));
                else
                    eventLocation.push(NULL);

                uint64_t prev = eventCount.fetch_add(1);
                if (prev == 0)
                {
                    // we need this lock here to make sure wait/wait_for
                    // is not blocking after we call notify_one
                    std::unique_lock<std::mutex> lck(emptyMutex);
                    lck.unlock();
                    condition.notify_one();
                }
            }

            std::shared_ptr<Link> Poll(uint64_t timeoutInMs = 0)
            {
                if (eventCount.load() == 0)
                {
                    std::unique_lock<std::mutex> lck(emptyMutex);
                    if (timeoutInMs > 0)
                    {
                        if (!condition.wait_for(lck, std::chrono::milliseconds(timeoutInMs), [this]{ return eventCount.load() > 0; }))
                        {
                            lck.unlock();
                            // we don't call AddEvent here because we know the Polling thread is active (not waiting)
                            // it would hence a waste to get the lock and do the notify we have in AddEvent
                            eventCount++;
                            eventLocation.push(NULL);
                        }
                    }
                    else
                        condition.wait(lck, [this]{ return eventCount.load() > 0; });
                }

                eventCount--;

                std::weak_ptr<Link> res;
                std::weak_ptr<Link>* ptr = NULL;

                if (eventLocation.pop(ptr))
                {
                    if (ptr)
                    {
                        std::swap(res, *ptr);
                        delete ptr;
                    }
                    else
                    {
                        // if we have a NULL weak_ptr* we return NULL because it is a control message intended
                        // for the owner of this link event listener
                        return NULL;
                    }
                }
                else
                {
                    SOLP_ERROR("No event location while eventcount is not 0... this should not be possible.... terminating....");
                    std::terminate();
                }

                // if weak_ptr expired the message is not valid anyway so we drop it and loop back
                // if it has not expired we return a shared_ptr
                if (auto shared = res.lock())
                    return shared;
                else
                    return Poll(timeoutInMs);
            }

        private:
            boost::lockfree::queue<std::weak_ptr<Link>*, boost::lockfree::fixed_sized<false>> eventLocation;

            std::mutex emptyMutex;
            std::atomic<uint64_t> eventCount;
            std::condition_variable condition;
        };

        class LinkEventOnExpire
        {
        public:
            LinkEventOnExpire(std::weak_ptr<LinkEventListener> list) : listener(list) {}
            ~LinkEventOnExpire()
            {
                if (auto shared = listener.lock())
                    shared->AddEvent(NULL);
            }

        private:
            std::weak_ptr<LinkEventListener> listener;
        };

        class LinkEventTimed
        {
        private:
            class LinkEventTimedSpecified : public std::enable_shared_from_this<LinkEventTimedSpecified>
            {
            public:
                LinkEventTimedSpecified(uint64_t timeInMs) : wakeUpTime(timeInMs), incs(0), first(true), stop(false) {}

                ~LinkEventTimedSpecified()
                {
                    std::pair<uint64_t, std::weak_ptr<LinkEventListener>>* ptr = NULL;

                    while (incs.pop(ptr))
                        if (ptr)
                            delete ptr;
                }

                // can only called by one thread at a time
                // (currently the case because AddTimedEventFor is locked)
                void Add(const std::weak_ptr<LinkEventListener>& listener)
                {
                    incs.push(new std::weak_ptr<LinkEventListener>(listener));

                    if (first)
                    {
                        first = false;
                        Run();
                    }
                }

                void Stop()
                {
                    stop = true;
                }

                void join()
                {
                    t->join();
                }

            private:
                void Run()
                {
                    std::shared_ptr<LinkEventTimedSpecified> self(shared_from_this());
                    // we give self to the runner to prevent deletes before this thread is killed
                    t = std::unique_ptr<std::thread>(new std::thread([self, this]
                    {
                        auto nextWakeUp = std::chrono::steady_clock::now() + std::chrono::milliseconds(wakeUpTime);

                        while (!stop)
                        {
                            std::this_thread::sleep_until(nextWakeUp);
                            nextWakeUp = nextWakeUp + std::chrono::milliseconds(wakeUpTime);

                            std::weak_ptr<LinkEventListener>* ptr = NULL;

                            while (incs.pop(ptr))
                            {
                                listeners.push_back(*ptr);
                                delete ptr;
                            }

                            listeners.erase(std::remove_if(listeners.begin(), listeners.end(),
                                [](std::weak_ptr<LinkEventListener>& listener) -> bool
                            {
                                if (auto locked = listener.lock())
                                {
                                    locked->AddEvent(NULL);
                                    return false;
                                }
                                else
                                    return true;
                            }), listeners.end());
                        }
                    }));
                }

                bool stop;
                bool first;
                uint64_t wakeUpTime;
                boost::lockfree::queue<std::weak_ptr<LinkEventListener>*, boost::lockfree::fixed_sized<false>> incs;
                std::vector<std::weak_ptr<LinkEventListener>> listeners;
                std::unique_ptr<std::thread> t;
            };

        public:
            LinkEventTimed() : stop(false) {}
            ~LinkEventTimed()
            {
                for (auto& timer : timers)
                    timer.second->Stop();

                for (auto& timer : timers)
                    timer.second->join();
            }

            void AddTimedEventFor(uint64_t timeoutInMs, const std::weak_ptr<LinkEventListener>& listener)
            {
                std::lock_guard<std::mutex> lck(mapAccess);
                auto found = timers.find(timeoutInMs);
                if (found == timers.end())
                    timers[timeoutInMs] = std::make_shared<LinkEventTimedSpecified>(timeoutInMs);

                timers[timeoutInMs]->Add(listener);
            }

        private:
            bool stop;
            std::mutex mapAccess;
            std::unordered_map<uint64_t, std::shared_ptr<LinkEventTimedSpecified>> timers;
        };
    }
}

#endif
