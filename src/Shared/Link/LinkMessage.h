#ifndef SOLP_SHARED_LINK_LINKMESSAGE_H
#define SOLP_SHARED_LINK_LINKMESSAGE_H

#include "Shared/Debug.h"
#include "Shared/Message/Message.h"

namespace Shared
{
    namespace Link
    {
        class LinkMessage : public Shared::Message::Message
        {
        public:
            enum LinkMessageType
            {
                LINKING_TO,
                I_AM_LISTENING,
                ACK,
                RESET,
                CONFIRM,

                MAX_VALUE
            };

            LinkMessage()
            {
                processType = Shared::Id::ProcessType::LINK;
            }

            LinkMessage(const Shared::JSon::JSon& json) : Shared::Message::Message(json)
            {
                MESSAGE_GET_FROM_JSON_STRICT(linkMessageType);
            }

            LinkMessageType linkMessageType;

            static std::shared_ptr<LinkMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                LinkMessageType lmt;
                json.QueryStrict("linkMessageType", lmt);

                if (lmt >= LinkMessageType::MAX_VALUE)
                {
                    SOLP_ERROR("received out of bounds link message type, aborting...");
                    std::terminate();
                }

                return enumToMessageMap[lmt](json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                Shared::Message::Message::SerializeToJSon(json);
                MESSAGE_ADD_TO_JSON(linkMessageType);
            }

        private:
            static const std::vector<std::function<std::shared_ptr<LinkMessage>(const Shared::JSon::JSon&)>> enumToMessageMap;
        };

        class LinkingToMessage : public LinkMessage
        {
        public:
            LinkingToMessage()
            {
                linkMessageType = LinkMessageType::LINKING_TO;
            }

            LinkingToMessage(const Shared::JSon::JSon& json) : LinkMessage(json) {}

            static std::shared_ptr<LinkingToMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<LinkingToMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                LinkMessage::SerializeToJSon(json);
            }
        };

        class IAmListeningMessage : public LinkMessage
        {
        public:
            IAmListeningMessage()
            {
                linkMessageType = LinkMessageType::I_AM_LISTENING;
            }

            IAmListeningMessage(const Shared::JSon::JSon& json) : LinkMessage(json) {}

            static std::shared_ptr<IAmListeningMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<IAmListeningMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                LinkMessage::SerializeToJSon(json);
            }
        };

        class AckLinkMessage : public LinkMessage
        {
        public:
            AckLinkMessage()
            {
                linkMessageType = LinkMessageType::ACK;
            }

            AckLinkMessage(const Shared::JSon::JSon& json) : LinkMessage(json) {}

            static std::shared_ptr<AckLinkMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<AckLinkMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                LinkMessage::SerializeToJSon(json);
            }
        };

        class ResetLinkMessage : public LinkMessage
        {
        public:
            ResetLinkMessage()
            {
                linkMessageType = LinkMessageType::RESET;
            }

            ResetLinkMessage(const Shared::JSon::JSon& json) : LinkMessage(json) {}

            static std::shared_ptr<ResetLinkMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ResetLinkMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                LinkMessage::SerializeToJSon(json);
            }
        };

        class ResetConfirmMessage : public LinkMessage
        {
        public:
            ResetConfirmMessage()
            {
                linkMessageType = LinkMessageType::CONFIRM;
            }

            ResetConfirmMessage(const Shared::JSon::JSon& json) : LinkMessage(json) {}

            static std::shared_ptr<ResetConfirmMessage> ParseJSon(const Shared::JSon::JSon& json)
            {
                return std::make_shared<ResetConfirmMessage>(json);
            }

            virtual void SerializeToJSon(Shared::JSon::JSon& json) const
            {
                LinkMessage::SerializeToJSon(json);
            }
        };
    }
}

#endif
