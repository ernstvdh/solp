#include "Moveable.h"
#include "LinkEventListener.h"

namespace Shared
{
    namespace Link
    {
        Moveable::Moveable(bool run) : Linkable()
        {
            moveState.store(run ? RUN : INIT);
        }

        Moveable::Moveable(const Shared::JSon::JSon& json) : Linkable(json)
        {
            moveState.store(INIT);
        }

        Moveable::~Moveable() {}

        void Moveable::PrepareState(std::shared_ptr<LinkEventListener> listener)
        {
            moveListener = listener;

            moveState.store(GET_STATE);

            eventListener->AddEvent(NULL);
        }

        void Moveable::ReadyForMove()
        {
            moveState.store(MOVEABLE);

            moveListener->AddEvent(NULL);
        }

        bool Moveable::PreparingForMove()
        {
            return moveState.load() == GET_STATE;
        }

        bool Moveable::IsReadyForMove()
        {
            return moveState.load() == MOVEABLE;
        }
    }
}
