#ifndef SOLP_SHARED_LINK_LINK_H
#define SOLP_SHARED_LINK_LINK_H

#include <list>
#include <memory>
#include <queue>
#include <mutex>
#include <future>
#include <functional>

#include "Shared/Id.h"
#include "Shared/Message/Message.h"
#include "Shared/Net/ConnectionOwner.h"

#include "boost/lockfree/queue.hpp"

namespace Shared
{
    namespace Link
    {
        class Link;
        class Linkable;
        class ResetLinkMessage;
        class ResetConfirmMessage;
        class IAmListeningMessage;
        class LinkEventListener;

        class LinkRequest
        {
        public:
            LinkRequest(const std::weak_ptr<Link>& rq) : requester(rq) {}
            ~LinkRequest();

        private:
            std::weak_ptr<Link> requester;
        };

        class Link : public Shared::Net::ConnectionOwner, public std::enable_shared_from_this<Link>, public Serializable
        {
        public:
            enum Status
            {
                NO_OUTGOING = 0x0001,
                CONNECTION_NOT_LISTENING = 0x0002,
                CONNECTION_LISTENING = 0x0004,
                LINK_NOT_LISTENING = 0x0008,
                LINK_LISTENING = 0x0010,
                NS_LOOKUP = 0x0020,
                CANNOT_SEND = NO_OUTGOING | CONNECTION_NOT_LISTENING | LINK_NOT_LISTENING | NS_LOOKUP,
                LINK_STATUS = LINK_NOT_LISTENING | LINK_LISTENING,
                CONNECTION_STATUS = CONNECTION_NOT_LISTENING | CONNECTION_LISTENING
            };

            enum SendFlag
            {
                NONE            = 0x0000,
                STREAM          = 0x0001,
                QUEUE           = 0x0002,
                RESET           = 0x0004
            };

            std::shared_ptr<const Shared::Id> ownId;
            std::shared_ptr<const Shared::Id> remoteId;

            // WARNING we have to call Init on this link if we want to connect
            Link(const std::shared_ptr<const Shared::Id>& lOwnId, const std::shared_ptr<const Shared::Id>& rmId,
                std::shared_ptr<LinkEventListener> listener) : eventListener(listener),
                ownId(lOwnId), messages(0), remoteId(rmId), outgoingStatus(NO_OUTGOING),
                incomingStatus(NO_OUTGOING), abortedConnections(0), abortedLinks(0) {}
            // the owner of the link is responsible for deleting the messages
            // if a link is dropped we need to cleanup the remaining messages
            ~Link();

            // this function boots the link/connection between us and the remote
            void InitOutgoing();
            // this function kills the link/connection between us and the remote
            void KillOutgoing();

            // lock free and thread safe
            bool PushMessage(std::shared_ptr<Shared::Message::Message> mess);
            // lock free and thread safe
            // returns NULL if nothing to pop
            std::shared_ptr<Shared::Message::Message> PopMessage();
            void FlushStreamResumptionIfNeeded();

            // unless canSend is true this function does nothing
            bool SendMess(std::shared_ptr<Shared::Message::Message> mess, int flag = SendFlag::STREAM | SendFlag::QUEUE);

            std::shared_ptr<const Shared::Id> GetID() { return ownId; }
            std::shared_ptr<const Shared::Id> GetRemoteID() { return remoteId; }

            bool IsFinished();

            void Aborted(Shared::Net::Connection::ABORTED_CODE code, Shared::Net::Connection* pointer);
            void AbortedLink(Link* pointer);
            void ExpiredLinkRequest();

            void SetConnection(std::shared_ptr<Shared::Net::Connection> out);
            void SetOutgoingLinkRequest(std::shared_ptr<LinkRequest> out);
            bool SetOutgoingLink(std::weak_ptr<Link> out);
            void NSLookupFinished(std::unique_ptr<std::function<bool()>>&& continuation, bool immediate);

            void SetIncConnection(std::shared_ptr<Shared::Net::Connection> out);
            void SetIncLink(std::weak_ptr<Link> out);

            std::string GetIpOfRemote();

            // Serialization
            Link(const Shared::JSon::JSon& json);
            void InitAfterMove(std::shared_ptr<LinkEventListener>& listener);
            void SerializeToJSon(Shared::JSon::JSon& json) const override;
            static std::shared_ptr<Link> ParseJSon(const Shared::JSon::JSon& json);

        protected:
            boost::lockfree::queue<Shared::Message::MessageContainer*, boost::lockfree::fixed_sized<false>> messages;

        private:
            boost::lockfree::queue<Shared::Net::Connection*, boost::lockfree::fixed_sized<false>> abortedConnections;
            boost::lockfree::queue<Link*, boost::lockfree::fixed_sized<false>> abortedLinks;

            struct StreamResumptionStorage : public Serializable
            {
                enum : uint32_t
                {
                    MAX_NUMBER = UINT32_MAX - 1,
                    NOT_INIT = UINT32_MAX
                };

                StreamResumptionStorage() : out(NOT_INIT), in(NOT_INIT), lastAcknowledged(NOT_INIT),
                    resettingSelf(false), sentReset(false), remoteWaitingForReset(false) {}

                static inline uint32_t GetNextNumber(uint32_t current)
                {
                    return current == NOT_INIT ? 0 : (current + 1) % (StreamResumptionStorage::MAX_NUMBER);
                }

                // we consider the distance between the inputIn and out
                // if the forward distance between out and inputIn is larger than
                // the forward distance between the inputIn and out then
                // the input
                static bool CircularLargerThan(uint32_t question, uint32_t base)
                {
                    if (question == NOT_INIT)
                        return false;

                    if (base == NOT_INIT && question != NOT_INIT)
                        return true;

                    if (question > base && question - base <= MAX_NUMBER / 2)
                        return true;

                    if (question < base && base - question > MAX_NUMBER / 2)
                        return true;

                    return false;
                }

                inline bool IsFinished()
                {
                    return in == NOT_INIT && out == NOT_INIT;
                }

                inline bool Resetting()
                {
                    return resettingSelf || sentReset;
                }

                uint32_t out;
                uint32_t in;
                uint32_t lastAcknowledged;
                std::list<std::shared_ptr<Shared::Message::Message>> messageQueue;

                bool sentReset;
                bool resettingSelf;
                bool remoteWaitingForReset;

                // Serializable
                StreamResumptionStorage(const Shared::JSon::JSon& json);
                void SerializeToJSon(Shared::JSon::JSon& json) const override;
                static StreamResumptionStorage ParseJSon(const Shared::JSon::JSon& json);
            };

            inline std::shared_ptr<Shared::Message::Message> HandleEmptyIncQueue()
            {
                FlushStreamResumption();
                return NULL;
            }

            void HandleNULLMessage();

            void HandleIAmListeningMessage(const std::shared_ptr<IAmListeningMessage>& mess);
            void HandleResetLinkMessage(const std::shared_ptr<ResetLinkMessage>& mess);
            void HandleResetConfirmMessage(const std::shared_ptr<ResetConfirmMessage>& mess);

            bool IsStreamResumptionMessage(const std::shared_ptr<Shared::Message::Message>& mess);
            bool ExpectedMessageForStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess);
            void UpdateStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess);
            void UpdateOutgoingStreamResumption(const std::shared_ptr<Shared::Message::Message>& mess);
            void SetStreamResumption(std::shared_ptr<Shared::Message::Message>& mess);
            void HandleStreamResumptionIAmListening(const std::shared_ptr<IAmListeningMessage>& mess);
            void ResendUnconfirmed();
            void FlushStreamResumption();
            void SendResetIfNeeded();

            std::shared_ptr<LinkEventListener> eventListener;
            std::list<std::shared_ptr<Shared::Message::Message>> outgoingMessages;

            void PushOutgoingMessagesToOut();
            void ExecuteContinuation();

            std::unique_ptr<std::function<bool()>> nsLookupContinuation;
            std::future<void> nsLookupFuture;
            std::atomic<int> outgoingStatus;
            std::atomic<int> incomingStatus;
            std::weak_ptr<Link> outgoingLink;
            std::weak_ptr<Link> incomingLink;
            std::weak_ptr<LinkRequest> linkRequest;
            std::shared_ptr<Shared::Net::Connection> outgoingConnection;
            std::shared_ptr<Shared::Net::Connection> incomingConnection;

            StreamResumptionStorage streamResumptionData;
        };
    }
}

#endif
