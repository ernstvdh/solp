#ifndef SOLP_SHARED_TIMEDSTORAGE_H
#define SOLP_SHARED_TIMEDSTORAGE_H

#include <queue>
#include <mutex>
#include <thread>
#include <chrono>
#include <utility>

namespace Shared
{
    template <class T> 
    class TimedStorage
    {
    public:
        // if zero it defaults to 1 minute
        TimedStorage(uint32_t seconds) : stop(false)
        {
            if (seconds == 0)
                secondsStored = 60;
            else
                secondsStored = seconds;

            Start();
        }

        ~TimedStorage()
        {
            stop = true;

            // wait for it to stop its internal thread
            runner.join();
        }

        void Add(T toAdd)
        {
            std::lock_guard<std::mutex> lock(queueMutex);
            storage.push(std::pair<std::chrono::steady_clock::time_point, T>(clock.now(), toAdd));
        }

    private:
        int secondsStored;
        std::mutex queueMutex;
        std::thread runner;
        std::chrono::steady_clock clock;
        std::queue<std::pair<std::chrono::steady_clock::time_point, T>> storage;

        bool stop;

        void Start()
        {
            runner = std::thread(&TimedStorage::Run, this);
        }

        void Run()
        {
            while (!stop)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));

                while (!storage.empty())
                {
                    std::chrono::steady_clock::time_point now(clock.now());
                    std::pair<std::chrono::steady_clock::time_point, T>& front = storage.front();
                    if (std::chrono::duration_cast<std::chrono::seconds>(now - front.first).count() >= secondsStored)
                    {
                        std::lock_guard<std::mutex> lock(queueMutex);
                        storage.pop();
                        continue;
                    }
                    else
                        break;
                }
            }
        }
    };
}

#endif
