#ifndef SOLP_SHARED_SERIALIZABLE_H
#define SOLP_SHARED_SERIALIZABLE_H

#include <string>

#include "Shared/JSon/JSon.h"

#define ADD_TO_JSON(c) json.Add(#c, c)
#define GET_FROM_JSON_STRICT(c) json.QueryStrict(#c, c)

namespace Shared
{
    class Serializable
    {
    public:
        Serializable() {};
        virtual ~Serializable() {}

        virtual void SerializeToJSon(Shared::JSon::JSon& json) const = 0;

        // also requires a static ParseJSon method with the following format:
        // static std::shared_ptr<CLASSTYPE> ParseJSon(const Shared::JSon::JSon& json)
        // or static CLASSTYPE Parse(const std::string& toParse)
        // depending on the required return type
    };
}

#endif
