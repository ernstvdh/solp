import matplotlib; matplotlib.use('Agg')
import pandas as pd
import numpy as np; np.random.seed(22)
import seaborn as sns; sns.set(color_codes=True)
import matplotlib.pyplot as plt
import glob
import itertools


# thanks stackoverflow.com/question/11869910
def mask(df, key, value):
    return df[df[key] == value]


pd.DataFrame.mask = mask


def addtoplotaggregated(data, index, aggregate, aggregatorf, aggregatorn, colour):
    aggregated = data.groupby(index)[aggregate].agg({aggregatorn: aggregatorf})
    plt.plot(aggregated.index, aggregated[aggregatorn], color=colour, label=aggregatorn)


def addtoplotfillaggregated(data, index, aggregate, aggregatorf1, aggregatorn1, aggregatorf2, aggregatorn2, colour, alpha):
    aggregated = data.groupby(index)[aggregate].agg({aggregatorn1: aggregatorf1, aggregatorn2: aggregatorf2})
    plt.fill_between(aggregated.index, aggregated[aggregatorn1], aggregated[aggregatorn2], color=colour, alpha=alpha)


def addtoplotgrouped(data, x, y, groupby):
    grouped = data.groupby(groupby)
    colours = itertools.cycle(sns.color_palette("husl", len(grouped.groups)))
    for name, dat in grouped:
        plt.plot(dat[x], dat[y], label=name, color=next(colours))


def addtosubplotgrouped(data, x, y, groupby, sub):
    grouped = data.groupby(groupby)
    colours = itertools.cycle(sns.color_palette("husl", len(grouped.groups)))
    for name, dat in grouped:
        sub.plot(dat[x], dat[y], label=name, color=next(colours))


def addlabels(xlabel, ylabel):
    # plt.set_title('axes title')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def finishplot(name, withlegend):
    print(name)
    if withlegend:
        legend = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.tight_layout()
        # l = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), handlelength=0)
        # for h in l.legendHandles:
        #    h.set_marker("o")
        plt.savefig(name + ".png", bbox_extra_artists=[legend], bbox_inches="tight")
    else:
        plt.savefig(name + ".png")
    plt.clf()


datRouter = []
files = glob.glob("2_*.server.monitor.solp.txt")
for file in files:
    monitorDat = pd.read_csv(file, delim_whitespace=True)

    filesRouter = glob.glob(file.split('-')[0] + "*.processing.router.solp.txt")
    if len(filesRouter) > 1:
        print("Too many files found when trying to match: " + file + " with its router data")
        exit()

    if len(filesRouter) < 1:
        print("No files found when trying to match: " + file + " with its router data")
        exit()

    routerDat = pd.read_csv(filesRouter[0], delim_whitespace=True)

    # combinedDat = pd.concat((fileDat, monitorDat), axis=1)
    combinedDat = pd.merge(monitorDat, routerDat, on="tick")

    combinedDat["filename"] = file
    combinedDat["fullID"] = file.split('-')[0]
    datRouter.append(combinedDat)
resultRouters = pd.concat(datRouter)
resultRouters["time"] = resultRouters["stime"] + resultRouters["utime"]
resultRouters["loadPerUser"] = resultRouters["time"] / resultRouters["clients"]
resultRouters["loadPerMessage"] = resultRouters["time"] / resultRouters["messages"]

# plot the CPU time of all the routers separately
addtoplotgrouped(resultRouters, "tick", "time", "fullID")
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-routers-separately", True)

# plot the min, max, mean CPU time of the routers
addtoplotaggregated(resultRouters, "tick", "time", np.mean, "mean", 'r')
addtoplotaggregated(resultRouters, "tick", "time", np.min, "min", 'b')
addtoplotaggregated(resultRouters, "tick", "time", np.max, "max", 'b')
addtoplotfillaggregated(resultRouters, "tick", "time", np.min, "min", np.max, "max", 'b', .5)
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-routers-min-max-mean", True)

# plot the aggregated CPU time of all the routers
addtoplotaggregated(resultRouters, "tick", "time", np.sum, "sum", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-routers-aggregated", False)

# plot per router separately the CPU load per user
addtoplotgrouped(resultRouters, "tick", "loadPerUser", "fullID")
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-per-client-routers-separately", True)

# plot per router the min, max, average CPU load per user
addtoplotaggregated(resultRouters, "tick", "loadPerUser", np.mean, "mean", 'r')
addtoplotaggregated(resultRouters, "tick", "loadPerUser", np.min, "min", 'b')
addtoplotaggregated(resultRouters, "tick", "loadPerUser", np.max, "max", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-per-client-routers-min-max-mean", True)

# plot per router the min, max, average CPU load per message
addtoplotaggregated(resultRouters, "tick", "loadPerMessage", np.mean, "mean", 'r')
addtoplotaggregated(resultRouters, "tick", "loadPerMessage", np.min, "min", 'b')
addtoplotaggregated(resultRouters, "tick", "loadPerMessage", np.max, "max", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-per-message-routers-min-max-mean", True)

# sns.factorplot(data=result, size=5, aspect=2, join=True, x="tick", y="time", hue="filename", estimator=np.median, ci=None)

# sns.pointplot(data=result, size=5, aspect=2, join=True, x="tick", y="time", estimator=np.median, ci=None)
# sns.pointplot(data=result, size=5, aspect=2, join=True, x="tick", y="time", estimator=np.min, ci=None)
# sns.pointplot(data=result, size=5, aspect=2, join=True, x="tick", y="time", estimator=np.max, ci=None)

# plt.xticks(np.arange(0, 61, 10), (0, 10, 20, 30, 40, 50, 60))
# plt.ylim(0.0, 1.0)

# plt.savefig("test")
# plt.clf()

# plt.scatter(result["tick"], result["time"])
# plt.savefig("test2")
# plt.clf()

maxX = 0
maxY = 0
datZones = []
filesZones = glob.glob("1_*.server.monitor.solp.txt")
for file in filesZones:
    monitorDat = pd.read_csv(file, delim_whitespace=True)

    startTime = file.split('-')[3].split('.')[0]
    zoneId = file.split('-')[0]
    filesZones = glob.glob(zoneId + "-" + startTime + "-*.processing.zone.solp.txt")
    if len(filesZones) > 1:
        print("Too many files found when trying to match: " + file + " with its zone data")
        exit()

    if len(filesZones) < 1:
        print("No files found when trying to match: " + file + " with its zone data")
        exit()

    zoneDat = pd.read_csv(filesZones[0], delim_whitespace=True)

    # combinedDat = pd.concat((fileDat, monitorDat), axis=1)
    combinedDat = pd.merge(monitorDat, zoneDat, on="tick")

    zoneX = zoneId.split('_')[1]
    zoneY = zoneId.split('_')[2]
    zoneXY = "(" + zoneX + ", " + zoneY + ")"
    zonePhase = zoneId.split('_')[3]
    combinedDat["filename"] = file
    combinedDat["fullID"] = file.split('-')[0]
    combinedDat["zoneX"] = int(zoneX)
    combinedDat["zoneY"] = int(zoneY)
    combinedDat["zoneXY"] = zoneXY
    combinedDat["phase"] = int(zonePhase)
    datZones.append(combinedDat)

    if int(zoneX) > maxX:
        maxX = int(zoneX)
    if int(zoneY) > maxY:
        maxY = int(zoneY)

resultZones = pd.concat(datZones)
resultZones["time"] = resultZones["stime"] + resultZones["utime"]
resultZones["loadPerEntity"] = resultZones["time"] / resultZones["entities"]

# plot CPU time of all zones separately per tick
addtoplotgrouped(resultZones, "tick", "time", "fullID")
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-zones-separately", True)

# plot CPU time of all zones aggregated
addtoplotaggregated(resultZones, "tick", "time", np.sum, "sum", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-zones-aggregated", False)

# plot the min, max, mean CPU time of the routers
addtoplotaggregated(resultZones, "tick", "time", np.mean, "mean", 'r')
addtoplotaggregated(resultZones, "tick", "time", np.min, "min", 'b')
addtoplotaggregated(resultZones, "tick", "time", np.max, "max", 'b')
addtoplotfillaggregated(resultZones, "tick", "time", np.min, "min", np.max, "max", 'b', .5)
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-zones-min-max-mean", True)

# plot per zone the min, max, average CPU load per entity
addtoplotaggregated(resultZones, "tick", "loadPerEntity", np.mean, "mean", 'r')
addtoplotaggregated(resultZones, "tick", "loadPerEntity", np.min, "min", 'b')
addtoplotaggregated(resultZones, "tick", "loadPerEntity", np.max, "max", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-per-entity-zones-min-max-mean", True)

if maxY + maxX > 0:
    fig, axarr = plt.subplots(maxY + 1, maxX + 1, sharex=True, sharey=True)

    for x in range(0, maxX + 1):
        for y in range(0, maxY + 1):
            if maxY == 0:
                addtosubplotgrouped(resultZones.mask("zoneX", x).mask("zoneY", y), "tick", "time", "phase", axarr[x])
                if y == maxY:
                    axarr[x].set_xlabel("Time (s)")
                if x == 0:
                    axarr[x].set_ylabel("CPU time")
            elif maxX == 0:
                addtosubplotgrouped(resultZones.mask("zoneX", x).mask("zoneY", y), "tick", "time", "phase", axarr[y])
                if y == maxY:
                    axarr[y].set_xlabel("Time (s)")
                if x == 0:
                    axarr[y].set_ylabel("CPU time")
            else:
                addtosubplotgrouped(resultZones.mask("zoneX", x).mask("zoneY", y), "tick", "time", "phase", axarr[y, x])
                if y == maxY:
                    axarr[y, x].set_xlabel("Time (s)")
                if x == 0:
                    axarr[y, x].set_ylabel("CPU time")
    fig.tight_layout()
else:
    addtoplotgrouped(resultZones, "tick", "time", "phase")
finishplot("cpu-time-per-zone-on-grid", True)

datClients = []
datResponseTimes = []
filesClientsMonitor = glob.glob("0_*.server.monitor.solp.txt")
for file in filesClientsMonitor:
    monitorDat = pd.read_csv(file, delim_whitespace=True)

    clientId = file.split('-')[0]
    filesClients = glob.glob(clientId + ".client.solp.txt")
    if len(filesClients) > 1:
        print("Too many files found when trying to match: " + file + " with its clients data")
        exit()

    if len(filesClients) < 1:
        print("No files found when trying to match: " + file + " with its clients data")
        exit()

    clientDat = pd.read_csv(filesClients[0], delim_whitespace=True)

    # combinedDat = pd.concat((fileDat, monitorDat), axis=1)
    combinedDatClientDat = pd.merge(monitorDat, clientDat, on="tick")

    filesClientResponseTime = glob.glob(clientId + ".responsetimes.client.solp.txt")
    if len(filesClientResponseTime) > 1:
        print("Too many files found when trying to match: " + file + " with its clients data")
        exit()

    if len(filesClientResponseTime) < 1:
        print("No files found when trying to match: " + file + " with its clients data")
        exit()

    clientResponseTimeDat = pd.read_csv(filesClientResponseTime[0], delim_whitespace=True)
    clientResponseTimeDatAgg = clientResponseTimeDat.groupby("tick")["rtime"].agg({"maxResponseTime": np.max})
    clientResponseTimeDatAgg.reset_index(level=0, inplace=True)
    datResponseTimes.append(clientResponseTimeDatAgg)

    combinedDat = combinedDatClientDat
    combinedDat["filename"] = file
    combinedDat["fullID"] = file.split('-')[0]
    datClients.append(combinedDat)

responseTimes = pd.concat(datResponseTimes)
resultClients = pd.concat(datClients)
resultClients["time"] = resultClients["stime"] + resultClients["utime"]
resultClients["loadPerMessage"] = resultClients["time"] / resultClients["messages"]

# boxplot message arrivals per tick on clients
fig = plt.figure(figsize=(20, 10))
ax = sns.boxplot(x="tick", y="messages", data=resultClients)
addlabels("Time (s)", "Messages")
finishplot("messages-per-tick-per-client-boxplot", False)

# boxplot message max response time per tick on clients
fig = plt.figure(figsize=(20, 10))
ax = sns.boxplot(x="tick", y="maxResponseTime", data=responseTimes)
addlabels("Time (s)", "Response time (ms)")
finishplot("max-response-time-per-tick-per-client-boxplot", False)

# aggregated messages per tick for the clients
addtoplotaggregated(resultClients, "tick", "messages", np.sum, "sum", 'b')
addlabels("Time (s)", "Messages")
finishplot("clients-messages-per-tick-aggregated", False)

datNetworking = []
filesNetworkMonitor = glob.glob("12_*.server.monitor.solp.txt")
for file in filesNetworkMonitor:
    monitorDat = pd.read_csv(file, delim_whitespace=True)

    networkId = file.split('-')[0]
    # filesNetwork = glob.glob(networkId + ".client.solp.txt")
    # if len(filesNetwork) > 1:
    #     print("Too many files found when trying to match: " + file + " with its network data")
    #     exit()

    # if len(filesNetwork) < 1:
    #     print("No files found when trying to match: " + file + " with its network data")
    #     exit()

    # clientDat = pd.read_csv(filesNetwork[0], delim_whitespace=True)

    # combinedDat = pd.concat((fileDat, monitorDat), axis=1)
    # combinedDat = pd.merge(monitorDat, clientDat, on="tick")
    combinedDat = monitorDat

    combinedDat["filename"] = file
    combinedDat["fullID"] = file.split('-')[0]
    datNetworking.append(combinedDat)
resultNetwork = pd.concat(datNetworking)
resultNetwork["time"] = resultNetwork["stime"] + resultNetwork["utime"]

# plot CPU time of all network threads separately per tick
addtoplotgrouped(resultNetwork, "tick", "time", "fullID")
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-network-separately", True)

# plot CPU time of all network threads aggregated
addtoplotaggregated(resultNetwork, "tick", "time", np.sum, "sum", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-network-aggregated", False)

datTotal = []
filesTotalMonitor = glob.glob("total.*.server.monitor.solp.txt")
for file in filesTotalMonitor:
    monitorDat = pd.read_csv(file, delim_whitespace=True)

    networkId = ".".join(file.split('.')[1:5])

    # filesNetwork = glob.glob(networkId + ".client.solp.txt")
    # if len(filesNetwork) > 1:
    #     print("Too many files found when trying to match: " + file + " with its network data")
    #     exit()

    # if len(filesNetwork) < 1:
    #     print("No files found when trying to match: " + file + " with its network data")
    #     exit()

    # clientDat = pd.read_csv(filesNetwork[0], delim_whitespace=True)

    # combinedDat = pd.concat((fileDat, monitorDat), axis=1)
    # combinedDat = pd.merge(monitorDat, clientDat, on="tick")
    combinedDat = monitorDat

    combinedDat["id"] = networkId
    datTotal.append(combinedDat)
resultTotal = pd.concat(datTotal)
resultTotal["time"] = resultTotal["stime"] + resultTotal["utime"]

# plot CPU time of all network threads separately per tick
addtoplotgrouped(resultTotal, "tick", "time", "id")
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-machines-separately", True)

# plot CPU time of all network threads aggregated
addtoplotaggregated(resultTotal, "tick", "time", np.sum, "sum", 'b')
addlabels("Time (s)", "CPU time")
finishplot("cpu-time-machines-aggregated", False)
