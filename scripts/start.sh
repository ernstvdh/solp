#!/bin/bash
debugnodes=" 0 1 3 ";

arr=(${PRUN_PE_HOSTS:?"PRUN_PE_HOSTS was not set"})
count=0;
numtrackers=${#arr[@]};
numclients=1;
startclients=1;
endclients=$((numclients + startclients));
numrouters=1;
startrouters=$((numclients + startclients));
endrouters=$((numrouters + startrouters));
maxcount=$numtrackers;
trackers="";
global="";
clients="";
routers="";
result="";
s=' ';

debug="false"

num=0
for i in "${arr[@]}"
do
    thisnode=$(hostname);

    if [[ $thisnode = $i ]]
    then
        found=$(echo "$debugnodes" | grep " $num ");

        if [[ $found = "" ]]
        then
            break
        else
            debug="true"
        fi

        break
    fi

    num=$((num + 1));
done

for i in "${arr[@]}"
do
    if [[ "$count" -ge "$maxcount" ]]
    then
        break
    fi

    temp=$(echo "$i" | nslookup | grep -v '#' | grep Address | cut -d ' ' -f 2);

    if [[ "$count" -eq 0 ]]
    then
        global=$temp;
    fi

    if [[ "$count" -ge "$startclients" ]]
    then
        if [[ "$count" -lt "$endclients" ]]
        then
            clients=$clients$s$temp;
        fi
    fi
    
    if [[ "$count" -ge "$startrouters" ]]
    then
        if [[ "$count" -lt "$endrouters" ]]
        then
            routers=$routers$s$temp;
        fi
    fi

    trackers=$trackers$s$temp;

    count=$((count + 1));
done

result=$global$s$numtrackers$trackers$s$numclients$clients$s$numrouters$routers;

if [[ $debug = "false" ]]
then
    ./../../../build/Solp $result
else
    gdb -x ../../debug/debug-settings.gdb -iex "set auto-load safe-path /" --args ./../../../build/Solp $result
fi
