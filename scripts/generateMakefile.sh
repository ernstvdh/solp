rm -R ../build
mkdir ../build

export CMAKE_ROOT=/home/vdhoeven/cmake/compiled
export CC=/cm/shared/package/gcc/4.9.3/bin/gcc
export CXX=/cm/shared/package/gcc/4.9.3/bin/g++

echo ""
echo ""

/home/vdhoeven/cmake/compiled/bin/cmake --version

echo ""

rm -R ../dep/metis/build/
mkdir --parents ../dep/metis/build/libmetis/Release/

cd ../dep/metis/

make config prefix=/home/vdhoeven/Solp/dep/metis/build/libmetis/Release/
make install

echo ""

cd ../../build
/home/vdhoeven/cmake/compiled/bin/cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/home/vdhoeven/Solp/install
#/home/vdhoeven/cmake/compiled/bin/cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/home/vdhoeven/Solp/install
